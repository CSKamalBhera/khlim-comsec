<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('company_fak')->nullable();
            $table->string('roc')->nullable();
            $table->string('incorporation_date')->nullable();
            $table->string('e_no')->nullable();
            $table->string('c_no')->nullable();
            $table->string('natureof_business')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('contact')->nullable();
            $table->string('fax_no')->nullable();
            $table->dateTime('date_of_change')->nullable();
            //account
            $table->string('bank_name')->nullable();
            $table->string('account_no')->nullable();
            //financial year
            $table->string('financial_year_end')->nullable();
            $table->string('annual_return_date')->nullable();
            $table->string('turnover')->nullable();
            $table->string('profit_before_tax')->nullable();
            $table->string('annual_meeting_date')->nullable();
            // mics
            $table->string('ep_certificate')->nullable();
            $table->dateTime('applied_date')->nullable();
            $table->dateTime('new_due_date')->nullable();
            $table->string('extension_time')->nullable();
            $table->string('company_etr')->nullable();
            $table->dateTime('etr_date')->nullable();
            $table->integer('acitivity_status')->nullable();
            $table->boolean('status')->default(0);
            $table->dateTime('status_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['email', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}