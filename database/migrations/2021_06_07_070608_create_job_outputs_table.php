<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_outputs', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('job_workflow_id')->unsigned();
            $table->foreign('job_workflow_id')->references('id')->on('job_workflows')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->text('output_file')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_outputs');
    }
}
