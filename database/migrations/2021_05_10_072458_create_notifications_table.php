<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type');
            $table->bigInteger('notifiable_id')->unsigned();
            $table->foreign('notifiable_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->string('notifiable_type');
            $table->index(['notifiable_type', 'notifiable_id'], 'notifiable');
            $table->text('data');
            $table->timestamp('read_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
