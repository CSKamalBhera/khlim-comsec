<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_notifications', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('job_type_id')->unsigned();
            $table->foreign('job_type_id')->references('id')->on('job_types')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('job_type_workflow_id')->unsigned();
            $table->foreign('job_type_workflow_id')->references('id')->on('job_type_workflows')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('workflow_type_id')->unsigned();
            $table->foreign('workflow_type_id')->references('id')->on('workflow_types')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->json('notification_value')->nullable();
            $table->integer('done_notify_day')->nullable();
            $table->integer('not_done_notify_day')->nullable();
            $table->boolean('notify_recurrence_day')->default(0);
            $table->boolean('notify_account_department')->default(0);
            $table->integer('super_active')->nullable();
            $table->integer('active')->nullable();
            $table->integer('semi_active')->nullable();
            $table->integer('dormant')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_notifications');
    }
}
