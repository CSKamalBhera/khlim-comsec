<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->bigInteger('company_id')->unsigned();
                $table->foreign('company_id')->references('id')->on('companies')
                        ->onUpdate('cascade')->onDelete('cascade');
                $table->bigInteger('staff_id')->unsigned();
                $table->foreign('staff_id')->references('id')->on('users')
                        ->onUpdate('cascade')->onDelete('cascade');
                $table->bigInteger('job_type_id')->unsigned();
                $table->foreign('job_type_id')->references('id')->on('job_types')
                        ->onUpdate('cascade')->onDelete('cascade');
                $table->bigInteger('manager_id')->unsigned();
                $table->foreign('manager_id')->references('id')->on('users')
                        ->onUpdate('cascade')->onDelete('cascade');
                $table->bigInteger('department_id')->unsigned();
                $table->foreign('department_id')->references('id')->on('departments')
                        ->onUpdate('cascade')->onDelete('cascade');
                $table->bigInteger('created_by')->unsigned()->nullable();
                $table->foreign('created_by')->references('id')->on('users')
                        ->onUpdate('cascade')->onDelete('cascade');
                $table->bigInteger('assigned_id')->unsigned()->nullable();
                $table->foreign('assigned_id')->references('id')->on('users')
                        ->onUpdate('cascade')->onDelete('cascade');
                $table->boolean('status')->default(0);
                $table->boolean('reminderStatus')->default(0);
                $table->softDeletes();
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
