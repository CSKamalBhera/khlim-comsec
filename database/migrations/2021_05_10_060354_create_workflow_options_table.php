<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_options', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('workflow_types_id')->unsigned();
            $table->foreign('workflow_types_id')->references('id')->on('workflow_types')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->string('options')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_options');
    }
}
