<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_workflows', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('job_type_workflow_id')->unsigned();
            $table->foreign('job_type_workflow_id')->references('id')->on('job_type_workflows')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('workflow_type_id')->unsigned();
            $table->foreign('workflow_type_id')->references('id')->on('workflow_types')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('workflow_option_id')->unsigned();
            $table->foreign('workflow_option_id')->references('id')->on('workflow_options')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('assigned_id')->unsigned()->nullable();
            $table->foreign('assigned_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->dateTime('recurrence_date')->nullable();
            $table->dateTime('afs_recurrence_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_workflows');
    }
}
