<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('username');
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('is_permission')->default(0); // 1 = Admin
            $table->string('activation_code')->nullable();
            $table->boolean('can_login')->default(0);
            $table->boolean('status')->default(0);
            $table->dateTime('approved_at')->nullable();
            $table->string('img_src')->default('storage/users/img/user.jpg');
            $table->string('ic_number')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('emergency_no')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['username','email', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
