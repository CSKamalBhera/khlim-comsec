<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobTypeWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_type_workflows', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('job_type_id')->unsigned();
            $table->foreign('job_type_id')->references('id')->on('job_types')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('workflow_type_id')->unsigned();
            $table->foreign('workflow_type_id')->references('id')->on('workflow_types')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->string('title')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_type_workflows');
    }
}
