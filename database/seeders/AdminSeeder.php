<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $superadmin_role = Role::where('slug', 'super-admin')->first();
        $admin_role = Role::where('slug', 'admin')->first();
        $manager = Role::where('slug', 'manager')->first();
        $senior_team_manager = Role::where('slug', 'senior-team-manager')->first();
        $team_member = Role::where('slug', 'team-leader')->first();
        $permissions = Permission::pluck('id','id')->all();

        $secretarials = \App\Models\Users\Department::get();
        $accounting = \App\Models\Users\Department::where('slug', 'accounting')->first();
        $audit = \App\Models\Users\Department::where('slug', 'audit')->first();
        $tax = \App\Models\Users\Department::where('slug', 'tax')->first();

        /*
        * Add Super Admin User
        *
        */
        if (\App\Models\Users\User::where('email', '=', 'admin@o2o.my')->first() === null) {

            $user = \App\Models\Users\User::create([
                'id' => 1,
                'name' => 'Super Admin',
                'username' => 'superadmin',
                'email' => 'admin@o2o.my',
                'can_login'=> 1,
                'status'=> 1,
                'activation_code' => 'BySeed',
                'approved_at' => now(),
                'password' => Hash::make('1qaz2wsx3edc'),
                'ic_number' => '514258-62-1589',
                'phone_no' => '(514) 258-6215',
            ]);

        }else{
            $user = \App\Models\Users\User::where('email', '=', 'admin@o2o.my')->first();
        }

        // Attach admin role
        $permissions = Permission::pluck('id','id')->all();
        $superadmin_role->syncPermissions($permissions);
        $user->assignRole([$superadmin_role->id]);
        foreach ($secretarials as $key => $secretarial) {
            $user->departments()->attach($secretarial);
        }
       
        /*
        * Add Client Admin User
        *
        */
        if (\App\Models\Users\User::where('email', '=', 'john@khlim.com')->first() === null) {

            $user = \App\Models\Users\User::create([
                'id' => 2,
                'name' => 'John Doe',
                'username' => 'john',
                'email' => 'john@khlim.com',
                'can_login'=> 1,
                'status'=> 1,
                'activation_code' => 'BySeed',
                'approved_at' => now(),
                'password' => Hash::make('Khlim123!'),
                'ic_number' => '514258-62-8915',
                'phone_no' => '(514) 258-6215',
            ]);

        }else{
            $user = \App\Models\Users\User::where('email', '=', 'john@khlim.com')->first();
        }

        // Attach admin role
        $admin_role->syncPermissions($permissions);
        $user->assignRole([$admin_role->id]);
        $user->departments()->attach($accounting);
       
        /*
        * Add Client Admin User
        *
        */
        if (\App\Models\Users\User::where('email', '=', 'bruce@khlim.com')->first() === null) {

            $user = \App\Models\Users\User::create([
                'id' => 3,
                'name' => 'Bruce',
                'username' => 'bruce231',
                'email' => 'bruce@khlim.com',
                'can_login'=> 1,
                'status'=> 1,
                'activation_code' => 'BySeed',
                'approved_at' => now(),
                'password' => Hash::make('Khlim123!'),
                'ic_number' => '514258-62-7815',
                'phone_no' => '(514) 258-6215',
            ]);

        }else{
            $user = \App\Models\Users\User::where('email', '=', 'bruce@khlim.com')->first();
        }

        // Attach admin role
        $senior_team_manager->syncPermissions($permissions);
        $user->assignRole([$senior_team_manager->id]);
        $user->departments()->attach($accounting);

         /*
         * Add other test users
         *
         */
        if (\App\Models\Users\User::where('email', '=', 'kamal.bhera@khlim.com')->first() === null) {

            $user = \App\Models\Users\User::create([
                'id' => 4,
                'name' => 'Kamal Bhera',
                'username' => 'kamalb896',
                'email' => 'kamal.bhera@khlim.com',
                'status'=> 1,
                'activation_code' => 'BySeed',
                'approved_at' => now(),
                'password' => Hash::make('Khlim123!'),
                'ic_number' => '514258-62-8715',
                'phone_no' => '(514) 258-6215',
            ]);
        }else{
            $user = \App\Models\Users\User::where('email', '=', 'kamal.bhera@khlim.com')->first();
        }

        // Attach role
        $manager->syncPermissions($permissions);
        $user->assignRole([$manager->id]);
        $user->departments()->attach($audit);    

         /*
         * Add other test users
         *
         */
        if (\App\Models\Users\User::where('email', '=', 'jane@khlim.com')->first() === null) {

            $user = \App\Models\Users\User::create([
                'id' => 5,
                'name' => 'Jane Doe',
                'username' => 'jane543',
                'email' => 'jane@khlim.com',
                'status'=> 1,
                'activation_code' => 'BySeed',
                'approved_at' => now(),
                'password' => Hash::make('Khlim123!'),
                'ic_number' => '514258-62-8715',
                'phone_no' => '(514) 258-6215',
            ]);
        }else{
            $user = \App\Models\Users\User::where('email', '=', 'jane@khlim.com')->first();
        }

        // Attach role
        $team_member->syncPermissions($permissions);
        $user->assignRole([$team_member->id]);
        $user->departments()->attach($tax);

    }
}
