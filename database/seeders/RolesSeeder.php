<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'Auditor',
            'slug' => 'auditor',
            'guard_name'=>'web',
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'Team Member',
            'slug' => 'team-member',
            'guard_name'=>'web',
        ]);
        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'Team Leader',
            'slug' => 'team-leader',
            'guard_name'=>'web',
        ]);
        DB::table('roles')->insert([
            'id' => 4,
            'name' => 'Senior Team Manager',
            'slug' => 'senior-team-manager',
            'guard_name'=>'web',
        ]);
        DB::table('roles')->insert([
            'id' => 5,
            'name' => 'Manager',
            'slug' => 'manager',
            'guard_name'=>'web',
        ]);
        DB::table('roles')->insert([
            'id' => 6,
            'name' => 'Admin',
            'slug' => 'admin',
            'guard_name'=>'web',
        ]);
        DB::table('roles')->insert([
            'id' => 7,
            'name' => 'Super Admin',
            'slug' => 'super-admin',
            'guard_name'=>'web',
        ]);

        $this->command->info('Role seeds are created');

    }
}
