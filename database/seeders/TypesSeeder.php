<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $departments = ['Secretarial','Accounting','Audit','Tax'];
    foreach($departments as $department){
        switch($department){
        case 'Secretarial':{
          
          DB::table('workflow_types')->insert(['name' => 'Status', 'department_id'=>1,'status' => 1]); 
          DB::table('workflow_types')->insert(['name' => 'Assign User & Status','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Status & Notification','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'SSM Result','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Generate Invoice','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'SSM Submission & Status','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Return Documents & Notification','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Signed Documents & Payment Status', 'department_id'=>1,'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Status & Payment','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Customer Approval & Status', 'department_id'=>1,'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Status & Action','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Status & Multiple Action','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Upload document','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Annual Return & Job Complete','department_id'=>1, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Client Approvals & Documents','department_id'=>1, 'status' => 1]);
          };
          break;
        case 'Accounting':{ 
            
          DB::table('workflow_types')->insert(['name' => 'Status', 'department_id'=>2,'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Assign User & Status','department_id'=>2, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Status & Notification','department_id'=>2, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Received Status & Document','department_id'=>2, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Invoice & Management Status','department_id'=>2, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Manager Review', 'department_id'=>2, 'status' => 1]);
          DB::table('workflow_types')->insert(['name' => 'Generate Invoice','department_id'=>2, 'status' => 1]);
        }
         break;
        case 'Audit':{
            
         }
         break;
        case 'Tax':{
            
         }
         break;
     
        }
    }
       
        
           
            
    }
}
