<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use DB;
class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		DB::table('settings_system')->insert([
            'id'    => 1,
            'name'  => 'Khlim',
            'logo'  => 'assets/img/logo.png',
            'url'   => env('APP_URL', 'http://localhost'),
            'description'   => 'Information system for executives Khlim',
            'publisher'=>'Kadamtech',
            'version' => '3.0-202101',
        ]);

        $this->command->info('System settings are created');
    }
}
