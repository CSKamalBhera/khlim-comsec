<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use DB;
class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'id' => 1,
            'name' => 'Secretarial',
            'slug' => 'secretarial',
            'description' => 'Secretarial',
        ]);
		DB::table('departments')->insert([
            'id' => 2,
            'name' => 'Accounting',
            'slug' => 'accounting',
            'description' => 'Accounting',
        ]);
        DB::table('departments')->insert([
            'id' => 3,
            'name' => 'Audit',
            'slug' => 'audit',
            'description' => 'Audit',
        ]);
        
        DB::table('departments')->insert([
            'id' => 4,
            'name' => 'Tax',
            'slug' => 'tax',
            'description' => 'Tax',
        ]);

        $this->command->info('Role seeds are created');

    }
}
