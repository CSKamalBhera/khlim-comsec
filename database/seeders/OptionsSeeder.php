<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class OptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seceterial workflow
        // Status
        DB::table('workflow_options')->insert(['options' => 'Mark as Done','workflow_types_id' => 1]);
        // Assigned user
        DB::table('workflow_options')->insert(['options' => 'Assigned','workflow_types_id' => 2]);
        DB::table('workflow_options')->insert(['options' => 'Not Assigned','workflow_types_id' => 2]);
        //Status & Notification
        DB::table('workflow_options')->insert(['options' => 'Mark as Done','workflow_types_id' => 3]);
        //SSM Result
        DB::table('workflow_options')->insert(['options' => 'Approved','workflow_types_id' => 4]);
        DB::table('workflow_options')->insert(['options' => 'Rejected','workflow_types_id' => 4]);
        DB::table('workflow_options')->insert(['options' => 'Not processed','workflow_types_id' => 4]);
        DB::table('workflow_options')->insert(['options' => 'Queried','workflow_types_id' => 4]);
        //Generate Invoice
        DB::table('workflow_options')->insert(['options' => 'Mark as Done','workflow_types_id' => 5]);
        //Submission SSM & Status
        DB::table('workflow_options')->insert(['options' => 'Approved','workflow_types_id' => 6]);
        DB::table('workflow_options')->insert(['options' => 'Queried','workflow_types_id' => 6]);
        //Documents Return & Notification
        DB::table('workflow_options')->insert(['options' => 'Mark as Done','workflow_types_id' => 7]);
        //Signed Documents & Payment Status
        DB::table('workflow_options')->insert(['options' => 'Done','workflow_types_id' => 8]);
        DB::table('workflow_options')->insert(['options' => 'Extended(Returned documents without payment)','workflow_types_id' => 8]);
        DB::table('workflow_options')->insert(['options' => 'Extended(Payment made without returning signed documents)','workflow_types_id' => 8]);
        DB::table('workflow_options')->insert(['options' => 'Extended(Returned documents & payment)','workflow_types_id' => 8]);
        DB::table('workflow_options')->insert(['options' => 'Close','workflow_types_id' => 8]);
        //Status & Payment
        DB::table('workflow_options')->insert(['options' => 'Approved','workflow_types_id' => 9]);
        DB::table('workflow_options')->insert(['options' => 'On Hold','workflow_types_id' => 9]);
        DB::table('workflow_options')->insert(['options' => 'Close','workflow_types_id' => 9]);
        //Customer Approval & Status
        DB::table('workflow_options')->insert(['options' => 'Accept','workflow_types_id' => 10]);
        DB::table('workflow_options')->insert(['options' => 'Decline','workflow_types_id' => 10]);
        //Status & Action
        DB::table('workflow_options')->insert(['options' => 'Complete','workflow_types_id' => 11]);
        DB::table('workflow_options')->insert(['options' => 'Extended','workflow_types_id' => 11]);
        DB::table('workflow_options')->insert(['options' => 'Complete & Generate invoice','workflow_types_id' => 11]);
        //Status & Multiple Action
        DB::table('workflow_options')->insert(['options' => 'Complete','workflow_types_id' => 12]);
        //Upload document
        DB::table('workflow_options')->insert(['options' => 'Mark as Done','workflow_types_id' => 13]);
        //Annual Return & Job Complete
        DB::table('workflow_options')->insert(['options' => 'Complete','workflow_types_id' => 14]);
        //Client Approvals & Documents
        DB::table('workflow_options')->insert(['options' => 'Proceed','workflow_types_id' => 15]);
        DB::table('workflow_options')->insert(['options' => 'Proceed without payment','workflow_types_id' => 15]);
        DB::table('workflow_options')->insert(['options' => 'Pending','workflow_types_id' => 15]);
        DB::table('workflow_options')->insert(['options' => 'Close','workflow_types_id' => 15]);

        // Accounting deparment
        // status
        DB::table('workflow_options')->insert(['options' => 'Mark as Done','workflow_types_id' => 16]);
       // assing user
        DB::table('workflow_options')->insert(['options' => 'Assigned','workflow_types_id' => 17]);
        DB::table('workflow_options')->insert(['options' => 'Not Assigned','workflow_types_id' => 17]);
       //Status & Notification
        DB::table('workflow_options')->insert(['options' => 'Mark as Done','workflow_types_id' => 18]);
        //Request Status
        DB::table('workflow_options')->insert(['options' => 'Complete','workflow_types_id' => 19]);
        DB::table('workflow_options')->insert(['options' => 'Incomplete','workflow_types_id' => 19]);
        //Invoice & Management Status
        DB::table('workflow_options')->insert(['options' => 'Confirm','workflow_types_id' => 20]);
        DB::table('workflow_options')->insert(['options' => 'Extended','workflow_types_id' => 20]);
        //Manager Review
        DB::table('workflow_options')->insert(['options' => 'Complete','workflow_types_id' => 21]);
        //Generate Invoice
        DB::table('workflow_options')->insert(['options' => 'Mark as Done','workflow_types_id' => 22]);
        
        
    }
}
