<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            'permission_access',
            'permission_create',
            'permission_edit',
            'permission_delete',
            'permission_status',
          
            'role_access',
            'role_create',
            'role_edit',
            'role_delete',
            'role_status',
            
            'user_access',
            'user_create',
            'user_edit',
            'user_delete',
            'user_status',
            
            'company_access',
            'company_create',
            'company_edit',
            'company_delete',
            'company_status',

            'jobtype_access',
            'jobtype_create',
            'jobtype_edit',
            'jobtype_delete',
            
            'department_access',
            'department_create',
            'department_edit',
            'department_delete',
            'department_status',

            'jobs_access',
            'jobs_create',
            'jobs_edit',
            'jobs_delete',
         
            'jobs_output_access',
            'jobs_output_create',
            'jobs_output_edit',
            'jobs_output_delete',
           
            'recurrence_access',
            'recurrence_create',
            'recurrence_edit',
            'recurrence_delete',
            
            'job_workflow_access',
            'job_workflow_create',
            'job_workflow_edit',
            'job_workflow_delete',
            
            'settings_access',
            'settings_create',
            'settings_edit',
            'settings_delete',
            
            'approval_access',
            'approval_create',
            'approval_edit',
            'approval_delete',
            
            'payment_access',
            'payment_create',
            'payment_edit',
            'payment_delete',

            'invoice_access',
            'invoice_create',
            'invoice_edit',
            'invoice_delete',
            
        ];
    
        foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
        }
        $this->command->info('Permission seeds are created');
    }
}
