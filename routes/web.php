<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('register', function(){
    return redirect('/login');
});
Auth::routes(['register' => false]);
Auth::routes();
Route::middleware(['auth:web'])->group(function () {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    //user
    Route::resource('/users', 'UserController');
    Route::get('/user/status/{id}', 'UserController@status');
    #UserProfile Routes
    Route::resource('profile','ProfileController');
    Route::post('/update-password','ProfileController@updatePassword')->name('update-password');
    //departments
    Route::resource('/departments', 'DepartmentController');
    Route::get('/departments/status/{id}', 'DepartmentController@status');
    //roles
    Route::resource('/roles', 'RoleController');
    Route::get('/roles/status/{id}', 'RoleController@status');
    //permissions
    Route::resource('/permissions', 'PermissionController');
    Route::get('/permissions/status/{id}', 'PermissionController@status');
    // jobtype
    Route::resource('/jobtype', 'JobTypeController');
    Route::get('/getOptions', 'JobTypeController@getOptions')->name('getOptions');
    Route::get('/getInputs', 'JobTypeController@getInputs')->name('getInputs');
    Route::get('/getTypes', 'JobTypeController@getTypes')->name('getTypes');
    Route::get('/getJobTypes', 'JobTypeController@getJobTypes')->name('getJobTypes');
    //department
    Route::resource('/departments', 'DepartmentController');
    Route::get('/departments/status/{id}', 'DepartmentController@status');
    //company
    Route::resource('/companies', 'CompanyController');
    Route::get('/company/status/{id}', 'CompanyController@status');
    Route::get('/company/portfolio/delete', 'CompanyController@deletePortfolio')->name('deletePortfolio');
    //jobs
    Route::resource('/jobs', 'JobController');
    Route::get('/job/status/{id}', 'JobController@status');
    Route::get('/getJobtype', 'JobController@departmentJobType')->name('getJobType');
    Route::get('/getStaff', 'JobController@departmentStaff')->name('getStaff');
    Route::get('/getManager', 'JobController@departmentManager')->name('getManager');
    Route::get('/completeJob', 'JobController@CompleteJob')->name('CompleteJob');
    Route::get('/assignedJob', 'JobController@AssignedJob')->name('AssignedJob');
    //job workflow
    Route::resource('/job-workflow', 'JobWorkflowController');
    //job output
    Route::resource('/job-output', 'JobOutputController');
    Route::get('/download/{id}', 'JobOutputController@getDownload');
    Route::get('/output-file/{id}', 'JobOutputController@pdfView')->name('output-file');
    //job recurrence
    Route::resource('/recurrence', 'RecurrenceController');
    // settings
    Route::resource('/settings', 'SettingController');
    Route::get('/setting/delete', 'SettingController@deleteSetting')->name('deleteSetting');
    
    // approval
    Route::resource('/approvals', 'ApprovalController');
    // invoice
    Route::resource('/invoices', 'InvoiceController');
    Route::get('/invoices-generate/{jobId}/{jobTypeWorkflow}/{workflowType}/{workflowOption}/{jobWorkflow?}','JobWorkflowController@invoiceGenerate')->name('invoices.generate');
    Route::post('/invoice/items/edit/{id}', 'InvoiceController@InvoiceItemEdit')->name('invoiceItem.edit');
    Route::get('/getJobs', 'InvoiceController@getCompanyJobs')->name('getJobs');
    Route::get('/getSettings', 'InvoiceController@getSettings')->name('getSettings');
    Route::get('/invoices/status/{id}', 'InvoiceController@status');
    Route::get('download-invoice/{id}','InvoiceController@downloadInvoice');
    Route::get('invoice-preview/{id}','InvoiceController@previewInvoice');
    Route::get('payment/{id}','InvoiceController@payment');
    Route::resource('/payments', 'PaymentController');
    // Notifications web routes
    Route::resource('/notifications', 'NotificationController');
    Route::get('notifications-{filter}', 'NotificationController@index')->name('notifications');
    Route::get('notification-markallasread', 'NotificationController@mark_all_as_read')->name('markAllRead');;
    Route::get('notification-markasread-{id}', 'NotificationController@mark_as_read');
    Route::get('notification-markasunread-{id}', 'NotificationController@mark_as_unread');
    Route::get('notification-delete-{id}', 'NotificationController@delete');
});