<?php

namespace App\Notifications;
use Auth;
use Config;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class JobMailNotification extends Notification
{
    use Queueable;
    protected $user; // This is always the user that is notified
    protected $job; // This is always the user that is notified
    protected $workflow; // This is always the user that is notified
    protected $days; // days
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$eventData)
    {
        $this->user = $user;  // the notified user
        $this->job  = $eventData->job;  // the notified job
        $this->workflow  = $eventData->workflow;  // the job workflow
        if ($eventData->workflowNotify) {
            $getHours = $eventData->workflowNotify->notification_value;
            $deCodeHours = json_decode($getHours);
            if (isset($deCodeHours->hours)) {
                $this->days  = $deCodeHours->hours;
            }else {
                $this->days  = $deCodeHours->days;
            }
        }else {
            $this->days = 14;
        }
       
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if (config('app.activate_mail_notifications')) {
            return ['mail'];
          } else {
            //return ['database'];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user       = $this->user;
        $days       = $this->days;
        $default_mail = Config::get('mail.from.address'); //test@gmail.com
        $mailMessage  = new MailMessage();
        $mailMessage
            ->from($default_mail,'administrator')
            ->subject('Job Mail')
            ->greeting(sprintf(__('Hello') . ' %s', $user->name))
            ->line('This task has been more than '. $days .' hours/days. Please complete it asap');
        $mailMessage
            ->line('We are delighted to welcome you')
            ->line(config('app.name'))
            ->salutation(new HtmlString('Thank you for using our app'.'<br>'.'administrator'));
        return $mailMessage;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
