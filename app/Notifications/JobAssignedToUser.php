<?php

namespace App\Notifications;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class JobAssignedToUser extends Notification
{
    use Queueable;

    protected $user; // This is always the user that is notified
    protected $job; // This is always the job that is notified
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$job)
    {
        $this->user = $user;  // the notified user
        $this->job  = $job;  // the notified job
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if (config('app.activate_mail_notifications')) {
            return ['mail','database'];
          } else {
            return ['database'];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user       = $this->user;
        $job        = $this->job;
        $default_mail = Config::get('mail.from.address'); //test@gmail.com
        $mailMessage  = new MailMessage();
        $mailMessage
            ->from($default_mail,'administrator')
            ->subject('New job assign')
            ->greeting(sprintf(__('Hello') . ' %s', $user->name))
            ->line('Welcome.You have been assign new job to our system.')
            ->line('You have been new Job assigned. Please work on this job');
        $action_link = route('jobs.show',$job->id);
        $mailMessage
            ->line($job->job_type->name. ' ('. $job->company->name .')')
            ->action('Click here', $action_link)
            ->line('We are delighted to welcome you')
            ->line(config('app.name'))
            ->salutation(new HtmlString('Thank you for using our app'.'<br>'.'administrator'));
        return $mailMessage;
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        /** @var User $user */
        $user = $this->user;
        $job  = $this->job;
        return [
            'title' => $job->job_type->name. ' ('. $job->company->name .') ' ,
            'detail' => 'You have been assigned new job following : '.$job->job_type->name. ' ('. $job->company->name .') ' ,
            'user_name' => auth()->user()->name,
            'action_link' => url('jobs/'.$job->id),
            'item_id' => $job->id,
        ];

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
