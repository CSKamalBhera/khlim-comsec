<?php

namespace App\Notifications;
use Config;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Support\HtmlString;

class UserRegisteredSuccessfully extends Notification implements ShouldQueue
{
    use Queueable;
    protected $user; // This is always the user that is notified
    protected $password; // This is always the password that is notified

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$password)
    {
        $this->user = $user;  // the notified user
        $this->password = $password;  // user password
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if (config('app.activate_mail_notifications')) {
            return ['mail','database'];
          } else {
            return ['database'];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user       = $this->user;
        $admin_list = User::with('roles')
                    ->whereHas('roles', function($q){
                        $q->where('slug','superadmin');
                        //$q->orwhere('slug','admin');
                    })
                    ->get();
        $admin_mail   = [];
        foreach($admin_list as $admin){
            array_push($admin_mail,$admin['email']);
        }
        $password     = $this->password;
        $tempPwd      = config('app.temp_pwd', 'Password123');
        $default_mail = Config::get('mail.from.address'); //test@gmail.com
        $mailMessage  = new MailMessage();
        $mailMessage
            ->from($default_mail,'administrator')
            ->subject('Welcome')
            ->greeting(sprintf(__('Hello') . ' %s', $user->name))
            ->line('Welcome. Your account has been successfully created')
            ->line('You have been successfully registered to our system.');

        if ($user->activation_code == 'ByAdmin') {
            $mailMessage
                ->line('Your password : ' . $password)
                ->cc($admin_mail);
            $action_link=route('login');
        }else{
            $action_link = route('login');
        }
        
        $mailMessage
            ->line('Your E-mail : ' . $user->email)
            ->action('Click here', $action_link)
            ->line('We are delighted to welcome you as a user')
            ->line(config('app.name'))
            ->salutation(new HtmlString('Thank you for using our app'.'<br>'.'administrator'));

        return $mailMessage;

    }

    public function toDatabase($notifiable)
    {
        /** @var User $user */
        $user = $this->user;
        return [
            'title' => 'Welcome. Your account has been successfully created',
            'detail' => 'You have been successfully registered to our system' .' '.config('app.name'),
            'user_name' => auth()->user()->name,
        ];
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
