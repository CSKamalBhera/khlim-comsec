<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ClientDocumentReturnToUser extends Notification
{
    use Queueable;

    protected $user; // This is always the user that is notified
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = $user;  // the notified user
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if (config('app.activate_mail_notifications')) {
            return ['mail'];
          } else {
            //return ['database'];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = $this->user;
        $default_mail = Config::get('mail.from.address'); //test@gmail.com
        $mailMessage  = new MailMessage();
        $mailMessage
            ->from($default_mail,'administrator')
            ->subject('Document Return')
            ->greeting(sprintf(__('Hello') . ' %s', $user->name))
            ->line('We have sent you a document to review and signatures. Please return them as sono as possible');
        $mailMessage
            ->line('We are delighted to welcome you')
            ->line(config('app.name'))
            ->salutation(new HtmlString('Thank you for using our app'.'<br>'.'administrator'));
        return $mailMessage;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    // public function toDatabase($notifiable)
    // {
    //     /** @var User $user */
    //     $user = $this->user;
    //     $job = $this->job;
    //     $workflow = $this->workflow;
    //     $days = $this->days;
    //     $description = $this->description;
    //     return [
    //         'title' => $job->job_type->name. ' ('. $job->company->name .') '  . $workflow->title,
    //         'detail' => $description ,
    //         'action_link' => route('jobs.show',$job->id),
    //         'item_id' => $job->id,
    //     ];

    // }  
}
