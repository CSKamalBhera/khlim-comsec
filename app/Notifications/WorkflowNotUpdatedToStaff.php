<?php

namespace App\Notifications;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;
#models
use App\Models\Jobs\Job;
use App\Models\Jobs\JobWorkflow;
use App\Models\Users\User;
use Carbon\Carbon;

class WorkflowNotUpdatedToStaff extends Notification
{
    use Queueable;
    protected $user; // This is always the user that is notified
    protected $job; // This is always the user that is notified
    protected $workflow; // This is always the user that is notified
    protected $days; // days
    protected $description; // description
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$event)
    {
        $this->user = $user;  // the notified user
        $this->job  = $event->job;  // the notified job
        $this->workflow  = $event->workflow;  // the job workflow
        $this->description  = $event->description;  // the job description
        $lastUpdatedWorkflow = JobWorkflow::where('job_id',$event->job->id)->latest('id')->first();
        $lastDate   = date('Y-m-d',strtotime($lastUpdatedWorkflow->created_at));
        $today      =  Carbon::now()->format('Y-m-d');
        $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $lastDate);
        $end_date   = \Carbon\Carbon::createFromFormat('Y-m-d', $today);
        $this->days = $start_date->diffInDays($end_date);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if (config('app.activate_mail_notifications')) {
            return ['mail'];
          } else {
            //return ['database'];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user       = $this->user;
        $job        = $this->job;
        $workflow   = $this->workflow;
        $days       = $this->days;
        $description = $this->description;
        $default_mail = Config::get('mail.from.address'); //test@gmail.com
        $mailMessage  = new MailMessage();
        $mailMessage
            ->from($default_mail,'administrator')
            ->subject('Job workflow not complete')
            ->greeting(sprintf(__('Hello') . ' %s', $user->name))
            ->line($job->job_type->name. ' ('. $job->company->name .')')
            ->line($description)
            ->line('Please work on this job');
        $action_link = route('jobs.show',$job->id);
        $mailMessage
            ->action('Click here', $action_link)
            ->line('We are delighted to welcome you')
            ->line(config('app.name'))
            ->salutation(new HtmlString('Thank you for using our app'.'<br>'.'administrator'));
        return $mailMessage;
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        /** @var User $user */
        $user = $this->user;
        $job = $this->job;
        $workflow = $this->workflow;
        $description = $this->description;
        $days = $this->days;
        return [
            'title' => $job->job_type->name. ' ('. $job->company->name .') '  . $workflow->title,
            'detail' => $description ,
            'action_link' => route('jobs.show',$job->id),
            'item_id' => $job->id,
        ];

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
