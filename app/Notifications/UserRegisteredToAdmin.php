<?php

namespace App\Notifications;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Support\HtmlString;

class UserRegisteredToAdmin extends Notification
{
    use Queueable;
    protected $user; // This is always the user that is notified
    protected $registeredUser; // This is always the user that is notified
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $registeredUser)
    {
        $this->user = $user; // the notified user
        $this->registeredUser = $registeredUser; // the notified user
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if (config('app.activate_mail_notifications')) {
            return ['mail','database'];
        } else {
            return ['database'];
        }
    }
 
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /** @var User $user */
        $user           = $this->user;
        $registeredUser = $this->registeredUser;
        $default_mail   = Config::get('mail.from.address'); //support@aefika.com
        $mailMessage  = new MailMessage();
        $mailMessage
            ->from($default_mail,'administrator')
            ->subject('User account successfully created' . ' : ' . $registeredUser->name)
            ->greeting(sprintf(__('Hello') . ' %s', $user->name))
            ->line('New account successfully created for the following user:'.' '.$registeredUser->name .' (' . $registeredUser->email . ')');

        if($user->status != 1){ 
            $mailMessage
                ->line('The user has been deactivated because of his domain. Please activate if compliant.');
        }
        $mailMessage
            ->line('If necessary, please check that its role (level of permissions for the use of features) is adequate.')
            ->salutation(new HtmlString('Greetings'.'<br>'.'administrator')); 

        return $mailMessage;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
    public function toDatabase($notifiable)
    {

        $registeredUser = $this->registeredUser;
        return [
            'title' => 'A new user has been successfully registered'. ' : ' . $registeredUser->name,
            'detail' => 'New account successfully created for the following user:' . ' ' . $registeredUser->name .' (' .   $registeredUser->email . ')',
            'user_name' => auth()->user()->name,
        ];

    }
   
}
