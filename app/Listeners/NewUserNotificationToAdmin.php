<?php

namespace App\Listeners;
use App\Models\Users\User;
use App\Events\NewUserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\UserRegisteredToAdmin;


class NewUserNotificationToAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserRegistered  $event
     * @return void
     */
    public function handle(NewUserRegistered $event)
    {
        $admin_list = User::with('roles')
                    ->whereHas('roles', function($q){
                        $q->where('slug','superadmin');
                        //$q->orwhere('slug','admin');
                    })
                    ->get();
        foreach($admin_list as $admin){
            if(isset($admin)){
               $admin->notify(new UserRegisteredToAdmin($admin, $event->user)); // notifications when registered
            }
        }
    }
}
