<?php

namespace App\Listeners;

use App\Events\WorkflowNotUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Users\User;
use App\Notifications\WorkflowNotUpdatedToManager;
class WorkflowNotUpdateNotificationToManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WorkflowNotUpdated  $event
     * @return void
     */
    public function handle(WorkflowNotUpdated $event)
    {
        $id = $event->job->manager_id;
        $manager = User::find($id);
        $manager->notify(new WorkflowNotUpdatedToManager($manager, $event));  // notifications when registered
    }
}
