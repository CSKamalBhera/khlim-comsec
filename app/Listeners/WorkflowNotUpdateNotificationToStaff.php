<?php

namespace App\Listeners;

use App\Events\WorkflowNotUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Users\User;
use App\Notifications\WorkflowNotUpdatedToStaff;
class WorkflowNotUpdateNotificationToStaff
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WorkflowNotUpdated  $event
     * @return void
     */
    public function handle(WorkflowNotUpdated $event)
    {
        $id = $event->job->staff_id;
        $workflow = $event->workflow;
        $staff = User::find($id);
        if ($workflow->workflow_type->name != 'Manager Review') {
            $staff->notify(new WorkflowNotUpdatedToStaff($staff, $event)); // notifications when registered
        }
        
    }
}
