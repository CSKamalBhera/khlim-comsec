<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Users\User;
use App\Events\WorkflowUpdates;
use App\Notifications\WorkflowUpdateToManager;
class WorkflowUpdateNotificationToManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(WorkflowUpdates $event)
    {
        $id = $event->workflow->jobs->manager_id;
        $manager = User::find($id);
        $manager->notify(new WorkflowUpdateToManager($manager, $event));  // notifications when registered
    }
}
