<?php

namespace App\Listeners;

use App\Events\WorkflowNotUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\WorkflowNotUpdatedToAssignedUser;
use App\Models\Users\User;
class WorkflowNotUpdateNotificationToAssignedUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WorkflowNotUpdated  $event
     * @return void
     */
    public function handle(WorkflowNotUpdated $event)
    {
        $job = $event->job;
        if ($job->assigned_id  != null) {
            $id = $event->job->assigned_id;
            $user = User::find($id);
            $workflow = $event->workflow;
            if ($workflow->workflow_type->name != 'Manager Review') {
                $user->notify(new WorkflowNotUpdatedToAssignedUser($user, $event));  // notifications when registered
            }
        }
    }
}
