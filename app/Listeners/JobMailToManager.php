<?php

namespace App\Listeners;
use App\Models\Users\User;
use App\Events\SeceterialJobMailEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\JobMailNotification;

class JobMailToManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SeceterialJobMailEvent  $event
     * @return void
     */
    public function handle(SeceterialJobMailEvent $event)
    {
        
        $staffId = $event->job->staff_id;
        $id = $event->job->manager_id;
        $manager = User::find($id);
        $staff = User::find($staffId);
        $staff->notify(new JobMailNotification($staff, $event->workflow)); // notifications when registered
        $manager->notify(new JobMailNotification($manager, $event->workflow)); // notifications when registered
    }
}
