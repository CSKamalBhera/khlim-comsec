<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Users\User;
use App\Events\NewJobCreatedEvent;
use App\Notifications\ManagerNotification;

class NewJobNotificationToManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * NewJobCreatedEvent Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewJobCreatedEvent $event)
    {
        $id = $event->job->manager_id;
        $manager = User::find($id);
        $manager->notify(new ManagerNotification($manager, $event->job)); // notifications when registered
        
    }
}
