<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Users\User;
use App\Events\NewJobCreatedEvent;
use App\Notifications\StaffNotification;

class NewJobNotificationToStaff
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewJobCreatedEvent $event)
    {
        $id = $event->job->staff_id;
        $staff = User::find($id);
        $staff->notify(new StaffNotification($staff, $event->job)); // notifications when registered
        
    }
}
