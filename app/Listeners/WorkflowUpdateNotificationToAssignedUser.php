<?php

namespace App\Listeners;

use App\Events\WorkflowUpdates;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Users\User;
use App\Models\Jobs\JobWorkflow;
use App\Notifications\WorkflowUpdateToAssignedUser;
class WorkflowUpdateNotificationToAssignedUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WorkflowUpdates  $event
     * @return void
     */
    public function handle(WorkflowUpdates $event)
    {
        $job = $event->job;
        $workflows = JobWorkflow::where('job_id',$job->id)->where('assigned_id','!=', null)->get();
        foreach ($workflows as $key => $workflow) {
            if ($workflow->assigned_id  != null) {
                $id = $workflow->assigned_id;
                $user = User::find($id);
                $workflow = $event->workflow;
                $user->notify(new WorkflowUpdateToAssignedUser($user, $event));  // notifications when registered
            }
        }
    }
}
