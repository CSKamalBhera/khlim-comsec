<?php

namespace App\Listeners;

use Lang;
use App\Models\Users\User;
use App\Helpers\Helper;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogPasswordReset
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PasswordReset  $event
     * @return void
     */
    public function handle(PasswordReset $event)
    {
        //activity log
        $causer = User::find($event->user->id);
        $log_name = 'authentication';
        $operation   = 'user_password_reset';
        $description = 'user_password_reset';
        $custom_properties = ['application' => config('app.name'),
                            'operation'     => $operation,
                            'causer_name'   => $causer->name,
                            'new_value'     => null,
                            'old_value'     => null,
                            ];
        $activity = activity($log_name)
           ->causedBy($causer)
           ->performedOn($causer)
           ->withProperties($custom_properties)
           ->log($description);
    }
}
