<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Jobs\Job;
use App\Models\Users\User;
use App\Models\Company\Company;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $departmentId = auth()->user()->departments->pluck('id')->toArray();
        $staff = User::count();
        $company = Company::count();
        $completeJobs = Job::where('status',1)->whereIn('department_id',$departmentId)->count();
        $assignedJobs = Job::where('assigned_id','!=',null)->where('status',0)->whereIn('department_id',$departmentId)->count();
        return view('dashboard',compact('completeJobs','assignedJobs','staff','company'));
    }
}
