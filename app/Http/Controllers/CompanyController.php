<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#models
use App\Models\Users\User;
use App\Models\Users\Department;
use App\Models\Company\Company;
use App\Models\Company\CompanyAddress;
use App\Models\Company\CompanyINCharge;
use App\Models\Company\RelatedCompany;
use App\Models\Company\CompanyShare;
use App\Models\Company\CompanyAccountResponsible;
use App\Models\Company\CompanyPortfolio;
use App\Models\Company\CompanyDocument;
use App\Models\Company\CompanyPayment;
use App\Models\Company\Invoice;
use App\Models\Company\CompanyReminder;
#request
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;

#classes
use Validator, Session, Hash;

class CompanyController extends Controller
{
    
    function __construct() {
        $this->middleware('permission:company_access|company_create|company_edit|company_delete|company_status', ['only' => ['index', 'show']]);
        $this->middleware('permission:company_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:company_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:company_delete', ['only' => ['destroy']]);
        $this->middleware('permission:company_status', ['only' => ['status']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::orderBy('created_at', 'DESC')->get();
        return view('companies.index',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::where('status',1)->get();
        $documentType = [0 => 'Annual Return',1 => 'Audit Report to SSM',2 => 'Audit Report to Tax Department'];
        return view('companies.create',compact('departments','documentType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyStoreRequest $request)
    { 
        //check validations
        $validator = $request->validated();
        // store data
        $company = new Company();
        $company->name         = $request->name;
        $company->email        = $request->email;
        $company->company_fak  = $request->company_fka;
        $company->roc          = $request->company_roc;
        $company->incorporation_date    = $request->incorporation_date;
        $company->e_no          = $request->e_no;
        $company->c_no          = $request->c_no;
        $company->natureof_business = $request->natureof_business;
        $company->phone_no      = $request->phone;
        $company->fax_no        = $request->fax;
        $company->bank_name     = $request->bank_name;
        $company->account_no    = $request->account_number;
        $company->financial_year_end     = $request->financial_year_end;
        $company->annual_return_date     = $request->annual_return_date;
        $company->turnover      = $request->turnover;
        $company->profit_before_tax     = $request->profit_before_tax;
        $company->annual_meeting_date   = $request->annual_meeting_date;
        $company->ep_certificate        = $request->ep_certificate;
        $company->date_of_change       = $request->date_of_change;
        if($request->extension_time == 'Yes'){
          $company->applied_date          = $request->new_due_date;  
        }else{
          $company->applied_date          = $request->applied_date;  
        }
        // $company->applied_date          = $request->applied_date;
        $company->new_due_date          = $request->new_due_date;
        $company->extension_time        = $request->extension_time;
//        $company->company_etr           = $request->company_etr;
//        $company->etr_date              = $request->etr_date;
        $company->status_date           = $request->status_date;
        $company->status                = 1;
        $company->acitivity_status      = $request->activity_status;
        $company->contact               = $request->contact;
        $company->save();
        // company addresses
        foreach ($request->address_type as $key => $addressType) {
            $companyAddress = new CompanyAddress();
            $companyAddress->company_id    = $company->id;
            $companyAddress->addresse_type = $addressType;
            $companyAddress->name          = $request->address_name[$key];
            $companyAddress->phone         = $request->address_phone[$key];
            $companyAddress->address       = $request->address[$key];
            $companyAddress->save();
        }
        // company incharge
        if (isset($request->incharge_name)) {
            foreach ($request->incharge_name as $key => $name) {
                $companyInCharge = new CompanyINCharge();
                $companyInCharge->company_id    = $company->id;
                $companyInCharge->department_id    =$request->incharge_department_id[$key];
                $companyInCharge->name          = $name;
                $companyInCharge->phone         = $request->incharge_phone[$key];
                $companyInCharge->email         = $request->incharge_email[$key];
                $companyInCharge->save();
            }
        }
        // company responsible
        if (isset($request->responsible_name)) {
            foreach ($request->responsible_name as $key => $name) {
                $companyResponsilbe = new CompanyAccountResponsible();
                $companyResponsilbe->company_id    = $company->id;
                $companyResponsilbe->department_Id = $key;
                $companyResponsilbe->name          = json_encode($name,true);
                $companyResponsilbe->save();
            }
        }
        // company shareholder
        if (isset($request->shareholder_name) && isset($request->values)) {
            $labels = $request->shareholder_name;
            $values = $request->values;
            $array = array_combine($labels, $values);
            $json = json_encode($array,true);
        }else {
            $labels = [''];
            $values = [''];
            $array = array_combine($labels, $values);
            $json = json_encode([],true);
        }
        if (isset($request->director)) {
            $directorJson = json_encode($request->director,true);
        }else {
            $directorJson = json_encode([],true);
        }
        // company shares
        $companyShare = new CompanyShare();
        $companyShare->company_id       = $company->id;
        $companyShare->share_capital    = $request->share_capital;
        $companyShare->shareholder_name = $json;
        $companyShare->shareholder_updated_on = $request->shareholder_updated_on;
        $companyShare->director         = $directorJson;
        $companyShare->director_updated_on = $request->director_updated_on;
        $companyShare->save();
        // relatable company
        if (isset($request->groupof_companies)) {
            $relatedCompany = new RelatedCompany();
            $relatedCompany->company_id    = $company->id;
            $relatedCompany->name          = json_encode($request->groupof_companies,true);
            $relatedCompany->save();
        }
        // company document
        if (isset($request->document_type)) {
            foreach ($request->document_type as $key => $item) {
                if(isset($item['report_year']) || isset($item['recevie_date']) || isset($item['submitted'])){
                    $report_year = json_encode($item['report_year'],true);
                    $recevie_date = json_encode($item['recevie_date'],true);
                    $submitted = json_encode($item['submitted'],true);
                }else{
                    $report_year = json_encode([],true);
                    $recevie_date = json_encode([],true);
                    $submitted = json_encode([],true);
                }
                $companyDocument = new CompanyDocument();
                $companyDocument->company_id    = $company->id;
                $companyDocument->report_type   = $item['type'];
                $companyDocument->report_year   = $report_year;
                $companyDocument->receive_date  = $recevie_date;
                $companyDocument->submited      = $submitted ;
                $companyDocument->save();
            }
        }
        // company portfolio
        if (isset($request->company_portfolio)) {
            foreach ($request->company_portfolio as $key1 => $item) {
                if (isset($item['user_id']) || count($item['user_id']) > 0) {
                    foreach($item['user_id'] as $key => $userId){
                        if($userId != null){
                        $companyPortfolio = new CompanyPortfolio();
                        $companyPortfolio->company_id    = $company->id;
                        $companyPortfolio->department_id = $item['department_id'];
                        $companyPortfolio->user_id       = $userId;
                        $companyPortfolio->date          =  $item['date'][$key];
                        $companyPortfolio->save();
                        }
                    }
                }
            }
        }
        // company reminder
       
                $company_reminder = new CompanyReminder();
                $company_reminder->company_id = $company->id;
                $company_reminder->secretarialFees = isset($request->company_reminder['secretarialfees']) ? $request->company_reminder['secretarialfees'] : 0;
                $company_reminder->is_annualReturn_notify = isset($request->company_reminder['annualReturn']) ?$request->company_reminder['annualReturn'] : 0;
                $company_reminder->is_taxAgent_notify = isset($request->company_reminder['taxAgent']) ? $request->company_reminder['taxAgent'] : 0;
                $company_reminder->is_auditorAppoinment_notify = isset($request->company_reminder['auditorAppoinment']) ? $request->company_reminder['auditorAppoinment'] : 0;
                $company_reminder->is_auditStatement_notify = isset($request->company_reminder['audited_statement']) ? $request->company_reminder['audited_statement'] : 0;
                $company_reminder->save();
          
        if ($company) {
            Session::flash('notification','Customer created successfully. ');
            Session::flash('notificationType','success');
            return redirect()->route('companies.index');
        } else {
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::with('addresses','company_incharge','company_incharge.departments','company_documents','company_portfolios','company_portfolios.departments','account_responsible.departments','shares','related_company','company_invoices.job.job_type','payments')->orderBy('created_at', 'DESC')->find($id);
        $departments = Department::where('status',1)->get();
        return view('companies.show',compact('departments','company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::with('addresses','company_incharge','company_documents','company_portfolios','account_responsible.departments','shares','related_company','company_reminders')->find($id);
        $departments = Department::where('status',1)->get();
        return view('companies.edit',compact('departments','company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUpdateRequest $request, $id)
    { 
        $validator = $request->validated(); //check validations
        //update
        $company =  Company::with('addresses','company_incharge','account_responsible','shares','related_company')->find($id);        
        $company->name         = $request->name;
        $company->email        = $request->email;
        $company->company_fak  = $request->company_fka;
        $company->roc          = $request->company_roc;
        $company->incorporation_date    = $request->incorporation_date;
        $company->e_no         = $request->e_no;
        $company->c_no         = $request->c_no;
        $company->natureof_business     = $request->natureof_business;
        $company->phone_no     = $request->phone;
        $company->fax_no       = $request->fax;
        $company->bank_name    = $request->bank_name;
        $company->account_no   = $request->account_number;
        $company->financial_year_end    = $request->financial_year_end;
        $company->annual_return_date    = $request->annual_return_date;
        $company->turnover              = $request->turnover;
        $company->profit_before_tax     = $request->profit_before_tax;
        $company->annual_meeting_date   = $request->annual_meeting_date;
        $company->ep_certificate        = $request->ep_certificate;
        $company->extension_time        = $request->extension_time;
        $company->new_due_date          = $request->new_due_date;
        if($request->extension_time == 'Yes'){
          $company->applied_date          = $request->new_due_date;  
        }else{
          $company->applied_date          = $request->applied_date;  
        }
//        $company->company_etr           = $request->company_etr;
//        $company->etr_date              = $request->etr_date;
        $company->acitivity_status      = $request->activity_status;
        $company->status_date           = $request->status_date;
        $company->status                = 1;
        $company->date_of_change        = $request->date_of_change;
        $company->contact               = $request->contact;
        $company->save();                                                                                                                                       
        // company addresses
        foreach ($request->address_type as $key => $addressType) {
            $companyAddress =  CompanyAddress::where('addresse_type',$addressType)->where('company_id',$company->id)->first();
            $companyAddress->company_id    = $company->id;
            $companyAddress->addresse_type = $addressType;
            $companyAddress->name          = $request->address_name[$key];
            $companyAddress->phone         = $request->address_phone[$key];
            $companyAddress->address       = $request->address[$key];
            $companyAddress->save();
        }
       // company incharge
        if ($request->incharge_id != null){
            CompanyINCharge::where('company_id',$company->id)->whereNotIn('id',$request->incharge_id)->delete();
        }
        if (isset($request->incharge_name)) {
            foreach ($request->incharge_name as $key => $name) {
             
                if ($request->incharge_id != null && count($request->incharge_id) > $key) {
                    $inchargeId      =  $request->incharge_id ? $request->incharge_id[$key] : null;
                    $companyInCharge =  CompanyINCharge::where('company_id', $company->id)->find($inchargeId);
                }else {
                  
                    $companyInCharge = new CompanyINCharge();
                    $companyInCharge->company_id    = $company->id;
                }
                $companyInCharge->name          = $name;
                $companyInCharge->department_id = $request->incharge_department_id[$key];
                $companyInCharge->phone         = $request->incharge_phone[$key];
                $companyInCharge->email         = $request->incharge_email[$key];
                $companyInCharge->save();
            }
        }else {
            if (!$company->company_incharge->isEmpty()) {
                $company->company_incharge()->delete();
            }
        }
        // company account responsible
        if ($request->accountResponsible_id != null){
            CompanyAccountResponsible::where('company_id',$company->id)->whereNotIn('id',$request->accountResponsible_id)->update(['name' => [null]]);
        }
        if (isset($request->responsible_name)) {
            foreach ($request->responsible_name as $key => $name) {
                if (isset($request->accountResponsible_id[$key])) {
                    $id = $request->accountResponsible_id[$key];
                    $companyResponsilbe = CompanyAccountResponsible::where('company_id',$company->id)->find($id);
                }else{
                    $companyResponsilbe = new CompanyAccountResponsible();   
                    $companyResponsilbe->company_id    = $company->id;
                    $companyResponsilbe->department_Id = $key;
                }

                $companyResponsilbe->name          = json_encode($name,true);
                $companyResponsilbe->save();
            }
        }else {
            if (!$company->account_responsible->isEmpty()) {
                $company->account_responsible()->delete();
            }
        }
        // company share holder
        if (isset($request->shareholder_name) && isset($request->values)) {
            $labels = $request->shareholder_name;
            $values = $request->values;
            $array = array_combine($labels, $values);
            $json = json_encode($array,true);
        }else {
            $labels = [''];
            $values = [''];
            $array = array_combine($labels, $values);
            $json = json_encode([],true);
        }
        if (isset($request->director)) {
            $directorJson = json_encode($request->director,true);
        }else {
            $directorJson = json_encode([],true);
        }
        $companyShare =  CompanyShare::where('company_id',$company->id)->first();
        $companyShare->share_capital          = $request->share_capital;
        $companyShare->shareholder_name       = $json;
        $companyShare->shareholder_updated_on = $request->shareholder_updated_on;
        $companyShare->director               = $directorJson;
        $companyShare->director_updated_on    = $request->director_updated_on;
        $companyShare->save();
        //relatebel company
        if (isset($request->groupof_companies)) {
            $relatedCompany =  RelatedCompany::where('company_id',$company->id)->find($request->related_companyId);
            if ($relatedCompany == null) {
                $relatedCompany = new RelatedCompany();   
            }
            $relatedCompany->company_id    = $company->id;
            $relatedCompany->name          = json_encode($request->groupof_companies,true);
            $relatedCompany->save();
        }else {
            $relatedCompany =  RelatedCompany::where('company_id',$company->id)->find($request->related_companyId);
            if (isset($relatedCompany)) {
                $relatedCompany->name          = json_encode([null],true);
                $relatedCompany->save();
                //$relatedCompany->delete();   
            }
        }
        // company document
        if (isset($request->document_type)) {
            foreach ($request->document_type as $key => $item) {
                if(isset($item['report_year']) || isset($item['recevie_date']) || isset($item['submitted'])){
                    $report_year = json_encode($item['report_year'],true);
                    $recevie_date = json_encode($item['recevie_date'],true);
                    $submitted = json_encode($item['submitted'],true);
                }else{
                    $report_year = [];
                    $recevie_date = [];
                    $submitted = [];
                }
                $companyDocument =  CompanyDocument::where('company_id',$company->id)->find($item['typeId']);
                $companyDocument->company_id    = $company->id;
                $companyDocument->report_type   = $item['type'];
                $companyDocument->report_year   = $report_year;
                $companyDocument->receive_date  = $recevie_date;
                $companyDocument->submited      = $submitted ;
                $companyDocument->save();
            }
        }
        // company portfolio
        if (isset($request->company_portfolio)) {
            foreach ($request->company_portfolio as $key1 => $item) {
                if (isset($item['user_id']) && count($item['user_id']) > 0) {
                    foreach($item['user_id'] as $key => $userId){
                       if($userId != null){
                        $companyPortfolio = '';
                        
                        if(isset($item['portfolio_id'][$key])){
                            $companyPortfolio = CompanyPortfolio::where('company_id',$company->id)->find($item['portfolio_id'][$key]);
                        }else{
                            $companyPortfolio = new CompanyPortfolio();   
                            $companyPortfolio->company_id    = $company->id;
                            $companyPortfolio->department_id = $item['department_id'];
                        }
                        $companyPortfolio->user_id       = $userId;
                        $companyPortfolio->date          =  $item['date'][$key];
                        $companyPortfolio->save();
                       } 
                    }
                }
            }
        }
        // company reminder
        
                $company_reminder = CompanyReminder::where('company_id',$company->id)->find($request->company_reminder_id);
                if($company_reminder != null){
                $company_reminder->company_id = $company->id;
                $company_reminder->secretarialFees = isset($request->company_reminder['secretarialfees']) ? $request->company_reminder['secretarialfees'] : 0;
                $company_reminder->is_annualReturn_notify = isset($request->company_reminder['annualReturn']) ?$request->company_reminder['annualReturn'] : 0;
                $company_reminder->is_taxAgent_notify = isset($request->company_reminder['taxAgent']) ? $request->company_reminder['taxAgent'] : 0;
                $company_reminder->is_auditorAppoinment_notify = isset($request->company_reminder['auditorAppoinment']) ? $request->company_reminder['auditorAppoinment'] : 0;
                $company_reminder->is_auditStatement_notify = isset($request->company_reminder['audited_statement']) ? $request->company_reminder['audited_statement'] : 0;
                $company_reminder->save();
                }
          
        if ($company) {
            Session::flash('notification','Customer updated successfully. ');
            Session::flash('notificationType','success');
            return redirect()->route('companies.index');;
        } else {
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id  
     * @return \Illuminate\Http\Response
     */
    public function deletePortfolio(Request $request)
    {
        $id = $request->id ;
        $companyPortfolio = CompanyPortfolio::find($id);
        if ($companyPortfolio->delete()) {
            Session::flash('notification','Company portfolio deleted successfully. ');
            Session::flash('notificationType','success');
            return response()->json(['message' => 'Company portfolio deleted successfully', 'notificationType' => 'success', 'status' => 200]);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::with('addresses','company_incharge','account_responsible','shares','related_company','company_invoices','payments','jobs','recurrences','company_portfolios','company_documents','company_reminders')->find($id);
        // check Company has bill or job or payment
        if (count($company->jobs) || count($company->company_invoices) || count($company->recurrences)) {
            Session::flash('notification', 'Company cannot be deleted, this Company is attached to (Jobs/Billing/Payment) available in system!.');
            Session::flash('notificationType', 'error');
            return back()->withInput();
        }
        $company->addresses()->delete();
        $company->company_incharge()->delete();
        $company->account_responsible()->delete();
        $company->shares()->delete();
        $company->related_company()->delete();
        $company->company_portfolios()->delete();
        $company->company_documents()->delete();
        $company->company_reminders()->delete();
        if ($company->delete()) {
            Session::flash('notification','Customer deleted successfully. ');
            Session::flash('notificationType','success');
            return redirect()->back();
        } else {
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->back();
        }
    }
    /**
     * 
     * @param type $id
     * @return type route
     *active inactive function
     */
    public function status($id = null) {
        $company = Company::find($id);
        if ($company->status == 0) {
            Company::find($id)->update(['status' => 1]);
            Session::flash('notification','Company activated successfully!.. ');
            Session::flash('notificationType','success');
            return redirect()->back();
        } else {
            Company::find($id)->update(['status' => 0]);
            Session::flash('notification','Company deactivated successfully!.. ');
            Session::flash('notificationType','success');
            return redirect()->back();
        }
        Session::flash('notification','Something went wrong, Please try again later!');
        Session::flash('notificationType','error');
        return redirect()->route('companies.index');
    }
}