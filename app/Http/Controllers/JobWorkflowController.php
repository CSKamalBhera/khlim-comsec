<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//helper
use App\Helpers\Helper;
//Activity
use Spatie\Activitylog\Models\Activity;
//models
use App\Models\Jobs\Job;
use App\Models\Users\User;
use App\Models\Jobs\JobType;
use App\Models\Jobs\Approval;
use App\Models\Jobs\Recurrence;
use App\Models\Company\Company;
use App\Models\Users\Department;
use App\Models\Jobs\JobWorkflow;
use App\Models\Jobs\WorkflowOption;
use App\Models\Jobs\JobTypeWorkflow;
use App\Models\Jobs\WorkflowType;
use App\Models\Jobs\JobOutput;
use App\Models\Company\Invoice;
use App\Models\Company\InvoiceItem;
use App\Models\Setting;
use App\Models\Jobs\WorkflowNotify;
use Illuminate\Console\Scheduling\Schedule;
#Events
use App\Events\WorkflowUpdates;
use App\Notifications\JobAssignedToUser;
use Illuminate\Support\Facades\Storage;
use Validator,Session,File;
use Carbon;

class JobWorkflowController extends Controller {

    function __construct() {
        $this->middleware('permission:job_workflow_access|job_workflow_create|job_workflow_edit|job_workflow_delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:job_workflow_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:job_workflow_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:job_workflow_delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return redirect()->route('jobs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $causer = auth()->user();
        $job = Job::find($request->job_id);
        $validator = Validator::make($request->all(), [
                    'job_id' => 'required',
                    'job_type_workflow_id' => 'required',
                    'workflow_type_id' => 'required',
                    'workflow_option_id' => 'required',
                        ], [
                    'job_id.required' => "The job field is required.",
                    'job_type_workflow_id.required' => "The job type workflow field is required.",
                    'workflow_type_id.required' => "The workflow type field is required.",
                    'workflow_option_id.required' => "The Workflow option field is required.",
        ]);
        if ($validator->fails()) {
            Session::flash('notification', 'Validation error.Please try again. ');
            Session::flash('notificationType', 'error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $exitWorkflow = JobWorkflow::where('job_id', $request->job_id)->where('job_type_workflow_id', $request->job_type_workflow_id)->first();
        if ($exitWorkflow != null) {
            Session::flash('notification', 'Workflow already submited.');
            Session::flash('notificationType', 'info');
            return redirect()->back();
        }
        $statusType = WorkflowType::find($request->workflow_type_id);
        $option = WorkflowOption::find($request->workflow_option_id);
        // request validate assigne staff
        if ($option->options == 'Assigned') {
            $validator = Validator::make($request->all(), [
                'assignId' => 'required',
            ]);
            // if validation fail
            if ($validator->fails()) {
                Session::flash('notification', 'Please select assigned staff.');
                Session::flash('notificationType', 'error');
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }
        // job Output
        if ($statusType->name == 'Status & JobOutput') {
            if ($option->options == 'Done') {
                $validator = Validator::make($request->all(), [
                    'file' => 'required|mimes:pdf|max:2048',
                ]);
                // if validation fail
                if ($validator->fails()) {
                    Session::flash('notification', 'Please select pdf file.');
                    Session::flash('notificationType', 'error');
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
        }
        // generate invoice
        if ($statusType->id == 11 || $statusType->name == 'Generate Invoice') {
            if ($option->id == 25 || $option->options == 'Mark as Done') {
                $job_id = $request->job_id;
                $jobTypeWorkflow = $request->job_type_workflow_id;
                $workflowType = $request->workflow_type_id;
                $workflowOption = $option->id;
                $user = auth()->user();
                $job = Job::with('department.settings', 'approval', 'company')->find($job_id);
                if($job->department->settings == null){
                    Session::flash('notification', 'Oops!.Please create department setting after you can create invoice!.. ');
                    Session::flash('notificationType', 'error');
                    return redirect()->back();
                }
                // get exiting record
                $record = Invoice::latest()->first();
                if ($record == null) {
                    $invoiceNum = 'INC-0000';
                } else {
                    $invoiceNum = $record->invoice_no;
                }
                $expNum = explode('-', $invoiceNum);
                //check first day in a year
                if (date('Y') . '-01-01' == date('Y-m-d')) {
                    $nextInvoiceNumber = 'INC-0001';
                } else {
                    //increase 1 with last invoice number
                    $numbers = str_pad($expNum[1] + 1, 4, "0", STR_PAD_LEFT);
                    $nextInvoiceNumber = $expNum[0] . '-' . $numbers;
                }
                $invoice = new Invoice();
                $invoice->company_id = $job->company_id;
                $invoice->job_id = $job->id;
                $invoice->department_id = $job->department_id;
                $invoice->setting_id = $job->department->settings->id;
                $invoice->invoice_no = $nextInvoiceNumber;
                $invoice->due_amount = 0;
                $invoice->total_amount = 0;
                $invoice->debtor_code = null;
                $now = \Carbon\Carbon::now();
                $invoice->invoice_date = $now->format('Y-m-d');
                $invoice->due_date = $now->addDays(7)->format('Y-m-d');
                $invoice->description = null;
                $invoice->status = 0;
                $invoice->save();
                // invoice items
                $job = Job::with('job_type')->find($invoice->job_id);
                $invoiceItem = new InvoiceItem();
                $invoiceItem->invoice_id = $invoice->id;
                $invoiceItem->service_name = $job->job_type->name;
                $invoiceItem->is_taxed = 0;
                $invoiceItem->amount = 0;
                $invoiceItem->total_amount = 0;
                $invoiceItem->save();
                if ($invoiceItem) {
                    // Activity logs
                    $causer = auth()->user();
                    $log_name = 'creation';
                    $operation = 'Invoice_create';
                    $description = 'Invoice_create';
                    $custom_properties = ['application' => config('app.name'),
                        'operation' => $operation,
                        'causer_name' => $causer->name,
                        'new_value' => 'hashed',
                        'old_value' => 'hashed',
                    ];
                    Helper::store_activity_log($causer, $invoice, $custom_properties, $log_name, $description);
                }
            }
        }
        // workflow
        $jobWorkflow = new JobWorkflow();
        $jobWorkflow->job_id = $request->job_id;
        $jobWorkflow->job_type_workflow_id = $request->job_type_workflow_id;
        $jobWorkflow->workflow_type_id = $request->workflow_type_id;
        $jobWorkflow->workflow_option_id = $request->workflow_option_id;
        $jobWorkflow->updated_by = $causer->id;
        $jobWorkflow->save();
        // notify workflow update status notification & mail
        $description = 'This job workflow successfully updated the following workflow : '. $jobWorkflow->jobTypeWorkflow->title .' mark as a - '. $jobWorkflow->option->options;
        event(new WorkflowUpdates($job,$jobWorkflow,$description));
        //approvals request
        $job = Job::with('company', 'job_type.workflows')->find($request->job_id);
        foreach ($job->job_type->workflows as $key => $workflow) {
            $workflowId = JobWorkflow::where('job_id', $job->id)->pluck('job_type_workflow_id')->toArray();
            if (in_array($workflow['id'], $workflowId)) {
                $arrayKey = $key + 1;
            } else {
                if ($key == $arrayKey) {
                    if ($workflow->workflow_type->name == 'Manager Review') {
                        $approval = new Approval();
                        $approval->job_id = $request->job_id;
                        $approval->job_type_workflow_id = $workflow->id;
                        $approval->description = 'This ' . $job->job_type->name . '-(' . $job->company->name . ') waiting for your Approval';
                        $approval->user_id = $job->manager_id;
                        $approval->save();
                    }
                }
            }
        }
        //assing user
        if (isset($request->assignId)) {
            if ($option->options == 'Assigned') {
                $jobWorkflow = JobWorkflow::find($jobWorkflow->id);
                $jobWorkflow->assigned_id = $request->assignId;
                $jobWorkflow->save();
                // job assign
                $user = User::find($request->assignId);
                $user->notify(new JobAssignedToUser($user, $job));  // notifications when registered
            }
        }
        // rejected ssm result
        if ($statusType->id == 4) {
            $item_type = $option->options;
            switch ($item_type) {
                case 'Rejected':
                    $item = JobWorkflow::where('job_id', $request->job_id)->delete();
                    break;
            }
        }
        // job Output
        if ($statusType->name == 'Status & JobOutput') {
            if ($option->options == 'Done') {
                $job = Job::find($request->job_id);
                $path = storage_path('app/public/outputFile/' . $job->id);
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                if ($request->hasfile('file')) {
                    $file = $request->file('file');
                    $fileName = time() . '.' . $request->file->extension();
                    $save = $request->file->move($path, $fileName);

                    $jobOutput = new JobOutput();
                    $jobOutput->job_id = $request->job_id;
                    $jobOutput->job_workflow_id = $jobWorkflow->id;
                    $jobOutput->output_file = 'outputFile/' . $job->id . '/' . $fileName;
                    $jobOutput->save();
                }
            }
        }
        // job done
        $jobWorkflowCount = JobWorkflow::where('job_id', $request->job_id)->count();
        $workflowCount = JobTypeWorkflow::where('job_type_id', $request->job_type_id)->count();
        if ($jobWorkflowCount == $workflowCount) {
            $job = Job::find($request->job_id);
            $job->status = 1;
            $job->save();
        }
        // Activity logs
        $log_name = 'creation';
        $operation = 'job_workflow_create';
        $description = 'JobWorkflow';
        $custom_properties = ['application' => config('app.name'),
            'operation' => $operation,
            'causer_name' => $causer->name,
            'new_value' => $option->options,
            'old_value' => 'hashed',
            'JobWorkflow_name' => $statusType->name,
        ];
        Helper::store_activity_log($causer, $job, $custom_properties, $log_name, $description);
        // return response
        if ($jobWorkflow) {
            Session::flash('notification', 'Job workflow created successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->back();
        } else {
            Session::flash('notification', 'Job workflow not created successfully!.. ');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jobs\JobWorkflow  $jobWorkflow
     * @return \Illuminate\Http\Response
     */
    public function show(JobWorkflow $jobWorkflow) {
        //   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jobs\JobWorkflow  $jobWorkflow
     * @return \Illuminate\Http\Response
     */
    public function edit(JobWorkflow $jobWorkflow) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Jobs\JobWorkflow  $jobWorkflow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobWorkflow $jobWorkflow) {
        //SSM result
        $causer = auth()->user();
        $job = Job::find($request->job_id);
        if (isset($request->workflow_option_id)) {
            // 1. Get the item
            $option = WorkflowOption::find($request->workflow_option_id);
            $workflowType = WorkflowType::find($request->workflow_type_id);
            $option_type = $option->options;
            $workflow_type = $workflowType->name;
            switch ($option_type) {
                case 'Rejected':
                    // workflow update notification & mail
                    $description = 'This job workflow successfully updated the following workflow : '. $jobWorkflow->jobTypeWorkflow->title .' mark as a - '. $option_type;
                    event(new WorkflowUpdates($job,$jobWorkflow,$description));
                    $item = JobWorkflow::where('job_id', $request->job_id)->delete();
                    break;
                case 'Complete & Generate invoice':
                    if ($workflowType->id == 11) {
                        if ($option->id == 25) {
                            $job_id = $request->job_id;
                            $jobTypeWorkflow = $request->job_type_workflow_id;
                            $workflowType    = $request->workflow_type_id;
                            $workflowOption  = $option->id;
                            $user = auth()->user();
                            $job = Job::with('department.settings', 'approval', 'company')->find($job_id);
                            if($job->department->settings == null){
                                Session::flash('notification', 'Oops!.Please create department setting after you can create invoice!.. ');
                                Session::flash('notificationType', 'error');
                                return redirect()->back();
                            }
                            // get exiting record
                            $record = Invoice::latest()->first();
                            if ($record == null) {
                                $invoiceNum = 'INC-0000';
                            } else {
                                $invoiceNum = $record->invoice_no;
                            }
                            $expNum = explode('-', $invoiceNum);
                            //check first day in a year
                            if (date('Y') . '-01-01' == date('Y-m-d')) {
                                $nextInvoiceNumber = 'INC-0001';
                            } else {
                                //increase 1 with last invoice number
                                $numbers = str_pad($expNum[1] + 1, 4, "0", STR_PAD_LEFT);
                                $nextInvoiceNumber = $expNum[0] . '-' . $numbers;
                            }
                            $now = \Carbon\Carbon::now();
                            $invoice = new Invoice();
                            $invoice->company_id = $job->company_id;
                            $invoice->job_id     = $job->id;
                            $invoice->department_id = $job->department_id;
                            $invoice->setting_id = $job->department->settings->id;
                            $invoice->invoice_no = $nextInvoiceNumber;
                            $invoice->due_amount = 0;
                            $invoice->total_amount = 0;
                            $invoice->debtor_code = null;
                            $invoice->invoice_date = $now->format('Y-m-d');
                            $invoice->due_date     = $now->addDays(7)->format('Y-m-d');
                            $invoice->description  = null;
                            $invoice->status = 0;
                            $invoice->save();
                            // invoice items
                            $job = Job::with('job_type')->find($invoice->job_id);
                            $invoiceItem = new InvoiceItem();
                            $invoiceItem->invoice_id = $invoice->id;
                            $invoiceItem->service_name = $job->job_type->name;
                            $invoiceItem->is_taxed = 0;
                            $invoiceItem->amount = 0;
                            $invoiceItem->total_amount = 0;
                            $invoiceItem->save();
                            if ($invoiceItem) {
                                // Activity logs
                                $causer = auth()->user();
                                $log_name = 'creation';
                                $operation = 'Invoice_create';
                                $description = 'Invoice_create';
                                $custom_properties = ['application' => config('app.name'),
                                    'operation' => $operation,
                                    'causer_name' => $causer->name,
                                    'new_value' => 'hashed',
                                    'old_value' => 'hashed',
                                ];
                                Helper::store_activity_log($causer, $invoice, $custom_properties, $log_name, $description);
                            }
                            $jobWorkflow->workflow_option_id = $request->workflow_option_id;
                            $jobWorkflow->save();
                            // workflow update notification & mail
                            $description = 'This job workflow successfully updated the following workflow : '. $jobWorkflow->jobTypeWorkflow->title .' mark as a - '. $jobWorkflow->option->options;
                            event(new WorkflowUpdates($job,$jobWorkflow,$description));
                        }
                    }
                    break;
                default;
                    $jobWorkflow->workflow_option_id = $request->workflow_option_id;
                    $jobWorkflow->save();
                    // workflow update notification & mail
                    $description = 'This job workflow successfully updated the following workflow : '. $jobWorkflow->jobTypeWorkflow->title .' mark as a - '. $jobWorkflow->option->options;
                    event(new WorkflowUpdates($job,$jobWorkflow,$description));
                    break;
            }
            switch ($workflow_type) {
                case 'Signed Documents & Payment Status':
                    $jobWorkflow->workflow_option_id = $request->workflow_option_id;
                    $jobWorkflow->save();
                    // workflow update notification & mail
                    $description = 'This job workflow successfully updated the following workflow : '. $jobWorkflow->jobTypeWorkflow->title .' mark as a - '. $jobWorkflow->option->options;
                    event(new WorkflowUpdates($job,$jobWorkflow,$description));
                    break;
            }
                        
            // Activity logs
            $log_name = 'modification';
            $operation = 'job Workflow_update';
            $description = 'JobWorkflow';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'new_value' => $option_type,
                'old_value' => 'hashed',
                'JobWorkflow_name' => $workflow_type,
            ];
            Helper::store_activity_log($causer, $job, $custom_properties, $log_name, $description);

            Session::flash('notification', 'Job workflow updated successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jobs\JobWorkflow  $jobWorkflow
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobWorkflow $jobWorkflow) {
        //
    }

    /**
     * invoiceGenerate the specified resource from storage.
     *
     * @param  \App\Models\Jobs\JobWorkflow  $jobWorkflow
     * @return \Illuminate\Http\Response
     */
    public function invoiceGenerate($job_id, $jobTypeWorkflow, $workflowType, $workflowOption, $jobWorkflow = null) {
        $user = auth()->user();
        $job = Job::with('department', 'approval', 'company')->find($job_id);
        $jobTypeWorkflow = $jobTypeWorkflow;
        $workflowType = $workflowType;
        $workflowOption = $workflowOption;
        $updatedBy = $user->id;
        $jobWorkflow = $jobWorkflow;
        return view('billings.generate', compact('job', 'jobTypeWorkflow', 'workflowType', 'workflowOption', 'updatedBy', 'jobWorkflow'));
    }

}
