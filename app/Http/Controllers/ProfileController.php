<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#helper
use App\Helpers\Helper;
#model
use App\Models\Users\User;
#classes
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Gate,Auth,Validator,Session;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('profile.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth()->user();
        $causer = $user;
        /* Validate the request */
        request()->validate([
            'name'      => 'required|regex:/^[a-z\s]+$/i|max:50',
            'email'     => 'required|email:rfc,dns|unique:users,email,'.$user->id,
            'username'  => 'required|regex:/^[a-z0-9\@]+$/i|max:50|unique:users,username,'.$user->id,
            'ic_number' => 'required|numeric|integer',
            'phone_no'  => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:14',
            'emergency_no' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:14',

        ]);
        if ($request->file('profile_image')) {
            request()->validate([
                'profile_image' => 'mimes:jpg,jpeg,gif,png,bmp',
            ]);
            $file = $request->file('profile_image');
            //delete old pic if exists, unless it is the default picture
            if(($user->img_src != 'storage/users/img/user.jpg') && (File::exists($user->img_src))) {
                File::delete($user->img_src);
            }
            $image = Image::make($file);
            // Crop and resize image to fixed size
             $image->resize(128, 128, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
             });

            // Orientation fix with intervention
            $image->orientate()->exif();
            $destinationPath = public_path(config('app.basepath')).strtolower($file->getClientOriginalName());
            //save image
            $image->save($destinationPath);
            //store path in database
            $user->img_src = config('app.basepath').strtolower($file->getClientOriginalName());
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->phone_no = $request->phone_no;
        $user->ic_number = $request->ic_number;
        $user->emergency_no = $request->emergency_no;
        $user->save();
        Session::flash('notification','Profile successful update');
        Session::flash('notificationType','success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /*
     * custom functions
     *
    */
    public function updatePassword(Request $request)
    {
        $user = auth()->user();
        $pass = auth()->guard('web')->attempt(['email' => $user->email, 'password' => $request->current_password]);
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        ]);
        if ($validator->fails()) {
            Session::flash('notification', 'Validation error. Please try again.');
            Session::flash('notificationType', 'error');
            return back()->withErrors($validator)->withInput();
        }

        if($pass == true)
        {
            if($request->new_password == $request->confirm_password && $request->confirm_password != '')
            {
                $user->password = Hash::make($request->new_password);
                $user->save();
                Session::flash('notification','Successful update');
                Session::flash('notificationType','success');
                Auth::logout();
                return redirect('login');
            }
            elseif($request->new_password != $request->confirm_password)
            {
                Session::flash('notification','Password and password confirmation do not match');
                Session::flash('notificationType','danger');
                return redirect()->back();
            }
            else
            {
                Session::flash('notification','New password not macth');
                Session::flash('notificationType','danger');
                return redirect()->back();
            }
        }
        elseif($request->current_password == '')
        {
            Session::flash('notification','Current password is requird');
            Session::flash('notificationType','danger');
            return redirect()->back();
        }
        else
        {
            Session::flash('notification','invalid password');
            Session::flash('notificationType','danger');
            return redirect()->back();
        }
    }
}
