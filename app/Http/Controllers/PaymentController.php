<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//helper
use App\Helpers\Helper;
//Activity
use Spatie\Activitylog\Models\Activity;
#models
use App\Models\Users\Department;
use App\Models\Company\Company;
use App\Models\Company\Invoice;
use App\Models\Company\CompanyPayment;
use App\Models\Jobs\Job;
use App\Models\Setting;
#classes
use Validator,Session;
class PaymentController extends Controller
{
    function __construct() {
        $this->middleware('permission:payment_access|payment_create|payment_edit|payment_delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:payment_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:payment_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:payment_delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //check validations
        $validator = Validator::make($request->all(), [
            'amount'             => 'required|regex:/^[0-9\.]+$/i|min:0|max:100',
            'paymentDate'        => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('notification','Validation error. Please try again.');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $invoice = Invoice::with('company')->find($request->invoice_id);
        $payment = new CompanyPayment();
        $payment->invoice_id   = $request->invoice_id;
        $payment->company_id   = $invoice->company->id;
        $payment->payment_note = $request->payment_note;
        $payment->amount       = $request->amount;
        $payment->date         = $request->paymentDate;
        $payment->description  = $request->description;
        $payment->status       = 1;
        if($invoice->due_amount >= $request->amount){
           $payment->save();
        }else{
            Session::flash('notification', 'Your Payment Amount should not be greater then Due Amount !');
            Session::flash('notificationType', 'error');
            return redirect()->back();   
        }
        if($payment){
            // invoice payment status
            $paidAmmount   = CompanyPayment::where('invoice_id',$request->invoice_id)->sum('amount');
            $peddingAmount = $invoice->total_amount-$paidAmmount;
            if($invoice->total_amount <= $paidAmmount){
                $invoice->status  = 1;
            }elseif($invoice->total_amount >= $paidAmmount){
                $invoice->status = 0;
            }
            $invoice->due_amount    = $peddingAmount;
            $invoice->save();
            // response
            Session::flash('notification','payment add successfully!..');
            Session::flash('notificationType','success');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //check validations
        $validator = Validator::make($request->all(), [
            'amount'             => 'required|regex:/^[0-9\.]+$/i|min:1|max:14',
            'paymentDate'        => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('notification','Validation error. Please try again.');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $invoice = Invoice::with('company')->find($id);
        $payment = CompanyPayment::with('company','invoice')->find($request->payment_id);
        $payment ->invoice_id   = $request->invoice_id;
        $payment->company_id    = $invoice->company->id;
        $payment->payment_note  = $request->payment_note;
        $payment->amount        = $request->amount;
        $payment->date          = $request->paymentDate;
        $payment->description   = $request->description;
        $payment->status        = 1;
    
        if($payment->save()){
            //invoice payment status
            $paidAmmount   = CompanyPayment::where('invoice_id',$request->invoice_id)->sum('amount');
            $peddingAmount = $invoice->total_amount-$paidAmmount;
            if($invoice->total_amount <= $paidAmmount){
                $invoice->status  = 1;
            }elseif($invoice->total_amount >= $paidAmmount){
                $invoice->status = 0;
            }
            $invoice->due_amount    = $peddingAmount;
            $invoice->save();
            // response
            Session::flash('notification','payment edit successfully!..');
            Session::flash('notificationType','success');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
