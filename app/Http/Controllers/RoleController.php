<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\Models\Users\Role as Roles;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;
use Session;

class RoleController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct() {
        $this->middleware('permission:role_access|role_create|role_edit|role_delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role_delete', ['only' => ['destroy']]);
        $this->middleware('permission:role_status', ['only' => ['status']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $roles = Role::orderBy('id', 'DESC')->get();
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $permissions = Permission::where('status',1)->get();
        return view('roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate request
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[a-z\s]+$/i|max:255|unique:roles,name',
                   
        ],[
            'name.regex'=>'Only Alphabets and Space is Allowed',
        ]);
        if ($validator->fails()) {
            Session::flash('notification', 'Validation error. Please try again.');
            Session::flash('notificationType', 'error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $role = Role::create([
            'name' => $request->input('name'),
            'slug' => Str::slug($request->input('name'), '-'),
        ]);
        $role->syncPermissions($request->input('permission'));
        if ($role) {
            Session::flash('notification', 'Role created successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('roles.index');
        } else {
            Session::flash('notification', 'Role not created successfully!.. ');
            Session::flash('notificationType', 'error');
            return redirect()->route('roles.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
                ->where("role_has_permissions.role_id", $id)
                ->get();

        return view('roles.show', compact('role', 'rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $role = Role::find($id);
        $permissions = Permission::where('status',1)->get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
                ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
                ->all();

        return view('roles.edit', compact('role', 'permissions', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        // validate request
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[a-z\s]+$/i|max:255||unique:roles,name,' . $id,
        ],[
            'name.regex'=>'Only Alphabets and Space is Allowed',
        ]);
        if ($validator->fails()) {
            Session::flash('notification', 'Validation error. Please try again.');
            Session::flash('notificationType', 'error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->slug = Str::slug($request->input('name'), '-');
        $role->save();
        $role->syncPermissions($request->input('permission'));
        Session::flash('notification', 'Role updated successfully.');
        Session::flash('notificationType', 'success');
        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $role = Role::with('users')->find($id);
        // check staff which is assigned to roles
        if (count($role->users)) {
            Session::flash('notification', 'Role cannot be deleted, this role is attached to staff(s) available in system!.');
            Session::flash('notificationType', 'error');
            return back()->withInput();
        }
        // delete roles which is not attached to staff
        if ($role->delete()) {
            Session::flash('notification', 'Role deleted successfully.');
            Session::flash('notificationType', 'success');
            return redirect()->back();
        } else {
            Session::flash('notification', 'Something went wrong, Please try again later.');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }
    }

    /**
     * 
     * @param type $id
     * @return type route
     */
    public function status($id) {
        //active inactive function
        $role = Role::find($id);
        if (!$role->status) {
            Role::whereId($id)->update(['status' => 1]);
            Session::flash('notification', 'Role activated successfully.');
            Session::flash('notificationType', 'success');
            return redirect()->back();
        } else {
            Role::whereId($id)->update(['status' => 0]);
            Session::flash('notification', 'Role deactivated successfully.');
            Session::flash('notificationType', 'success');
            return redirect()->back();
        }
        Session::flash('notification', 'Something went wrong, Please try again later!');
        Session::flash('notificationType', 'error');
        return redirect()->back();
    }

}
