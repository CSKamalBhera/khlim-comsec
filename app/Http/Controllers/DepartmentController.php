<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#models
use App\Models\Users\Department;
use App\Models\Users\User;
#classes
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Session;
class DepartmentController extends Controller
{
    
    function __construct()
    {
        $this->middleware('permission:department_access|department_create|department_edit|department_delete', ['only' => ['index','show']]);
        $this->middleware('permission:department_create', ['only' => ['create','store']]);
        $this->middleware('permission:department_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:department_delete', ['only' => ['destroy']]);
        $this->middleware('permission:department_status', ['only' => ['status']]);

    }
    /**
     * Display a listing of the Departments.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::orderBy('created_at', 'DESC')->get();
        return view('departments.index', compact('departments'));
    }

    /**
     * Show the form and create  a new Departments.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {

        return view('departments.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check validations
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[a-z\s\-]+$/i|max:255|unique:departments,name,NULL,id,deleted_at,NULL',
            'description' => 'required|string|max:50',
        ]);
        if ($validator->fails()) {
            Session::flash('notification','Please try again.. ');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $department = new Department();
        $department->name = ucwords($request->name);
        $department->slug =  strtolower($request->name);
        $department->description =  $request->description;
        if ($department->save()) {
            Session::flash('notification','Department created successfully!.. ');
            Session::flash('notificationType','success');
            return redirect()->route('departments.index');
        } else {
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->route('departments.index');
        }
            
    }

    /**
     * Display the specified Departments.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $department = Department::find($id);
        // return view('departments.view', compact('department')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
        $department = Department::find($id);
        return view('departments.edit', compact('department'));    
    }

    /**
     * Update the specified Departments in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $department = Department::find($id);
       //check validations
       $validator = Validator::make($request->all(), [
        'name' => 'required|string|regex:/^[a-z\s\-]+$/i|max:50|unique:departments,name,'.$department->id.',id,deleted_at,NULL',
        'description' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            Session::flash('notification','Please try again.. ');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $department->name = ucwords($request->name);
        $department->slug =  strtolower($request->name);
        $department->description =  $request->description;
        if ($department->save()) {
            Session::flash('notification','Department updated successfully!.. ');
            Session::flash('notificationType','success');
            return redirect()->route('departments.index');
        } else {
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->route('departments.index');
        }
    }

    /**
     * Remove the specified Departments from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::with('users','jobtypes','jobs','companyResponsible','companyPortfolio','company_incharge','recurrences','company_invoices')->find($id);
        $hasPivotUser = User::whereHas('departments', function ($q) use ($id) {
            $q->where('id', $id);
        })->exists();
        if ($hasPivotUser || !$department->jobtypes->isEmpty() || !$department->jobs->isEmpty() || !$department->companyResponsible->isEmpty() || !$department->companyPortfolio->isEmpty() || !$department->company_incharge->isEmpty() || !$department->recurrences->isEmpty() || !$department->company_invoices->isEmpty()) {
            Session::flash('notification','This department has attached and cant be deleted.');
            Session::flash('notificationType','error');
            return redirect()->back();
        }else{
            if ($department->delete()) {
                Session::flash('notification','Department deleted successfully!.. ');
                Session::flash('notificationType','success');
                return redirect()->back();
            } else {
                Session::flash('notification','Something went wrong, Please try again later!');
                Session::flash('notificationType','error');
                return redirect()->back();
            }
        }
        
    }
    
    /**
     * 
     * @param type $id
     * @return type route
     *active inactive function
     */
    public function status($id = null) {
       $department = Department::find($id);
       if ($department->status == 0) {
           Department::find($id)->update(['status' => 1]);
           Session::flash('notification','Department activated successfully!.. ');
           Session::flash('notificationType','success');
           return redirect()->back();
       } else {
           Department::find($id)->update(['status' => 0]);
           Session::flash('notification','Department deactivated successfully!.. ');
           Session::flash('notificationType','success');
           return redirect()->back();
       }
       Session::flash('notification','Something went wrong, Please try again later!');
       Session::flash('notificationType','error');
       return redirect()->route('departments.index');
    }

}
