<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//helper
use App\Helpers\Helper;
//Activity
use Spatie\Activitylog\Models\Activity;
//models
use App\Models\Jobs\Job;
use App\Models\Users\User;
use App\Models\Jobs\JobType;
use App\Models\Company\Company;
use App\Models\Users\Department;
use App\Models\Jobs\JobWorkflow;
use App\Models\Jobs\JobOutput;
use Illuminate\Support\Facades\Storage;
use Validator,Session,File;
use Response;
class JobOutputController extends Controller
{
    function __construct() {
        $this->middleware('permission:jobs_output_access|jobs_output_create|jobs_output_edit|jobs_output_delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:jobs_output_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:jobs_output_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:jobs_output_delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departmentId = auth()->user()->departments->pluck('id')->toArray();
        $joboutputs = JobOutput::with('job')->whereHas('job', function($query) use ($departmentId){
            $query->whereIn('department_id',$departmentId); 
        })->orderBy('created_at', 'DESC')->get();
            
        return view('joboutput.index', compact('joboutputs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $job = Job::find($request->job_id);
        // // validate request
        // $validator = Validator::make($request->all(), [
        //     'file' => 'required|mimes:csv,txt,xlx,xls,pdf|max:2048',
        // ]);
        // if validation fail
        // if ($validator->fails()) {
        //     Session::flash('notification','Validation error.Please try again. ');
        //     Session::flash('notificationType','error');
        //     return redirect()->back()->withErrors($validator)->withInput();
        // }
        // $path = storage_path('app/public/outputFile/'.$job->id);
        // if(!File::isDirectory($path)){
        //     File::makeDirectory($path, 0777, true, true);
        // }
        // if($request->hasfile('file')){
        //     $file = $request->file('file');
        //     $fileName = time().'.'.$request->file->extension();
        //     $save = $request->file->move($path, $fileName);
        //     $job->output_file = 'outputFile/'.$job->id.'/'.$fileName;
        //     $job->save();
        // }
        // if($save){
        //     Session::flash('notification', 'Job Output created file successfully!.. ');
        //     Session::flash('notificationType', 'success');
        //     return redirect()->back();
        // }
        // return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $joboutput = JobOutput::with('job')->find($id);
        return view('joboutput.show',compact('joboutput'));
    }

    public function getDownload($id)
    {
        $jobOutput = JobOutput::find($id);
        if($jobOutput->output_file == ''){
            Session::flash('notification', 'Job output file not available.');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }
        //PDF file is stored under project/public/download/info.pdf
        $file = storage_path('app/public/').$jobOutput->output_file;
        $headers = array(
            'Content-Type: application/pdf',
        );
        return Response::download($file, 'jobOutput.pdf', $headers);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function pdfView($id)
    {
        //$filename = '1626067343.pdf';
        // $path = storage_path($filename);
        $jobOutput = JobOutput::find($id);
        $filename = $jobOutput->output_file;
        //PDF file is stored under project/public/download/info.pdf
        $path = storage_path('app/public/').$jobOutput->output_file;
        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }

}
