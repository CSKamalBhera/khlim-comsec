<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#models
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
#classes
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Session;

class PermissionController extends Controller {

    function __construct() {
        $this->middleware('permission:permission_access|permission_create|permission_edit|permission_delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:permission_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:permission_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:permission_delete', ['only' => ['destroy']]);
        $this->middleware('permission:permission_status', ['only' => ['status']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $permissions = Permission::orderBy('id','DESC')->get();
        return view('permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate request
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[a-z\_]+$/i|max:50|unique:permissions,name,NULL,id,deleted_at,NULL',
            'status' => 'required',

        ],[
            'name.regex'=>'Only Alphabets and Underscore is Allowed',
            'status.required' => 'The status field is required',
        ]);

        if ($validator->fails()) {
            Session::flash('notification', 'Validation error. Please try again.');
            Session::flash('notificationType', 'error');
            return back()->withErrors($validator)->withInput();
        }
        $permissionName = strtolower($request->name);
        $permission = Permission::create(['name' => $permissionName,'status' => $request->status]);
        if ($permission) {
            Session::flash('notification', 'Permission created successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('permissions.index');
        } else {
            Session::flash('notification', 'Permission not created successfully!.. ');
            Session::flash('notificationType', 'error');
            return redirect()->route('permissions.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $permission = Permission::find($id);
        $rolePermissions = Permission::with('roles')->find($id);
        return view('permissions.show', compact('permission', 'rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $permission = Permission::find($id);
        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        // validate request
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[a-z\_]+$/i|max:50|unique:permissions,name,'.$id.',id,deleted_at,NULL',
            'status' => 'required',
        ],[
            'name.regex'=>'Only Alphabets and Underscore is Allowed',
            'status.required'=>'The Status field is required',
        ]);

        if ($validator->fails()) {
            Session::flash('notification', 'Validation error. Please try again.');
            Session::flash('notificationType', 'error');
            return back()->withErrors($validator)->withInput();
        }
        $permissionName = strtolower($request->name);
        $permission = Permission::find($id);
        $permission->name = $permissionName;
        $permission->status = $request->status;
        if ($permission->save()) {
            Session::flash('notification', 'Permission updated successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('permissions.index');
        } else {
            Session::flash('notification', 'Permission not update successfully!.. ');
            Session::flash('notificationType', 'error');
            return redirect()->route('permissions.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $permission = Permission::with('roles')->find($id);
        // check staff which is assigned to roles
        if (count($permission->roles)) {
            Session::flash('notification', 'Permission cannot be deleted, this permission is attached to Role available in system!.. ');
            Session::flash('notificationType', 'error');
            return back()->withInput();
        }

        // delete roles which is not attached to staff
        if ($permission->delete()) {
            Session::flash('notification', 'Permission deleted successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('permissions.index');
        } else {
            Session::flash('notification', 'Something went wrong, Please try again later!.. ');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }
    }

    /**
     * 
     * @param type $id
     * @return type route
     */
    public function status($id) {
        //active inactive function
        $permission = Permission::find($id);

        if (!$permission->status) {
            Permission::whereId($id)->update(['status' => 1]);
            Session::flash('notification', 'Permission activated successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('permissions.index');
        } else {
            Permission::whereId($id)->update(['status' => 0]);
            Session::flash('notification', 'Permission deactivated successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('permissions.index');
        }
        Session::flash('notification', 'Something went wrong, Please try again later!.. ');
        Session::flash('notificationType', 'error');
        return redirect()->back();
    }

}
