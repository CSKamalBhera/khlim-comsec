<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//helper
use App\Helpers\Helper;
//Activity
use Spatie\Activitylog\Models\Activity;
#models
use App\Models\Users\Department;
use App\Models\Company\Company;
use App\Models\Company\Invoice;
use App\Models\Company\InvoiceItem;
use App\Models\Company\CompanyPayment;
use App\Models\Jobs\Job;
use App\Models\Setting;
use App\Models\Jobs\JobWorkflow;
use App\Models\Jobs\JobTypeWorkflow;
#classes
use Validator,
    Session;
use PDF;

class InvoiceController extends Controller {

    function __construct() {
        $this->middleware('permission:invoice_access|invoice_create|invoice_edit|invoice_delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:invoice_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:invoice_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:invoice_delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $departmentId = auth()->user()->departments->pluck('id')->toArray();
        $departments = Department::get();
        $invoiceQuery = Invoice::query()->with('company', 'job', 'setting', 'department')->whereIn('department_id', $departmentId);
        // Search for a user based on their department.
        if ($request->department != '') {
            $invoiceQuery->where('department_id', $request->department);
        }
        // Search for a user based on their status.
        if ($request->status != '') {
            $invoiceQuery->where('status', $request->status);
        }
        // get invoice
        $invoices = $invoiceQuery->orderBy('created_at', 'DESC')->get();
        return view('billings.index', compact('departments', 'invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $departments = Department::where('status', 1)->get();
        $settings = Setting::get();
        $companies = Company::where('status', 1)->get();
        $jobs = Job::get();
        return view('billings.create', compact('departments', 'settings', 'companies', 'jobs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $causer = auth()->user();
        $validator = Validator::make($request->all(), [
            'company_id' => 'required',
            'job_id' => 'nullable',
            'department_id' => 'required',
            'setting_id' => 'required',
            'amount' => 'required|regex:/^[0-9\,\.]+$/i|min:0|max:100',
            'invoiceDate' => 'required|date',
            'dueDate' => 'required|date',
//            'debtorCode' => 'required|regex:/^[a-zA-Z0-9 ]+$/|min:2|max:20',
//            'description' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('notification', 'Validation error.Please try again. ');
            Session::flash('notificationType', 'error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        // get exiting record
        $record = Invoice::latest()->first();
        if ($record == null) {
            $invoiceNum = 'INC-0000';
        } else {
            $invoiceNum = $record->invoice_no;
        }
        $expNum = explode('-', $invoiceNum);
        //check first day in a year
        if (date('Y') . '-01-01' == date('Y-m-d')) {
            $nextInvoiceNumber = 'INC-0001';
        } else {
        //increase 1 with last invoice number
            $numbers = str_pad($expNum[1] + 1, 4, "0", STR_PAD_LEFT);
            $nextInvoiceNumber = $expNum[0] . '-' . $numbers;
        }
        $invoice = new Invoice();
        $invoice->company_id = $request->company_id;
        $invoice->job_id = $request->job_id;
        $invoice->department_id = $request->department_id;
        $invoice->setting_id = $request->setting_id;
        $invoice->invoice_no = $nextInvoiceNumber;
        $invoice->due_amount = $request->amount;
        $invoice->total_amount = $request->amount;
        $invoice->debtor_code = $request->debtorCode;
        $invoice->invoice_date = $request->invoiceDate;
        $invoice->due_date = $request->dueDate;
        $invoice->description = $request->description;
        $invoice->status = 0;
        $invoice->save();
        // invoice items
        $job = Job::with('job_type')->find($invoice->job_id);
        $invoiceItem = new InvoiceItem();
        $invoiceItem->invoice_id = $invoice->id;
        $invoiceItem->service_name = isset($job->job_type->name)?$job->job_type->name:null;
        $invoiceItem->is_taxed = 0;
        $invoiceItem->amount = $request->amount;
        $invoiceItem->total_amount = $request->amount;
        $invoiceItem->save();

        // if request come from workflow invoice generate
        if (isset($request->jobtype_workflow_id) && isset($request->workflow_type_id) && isset($request->workflow_option_id)) {
            $jobTypeWorkflow = JobTypeWorkflow::find($request->jobtype_workflow_id);
            // create new incorporation job
            if($request->workflow_option_id == 21){
                $notify = json_decode($jobTypeWorkflow->workflowNotify->notification_value);
                $job_new = new Job();
                $job_new->company_id    = $job->company_id;
                $job_new->staff_id      = $job->staff_id;
                $job_new->job_type_id   = $notify->jobtype_id;
                $job_new->manager_id    = $job->manager_id;
                $job_new->department_id = $job->department_id;
                $job_new->created_by    = auth()->user()->id;
                $job_new->status        = 0;
                $job_new->save();
            }
            // workflow create after invoice generate
            $log_name = 'modification';
            $operation = 'job_modification';
            $description = 'job modification';
            $jobWorkflow = JobWorkflow::find($request->jobWorkflow);
            if ($jobWorkflow == null) {
                $jobWorkflow = new JobWorkflow();
                $log_name = 'creation';
                $operation = 'job_workflow_create';
                $description = 'Job Workflow chnaged';
            }
            $jobWorkflow->job_id = $request->job_id;
            $jobWorkflow->job_type_workflow_id = $request->jobtype_workflow_id;
            $jobWorkflow->workflow_type_id = $request->workflow_type_id;
            $jobWorkflow->workflow_option_id = $request->workflow_option_id;
            $jobWorkflow->updated_by = $request->updatedBy;
            $jobWorkflow->save();
            // Activity logs          
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'new_value' => 'hashed',
                'old_value' => 'hashed',
            ];
            Helper::store_activity_log($causer, $jobWorkflow, $custom_properties, $log_name, $description);
            // job done
            $jobWorkflowCount = JobWorkflow::where('job_id', $request->job_id)->count();
            $workflowCount = JobTypeWorkflow::where('job_type_id', $request->job_type_id)->count();
            if ($jobWorkflowCount == $workflowCount) {
                $job = Job::find($request->job_id);
                $job->status = 1;
                $job->save();
            }
        }
        if ($invoiceItem) {
            // Activity logs
            $causer = auth()->user();
            $log_name = 'creation';
            $operation = 'Invoice_create';
            $description = 'Invoice_create';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'new_value' => 'hashed',
                'old_value' => 'hashed',
            ];
            Helper::store_activity_log($causer, $invoice, $custom_properties, $log_name, $description);

            Session::flash('notification', 'Invoice created successfully!.. ');
            Session::flash('notificationType', 'success');

            if (isset($request->jobtype_workflow_id) && isset($request->workflow_type_id)) {
                return redirect()->route('jobs.show', $request->job_id);
            } else {
                return redirect()->route('invoices.index');
            }
        } else {
            Session::flash('notification', 'Something went wrong, Please try again later!');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $model_name = 'App\Models\Company\Invoice';
        $activities = Activity::where('subject_type', $model_name)->where('subject_id', $id)->where('log_name', 'creation')->orderBy('created_at', 'desc')->latest()->first();
        $lastDawnload = Activity::where('subject_type', $model_name)->where('subject_id', $id)->where('log_name', 'downloaded')->orderBy('created_at', 'desc')->latest()->first();
        $invoice = Invoice::with('company', 'job', 'setting', 'department', 'payments', 'invoiceItems')->find($id);
        return view('billings.show', compact('invoice', 'activities', 'lastDawnload'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $departments = Department::where('status', 1)->get();
        $settings = Setting::get();
        $companies = Company::where('status', 1)->get();
        $jobs = Job::get();
        $invoice = Invoice::with('company', 'job', 'setting', 'department')->find($id);
        return view('billings.edit', compact('departments', 'settings', 'companies', 'invoice', 'jobs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $validator = Validator::make($request->all(), [
            'company_id' => 'required',
            'job_id' => 'nullable',
            'department_id' => 'required',
            'amount' => 'required|regex:/^[0-9\,\.]+$/i|min:0|max:100',
            'setting_id' => 'required',
            'invoiceDate' => 'required|date',
            'dueDate' => 'required|date',
            // 'debtorCode' => 'required|regex:/^[a-zA-Z0-9 ]+$/|min:2|max:20',
            //'description' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('notification', 'Validation error.Please try again. ');
            Session::flash('notificationType', 'error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $paidAmmount = CompanyPayment::where('invoice_id', $id)->sum('amount');
        $peddingAmount = $request->amount - $paidAmmount;
        $invoice = Invoice::with('invoiceItems')->find($id);
        $invoice->company_id = $request->company_id;
        $invoice->job_id     = $request->job_id;
        $invoice->department_id = $request->department_id;
        $invoice->setting_id = $request->setting_id;
        $invoice->due_amount = $peddingAmount;
        $invoice->total_amount = $request->amount;
        $invoice->invoiceItems[0]->amount = $request->amount;
        $invoice->invoiceItems[0]->total_amount = $request->amount;
        $invoice->invoice_date = $request->invoiceDate;
        $invoice->due_date = $request->dueDate;
        $invoice->debtor_code = $request->debtorCode;
        $invoice->description = $request->description;
        if ($request->amount <= $paidAmmount) {
            $invoice->status = 1;
        } elseif ($request->amount >= $paidAmmount) {
            $invoice->status = 0;
        }

        if ($invoice->push()) {
            // Activity logs
            $causer = auth()->user();
            $log_name = 'modification';
            $operation = 'invoice_modification';
            $description = 'invoice modification';
            $custom_properties = ['application' => config('app.name'),
                'operation' => $operation,
                'causer_name' => $causer->name,
                'new_value' => 'hashed',
                'old_value' => 'hashed',
            ];
            Helper::store_activity_log($causer, $invoice, $custom_properties, $log_name, $description);

            Session::flash('notification', 'Invoice updated successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('invoices.index');
        } else {
            Session::flash('notification', 'Something went wrong, Please try again later!');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
         $invoice = Invoice::with('invoiceItems','payments')->find($id);
         $invoice->invoiceItems()->delete();
         if(isset($invoice->payments)){
         $invoice->payments()->delete();
         }
         if ($invoice->delete()) {
            Session::flash('notification','Invoice deleted successfully. ');
            Session::flash('notificationType','success');
            return redirect()->back();
        } else {
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->back();
        }
    }

    /**
     * Get Job the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getCompanyJobs(Request $request) {
        $search = $request->company;
        if ($search == '') {
            $jobs = Job::get();
        } else {
            $jobs = Job::with('job_type')->where('company_id', $request->company)->where('status', 1)->get();
        }
        $response = array();
        foreach ($jobs as $job) {
            echo '<option value="' . $job['id'] . '">' . $job->job_type->name . '</option>';
        }
    }

    /**
     * Get Job the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getSettings(Request $request) {
        $search = $request->department;
        if ($search == '') {
            $settings = Setting::get();
        } else {
            $settings = Setting::where('department_id', $request->department)->get();
        }
        $response = array();
        foreach ($settings as $setting) {
            echo '<option value="' . $setting['id'] . '">' . $setting->name . '</option>';
        }
    }

    /**
     * 
     * @param type $id
     * @return type route
     * active inactive function
     */
    public function status($id = null) {
        $invoice = Invoice::find($id);
        if ($invoice->status == 0) {
            Invoice::find($id)->update(['status' => 1]);
            Session::flash('notification', 'status updated successfully. ');
            Session::flash('notificationType', 'success');
            return redirect()->back();
        } else {
            Invoice::find($id)->update(['status' => 0]);
            Session::flash('notification', 'status updated successfully. ');
            Session::flash('notificationType', 'success');
            return redirect()->back();
        }
        Session::flash('notification', 'Something went wrong, Please try again later!');
        Session::flash('notificationType', 'error');
        return redirect()->back();
    }

    /**
     * download invoice in new tab
     * @param Request $request
     * @param type $id
     */
    public function downloadInvoice($id) {
        $settings = Setting::all();
        $invoice = Invoice::with('company', 'job', 'setting', 'department')->find($id);
        $companyAdderss = $invoice->company->addresses->where('addresse_type', 'Registered Address')->first();
        $companyIncharge = $invoice->company->company_incharge->where('department_id', $invoice->department->id)->first();

        // Activity logs
        $causer = auth()->user();
        $log_name = 'downloaded';
        $operation = 'invoice_download';
        $description = 'invoice download';
        $custom_properties = ['application' => config('app.name'),
            'operation' => $operation,
            'causer_name' => $causer->name,
            'new_value' => 'hashed',
            'old_value' => 'hashed',
        ];
        Helper::store_activity_log($causer, $invoice, $custom_properties, $log_name, $description);
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('billings.invoice-pdf', compact('invoice', 'settings', 'companyAdderss', 'companyIncharge'));
        return $pdf->download('invoice.pdf');
    }

    /**
     * payment create
     * @param Request $request
     * @param type $id
     */
    public function payment(Request $request, $id) {

        return view('billings.payment', compact('id'));
    }

    /**
     * Invoice can be add more items create
     * @param Request $request
     * @param type $id
     */
    public function InvoiceItemEdit(Request $request, $id) {
        // validate request
        $validator = Validator::make($request->all(), [
            'invoiceItem.*.service_name' => 'required|regex:/^[a-z\s0-9]+$/i|max:50',
            'invoiceItem.*.amount' => 'required|regex:/^[0-9\,\.]+$/i|min:1|max:14',
        ], [
            'invoiceItem.*.service_name.required' => "The Service Name field is required.",
            'invoiceItem.*.service_name.regex' => "The Service Name field is not valid.",
            'invoiceItem.*.service_name.max' => "The Service Name field must not be greater than 50 characters.",
            'invoiceItem.*.amount.required' => "The Amount field is required.",
            'invoiceItem.*.amount.regex' => "The Amount field is not valid.",
            'invoiceItem.*.amount.min' => "The Amount field field must be at least 1 digits.",
            'invoiceItem.*.amount.max' => "The Amount field field must not be greater than 14 digits.",
        ]);
        // retrun resopnse
        if ($validator->fails()) {
            Session::flash('notification', 'Validation error.Please try again. ');
            Session::flash('notificationType', 'error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if (isset($request->invoiceItem)) {
            $invoice = Invoice::with('invoiceItems')->find($id);
            InvoiceItem::where('invoice_id', $invoice->id)->whereNotIn('id', $request->invoice_items_id)->delete();
            foreach ($request->invoiceItem as $key => $item) {

                if (isset($request->invoice_items_id[$key])) {
                    $invoiceItem = InvoiceItem::find($request->invoice_items_id[$key]);
                } else {
                    $invoiceItem = new InvoiceItem();
                }
                $tax = isset($item['tax']) ? $item['tax'] : 0;
                $amountRouded = round($item['amount'], 2);
                $invoiceItem->invoice_id = $invoice->id;
                $invoiceItem->service_name = $item['service_name'];
                $invoiceItem->is_taxed = $tax;
                $invoiceItem->amount = $amountRouded;
                if ($tax == 1) {
                    $total = $amountRouded + $amountRouded * 6 / 100;
                } else {
                    $total = $amountRouded;
                }
                $invoiceItem->total_amount = $total;
                $invoiceItem->save();
            }
            //invoice payment status
            $invoice = Invoice::with('invoiceItems')->find($id);
            $paidAmmount = CompanyPayment::where('invoice_id', $invoice->id)->sum('amount');
            // amount items calculate
            $subTotal = $invoice->invoiceItems->sum('amount');
            $taxAmount = $invoice->invoiceItems->where('is_taxed', 1)->sum('amount');
            $taxSSTAmount = round(6 * ($taxAmount / 100), 2);
            $totalDue = round($taxSSTAmount + $subTotal, 2);
            $peddingAmount = $totalDue - $paidAmmount;
            // invoice amount
            $invoice->due_amount = $peddingAmount;
            $invoice->total_amount = $totalDue;
            if ($invoice->total_amount <= $paidAmmount) {
                $invoice->status = 1;
            } elseif ($invoice->total_amount >= $paidAmmount) {
                $invoice->status = 0;
            }
            $invoice->save();
            Session::flash('notification', 'Invoice Item updated successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('invoices.index');
        } else {
            Session::flash('notification', 'Something went wrong, Please try again later!');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }
    }

    /**
     * invoice preview and print directly invoice the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function previewInvoice($id) {
        $invoice = Invoice::with('invoiceItems', 'setting', 'company')->find($id);
        $companyAdderss = $invoice->company->addresses->where('addresse_type', 'Registered Address')->first();
        $companyIncharge = $invoice->company->company_incharge->where('department_id', $invoice->department->id)->first();
        return view('billings.invoice_preview', compact('invoice', 'companyAdderss', 'companyIncharge'));
    }
    /**
     * invoice preview and print directly invoice the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generate($id) {
        $job = Job::with('department', 'approval', 'company', 'staff', 'manager', 'jobWorkflow', 'job_type.workflows.workflow_type.workflowoption', 'job_type.workflows.workflowNotify')->find($id);
        return view('billings.generate', compact('job'));
    }

}
