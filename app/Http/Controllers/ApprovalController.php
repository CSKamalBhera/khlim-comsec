<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#models
use App\Models\Jobs\Approval;
use App\Models\Company\Company;
use App\Models\Jobs\JobWorkflow;
use App\Models\Jobs\Job;
#classes
use Session;
class ApprovalController extends Controller
{
    function __construct() {
        $this->middleware('permission:approval_access|approval_create|approval_edit|approval_delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:approval_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:approval_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:approval_delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $companies = Company::select('id','name')->get();
        $approvalQuery = Approval::query()->with('job')->where('user_id',auth()->user()->id);
        // Search for a user based on their name.
        if ($request->has('company')) {
            $approvalQuery->whereHas('job', function($query) use ($request){
                    $query->where('company_id',$request->company);
                });
        }
        // Search for a user based on their run date start.
        if ($request->has('status')) {
            $approvalQuery->where('status', $request->status);
        }
        $approvals = $approvalQuery->orderBy('created_at', 'DESC')->get();
        return view('approvals.index',compact('approvals','companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $causer   = auth()->user();
        $approval = Approval::with('job','jobtypeWorkflow.workflow_type.workflowoption')->where('user_id',auth()->user()->id)->find($id);
        // workflow
        $typeId = $approval->jobtypeWorkflow->workflow_type->id;
        foreach ($approval->jobtypeWorkflow->workflow_type->workflowoption as $key => $option) {
            if ($request->status == 1) {
                if ($option->options == 'Complete') {
                    $optionId = $option->id;
                    $jobWorkflow = new JobWorkflow();
                    $jobWorkflow->job_id                = $approval->job_id;
                    $jobWorkflow->job_type_workflow_id  = $approval->job_type_workflow_id;
                    $jobWorkflow->workflow_type_id      = $typeId;
                    $jobWorkflow->workflow_option_id    = $optionId;
                    $jobWorkflow->updated_by            = $causer->id;
                    $jobWorkflow->save();
                }
            }else{
                if ($option->options == 'Incomplete') {
                    $lastUpdatedWorkflow = JobWorkflow::where('job_id',$approval->job_id)->latest('id')->first();
                    $lastUpdatedWorkflow->delete();
                }
            }
        }
        $approval->status = $request->status;
        if($approval->save()){
            Session::flash('notification', 'Job workflow approval updated successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->back();
        } else {
            Session::flash('notification', 'Job workflow approval not updated successfully!.. ');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
