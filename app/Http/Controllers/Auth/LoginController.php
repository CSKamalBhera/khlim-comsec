<?php

namespace App\Http\Controllers\Auth;
#default
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
#custom
use Auth,Validator;
use Session,Cookie,Hash,Route;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $logout_redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware('guest')->except('logout');
    }

    public function maxAttempts()
    {
    	return property_exists($this, 'maxAttempts') ? $this->maxAttempts : 2;
    }

    /**
     * Get the number of minutes to throttle for.
     *
     * @return int
     */

    public function decayMinutes()
    {
    	return property_exists($this, 'decayMinutes') ? $this->decayMinutes : 2;
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */

    protected function hasTooManyLoginAttempts(Request $request)
    {
    	return $this->limiter()->tooManyAttempts(
    		$this->throttleKey($request), $this->maxAttempts()
    	);
    }


    protected function clearLoginAttempts(Request $request)
    {
    	$this->limiter()->clear($this->throttleKey($request));
    }


    protected function incrementLoginAttempts(Request $request)
    {
    	$this->limiter()->hit(
    		$this->throttleKey($request), $this->decayMinutes() * 60
    	);
    }
    public function login(Request $request)
    {
        // Input Form Validation
    	Validator::make($request->all(), [
            'email' => 'required|email|email:rfc',
    		'password' => 'required|min:6|max:20',
        ])->validate();
        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
    	if ($this->hasTooManyLoginAttempts($request)) {
    		$this->fireLockoutEvent($request);
    		return $this->sendLockoutResponse($request);
    	}
        // Customization: Validate if client status is active (1)
    	$inpUsername = $request->get('email');
    	$inpPass = $request->get('password');
    	$isHasRemember = "on";

        // Customization: It's assumed that email field should be an unique field
    	$client = User::where('email', $inpUsername)->first();
    	if (!empty($client)) {
    		$user_id = $client->id;
            $user = User::active()->where('email',$request->email)->get()->first(); // Determine if user is active, with active scope
            if($user){
            	$can_login = User::Canlogin()->where('email',$request->email)->get()->first();
            	if (Hash::check($inpPass, $client->password)) {
                    Auth::loginUsingId($client->id, $isHasRemember);
                    Session::flash('notification', 'Welcome Back'.' '.Auth::user()->name);
                    Session::flash('notificationType','success');
                    return redirect()->intended('/dashboard');
            	}else{
            		$this->incrementLoginAttempts($request);
            		return $this->sendFailedLoginResponse($request, 'auth.failed');
            	}
            }else {
                //active inactive view
            	return view('errors.401',compact('user_id'));
            }
        }
        else{
        	return $this->sendFailedLoginResponse($request, 'auth.failed');
        }


        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        // Customization: If client status is inactive (0) return failed_status error.
        if ($client->status === 0) {
        	return $this->sendFailedLoginResponse($request, 'auth.failed_status');
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $field
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request, $trans = 'auth.failed')
    {
    	$errors = [$this->username() => trans($trans)];
    	if ($request->expectsJson()) {
    		return response()->json($errors, 422);
    	}
    	return redirect()->back()
    	->withInput($request->only($this->username(), 'remember'))
    	->withErrors($errors);
    }


}
