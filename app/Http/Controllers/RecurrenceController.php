<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//helper
use App\Helpers\Helper;
//Activity
use Spatie\Activitylog\Models\Activity;
//models
use App\Models\Jobs\Job;
use App\Models\Users\User;
use App\Models\Jobs\JobType;
use App\Models\Company\Company;
use App\Models\Users\Department;
use App\Models\Jobs\JobWorkflow;
use App\Models\Jobs\Recurrence;
#classes
use Carbon\Carbon;
use Validator,Session;
class RecurrenceController extends Controller
{
    function __construct() {
        $this->middleware('permission:recurrence_access|recurrence_create|recurrence_edit|recurrence_delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:recurrence_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:recurrence_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:recurrence_delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $departments = Department::get();
        $departmentId = auth()->user()->departments->pluck('id')->toArray();
        $recurrenceQuery = Recurrence::query()->whereIn('department_id',$departmentId);
        // Search for a user based on their name.
        if ($request->department != '') {
            $recurrenceQuery->where('department_id',$request->department);
        }
        // Search for a user based on their run date start.
        if ($request->start_date != '') {
            $start = Carbon::parse($request->start_date)->format('Y-m-d');
            $recurrenceQuery->whereDate('run_date','>=', $start);
        }        
        // Search for a recurrenceQuery based on their run date end.
        if ($request->end_date != '') {
            $end = Carbon::parse($request->end_date)->format('Y-m-d');
            $recurrenceQuery->whereDate('run_date','<=', $end);;
        }
        // Search for a recurrenceQuery based on their status.
        if ($request->status != '') {
            $recurrenceQuery->where('status', $request->status);
        }
        
        $recurrences = $recurrenceQuery->latest()->get();
        return view('recurrence.index',compact('recurrences','departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recurrence = Recurrence::with('department','company','staff','manager','job_type.workflows')->find($id);
        return view('recurrence.show', compact('recurrence'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recurrence = Recurrence::with('department','company','staff','manager','job_type')->find($id);
        $users       = User::where('status',1)->get();
        return view('recurrence.edit', compact('recurrence','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            'runDate'   =>  'required|date|after:today',
            'managerId' =>  'required',
            'staffId'   =>  'required',
        ]);
        // if validation fail
        if ($validator->fails()) {
            Session::flash('notification','Validation error.Please try again. ');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $recurrence = Recurrence::find($id);
        $recurrence->manager_id  = $request->managerId;
        $recurrence->staff_id    = $request->staffId;
        $recurrence->run_date   = $request->runDate;
        if ($recurrence->save()) {
            Session::flash('notification','Recurrence updated successfully. ');
            Session::flash('notificationType','success');
            return redirect()->back();
        }else{
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recurrence = Recurrence::find($id);
        if ($recurrence->delete()) {
            Session::flash('notification','Recurrence deleted successfully. ');
            Session::flash('notificationType','success');
            return redirect()->route('recurrence.index');
        } else {
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->back();
        }
    }
}
