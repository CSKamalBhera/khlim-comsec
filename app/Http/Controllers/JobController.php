<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//helper
use App\Helpers\Helper;
//Activity
use Spatie\Activitylog\Models\Activity;
//models
use App\Models\Jobs\Job;
use App\Models\Users\User;
use App\Models\Jobs\JobType;
use App\Models\Company\Company;
use App\Models\Users\Department;
use App\Models\Jobs\JobWorkflow;
use App\Models\Jobs\Approval;
#event
use App\Events\NewJobCreatedEvent;
use App\Events\WorkflowNotUpdated;
use App\Events\WorkflowUpdates;
#notification
use App\Notifications\ManagerNotification;
use App\Notifications\StaffNotification;
#classes
use Carbon\Carbon;
use Validator,Session;
class JobController extends Controller
{
    function __construct() {
        $this->middleware('permission:jobs_access|job_create|job_edit|job_delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:jobs_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:jobs_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:jobs_delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $departmentId = auth()->user()->departments->pluck('id')->toArray();
        $jobs = Job::with('department','company','jobWorkflow.jobTypeWorkflow','staff','manager','job_type')->whereIn('department_id',$departmentId)->orderBy('created_at', 'DESC')->get();
        return view('jobs.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies   = Company::where('status',1)->get();
        $departments = Department::where('status',1)->get();
        $jobTypes    = JobType::where('status',1)->get();
        $users       = User::where('status',1)->get();
        return view('jobs.create',compact('companies','departments','jobTypes','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'companyId'    => 'required',
            'staffId'      => 'required',
            'job_type_id'  => 'required',
            'managerId'    => 'required',
            'departmentId' => 'required',
        ],[
            'companyId.required'    => "The Company field is required.",
            'staffId.required'      => "The Staff field is required.",
            'job_type_id.required'  => "The Job type field is required.",
            'job_type_id.unique'  => "The job type has already been taken.",
            'managerId.required'    => "The Manager field is required.",
            'departmentId.required' => "The Department field is required.",
        ]);
        if ($validator->fails()) {
            Session::flash('notification','Validation error.Please try again. ');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $job = new Job();
        $job->company_id    = $request->companyId;
        $job->staff_id      = $request->staffId;
        $job->job_type_id   = $request->job_type_id;
        $job->manager_id    = $request->managerId;
        $job->department_id = $request->departmentId;
        $job->created_by    = auth()->user()->id;
        $job->status        = 0;
        $job->save();
        // Activity logs
        $causer   = auth()->user();
        $log_name = 'creation';
        $operation = 'job_create';
        $description = 'job';
        $custom_properties = ['application' => config('app.name'),
                            'operation' => $operation,
                            'causer_name' => $causer->name,
                            'new_value' => 'hashed',
                            'old_value' => 'hashed',
                            ];
        Helper::store_activity_log($causer, $job, $custom_properties, $log_name, $description);
        // notification & mail
        event(New NewJobCreatedEvent($job));

        if($job){
            Session::flash('notification', 'Job created successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('jobs.index');
        } else {
            Session::flash('notification', 'Job not created successfully!.. ');
            Session::flash('notificationType', 'error');
            return redirect()->route('jobs.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model_name  = 'App\Models\Jobs\Job';
        $activities  = Activity::where('subject_type', $model_name)->where('description', 'JobWorkflow')->where('subject_id', $id)->orderBy('created_at', 'desc')->get();
        $job = Job::with('department','approval','company','staff','manager','jobWorkflow','job_type.workflows.workflow_type.workflowoption','job_type.workflows.workflowNotify')->find($id);
        $approval = Approval::where('job_id',$job->id)->latest()->first();
        $users    = User::where('status',1)->get();
        //get fifinancial year
        $company = $job->company;
        $endfinancialYear = Carbon::now()->endOfYear()->format('Y');
        $calDate  = $company->annual_return_date.'-'.$endfinancialYear;
        $lastDate = Carbon::parse($calDate)->format('Y-m-d');
        $annualDate = date('Y-m-d', strtotime( ' - 3 months',strtotime($lastDate)));
        // auditer financial statment
        $startLastFinancialYearDate = date("Y-01-01", strtotime("0 year")); // get next year date from here
        $endLastFinancialYearDate = date("Y-12-t", strtotime($startLastFinancialYearDate)); // get financial year date from here
        $reminderDate = date('Y-m-d', strtotime( ' + 5 months',strtotime($endLastFinancialYearDate)));
            
        return view('jobs.show', compact('job','users','activities','approval','reminderDate','annualDate'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::with('jobWorkflow')->find($id);
        $model_name  = 'App\Models\Jobs\Job';
        $activities  = Activity::where('subject_type', $model_name)->where('subject_id', $id)->where('description', 'job')->orderBy('created_at', 'desc')->get();
        $companies   = Company::where('status',1)->get();
        $departments = Department::where('status',1)->get();
        $jobTypes    = JobType::where('status',1)->get();
        $users       = User::where('status',1)->get();
        return view('jobs.edit',compact('job','companies','departments','jobTypes','users','activities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'companyId'    => 'required',
            'staffId'      => 'required',
            'job_type_id'  => 'required',
            'managerId'    => 'required',
            'departmentId' => 'required',
        ],[
            'companyId.required'    => "The Company field is required.",
            'staffId.required'      => "The Staff field is required.",
            'job_type_id.required'  => "The Job type field is required.",
            'job_type_id.unique'    => "The job type has already been taken.",
            'managerId.required'    => "The Manager field is required.",
            'departmentId.required' => "The Department field is required.",
        ]);
        if ($validator->fails()) {
            Session::flash('notification','Validation error.Please try again. ');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $job = Job::find($id);
        // if(!$job->jobWorkflow->isEmpty()){
        //     Session::flash('notification', 'This Job has attached to JobWorkFlows and cannot be Edited.');
        //     Session::flash('notificationType', 'error');
        //     return redirect()->back();
        // }
        $job->company_id    = $request->companyId;
        $job->staff_id      = $request->staffId;
        $job->job_type_id   = $request->job_type_id;
        $job->manager_id    = $request->managerId;
        $job->department_id = $request->departmentId;
        $job->save();
        // Activity logs
        $causer   = auth()->user();
        $log_name = 'modification';
        $operation = 'job_modification';
        $description = 'job';
        $custom_properties = ['application' => config('app.name'),
                            'operation' => $operation,
                            'causer_name' => $causer->name,
                            'new_value' => 'hashed',
                            'old_value' => 'hashed',
                            ];
        Helper::store_activity_log($causer, $job, $custom_properties, $log_name, $description);
        if($job){
            Session::flash('notification', 'Job updated successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('jobs.index');
        } else {
            Session::flash('notification', 'Job not updated successfully!.. ');
            Session::flash('notificationType', 'error');
            return redirect()->route('jobs.create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = Job::with('recurrences','jobWorkflow','company_invoices','job_type')->find($id);
        if (!$job->job_type->isEmpty() || !$job->recurrences->isEmpty() || !$job->jobWorkflow->isEmpty() || !$job->company_invoices->isEmpty()) {
            Session::flash('notification','This Job has attached and can not be deleted.');
            Session::flash('notificationType','error');
            return redirect()->back();
        }else{
            if ($job->delete()) {
                Session::flash('notification','Job deleted successfully!.. ');
                Session::flash('notificationType','success');
                return redirect()->back();
            } else {
                Session::flash('notification','Something went wrong, Please try again later!');
                Session::flash('notificationType','error');
                return redirect()->back();
            }
        }
    }

    /**
     * Update Job Workflow the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function departmentJobType(Request $request)
    {
        $search = $request->department;
		if($search == ''){
            $jobTypes = JobType::get();
        }else{
            $jobTypes = JobType::where('department_id',$request->department)->get();
		}
		$response = array();
        foreach($jobTypes as $jobType){
            echo '<option value="'.$jobType['id'].'">'.$jobType['name'].'</option>'; 
        }
        
    }
    
    /**
     * Update Job Workflow the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function departmentStaff(Request $request)
    {
        $departmentId = $request->department;
		if($departmentId == ''){
            $users = User::where('status',1)->get();
        }else{
            $users = User::with('departments','roles')
                ->whereHas('departments', function($query) use ($departmentId){
                    $query->where('id',$departmentId);
                })->get();
		}
		$response = array();
        foreach($users as $user){
            echo '<option value="'.$user['id'].'">'.$user['name'].'</option>'; 
        }
        
    }
    
    /**
     * Update Job Workflow the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function departmentManager(Request $request)
    {
        $search = $request->department;
		if($search == ''){
            $users = User::where('status',1)->get();
        }else{
            $users = User::with('departments','roles')
                ->whereHas('departments', function($query) use ($request){
                    $query->where('id',$request->department);
                })->get();
		}
		$response = array();
        foreach($users as $user){
            echo '<option value="'.$user['id'].'">'.$user['name'].'</option>'; 
        }
    }
    /**
     * 
     * @param type $id
     * @return type route
     *active inactive function
    */
    public function status($id = null) {
        $Job = Job::find($id);
        if ($Job->status == 0) {
            Job::find($id)->update(['status' => 1]);
            Session::flash('notification','Job status updated successfully. ');
            Session::flash('notificationType','success');
            return redirect()->back();
        } else {
            Job::find($id)->update(['status' => 0]);
            Session::flash('notification','Job status updated successfully. ');
            Session::flash('notificationType','success');
            return redirect()->back();
        }
        Session::flash('notification','Something went wrong, Please try again later!');
        Session::flash('notificationType','error');
        return redirect()->back();
    }


    public function CompleteJob(){
        $departmentId = auth()->user()->departments->pluck('id')->toArray();
        $jobs = Job::with('department','company','jobWorkflow.jobTypeWorkflow','staff','manager','job_type')->whereIn('department_id',$departmentId)->where('status',1)->orderBy('created_at', 'DESC')->get();
        return view('jobs.index', compact('jobs'));
    }
    public function AssignedJob(){
        $departmentId = auth()->user()->departments->pluck('id')->toArray();
        $jobs = Job::with('department','company','jobWorkflow.jobTypeWorkflow','staff','manager','job_type')->where('assigned_id','!=',null)->where('status',0)->whereIn('department_id',$departmentId)->orderBy('created_at', 'DESC')->get();
        
        return view('jobs.index', compact('jobs'));
    }
    
}
