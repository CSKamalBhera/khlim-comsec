<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,Session;
class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($filter = '')
    {
        switch ($filter) {
            case 'unread':
                $title= __('Unread notifications');
                    return view('notifications.index', ['filterread' => 'unread', 'title' => $title]);
                break;
            case 'read':
                $title= __('Read notifications');
                    return view('notifications.index', ['filterread' => 'read', 'title' => $title]);
                break;
            case 'all':
                $title= __('All notifications');
                    return view('notifications.index', ['filterread' => 'all', 'title' => $title]);
                break;
            default :
            $title= __('All notifications');
                return view('notifications.index', ['filterread' => 'all', 'title' => $title]);
            break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function delete($id){
        $notification = Auth::user()->notifications()->findOrFail($id);
        $notification->delete();
        Session::flash('notification', 'Notification deleted successfully!.. ');
        Session::flash('notificationType', 'success');
        return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mark_all_as_read(){
        Auth::user()->unreadNotifications->markAsRead();
        Session::flash('notification', 'Notification mark all as read successfully!.. ');
        Session::flash('notificationType', 'success');
        return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mark_as_read($id){

        $notification = Auth::user()->notifications()->findOrFail($id);
        $notification->markAsRead();
        Session::flash('notification', 'Notification mark as read successfully!.. ');
        Session::flash('notificationType', 'success');
        return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mark_as_unread($id){
        $notification = Auth::user()->notifications()->findOrFail($id);
        $notification->markAsUnread();
        Session::flash('notification', 'Notification mark as unread successfully');
        Session::flash('notificationType', 'success');
        return redirect()->back();
    }
}
