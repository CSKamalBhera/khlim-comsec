<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#models
use App\Models\Users\User;
use App\Models\Users\Department;
use Spatie\Permission\Models\Role;
use App\Models\Jobs\Job;
use Carbon\Carbon;
use Validator, Session, Hash;
use Illuminate\Support\Facades\Mail;
#Events
use App\Events\NewUserRegistered;
#Notification
use App\Notifications\UserRegisteredSuccessfully;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user_access|user_create|user_edit|user_delete', ['only' => ['index','show']]);
        $this->middleware('permission:user_create', ['only' => ['create','store']]);
        $this->middleware('permission:user_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user_delete', ['only' => ['destroy']]);
        $this->middleware('permission:user_status', ['only' => ['status']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $departments = Department::get();
        $roles = Role::get();
        //staff filter
        if (isset($request->department) && isset($request->role)) {
            $users = User::with('departments','roles')
            ->whereHas('departments', function($query) use ($request){
                    $query->where('id',$request->department);
                })
            ->whereHas('roles', function($query) use ($request){
                    $query->where('id',$request->role);
                })->get();
        } elseif(isset($request->department)){
            $users = User::with('departments','roles')
                ->whereHas('departments', function($query) use ($request){
                    $query->where('id',$request->department);
                })->get();
        }elseif(isset($request->role)){ 
            $users = User::with('departments','roles')->whereHas('roles', function($query) use ($request){
                    $query->where('id',$request->role);
                })->get();
        }else {
            $users = User::with('departments','roles')->orderBy('created_at', 'DESC')->get();
        }
        return view('users.index',compact('users','roles','departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('status', 1)->orderBy('id','DESC')->get();
        $departments = Department::where('status', 1)->get();
        return view('users.create',compact('roles','departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check validations
        $validator = Validator::make($request->all(), [
            'name'            => 'required|max:255',
            'email'           => 'required|email:rfc,dns|unique:users,email,NULL,id,deleted_at,NULL',
            'username'        => 'required|regex:/^[a-z0-9\@]+$/i|max:50|unique:users,username,NULL,id,deleted_at,NULL',
            'password'        => 'required|min:6|max:50|confirmed',
            'password_confirmation'=>'required',
            'phone_number'    => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:30',
            'ic_number'       => 'required|regex:/^[0-9]{6}-[0-9]{2}-[0-9]{4}$/',
            'role'            => 'required',
            'department'      => 'required',
            'emercency_contact'=> 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:30',
        ]);
        if ($validator->fails()) {
            Session::flash('notification','Validation error. Please try again.');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $user = new User();
        $user->name         = $request->name;
        $user->username     = $request->username;
        $user->email        = $request->email;
        $user->phone_no     = $request->phone_number;
        $user->emergency_no = $request->emercency_contact;
        $user->ic_number    = $request->ic_number;
        $user->password     = Hash::make($request->password);
        $user->can_login    = 1;
        $user->status       = 1;
        $user->activation_code = 'ByAdmin';
        $user->save();
        // Attach all selected deparment
        foreach ($request->department as $deparment_id) {
            $department = Department::find($deparment_id);
            $user->departments()->attach($department);
        }
        // assign Role to User
        $user->assignRole($request->role);
        //Email & Notifications
        $userPassword = $request->password;
        //$user->notify(new UserRegisteredSuccessfully($user,$userPassword)); // notifications when registered
        event(New NewUserRegistered($user,$userPassword)); // notifications when registered
        if ($user) {
            Session::flash('notification','Staff created successfully. ');
            Session::flash('notificationType','success');
            return redirect()->route('users.index');
        } else {
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->route('users.index');
        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        
        $user = User::with('roles','departments','jobs')->find($id);
        $departments = Department::where('status', 1)->get();
        $jobQuery = Job::query();
        // Search for a user based on their name.
        if (isset($request->department) || isset($request->start_date) || isset($request->end_date)) {
            $jobQuery->where(function($query) use ($user,$request){
                $query->where('staff_id', $user->id);
                if(isset($request->department)){
                    $query->where('department_id' ,'=' ,$request->department);
                }
                if(isset($request->start_date)){
                    $start = Carbon::parse($request->start_date)->format('Y-m-d');
                    $query->whereDate('created_at','>=', $start);
                }
                if(isset($request->end_date)){
                    $end = Carbon::parse($request->end_date)->format('Y-m-d');
                    $query->whereDate('created_at','<=',$end);
                }
                if (isset($request->status)) {
                    $query->where('status','=',$request->status);
                }
            })->orWhere(function($query) use ($user,$request) {
                $query->where('manager_id', $user->id);
                if(isset($request->department)){
                    $query->where('department_id' ,'=' ,$request->department);
                }
                if(isset($request->start_date)){
                    $start = Carbon::parse($request->start_date)->format('Y-m-d');
                    $query->whereDate('created_at','>=', $start);
                }
                if(isset($request->end_date)){
                    $end = Carbon::parse($request->end_date)->format('Y-m-d');
                    $query->whereDate('created_at','<=',$end);
                }
                if (isset($request->status)) {
                    $query->where('status','=',$request->status);
                }
            });
        }else{
            $jobQuery->where(function($query) use ($user,$request){
                $query->where('staff_id', $user->id);
            })->orWhere(function($query) use ($user,$request) {
                $query->where('manager_id', $user->id);
            });
        }
        $jobs = $jobQuery->get();
        return view('users.show',compact('user','jobs','departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('departments')->find($id);
        $roles = Role::where('status', 1)->orderBy('id','DESC')->get();
        $departments = Department::where('status', 1)->get();
        return view('users.edit',compact('roles','departments','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //check validations
        $user = User::find($id);
        $validator = Validator::make($request->all(), [
            'name'            => 'required|max:255',
            'email'           => 'required|email:rfc,dns|unique:users,email,'.$user->id.',id,deleted_at,NULL',
            'username'        => 'required|regex:/^[a-z0-9\@]+$/i|max:50|unique:users,username,'.$user->id.',id,deleted_at,NULL',
            'phone_number'    => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:30',
            'ic_number'       => 'required|regex:/^[0-9]{6}-[0-9]{2}-[0-9]{4}$/',
            'role'            => 'required',
            'department'      => 'required',
            'emercency_contact'=> 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:30',
            'password'        => 'nullable|min:6|max:50|confirmed',
            ]);
        if ($validator->fails()) {
            Session::flash('notification','Validation error. Please try again.');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $user->name         = $request->name;
        $user->username     = $request->username;
        $user->email        = $request->email;
        $user->phone_no     = $request->phone_number;
        $user->emergency_no = $request->emercency_contact;
        $user->ic_number    = $request->ic_number;
        if($request->password != null){
            $user->password     = Hash::make($request->password);
        }
        $user->save();
        // Attach all selected deparment
        $user->departments()->detach();
        foreach ($request->department as $deparment_id) {
            $department = Department::find($deparment_id);
            $user->departments()->attach($department);
        }
        // assign Role to User
        foreach ($user->roles->pluck('id') as $role) {}
        if (!empty($request->role)) {
            if ($role != $request->role) {
                $user->removeRole($role);
                $user->assignRole($request->role);
            }
        }
        //$user->assignRole($request->role);
        if ($user) {
            Session::flash('notification','Staff updated successfully. ');
            Session::flash('notificationType','success');
            return redirect()->route('users.index');
        } else {
            Session::flash('notification','Something went wrong, Please try again later!');
            Session::flash('notificationType','error');
            return redirect()->route('users.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::with('jobs','jobsManager','jobAssignApproval','companyPortfolio','recurrenceStaff','recurrenceManager')->find($id);
        if (!$user->jobs->isEmpty() || !$user->jobsManager->isEmpty() || !$user->jobAssignApproval->isEmpty() || !$user->companyPortfolio->isEmpty() || !$user->recurrenceStaff->isEmpty() || !$user->recurrenceManager->isEmpty()) {
            Session::flash('notification','This user has attached and can not be deleted.');
            Session::flash('notificationType','error');
            return redirect()->back();
        }else{
            //role delete
            foreach ($user->roles->pluck('id') as $role) {}
            $user->removeRole($role);
            //department delete
            $user->departments()->detach();
            if ($user->delete()) {
                Session::flash('notification','Staff deleted successfully. ');
                Session::flash('notificationType','success');
                return redirect()->back();
            } else {
                Session::flash('notification','Something went wrong, Please try again later!');
                Session::flash('notificationType','error');
                return redirect()->back();
            }
        }
       
    }
    /**
     * 
     * @param type $id
     * @return type route
     *active inactive function
    */
    public function status($id = null) {
        $user = User::find($id);
        if ($user->status == 0) {
            User::find($id)->update(['status' => 1]);
            Session::flash('notification','User activated successfully. ');
            Session::flash('notificationType','success');
            return redirect()->back();
        } else {
            User::find($id)->update(['status' => 0]);
            Session::flash('notification','User deactivated successfully. ');
            Session::flash('notificationType','success');
            return redirect()->back();
        }
        Session::flash('notification','Something went wrong, Please try again later!');
        Session::flash('notificationType','error');
        return redirect()->route('users.index');
    }
}
