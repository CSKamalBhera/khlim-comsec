<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#models
use App\Models\Setting;
use App\Models\SettingSystem;
use App\Models\Users\Department;
#classes
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Validator,Session;
class SettingController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:settings_access|settings_create|settings_edit|settings_delete', ['only' => ['index','show']]);
        $this->middleware('permission:settings_create', ['only' => ['create','store']]);
        $this->middleware('permission:settings_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:settings_delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('settings.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::with('settings')->where('status',1)->get();
        $settingSystem = SettingSystem::first();
        $settings  = Setting::get();
        $existDepartment = Setting::pluck('department_id')->toArray();
        return view('settings.index', compact('departments','settingSystem','settings','existDepartment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $validator = Validator::make($request->all(), [
            'name'                      => 'required|regex:/^[a-z\s\-]+$/i|max:50',
            'logo'                      => 'image|max:2048', 
            'termsCondition'            => 'required|string|max:50',
            'settings.department_id.*'  => 'required',
            'settings.companyName.*'    => 'required|regex:/^[a-z\s\-\.\&]+$/i|max:50',
            'settings.tax_no.*'         => 'required|alpha_num|min:10|max:50',
            'settings.companyPhone.*'   => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:14',
            'settings.companyEmail.*'   => 'required|email:rfc,dns',
            'settings.companyAddress.*' => 'required|max:100',
            'settings.companyComment.*' => 'required|max:100',
            'settings.companyHeader.*'  => 'required|max:400',
            'settings.companyFooter.*'  => 'required|max:400',
        ],[
            'settings.department_id.*.required' => "The Department field is required.",          
            'settings.companyName.*.required'   => "The Company Name is required.",
            'settings.companyName.*.regex'      => "The Company Name format is invailid.",
            'settings.companyName.*.max'       => "The Company Name not be greater than 50.",
            'settings.tax_no.*.required'        => "The Tax Number field is required.",
            'settings.tax_no.*.min'             => "The Tax Number must be nine-digit.",
            'settings.tax_no.*.max'             => "The Tax Number not be greater than 25.",
            'settings.tax_no.*.alpha_num'       => "The Tax Number field is not valid.",
            'settings.companyPhone.*.required'  => "The Company Phone field is required.",
            'settings.companyPhone.*.regex'     => "The Company Phone field is not valid.",
            'settings.companyPhone.*.min'       => "The Company Phone field must be 10 digit.",
            'settings.companyPhone.*.max'       => "The Company Phone field not be greater than 14.",
            'settings.companyEmail.*.required'  => "The Company Email field is required.",
            'settings.companyEmail.*.email'     => "The Company Email field is not valid.",
            'settings.companyAddress.*.required'=> "The Company Address is required.",
            'settings.companyAddress.*.max'     => "The Company Address must not be greater than 100 characters.",
            'settings.companyComment.*.required'=> "The Company Comment field is required.",        
            'settings.companyComment.*.max'     => "The Company Comment must not be greater than 100 characters.",        
            'settings.companyHeader.*.required' => "The Company Header field is required.",
            'settings.companyHeader.*.max'      => "The Company Header must not be greater than 400 characters.",
            'settings.companyFooter.*.required' => "The Company Footer field is required.",
            'settings.companyFooter.*.max'      => "The Company Footer must not be greater than 400 characters.",
        ]);
        if ($validator->fails()) { 
            Session::flash('notification','Validation error.Please try again. ');
            Session::flash('notificationType','error');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $settingSystem = SettingSystem::first();
        if ($settingSystem == null) {
            $settingSystem = new SettingSystem();
        }
        $settingSystem->name        = $request->name;
        $settingSystem->description = $request->termsCondition;
        if ($request->hasFile('logo')){
            $file       =  $request->file('logo');
            $imagename  =  $file->getClientOriginalName();
            $image = Image::make($file);
            // Crop and resize image to fixed size
             $image->resize(128, 128, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
             });

            // Orientation fix with intervention
            $image->orientate()->exif();
            $destinationPath = public_path('assets/img/').strtolower($file->getClientOriginalName());
            //save image
            $image->save($destinationPath);
            //store path in database
            $settingSystem->logo = 'assets/img/'.strtolower($file->getClientOriginalName());
        }
        $settingSystem->save();
        if (isset($request->settings)) {
            if(isset($request->settings['id'])){
                Setting::whereNotIn('id',$request->settings['id'])->delete();
            }
            
            foreach ($request->settings['department_id'] as $key => $value) {
                if (isset($request->settings['id'][$key])) {
                    $id = $request->settings['id'][$key];
                    $setting  =  Setting::with('department')->find($id);
                    }else{
                    $settings  =  Setting::with('department')->get();
                    $existDepartment = Setting::pluck('department_id')->toArray();
                    if(!in_array($value,$existDepartment)){
                          $setting  = new Setting();  
                        }else{
                            Session::flash('notification','This department settings already there.Please select another department setting.');
                            Session::flash('notificationType','error');
                            return redirect()->back(); 
                        }
                    }
                    $setting->department_id  = $value;
                    $setting->name           = $request->settings['companyName'][$key];
                    $setting->tax_no         = $request->settings['tax_no'][$key];
                    $setting->email          = $request->settings['companyEmail'][$key];
                    $setting->phone_no       = $request->settings['companyPhone'][$key];
                    $setting->address        = $request->settings['companyAddress'][$key];
                    $setting->comment        = $request->settings['companyComment'][$key];
                    $setting->invoice_header = $request->settings['companyHeader'][$key];
                    $setting->invoice_footer = $request->settings['companyFooter'][$key];
                    $setting->save();
                }
            }
       
        Session::flash('notification','Application setting successfully added');
        Session::flash('notificationType','success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    public function deleteSetting(Request $request)
    {
         $setting  =  Setting::with('department')->find($request->id);
         if($setting->department->company_invoices->isEmpty()){
            if ($setting->delete()){
            Session::flash('notification','setting deleted successfully. ');
            Session::flash('notificationType','success');
            return response()->json(['message' => 'setting deleted successfully', 'notificationType' => 'success', 'status' => 200,'success'=>true]);
            }
         }else{
            Session::flash('notification','Setting cannot be deleted, this Setting is attached to (Billing/Payment) available in system!.');
            Session::flash('notificationType','error');
            return response()->json(['success'=>false]);
         }
    }
    
    
}
