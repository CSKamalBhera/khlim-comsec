<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jobs\JobType;
use App\Models\Users\Department;
use App\Models\Jobs\WorkflowType;
use App\Models\Jobs\WorkflowOption;
use App\Models\Jobs\JobTypeWorkflow;
use App\Models\Jobs\WorkflowNotify;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Session;
# request JobTypeStoreRequest
use App\Http\Requests\JobTypeUpdateRequest;
use App\Http\Requests\JobTypeStoreRequest;

class JobTypeController extends Controller {

    function __construct() {
        $this->middleware('permission:jobtype_access|jobtype_create|jobtype_edit|jobtype_delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:jobtype_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:jobtype_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:jobtype_delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $departmentId = auth()->user()->departments->pluck('id')->toArray();
        $jobtypes = JobType::whereIn('department_id', $departmentId)->orderBy('created_at', 'DESC')->get();
        return view('jobtypes.index', compact('jobtypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $departments = Department::where('status', 1)->get();
        $types = WorkflowType::where('status', 1)->get();
        $options = WorkflowOption::get();
        return view('jobtypes.create', compact('departments', 'types', 'options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobTypeStoreRequest $request) {

        $jobType = new JobType();
        $jobType->name = $request->name;
        $jobType->department_id = $request->department_id;
        $jobType->status = 1;
        if ($jobType->save()) {
            foreach ($request->flow as $key => $status) {
                $jobTypeWorkflow = new JobTypeWorkflow();
                $jobTypeWorkflow->job_type_id = $jobType->id;
                $jobTypeWorkflow->workflow_type_id = $status['type'];
                $jobTypeWorkflow->title = $status['title'];
                $jobTypeWorkflow->save();
                // check workflow notify status
                if ($status['type'] == 3 || $status['type'] == 7 || $status['type'] == 18) {

                    $workflowNotify = new WorkflowNotify();
                    $workflowNotify->job_type_id = $jobType->id;
                    $workflowNotify->job_type_workflow_id = $jobTypeWorkflow->id;
                    $workflowNotify->workflow_type_id = $status['type'];
                    

                    if (isset($status['notify_Hours'])) {
                        $notify_hour['hours'] = $status['notify_Hours'];
                        $workflowNotify->notification_value = json_encode($notify_hour, true);
                    } elseif (isset($status['notify_Days'])) {
                        $notify_day['days'] = $status['notify_Days'];
                        $workflowNotify->notification_value = json_encode($notify_day, true);
                    }



                    if (isset($status['notifyDone'])) {
                        $workflowNotify->done_notify_day = isset($status['notifyDone']) ? $status['notifyDone'] : null;
                    } elseif (isset($status['payment'])) {
                        $workflowNotify->done_notify_day = isset($status['payment']) ? $status['payment'] : null;
                    } elseif (isset($status['adhocConsolidation'])) {
                        $workflowNotify->done_notify_day = isset($status['adhocConsolidation']) ? $status['adhocConsolidation'] : null;
                    }
                    if (isset($status['notifyNotDone'])) {
                        $workflowNotify->not_done_notify_day = isset($status['notifyNotDone']) ? $status['notifyNotDone'] : null;
                    } elseif (isset($status['adhocMBRS'])) {
                        $workflowNotify->done_notify_day = isset($status['adhocMBRS']) ? $status['adhocMBRS'] : null;
                    }
                    $workflowNotify->notify_recurrence_day = isset($status['recurrence']) ? $status['recurrence'] : 0;
                    $workflowNotify->active = isset($status['company_status_Active']) ? $status['company_status_Active'] : null;
                    $workflowNotify->semi_active = isset($status['company_status_SemiActive']) ? $status['company_status_SemiActive'] : null;
                    $workflowNotify->dormant = isset($status['company_status_Dormant']) ? $status['company_status_Dormant'] : null;
                    $workflowNotify->super_active = isset($status['company_status_SuperActive']) ? $status['company_status_SuperActive'] : null;
                    $workflowNotify->notify_account_department = isset($status['notify_account']) ? $status['notify_account'] : 0;
                    $workflowNotify->save();
                }
            }
            Session::flash('notification', 'JobType created successfully!.. ');
            Session::flash('notificationType', 'success');
            return redirect()->route('jobtype.index');
        } else {
            Session::flash('notification', 'JobType not created successfully!.. ');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $jobtype = JobType::with('workflows.workflow_type.workflowoption', 'workflows.workflowNotify')->find($id);
        $jobtypes = Jobtype::where('department_id', $jobtype->department_id)->get();
        return view('jobtypes.show', compact('jobtype', 'jobtypes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $jobtype = JobType::with('workflows.workflow_type.workflowoption', 'workflows.workflowNotify')->find($id);
        $jobtypes = Jobtype::where('department_id', $jobtype->department_id)->get();
        $departments = Department::where('status', 1)->get();
        $types = WorkflowType::where('department_id', $jobtype->department_id)->where('status', 1)->get();
        $options = WorkflowOption::get();
        return view('jobtypes.edit', compact('jobtype', 'departments', 'types', 'options', 'jobtypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\JobTypeUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobTypeUpdateRequest $request, $id) {

        $jobtype = JobType::with('workflowtype', 'jobs')->find($id);
        if (!$jobtype->jobs->isEmpty()) {
            Session::flash('notification', 'This job type is associated with a job and cannot be edited.');
            Session::flash('notificationType', 'info');
            return redirect()->back();
        }
        $jobtype->name = $request->input('name');
        $jobtype->department_id = $request->department_id;
        if ($jobtype->update()) {

            JobTypeWorkflow::where('job_type_id', $jobtype->id)->whereNotIn('id', $request->workflow_id)->delete();
            if ($request->workflowNotify_id != null) {
                WorkflowNotify::where('job_type_id', $jobtype->id)->whereNotIn('job_type_workflow_id', $request->workflow_id)->whereNotIn('id', $request->workflowNotify_id)->delete();
            }

            foreach ($request->flow as $key => $status) {

                $typeId = $status['type'];
                if (isset($request->workflow_id[$key])) {
                    $JobTypeWorkflows = JobTypeWorkflow::find($request->workflow_id[$key]);
                    // when change workflowtype with workflownotify delete previous workflownotify
                    if (isset($JobTypeWorkflows->workflowNotify) && $JobTypeWorkflows->workflowNotify->workflow_type_id != $typeId && !in_array($typeId, [3, 7, 12, 14])) {
                        WorkflowNotify::where('job_type_id', $jobtype->id)->where('job_type_workflow_id', $JobTypeWorkflows->workflowNotify->job_type_workflow_id)->delete();
                        $JobTypeWorkflows = JobTypeWorkflow::find($request->workflow_id[$key]);
                    }
                } else {
                    $JobTypeWorkflows = new JobTypeWorkflow();
                }
                $JobTypeWorkflows->job_type_id = $jobtype->id;
                $JobTypeWorkflows->workflow_type_id = $typeId;
                $JobTypeWorkflows->title = $status['title'];
                $JobTypeWorkflows->save();
                // check workflow status notify
                if ($status['type'] == 3 || $status['type'] == 7 || $status['type'] == 18) {
                    if (isset($JobTypeWorkflows->workflowNotify)) {
                        $workflowNotify = WorkflowNotify::find($JobTypeWorkflows->workflowNotify->id);
                        if (!isset($status['notify_Hours'])) {

                            $workflowNotify->notification_value = null;
                        } elseif (!isset($status['notify_Days'])) {

                            $workflowNotify->notification_value = null;
                        }
                        if (!isset($status['notifyDone'])) {
                            $workflowNotify->done_notify_day = isset($status['notifyDone']) ? $status['notifyDone'] : null;
                        } elseif (!isset($status['payment'])) {
                            $workflowNotify->done_notify_day = isset($status['payment']) ? $status['payment'] : null;
                        } elseif (!isset($status['adhocConsolidati on'])) {
                            $workflowNotify->done_notify_day = isset($status['adhocConsolidation']) ? $status['adhocConsolidation'] : null;
                        }
                        if (!isset($status['notifyNotDone'])) {
                            $workflowNotify->not_done_notify_day = isset($status['notifyNotDone']) ? $status['notifyNotDone'] : null;
                        } elseif (!isset($status['adhocMBRS'])) {
                            $workflowNotify->not_done_notify_day = isset($status['adhocMBRS']) ? $status['adhocMBRS'] : null;
                        }
                    } else {
                        $workflowNotify = new WorkflowNotify();
                    }
                    $workflowNotify->job_type_id = $jobtype->id;
                    $workflowNotify->job_type_workflow_id = $JobTypeWorkflows->id;
                    $workflowNotify->workflow_type_id = $typeId;
                    # seceterial workflow options notification reminder
                    if (isset($status['notify_Hours'])) {
                        $notify_hour['hours'] = $status['notify_Hours'];
                        $workflowNotify->notification_value = json_encode($notify_hour, true);
                    } elseif (isset($status['notify_Days'])) {
                        $notify_day['days'] = $status['notify_Days'];
                        $workflowNotify->notification_value = json_encode($notify_day, true);
                    }
                    
                    # accounts department workflow options notification reminder
                    if (isset($status['notifyDone'])) {
                        $workflowNotify->done_notify_day = isset($status['notifyDone']) ? $status['notifyDone'] : null;
                    } elseif (isset($status['payment'])) {
                        $workflowNotify->done_notify_day = isset($status['payment']) ? $status['payment'] : null;
                    } elseif (isset($status['adhocConsolidation'])) {
                        $workflowNotify->done_notify_day = isset($status['adhocConsolidation']) ? $status['adhocConsolidation'] : null;
                    }
                    if (isset($status['notifyNotDone'])) {
                        $workflowNotify->not_done_notify_day = isset($status['notifyNotDone']) ? $status['notifyNotDone'] : null;
                    } elseif (isset($status['adhocMBRS'])) {
                        $workflowNotify->not_done_notify_day = isset($status['adhocMBRS']) ? $status['adhocMBRS'] : null;
                    }
                    $workflowNotify->notify_recurrence_day = isset($status['recurrence']) ? $status['recurrence'] : 0;
                    $workflowNotify->active = isset($status['company_status_Active']) ? $status['company_status_Active'] : null;
                    $workflowNotify->semi_active = isset($status['company_status_SemiActive']) ? $status['company_status_SemiActive'] : null;
                    $workflowNotify->dormant = isset($status['company_status_Dormant']) ? $status['company_status_Dormant'] : null;
                    $workflowNotify->super_active = isset($status['company_status_SuperActive']) ? $status['company_status_SuperActive'] : null;
                    $workflowNotify->notify_account_department = isset($status['notify_account']) ? $status['notify_account'] : 0;
                    $workflowNotify->save();
                }
            }

            Session::flash('notification', 'JobType updated successfully..');
            Session::flash('notificationType', 'success');
            return redirect()->back();
        } else {
            Session::flash('notification', 'JobType not updated successfully..');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $jobtype = JobType::with('workflowtype', 'jobs', 'recurrences')->find($id);
        if (!$jobtype->jobs->isEmpty() || !$jobtype->recurrences->isEmpty()) {
            Session::flash('notification', 'This Job type has attached and can not be deleted.');
            Session::flash('notificationType', 'error');
            return redirect()->back();
        } else {
            $jobtypeworkflows = JobTypeWorkflow::where('job_type_id', $id)->delete();
            $workflowNotify = WorkflowNotify::where('job_type_id', $id)->delete();
            if ($jobtype->delete()) {
                Session::flash('notification', 'JobType deleted successfully..');
                Session::flash('notificationType', 'success');
                return redirect()->back();
            } else {
                Session::flash('notification', 'Something went wrong, Please try again later !');
                Session::flash('notificationType', 'error');
                return redirect()->back();
            }
        }
    }

    /**
     * get option dependency workflow type the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getOptions(Request $request) {
        $search = $request->type;
        if ($search == '') {
            $options = WorkflowOption::get();
        } else {
            $options = WorkflowOption::where('workflow_types_id', $search)->get();
        }
        $response = array();
        foreach ($options as $option) {
            echo '<option value="' . $option['id'] . '" selected>' . $option['options'] . '</option>';
        }
    }

    /**
     * get workflowNotify dependency on workflow type the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getInputs(Request $request) {
        $workfow_type_id = $request->type;
        $id = $request->id;

        if ($workfow_type_id == 1) {
            $response = array();
        } elseif ($workfow_type_id == 3 || $workfow_type_id == 7 || $workfow_type_id == 18) {
            $span = $workfow_type_id == 3 ? 'Hours' : 'Days';
//            $name = $workfow_type_id == 3 ? 'Hours' : 'Days';
            $type = WorkflowType::find($workfow_type_id);
            $response = array();

            echo '<div class="row add pt-3"><div class="col-md-3 col-xl-3 offset-md-4 offset-xl-4 text-start" style="text-align:right"><label class="ml-2"> Notify,If more than</label></div><div class="col-md-4 col-xl-3 input-group"><input class="form-control notify" id="notify' . $workfow_type_id . '" type="text" name="flow[' . $id . '][notify_' . $span . ']" value="' . ('' ?? old('flow')[$id]['notify_' . $span . '']) . '" placeholder="' . $span . '" ><div class="input-group-append"><span class="input-group-text">' . $span . '</span></div></div></div>';
        }
//        elseif($search == 8 || $search == 11){
//            $options  = array('Dormant', 'Semi Active', 'Active', 'Super Active');
//            $response = array();
//            foreach($options as $option){
//                echo'<div class="row add py-1"><div class="col-md-3 col-xl-3 offset-md-4 offset-xl-4" style="text-align:right"><label>'.$option.'</label></div><div class="col-md-4 col-xl-3 input-group"><input class="form-control" type="text" name="flow['.$id.'][company_status_'.str_replace(' ','',$option).']" value="'.(0??old('flow')[$i]['company_status_'.str_replace(' ','',$option)]).'"><div class="input-group-append"><span class="input-group-text">Days</span></div></div></div>';
//            }
//        }elseif($search == 12){
//            $response = array(); 
//            echo '<div class="row add py-1"><div class="col-md-2 col-xl-2"></div><div class="col-md-5 col-xl-5 text-start"><input type="checkbox" class="check" id="check" onclick="paym(this)" ><label class="ml-2"> Reminder for Payment after</label></div><div class="col-md-4 col-xl-3 input-group"><input class="form-control" type="text" id="payment" name="flow['.$id.'][payment]" value="'.(0??old('flow')[$i]['payment']).'" disabled><div class="input-group-append"><span class="input-group-text">Days</span></div></div><br><div class="col-md-6 col-xl-6 offset-md-2 offset-xl-2 text-start"><input type="checkbox" name="flow['.$id.'][recurrence]" value="1"><label class="ml-2"> Reminder job for recurrence on selected day</label></div><br><div class="col-md-auto col-xl-auto offset-md-2 offset-xl-2  text-start"><input type="checkbox" name="flow['.$id.'][notify_account]" value="1"><label class="ml-2"> Notify Accounts Department</label></div></div>'; 
//        }elseif($search == 10){
//            $options  = array('Consolidation', 'MBRS');
//            $response = array();
//            foreach($options as $option){
//                echo'<div class="row add py-1"><div class="col-md-4 col-xl-4"></div><div class="col-md-3 col-xl-3 text-start"><label>'.$option.'</label></div><div class="col-md-4 col-xl-3 input-group"><input class="form-control" type="text" name="flow['.$id.'][adhoc'.$option.']" value="'.(0??old('flow')[$i]['adhoc'.$option]).'"><div class="input-group-append"><span class="input-group-text">Days</span></div></div></div>';
//            }
//        }
    }

    /**
     * get workflow type dependency on Deparment the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTypes(Request $request) {
        $department_id = $request->department;

        if ($department_id) {
            $types = WorkflowType::where('department_id', $department_id)->get();
        }
        $response = array();
        foreach ($types as $type) {
            echo '<option value="' . $type['id'] . '">' . $type['name'] . '</option>';
        }
    }

    /**
     * get jobtype for particular department for annual return.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getJobTypes(Request $request) {
        $department_id = $request->department;

        if ($department_id) {

            $jobtypes = JobType::where('department_id', $department_id)->get();
        }
        $response = array();
        foreach ($jobtypes as $jobtype) {
            echo '<option value="' . $jobtype['id'] . '">' . $jobtype['name'] . '</option>';
        }
    }

}
