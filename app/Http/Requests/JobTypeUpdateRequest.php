<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Session;
class JobTypeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Determine if the user is flash message to make this request.
     *
     * @return bool
    */
    public function flashMessages()
    {
        Session::flash('notification','Validation failed. Please try again.');
        Session::flash('notificationType','error');
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->flashMessages();
         $rules=[
            'name'                => 'required|regex:/^[a-z\s\-0-9\&]+$/i|max:50',
            'department_id'       => 'required',
            'flow.*.title'        => 'required|regex:/^[a-z\s\-0-9\/\?\&\(\)]+$/i|max:100',
            'flow.*.type'         => 'required',
            'flow.*.option'       => 'required',
            'flow.*.notifyDone'   => 'nullable|integer|between:0,30',
            'flow.*.notifyNotDone'=> 'nullable|integer|between:0,30',
            'flow.*.payment'      => 'nullable|integer|between:0,30',
            'flow.*.company_status_Dormant'     => 'nullable|integer|between:0,30',
            'flow.*.company_status_SemiActive'  => 'nullable|integer|between:0,30',
            'flow.*.company_status_Active'      => 'nullable|integer|between:0,30',
            'flow.*.company_status_SuperActive' => 'nullable|integer|between:0,30',
            'flow.*.adhocConsolidation'         => 'nullable|integer|between:0,30',
            'flow.*.adhocMBRS'                  => 'nullable|integer|between:0,30',
            'flow.*.notify_Hours'               => 'sometimes|required|regex:/^[0-9\:]+$/|between:0,100', 
            'flow.*.notify_Days'                => 'sometimes|required|integer|between:0,30',
            'flow.*.annual_jobtype_id'          => 'sometimes|required',
            'flow.*.afs_jobtype_id'             => 'sometimes|required',
            'flow.*.annual_return_jobtype_id'   => 'sometimes|required',
            'flow.*.jobtype_id'                 => 'sometimes|required',
        ];
        
        return $rules;
    }
    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'flow.*.title.required'            => "The Title field is required.",
            'flow.*.title.regex'               => "The Title field is not valid.",
            'flow.*.type.required'             => "The Type field is required.",
            'flow.*.option.required'           => "The Option field is required.",
            'flow.*.notifyDone.integer'        => 'This field  is not valid.',
            'flow.*.notifyDone.between'        => 'In this field only 0 to 30 Allowed.',
            'flow.*.notifyNotDone.integer'     => 'This field  is not valid.',
            'flow.*.notifyNotDone.between'     => 'In this field only 0 to 30 Allowed.',
            'flow.*.payment.integer'           => 'This field is not valid.',
            'flow.*.payment.between'           => 'In this field only 0 to 30 Allowed.',
            'flow.*.company_status_Dormant.integer'     => 'This field is not valid.',
            'flow.*.company_status_Dormant.between'     => 'In this field only 0 to 30 Allowed.',
            'flow.*.company_status_SemiActive.integer'  => 'This field  is not valid.',
            'flow.*.company_status_SemiActive.between'  => 'In this field only 0 to 30 Allowed.',
            'flow.*.company_status_Active.integer'      => 'This field  is not valid.',
            'flow.*.company_status_Active.between'      => 'In this field only 0 to 30 Allowed.',
            'flow.*.company_status_SuperActive.integer' => 'This field  is not valid.',
            'flow.*.company_status_SuperActive.between' => 'In this field only 0 to 30 Allowed.',
            'flow.*.adhocConsolidation.integer'         => 'This field  is not valid.',
            'flow.*.adhocConsolidation.between'         => 'In this field only 0 to 30 Allowed.',
            'flow.*.adhocMBRS.integer'                  => 'This field  is not valid.',
            'flow.*.adhocMBRS.between'                  => 'In this field only 0 to 30 Allowed.',
            'flow.*.notify_Hours.required'  => 'This field field is required.',
            'flow.*.notify_Hours.regex'     => 'This field  is not valid.',
            'flow.*.notify_Hours.between'     => 'In this field only 0 to 100 Allowed.',
            'flow.*.notify_Days.required'   => 'This field field is required.',
            'flow.*.notify_Days.integer'    => 'This field  is not valid.',
            'flow.*.notify_Days.between'    => 'In this field only 0 to 30 Allowed.',
            'flow.*.afs_jobtype_id.required'             => 'Please Select Audited Financial Statement Jobtype.',
            'flow.*.annual_jobtype_id.required'          => 'Please Select Annual Jobtype.',
            'flow.*.annual_return_jobtype_id.required'   => 'Please Select Annual Return Jobtype.',
            'flow.*.jobtype_id.required'                 => 'Please Select New Jobtype.',
        ];
        
    }
}
