<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Session;
class CompanyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Determine if the user is flash to make this request.
     *
     * @return bool
     */
    public function flashMessages()
    {
        Session::flash('notification','Validation failed. Please try again.');
        Session::flash('notificationType','error');
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        $this->flashMessages();
        return [
            'name'              => 'required|regex:/^[a-z0-9\.\_\s\-\@\$\&\%\*]+$/i|max:50',
            'email'             => 'required|email:filter|unique:companies,email,NULL,id,deleted_at,NULL',
            'company_fka'       => 'nullable|regex:/^[a-z0-9\.\_\s\-]+$/i|max:50',
            'company_roc'       => 'nullable|max:50',
            'incorporation_date' => 'required|date',
            'e_no'              => 'nullable|max:50',
            'c_no'              => 'nullable|max:50',
            'natureof_business' => 'nullable',
            'phone'             => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:30',
            'fax'               => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:30',
            'bank_name'         => 'nullable|regex:/^[a-z0-9\.\_\s\-]+$/i|max:50',
            'account_number'    => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:5|max:20',
            'financial_year_end'=> 'required|regex:/^[0-9\-]+$/i',
            'annual_return_date'=> 'nullable|regex:/(^[A-Za-z0-9\s\-]+$)+/',
            'turnover'          => 'nullable|regex:/^[0-9\,\.]+$/i|min:0|max:50',
            'profit_before_tax' => 'nullable|regex:/^[0-9\,\.]+$/i|min:0|max:50',
            'ep_certificate'    => 'required',
            'applied_date'      => 'nullable|date',
            'new_due_date'      => 'nullable|date|after:applied_date',
//            'company_etr'       => 'required|regex:/^[a-z0-9\.\_\s\-]+$/i|max:50',
//            'etr_date'          => 'required|date',
            'document_type.*.report_year.*'=>'nullable|date_format:Y',
            'status_date'       => 'nullable|date',
            'activity_status'   => 'nullable',
            'share_capital'     => 'required|regex:/^[0-9\,\.]+$/i|min:1|max: 50|gt:0',
            'incharge_name.*'   => 'nullable|regex:/^[a-z0-9\.\_\s\-]+$/i|max:50',
            'incharge_phone.*'  => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:30',
            'incharge_email.*'  => 'nullable|email:filter|unique:companies,email,NULL,id,deleted_at,NULL',
            'address_name.*'    =>'nullable|regex:/^[a-z0-9\.\_\s\-]+$/i|max:50',
            'address_phone.*'   =>'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:30',
            'address.*'         =>'nullable|string',
            'responsible_name.*.*'  =>'nullable|regex:/^[a-z0-9\.\_\s\-]+$/i|max:50',
            'shareholder_name.*' =>'nullable|regex:/^[a-z0-9\.\_\s\-]+$/i|max:50',
            'values.*'          =>'nullable|numeric|min:0|max:100',
            'director.*'        =>'nullable|regex:/^[a-z0-9\.\_\s\-]+$/i|max:50',
            'groupof_companies.*' =>'nullable|regex:/^[a-z0-9\.\_\s\-]+$/i|max:50',
            'date_of_change'      => 'nullable|date',
            'contact'             => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:30',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
       
        return [
            'document_type.*.report_year.*.date_format' =>'The Report Year field is not valid',
            'incharge_name.*.regex'      =>'The Incharge Name field is not valid.',
            'incharge_name.*.max'        =>'The Incharge Name field should only max 50 alphabets.',
            'incharge_phone.*.regex'     =>'The Incharge Phone field is not valid.',
            'incharge_phone.*.min'       =>'The Incharge Phone field must be at least 9 digits.',
            'incharge_phone.*.max'       =>'The Incharge Phone field must not be greater than 30 digits.',
            'incharge_email.*.regex'     =>'The Incharge Email field is not valid.',
            'incharge_email.*.unique'    =>'The Incharge Email field must be Unique.',
            'address_name.*.regex'       =>'The Addressn Name field is not valid.',
            'address_name.*.max'         =>'The Address Name field should only max 50 alphabets.',
            'address_phone.*.regex'      =>'The Address Phone field is not valid.',
            'address_phone.*.min'        =>'The Address Phone field must be at least 9 digits.',
            'address_phone.*.max'        =>'The Address Phone field must not be greater than 30 digits.',
            'address.*.string'           =>'The Address field must be string.',
            'responsible_name.*.*.regex' =>'The Responsible Name field is not valid.',
            'responsible_name.*.*.max'   =>'The Responsible name should only max 50 alphabets.',
            'shareholder_name.*.regex'   =>'The Shareholder Name field is not valid.',
            'shareholder_name.*.max'     =>'The Shareholder Name field should only max 50 alphabets.',
            'values.*.numeric'           =>'The shareholder percentage field must be Numeric.',
            'values.*.min'               =>'The shareholder percentage field should only min 0.',
            'values.*.max'               =>'The shareholder percentage field should only max 100.',
            'director.*.regex'           =>'The Director field is not valid.',
            'director.*.max'             =>'The Director field should only max 50 alphabets.',
            'groupof_companies.*.regex'  =>'The Group Of Companies field is not valid.',
            'groupof_companies.*.max'    =>'The Group Of Companies field should only max 50 alphabets.',
        ];
        
    }

    
}