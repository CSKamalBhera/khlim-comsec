<?php
namespace App\Helpers;

class Helper{

    public static function store_activity_log($causer, $performed_on, $custom_properties, $log_name, $description)
    {
        $activity = activity($log_name)
           ->causedBy($causer)
           ->performedOn($performed_on)
           ->withProperties($custom_properties)
           ->log($description);

        return $activity;
    }

    public static function store_user_model_log($causer, $user, $field, $old_value, $new_value)
    {
        $log_name = 'authentication';
        $operation = 'user_'.$field;
        $description = $operation.'_update';
        $custom_properties = ['application' => config('app.name'),
                            'operation' => $operation,
                            'causer_name' => $causer->name,
                            'new_value' => $new_value,
                            'old_value' => $old_value,
        ];
        $activity = activity($log_name)
           ->causedBy($causer)
           ->performedOn($user)
           ->withProperties($custom_properties)
           ->log($description);

        return $activity;
    }
   
    
}// End of class
