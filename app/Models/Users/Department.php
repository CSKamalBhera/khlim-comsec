<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable   = ['id','slug','slug','description','status'];

    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\Users\User','user_departments', 'user_id', 'department_id');
    }
    /**
    * Get all of the job types.
    */
    public function jobtypes() 
    {
        return $this->hasMany('App\Models\Jobs\JobType','department_id');
    }
    /**
    * Get all of the jobs.
    */
    public function jobs() 
    {
        return $this->hasMany('App\Models\Jobs\JobType','department_id');
    }
    /**
    * Get all of the company Responsible.
    */
    public function companyResponsible()
    {
        return $this->hasMany('App\Models\Company\CompanyAccountResponsible', 'department_id');
    }
    /**
    * Get all of the Company Portfolio.
    */
    public function companyPortfolio()
    {
        return $this->hasMany('App\Models\Company\CompanyPortfolio', 'department_id');
    }
    
     /**
    * Get all of the company Incharge.
    */
    public function company_incharge()
    {
        return $this->hasMany('App\Models\Company\CompanyINCharge', 'department_id');
    }
    
     /**
    * Get all of the recurrence.
    */
    public function recurrences()
    {
        return $this->hasMany('App\Models\Jobs\Recurrence', 'department_id');
    }
    
    /**
    * Get all of the company Invoices.
    */
    public function company_invoices()
    {
        return $this->hasMany('App\Models\Company\Invoice', 'department_id');
    }
    
    /**
    * Get all of the settings.
    */
    public function settings()
    {
        return $this->hasOne('App\Models\Setting', 'department_id');
    }
}
