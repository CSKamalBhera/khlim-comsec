<?php

namespace App\Models\Users;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasFactory, Notifiable,HasRoles,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = ['name','email','password','is_permission','activation_code','status','approved_at','img_src'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function scopeActive($query) {
        return $query->where('status', '=', '1');
    }

    public function scopeCanlogin($query) {
        return $query->where('can_login', '=', '1');
    }
    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function departments()
    {
        return $this->belongsToMany(Department::class, 'user_departments', 'user_id', 'department_id');
    }

    public function getDepartments(){
		$departments = [];
        if ($this->departments()) {
            $departments = $this->departments()->get();
        }
        return $departments;
    }
    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function jobs()
    {
        return $this->hasMany('App\Models\Jobs\Job','staff_id');
    }
    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function jobsManager()
    {
        return $this->hasMany('App\Models\Jobs\Job','manager_id');
    }
    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function jobAssignApproval()
    {
        return $this->hasMany('App\Models\Jobs\JobWorkflow','assigned_id');
    }
    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function companyPortfolio()
    {
        return $this->hasMany('App\Models\Company\CompanyPortfolio','user_id');
    }
        /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function recurrenceStaff()
    {
        return $this->hasMany('App\Models\Jobs\Job','staff_id');
    }
    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function recurrenceManager()
    {
        return $this->hasMany('App\Models\Jobs\Job','manager_id');
    }
}
