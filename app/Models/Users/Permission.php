<?php

namespace App\Models\Users;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use SoftDeletes;

    protected $table      = 'permissions';
    protected $dates = ['created_at','updated_at','deleted_at'];

    protected $fillable = ['name','guard_name','created_at','updated_at','deleted_at'];
    /**
    * Get all of the roles.
    */
    public function roles()
    {
        return $this->belongsToMany(role::class,'rolehaspermission');
    }
}
