<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleHasPermission extends Model
{
    use HasFactory,SoftDeletes;
    /**
    * Get all of the jobs.
    */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    /**
    * Get all of the jobs.
    */
    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }   
}
