<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SettingSystem extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'settings_system';
    protected $fillable = ['status','url','name','logo','publisher','description','version'];
    
}
