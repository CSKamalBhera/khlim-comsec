<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Approval extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'approvals';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['job_id','user_id','job_workflow_id','description','status'];
    /**
    * Get all of the jobs.
    */
    public function job()
    {
        return $this->belongsTo('App\Models\Jobs\Job');
    }
    /**
    * Get all of the jobtypeWorkflow relation.
    */
    public function jobtypeWorkflow()
    {
        return $this->belongsTo('App\Models\Jobs\JobTypeWorkflow','job_type_workflow_id');
    }
}
