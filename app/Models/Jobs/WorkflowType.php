<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkflowType extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'workflow_types';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['name','status','deparment_id'];
    /**
    * Get all of the workflowoption.
    */
    public function workflowoption()
    {
        return $this->hasMany(WorkflowOption::class,'workflow_types_id','id');
    }
    /**
    * Get all of the notify.
    */
    public function notify()
    {
        return $this->hasOne('App\Models\Jobs\WorkflowNotify','workflow_type_id');
    }
}
