<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'jobs';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['company_id','staff_id','job_type_id','manager_id','department_id','created_by','status','reminderStatus'];
    /**
    * Get all of the department.
    */
    public function department()
    {
        return $this->belongsTo('App\Models\Users\Department');
    }
    /**
    * Get all of the company.
    */
    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company');
    }
    /**
    * Get all of the staff.
    */
    public function staff()
    {
        return $this->belongsTo('App\Models\Users\User');
    }
    /**
    * Get all of the created_user.
    */
    public function created_user()
    {
        return $this->belongsTo('App\Models\Users\User','created_by');
    }
    /**
    * Get all of the manager.
    */
    public function manager()
    {
        return $this->belongsTo('App\Models\Users\User');
    }
    /**
    * Get all of the job_type.
    */
    public function job_type()
    {
        return $this->belongsTo('App\Models\Jobs\JobType');
    }
    /**
    * Get all of the jobWorkflow.
    */
    public function jobWorkflow()
    {
        return $this->hasMany('App\Models\Jobs\JobWorkflow');
    }
    /**
    * Get all of the approval.
    */
    public function approval()
    {
        return $this->hasMany('App\Models\Jobs\Approval');
    }
    /**
    * Get all of the jobOutput.
    */
    public function jobOutput()
    {
        return $this->hasOne('App\Models\Jobs\JobOutput');
    }
    
    /**
    * Get all of the company Invoices.
    */
    public function company_invoices()
    {
        return $this->hasMany('App\Models\Company\Invoice');
    }
    /**
    * Get all of the company Invoices.
    */
    public function notify()
    {
        return $this->hasMany('App\Models\Company\WorkflowNotify');
    }
}
