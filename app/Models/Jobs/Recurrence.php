<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recurrence extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'recurrences';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['job_id','company_id','staff_id','job_type_id','manager_id','department_id','created_by','run_date','status'];

    /**
    * Get all of the department.
    */
    
    public function department()
    {
        return $this->belongsTo('App\Models\Users\Department');
    }
    /**
    * Get all of the Company.
    */
    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company');
    }
    /**
    * Get all of the user.
    */
    public function staff()
    {
        return $this->belongsTo('App\Models\Users\User');
    }
    /**
    * Get all of the user.
    */
    public function created_user()
    {
        return $this->belongsTo('App\Models\Users\User','created_by');
    }
    /**
    * Get all of the user.
    */
    public function manager()
    {
        return $this->belongsTo('App\Models\Users\User');
    }
    /**
    * Get all of the JobType.
    */
    public function job_type()
    {
        return $this->belongsTo('App\Models\Jobs\JobType');
    }
}
