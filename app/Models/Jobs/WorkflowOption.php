<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkflowOption extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'workflow_options';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['workflow_types_id','options'];
    /**
    * Get all of the workflowType.
    */
    public function workflowtype()
    {
        return $this->belongsTo(WorkflowType::class, 'workflow_type_id','id');
    }
}
