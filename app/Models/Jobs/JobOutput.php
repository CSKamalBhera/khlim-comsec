<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobOutput extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'job_outputs';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['job_id','job_workflow_id','output_file'];
    // relation
    /**
    * Get all of the jobs.
    */
    public function job()
    {
        return $this->belongsTo('App\Models\Jobs\Job');
    }
}
