<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobType extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'job_types';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['name','department_id','status'];
    
    public function department()
    {
        return $this->belongsTo('App\Models\Users\Department');
    }
    
    public function workflowtype()
    {
        return $this->belongsToMany(WorkflowType::class, 'job_type_workflows','job_type_id','workflow_type_id');
    }
    public function workflows()
    {
        return $this->hasMany('App\Models\Jobs\JobTypeWorkflow','job_type_id');
    }

    /**
    * Get all of the jobs.
    */
    public function jobs()
    {
        return $this->hasMany('App\Models\Jobs\Job','job_type_id');
    }
    /**
    * Get all of the recurrence.
    */
    public function recurrences()
    {
        return $this->hasMany('App\Models\Jobs\Recurrence','job_type_id');
    }
    /**
    * Get all of the notify.
    */
    public function notify()
    {
        return $this->hasMany('App\Models\Jobs\WorkflowNotify','job_type_id');
    }


}
