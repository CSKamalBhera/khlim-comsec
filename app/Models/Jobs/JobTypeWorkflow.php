<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class JobTypeWorkflow extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = ['title','job_type_id','workflow_type_id'];
    /**
    * Get all of the work flowtype.
    */
    public function workflowtype()
    {
        return $this->hasMany(WorkflowType::class, 'workflow_type_id','id');
    }
    /**
    * Get all of the jobtype.
    */
    public function jobtype()
    {
        return $this->hasMany(JobType::class, 'job_type_id','id');
    }
    /**
    * Get all of the workflow_type.
    */
    public function workflow_type()
    {
        return $this->belongsTo('App\Models\Jobs\WorkflowType');
    }
    /**
    * Get all of the notify.
    */
    public function workflowNotify()
    {
        return $this->hasOne('App\Models\Jobs\WorkflowNotify','job_type_workflow_id');
    }
}
