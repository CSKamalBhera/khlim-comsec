<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobWorkflow extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'job_workflows';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['job_id','job_type_workflow_id ','workflow_type_id','workflow_option_id','updated_by','assigned_id','recurrence_date'];
    /**
    * Get all of the staff.
    */
    public function staff()
    {
        return $this->belongsTo('App\Models\Users\User');
    }
    /**
    * Get all of the user.
    */
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User','updated_by');
    }
    /**
    * Get all of the jobs.
    */
    public function jobs()
    {
        return $this->belongsTo('App\Models\Jobs\Job','job_id');
    }
    /**
    * Get all of the jobTypeWorkflow.
    */
    public function jobTypeWorkflow()
    {
        return $this->belongsTo('App\Models\Jobs\JobTypeWorkflow');
    }
    /**
    * Get all of the workflowType.
    */
    public function workflowType()
    {
        return $this->belongsTo('App\Models\Jobs\WorkflowType');
    }
    /**
    * Get all of the option.
    */
    public function option()
    {
        return $this->belongsTo('App\Models\Jobs\WorkflowOption','workflow_option_id');
    }
}
