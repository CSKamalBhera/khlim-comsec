<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkflowNotify extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'workflow_notifications';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['job_type_id','workflow_type_id','workflow_option_id','done_notify_day','not_done_notify_day',
                            'notify_recurrence_day','notify_account_department','super_active','active','semi_active','dormant'];

    public function jobTypeWorkflow()
    {
        return $this->belongsTo('App\Models\Jobs\JobTypeWorkflow');
    }
}
