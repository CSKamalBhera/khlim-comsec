<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'settings';
    protected $fillable = ['department_id','name','email','tax_no','phone_no','address','comment','invoice_header','invoice_footer'];


    /**
    * Get all of the settings.
    */
    public function department()
    {
        return $this->belongsTo('App\Models\Users\Department');
    }
    
}