<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyINCharge extends Model
{
    use HasFactory,SoftDeletes;
    protected $table  = 'company_incharge';
    protected $dates  = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['company_id','department_id','name','phone','email',];
   /**
    * Get all of the departments.
    */
    public function departments()
    {
        return $this->belongsTo('App\Models\Users\Department', 'department_id');
    }
}
