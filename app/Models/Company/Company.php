<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
class Company extends Model
{
    use HasFactory,SoftDeletes,Notifiable;
    protected $table    = 'companies';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['name','email','company_fak','roc','incorporation_date','e_no','c_no','natureof_business','phone_no'
        ,'fax_no','bank_name','account_no','financial_year_end','annual_return_date','turnover','profit_before_tax','annual_meeting_date'
        ,'ep_certificate','applied_date','new_due_date','extension_time','company_etr','etr_date','strike_of','strike_date','status'
    ];

    // relationship
    public function addresses()
    {
        return $this->hasMany('App\Models\Company\CompanyAddress', 'company_id');
    }
    /**
    * Get all of the company Incharge.
    */
    public function company_incharge()
    {
        return $this->hasMany('App\Models\Company\CompanyINCharge', 'company_id');
    }
    /**
    * Get all of the company Responsible.
    */
    public function account_responsible()
    {
        return $this->hasMany('App\Models\Company\CompanyAccountResponsible', 'company_id');
    }
    /**
    * Get all of the company Shares.
    */
    public function shares()
    {
        return $this->hasOne('App\Models\Company\CompanyShare', 'company_id');
    }
    /**
    * Get all of the company Relateds.
    */
    public function related_company()
    {
        return $this->hasOne('App\Models\Company\RelatedCompany', 'company_id');
    }
    /**
    * Get all of the company Invoices.
    */
    public function invoices()
    {
        return $this->hasMany('App\Models\Company\Invoice', 'company_id');
    }
    /**
    * Get all of the company Documents.
    */
    public function company_documents()
    {
        return $this->hasMany('App\Models\Company\CompanyDocument', 'company_id');
    }
    /**
    * Get all of the company Documents.
    */
    public function company_portfolios()
    {
        return $this->hasMany('App\Models\Company\CompanyPortfolio', 'company_id');
    }
    /**
    * Get all of the company company_invoices.
    */
    public function company_invoices()
    {
        return $this->hasMany('App\Models\Company\Invoice', 'company_id');
    }
     /**
    * Get all of the company payments.
    */
    public function payments()
    {
        return $this->hasMany('App\Models\Company\CompanyPayment','company_id');
    }
     /**
    * Get all of the company jobs.
    */
    public function jobs()
    {
        return $this->hasMany('App\Models\Jobs\Job', 'company_id');
    }
     /**
    * Get all of the company recurrences.
    */
    public function recurrences()
    {
        return $this->hasMany('App\Models\Jobs\Recurrence', 'company_id');
    }
    /**
    * Get all of the company Documents.
    */
    public function company_reminders()
    {
        return $this->hasOne('App\Models\Company\CompanyReminder', 'company_id');
    }
    
}
