<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyAddress extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'company_addresses';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['company_id','addresse_type','name','phone','address',];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // relation
    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }
}
