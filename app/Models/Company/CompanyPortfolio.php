<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyPortfolio extends Model
{
    use HasFactory,SoftDeletes;
    protected $table   = 'company_portfolio_holders';
    protected $dates   = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['company_id','user_id','department_id','date',];
   /**
    * Get all of the departments.
    */
    public function departments()
    {
        return $this->belongsTo('App\Models\Users\Department', 'department_id');
    }
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // relation
    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }
}
