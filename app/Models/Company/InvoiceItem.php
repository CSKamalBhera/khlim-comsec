<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceItem extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'invoice_items';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['invoice_id','service_name','is_taxed','amount','total_amount'];
}
