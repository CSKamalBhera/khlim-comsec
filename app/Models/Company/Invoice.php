<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'company_invoices';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['company_id','job_id ','department_id','setting_id','invoice_no','invoice_date','debtor_code','service_type','description','due_amount','total_amount','due_date','status'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
    */
    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }
    /**
    * Get all of the departments.
    */
    public function department()
    {
        return $this->belongsTo('App\Models\Users\Department', 'department_id');
    }
    /**
    * Get all of the settings.
    */
    public function setting()
    {
        return $this->belongsTo('App\Models\Setting', 'setting_id');
    }
    /**
    * Get all of the job.
    */
    public function job()
    {
        return $this->belongsTo('App\Models\Jobs\Job');
    }
    
    public function payments()
    {
        return $this->hasMany('App\Models\Company\CompanyPayment','invoice_id');
    }
    
    public function invoiceItems()
    {
        return $this->hasMany('App\Models\Company\InvoiceItem','invoice_id');
    }
}
