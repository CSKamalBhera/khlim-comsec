<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyPayment extends Model
{
    use HasFactory,SoftDeletes;
    protected $table    = 'company_payments';
    protected $dates    = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['company_id','invoice_id','payment_note','amount','description','date','status'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // relation
    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // relation
    public function invoice()
    {
        return $this->belongsTo('App\Models\Company\Invoice', 'invoice_id');
    }
}
