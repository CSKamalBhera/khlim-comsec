<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyReminder extends Model
{
     use HasFactory,SoftDeletes;
    protected $table = 'company_reminders';
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['company_id','secretarialFees','is_annualReturn_notify','is_taxAgent_notify','is_auditorAppoinment_notify','is_auditStatement_notify'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // relation
    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company', 'company_id');
    }
}
