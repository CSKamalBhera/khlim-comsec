<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
#models
use App\Models\Jobs\Job;
use App\Models\Jobs\JobWorkflow;
use App\Models\Company\Company;
use App\Models\Users\User;
#Notification
use App\Notifications\ManagerNotification;
use App\Notifications\StaffNotification;
use App\Notifications\ClientDocumentReturn;
use App\Notifications\AnnualReturnNotification;
use App\Notifications\AuditorAppointmentNotification;
use App\Notifications\AuditedFinancialStatement;
use App\Notifications\TaxAgentNotification;
use App\Models\Jobs\Recurrence;
#event
use App\Events\NewJobCreatedEvent;
use App\Events\SeceterialJobMailEvent;
use App\Events\WorkflowNotUpdated;
use App\Events\WorkflowUpdates;
#default
use Carbon\Carbon;
use Log;
use DateTime;
class SeceterialJobMailReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seceterial-job:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seceterial job notification reminder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Reminder Daemon Started');
        $now  = Carbon::now()->format('Y-m-d');
        $jobs = Job::with('department','company','jobWorkflow','job_type.workflows.workflow_type.workflowoption','job_type.workflows.workflowNotify')->where('department_id',1)->get();
        foreach ($jobs as $key => $job) {
            $lastUpdatedWorkflow = JobWorkflow::where('job_id',$job->id)->latest('id')->first();
            if($lastUpdatedWorkflow != null){
                $lastUpdateWorkflowDate = $lastUpdatedWorkflow->created_at;
            }else{
                $lastUpdateWorkflowDate = null;   
            }
            \Log::info("Cron is working fine workflow reminder!");
            $workflowId = JobWorkflow::where('job_id',$job->id)->pluck('job_type_workflow_id')->toArray();
            foreach ($job->jobWorkflow as $key => $workflow) {}
            // check job current workflow and notify
            // Status & Recurrence Notify
            if(!$job->jobWorkflow->isEmpty()){
                if ($workflow->option->options == 'Queried') {
                    $totalDays = 14;
                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $totalDays . 'days',strtotime($lastUpdateWorkflowDate)));
                    if ($reminderDate <= $now) {
                        event(New SeceterialJobMailEvent($job,$workflow,$totalDays));
                    }
                }
            }
            //Job Status & Manager Review if workflow not complete x days define as job type send warning notification
             if(!$job->job_type->workflows->isEmpty()){
                 foreach ($job->job_type->workflows as $key => $workflow) {
                    if($workflowId == null){
                         $arrayKey = 0;
                    }
                    if (in_array($workflow['id'], $workflowId)) {
                         $arrayKey = $key+1;
                    }else{
                        if ($key == $arrayKey) {
                            $lastDate = date('Y-m-d',strtotime($lastUpdateWorkflowDate));
                            $lastWorkflowHour = Carbon::parse($lastUpdateWorkflowDate)->format('H:i');
                            $today      = Carbon::now()->format('Y-m-d');
                            $start_date = Carbon::createFromFormat('Y-m-d', $lastDate);
                            $end_date   = Carbon::createFromFormat('Y-m-d', $today);
                            $days = $start_date->diffInDays($end_date);
                            \Log::info('Next Job Workflow - '.$workflow->workflow_type->name);
                            //Status & Notification Notify
                            if ($workflow->workflow_type->id == 3) {
                                if ($workflow->workflowNotify->notification_value !== null) {
                                    $getHours = $workflow->workflowNotify->notification_value;
                                    $deCodeHours = json_decode($getHours);
                                    $updateDate = new DateTime($lastUpdateWorkflowDate); // first date
                                    $today = new DateTime(); // second date
                                    $interval = $updateDate->diff($today); // get difference between two dates
                                    $calLastupdateHour = ($interval->days * 24) + $interval->h; // convert days to hours and add hours from difference 
                                    $totalHour = $deCodeHours->hours;
                                    if ($totalHour <= $calLastupdateHour) {
                                        event(New SeceterialJobMailEvent($job,$workflow));  // notification & mail
                                    }
                                }
                            }
                            //return documents and notification
                            if ($workflow->workflow_type->id == 7) {
                                if ($workflow->workflowNotify->notification_value !== null) {
                                    $getHours = $workflow->workflowNotify->notification_value;
                                    $deCodeHours = json_decode($getHours);
                                    $totalDays = $deCodeHours->days;
                                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $totalDays . 'days',strtotime($lastUpdateWorkflowDate)));
                                    if ($reminderDate <= $now) {
                                        $company = Company::with('company_documents')->find($job->company_id);
                                        // notification & mail
                                        event(New SeceterialJobMailEvent($job,$workflow));  // notification & mail
                                    }
                                }
                            }
                        }
                    }
                }        
            }
         }
        $this->info('Seceterial Reminder Daemon End');
    }
}
