<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Company\Company;
use App\Notifications\SecretarialFeesNotification;
use App\Notifications\AnnualReturnNotification;
use App\Notifications\AuditorAppointmentNotification;
use App\Notifications\AuditedFinancialStatement;
use App\Notifications\TaxAgentNotification;
use Carbon\Carbon;
class CompanyReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'company cron job mail reminder description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Company Reminder Started');
        $companies = Company::with('company_reminders')->where('status', 1)->get();
        foreach ($companies as $company) {
            $reminder = $company->company_reminders;
            if (isset($reminder->secretarialFees)) {

                $lastDate = $company->incorporation_date;
                $today = Carbon::now()->format('Y-m-d');
                $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $lastDate);
                $end_date = \Carbon\Carbon::createFromFormat('Y-m-d', $today);
                $days = $start_date->diffInDays($end_date);
                // check incorporation days count
                if ($days >= 180) {
                    // check company auditer exist or not
                    $getAppointedAuditer = $company->whereHas('company_portfolios', function($query) {
                            $query->where('department_id', 3);
                        })->first();
                    // notifiy client to appoint auditer
                    if ($getAppointedAuditer == null) {
                        $company->notify(new SecretarialFeesNotification($company));
                    }
                }
            }
            if (isset($reminder->is_annualReturn_notify) && $reminder->is_annualReturn_notify == 1) {
                $today = Carbon::now()->format('Y-m-d');
                $startFinancialYearDate = Carbon::now()->startOfYear()->format('Y-m-d');
                $endfinancialYearDate = Carbon::now()->endOfYear()->format('Y-m-d');
                //get fifinancial year
                $financialYear = Carbon::parse($startFinancialYearDate)->format('Y');
                $endfinancialYear = Carbon::parse($endfinancialYearDate)->format('Y');
                // get compnay annual date and change format
                $calDate = $company->annual_return_date . '-' . $endfinancialYear;
                $lastDate = Carbon::parse($calDate)->format('Y-m-d');
                // calculation total financial days
                $start_date_financial = Carbon::createFromFormat('Y-m-d', $startFinancialYearDate);
                $annualReturnDate = Carbon::createFromFormat('Y-m-d', $lastDate);
                $totalFinancalDays = $start_date_financial->diffInDays($annualReturnDate);
                // get current date to total left days
                $start_date = Carbon::createFromFormat('Y-m-d', $startFinancialYearDate);
                $end_date = Carbon::createFromFormat('Y-m-d', $today);
                $totalLeftDays = $start_date->diffInDays($end_date);
                // get pendding days for reminder annual date
                $penddingDays = ($totalFinancalDays - $totalLeftDays);

                $reminderDate = date('Y-m-d', strtotime(' - 3 months', strtotime($lastDate)));
                $reminderStartDate = Carbon::createFromFormat('Y-m-d', $lastDate);
                $reminderEndDate = Carbon::createFromFormat('Y-m-d', $reminderDate);
                $reminderDay = $reminderStartDate->diffInDays($reminderEndDate);
                // check incorporation days count
                if ($penddingDays <= $reminderDay) {
                    $company->notify(new AnnualReturnNotification($company)); // notifiy client to appoint auditer
                }
            }
            if (isset($reminder->is_taxAgent_notify) && $reminder->is_taxAgent_notify == 1) {
                // get incorporation days
                $currentYear = Carbon::now()->format('Y');
                $notificationDate = Carbon::parse($currentYear .'-01-15')->format('Y-m-d');
                $currentDate = Carbon::now()->format('Y-m-d');
                // check incorporation days count
                if ($notificationDate == $currentDate) {
                    // check company taxAgent exist or not
                    $getAppointedtaxAgent = $company->whereHas('company_portfolios', function($query) {
                                $query->where('department_id', 4);
                            })->first();
                    // notifiy client to appoint taxAgent
                    if ($getAppointedtaxAgent == null) {
                        $company->notify(new TaxAgentNotification($company));
                    }
                }
            }
            if (isset($reminder->is_auditorAppoinment_notify) && $reminder->is_auditorAppoinment_notify == 1) {
                $lastDate = $company->incorporation_date;
                $today = Carbon::now()->format('Y-m-d');
                $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $lastDate);
                $end_date = \Carbon\Carbon::createFromFormat('Y-m-d', $today);
                $days = $start_date->diffInDays($end_date);
                // check incorporation days count
                $reminderDate = date('Y-m-d', strtotime(' + 12 months', strtotime($lastDate)));
                $startReminderDay = \Carbon\Carbon::createFromFormat('Y-m-d', $lastDate);
                $end_date_reminder = \Carbon\Carbon::createFromFormat('Y-m-d', $reminderDate);
                $reminderDay = $startReminderDay->diffInDays($end_date_reminder);
                if ($days >= $reminderDay) {
                    // check company auditer exist or not
                    $getAppointedAuditer = $company->whereHas('company_portfolios', function($query) {
                            $query->where('department_id', 3);
                        })->first();
                    // notifiy client to appoint auditer
                    if ($getAppointedAuditer == null) {
                        $company->notify(new AuditorAppointmentNotification($company));
                    }
                }
            }
            if (isset($reminder->is_auditStatement_notify) && $reminder->is_auditStatement_notify == 1) {
                $today = Carbon::now()->format('Y-m-d');
                $startLastFinancialYearDate = date("Y-01-01", strtotime("-1 year")); // get next year date from here
                $endLastFinancialYearDate = date("Y-12-t", strtotime($startLastFinancialYearDate)); // get financial year date from here
                $reminderDate = date('Y-m-d', strtotime(' + 3 months', strtotime($endLastFinancialYearDate)));
                // check incorporation days count
                if ($today >= $reminderDate) {
                    $company->notify(new AuditedFinancialStatement($company)); // notifiy client to appoint auditer
                }
            }
        }
        $this->info('Company Reminder End');
    }
}
