<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
#models
use App\Models\Jobs\Job;
use App\Models\Jobs\JobWorkflow;
use App\Models\Users\User;
#Notification
use App\Notifications\ManagerNotification;
use App\Notifications\StaffNotification;
#event
use App\Events\NewJobCreatedEvent;
use App\Events\WorkflowNotUpdated;
use App\Events\WorkflowUpdates;
#default
use Carbon\Carbon;
use Log;
class SendReminder extends Command
{
   /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:send';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends out reminders';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Reminder Daemon Started \n');
        $now  = Carbon::now()->format('Y-m-d');
        $jobs = Job::with('department','company','jobWorkflow','job_type.workflows.workflow_type.workflowoption','job_type.workflows.workflowNotify')->where('reminderStatus',0)->get();
        foreach ($jobs as $key => $job) {
            $lastUpdatedWorkflow = JobWorkflow::where('job_id',$job->id)->latest('id')->first();
            if($lastUpdatedWorkflow != null){
                $lastUpdateWorkflowDate = $lastUpdatedWorkflow->created_at;
            }else{
                $lastUpdateWorkflowDate = null;   
            }
            \Log::info("Cron is working fine in Send workflow reminder!");
            $workflowId = JobWorkflow::where('job_id',$job->id)->pluck('job_type_workflow_id')->toArray();
            foreach ($job->jobWorkflow as $key => $workflow) {}
            //Status & Recurrence Notify
            if(!$job->jobWorkflow->isEmpty()){
                if ($workflow->workflowType->name == 'Status & Recurrence Notify') {
                    if ($workflow->jobTypeWorkflow->workflowNotify->done_notify_day !== null) {
                        $total = $workflow->jobTypeWorkflow->workflowNotify->done_notify_day;
                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                        if ($reminderDate == $now) {
                            $description = 'This job workflow successfully updated the following workflow : '. $workflow->jobTypeWorkflow->title .' mark as a - '. $workflow->option->options;
                            // notification & mail
                            event(New WorkflowUpdates($job,$workflow,$description));
                        }
                    }
                }
                //Status & Notify
                if ($workflow->workflowType->name == 'Status & Notify') {
                    if ($workflow->jobTypeWorkflow->workflowNotify->done_notify_day !== null) {
                        $total = $workflow->jobTypeWorkflow->workflowNotify->done_notify_day;
                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                        if ($reminderDate == $now) {
                            $description = 'This job workflow successfully updated the following workflow : '. $workflow->jobTypeWorkflow->title .' mark as a - '. $workflow->option->options;
                            // notification & mail
                            event(New WorkflowUpdates($job,$workflow,$description));
                        }
                    }
                }
                //Job Complete
                if ($workflow->workflowType->name == 'Job Complete') {
                    if ($workflow->jobTypeWorkflow->workflowNotify->done_notify_day == null){
                        $job->reminderStatus = 1;
                        $job->save();
                    }
                    if ($workflow->jobTypeWorkflow->workflowNotify->done_notify_day !== null) {
                        $total = $workflow->jobTypeWorkflow->workflowNotify->done_notify_day;
                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                        \Log::info($reminderDate. '- Last updated workflow - '. $workflow->workflowType->name);
                        if ($reminderDate == $now) {
                            // notification & mail
                            $description = 'This is email confirmation to let them know you have received/not received the payment.';
                            $job->reminderStatus = 1;
                            $job->save();
                            event(New WorkflowUpdates($job,$workflow,$description));
                        }
                    }
                    if ($workflow->jobTypeWorkflow->workflowNotify->notify_account_department != null) {
                        $reminderDate = date('Y-m-d',strtotime($lastUpdateWorkflowDate));
                        if ($reminderDate == $now) {
                            // notification & mail
                            $description = 'This is email account department notify that this is job has been successfully completed.';
                            event(New WorkflowUpdates($job,$workflow,$description));
                        }
                    }
                }
                //Adhoc
                if ($workflow->workflowType->name == 'Adhoc') {
                    if ($workflow->option->options == 'Consolidation') {
                        if ($workflow->jobTypeWorkflow->workflowNotify->done_notify_day != null) {
                            $total = $workflow->jobTypeWorkflow->workflowNotify->done_notify_day;
                            $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                            if ($reminderDate == $now) {
                                // notification & mail
                                event(New NewJobCreatedEvent($job));
                            }
                        }
                    }
                    if ($workflow->option->options == 'MBRS') {
                        if ($workflow->jobTypeWorkflow->workflowNotify->done_notify_day != null) {
                            $total = $workflow->jobTypeWorkflow->workflowNotify->done_notify_day;
                            $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                            if ($reminderDate == $now) {
                                // notification & mail
                                event(New NewJobCreatedEvent($job));
                            }
                        }
                    }
                }
            }
            //Job Status & Manager Review if workflow not complete x days define as job type send warning notification
             if(!$job->job_type->workflows->isEmpty()){
                 foreach ($job->job_type->workflows as $key => $workflow) {
                    if($workflowId == null){
                         $arrayKey = 0;
                    }
                    if (in_array($workflow['id'], $workflowId)) {
                         $arrayKey = $key+1;
                    }else{
                        if ($key == $arrayKey) {
                            \Log::info("Cron is working fine in workflow checking next updateing workflow!");
                            $lastDate   = date('Y-m-d',strtotime($lastUpdateWorkflowDate));
                            $today      =  Carbon::now()->format('Y-m-d');
                            $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $lastDate);
                            $end_date   = \Carbon\Carbon::createFromFormat('Y-m-d', $today);
                            $days = $start_date->diffInDays($end_date);
                            //Status & Recurrence Notify
                            \Log::info('Next Job Workflow - '.$workflow->workflow_type->name);
                            if ($workflow->workflow_type->name == 'Status & Recurrence Notify') {
                                if ($workflow->workflowNotify->not_done_notify_day !== null) {
                                    $total = $workflow->workflowNotify->not_done_notify_day;
                                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                    if ($reminderDate <= $now) {
                                        // notification & mail
                                        $description = 'You have not been completing your workflow since '.$days.' days. please complete the workflow : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                        event(New WorkflowNotUpdated($job,$workflow,$description));
                                    }
                                }
                            }
                            //Status & Notify
                            if ($workflow->workflow_type->name == 'Status & Notify') {
                                if ($workflow->workflowNotify->not_done_notify_day !== null) {
                                    $total = $workflow->workflowNotify->not_done_notify_day;
                                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                    if ($reminderDate <= $now) {
                                        // notification & mail
                                        $description = 'You have not been completing your workflow since '.$days.' days. please complete the workflow : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                        event(New WorkflowNotUpdated($job,$workflow,$description));
                                    }
                                }
                            }
                            //Job Status
                            if ($workflow->workflow_type->name == 'Job Status') {
                                //super_active
                                if ($job->company->acitivity_status == 1) {
                                    if ($workflow->workflowNotify->super_active !== null) {
                                        $total = $workflow->workflowNotify->super_active;
                                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                        if ($reminderDate <= $now) {
                                            // notification & mail
                                            $description = 'This company status was super active with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                            event(New WorkflowNotUpdated($job,$workflow,$description));
                                        }
                                    }
                                }
                                //active
                                if ($job->company->acitivity_status == 2) {
                                    if ($workflow->workflowNotify->active !== null) {
                                        $total = $workflow->workflowNotify->active;
                                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                        if ($reminderDate <= $now) {
                                            // notification & mail
                                            $description = 'This company status was active with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                            event(New WorkflowNotUpdated($job,$workflow,$description));
                                        }
                                    }
                                }
                                //semi_active
                                if ($job->company->acitivity_status == 3) {
                                    if ($workflow->workflowNotify->semi_active !== null) {
                                        $total = $workflow->workflowNotify->semi_active;
                                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                        if ($reminderDate <= $now) {
                                            // notification & mail
                                            $description = 'This company status was semi active with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                            event(New WorkflowNotUpdated($job,$workflow,$description));
                                        }
                                    }
                                }
                                //dormant
                                if ($job->company->acitivity_status == 4) {
                                    if ($workflow->workflowNotify->dormant !== null) {
                                        $total = $workflow->workflowNotify->dormant;
                                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                        if ($reminderDate <= $now) {
                                            // notification & mail
                                            $description = 'This company status was dormant with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                            event(New WorkflowNotUpdated($job,$workflow,$description));
                                        }
                                    }
                                }
                            }
                            //Manager Review
                            if ($workflow->workflow_type->name == 'Manager Review') {
                                //super_active
                                if ($job->company->acitivity_status == 1) {
                                    if ($workflow->workflowNotify->super_active !== null) {
                                        $total = $workflow->workflowNotify->super_active;
                                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                        if ($reminderDate <= $now) {
                                            // notification & mail
                                            $description = 'This company status was super active with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                            event(New WorkflowNotUpdated($job,$workflow,$description));
                                        }
                                    }
                                }
                                //active
                                if ($job->company->acitivity_status == 2) {
                                    if ($workflow->workflowNotify->active !== null) {
                                        $total = $workflow->workflowNotify->active;
                                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                        if ($reminderDate <= $now) {
                                            // notification & mail
                                            $description = 'This company status was active with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                            event(New WorkflowNotUpdated($job,$workflow,$description));
                                        }
                                    }
                                }
                                //semi_active
                                if ($job->company->acitivity_status == 3) {
                                    if ($workflow->workflowNotify->semi_active !== null) {
                                        $total = $workflow->workflowNotify->semi_active;
                                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                        if ($reminderDate <= $now) {
                                            // notification & mail
                                            $description = 'This company status was semi active with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                            event(New WorkflowNotUpdated($job,$workflow,$description));
                                        }
                                    }
                                }
                                //dormant
                                if ($job->company->acitivity_status == 4) {
                                    if ($workflow->workflowNotify->dormant !== null) {
                                        $total = $workflow->workflowNotify->dormant;
                                        $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                        if ($reminderDate <= $now) {
                                            // notification & mail
                                            $description = 'This company status was dormant with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                            event(New WorkflowNotUpdated($job,$workflow,$description));
                                        }
                                    }
                                }
                           }
                        }
                    }
                }        
            }
         }
        $this->info('Reminder Daemon End');
    }
}
