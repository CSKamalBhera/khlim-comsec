<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
#model
use App\Models\Jobs\Recurrence;
use App\Models\Jobs\Job;
#event
use App\Events\NewJobCreatedEvent;
use Carbon\Carbon;
use Log;
class RecurrenceScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recurrence:scheduler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recurrence scheduler has been run';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Recurrence Cron Cummand Run successfully!');
        $recurrences = Recurrence::where('status',0)->where('run_date',Carbon::now()->format('Y-m-d'))->get();
        if ($recurrences->isEmpty()) {
            \Log::info("There are no recurrence jobs available this date -" .Carbon::now()->format('d-m-Y'));
        }else{
            foreach ($recurrences as $key => $recurrence) {
                $job = new Job();
                $job->company_id    = $recurrence->company_id;
                $job->staff_id      = $recurrence->staff_id;
                $job->job_type_id   = $recurrence->job_type_id;
                $job->manager_id    = $recurrence->manager_id;
                $job->department_id = $recurrence->department_id;
                $job->created_by    = $recurrence->created_by;;
                $job->status        = 0;
                $job->save();
                // notification & mail
                event(new NewJobCreatedEvent($job));
                // recurrence status update
                $recurrence->status = 1;
                $recurrence->save();
            }
        }
    }
}
