<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
#models
use App\Models\Jobs\Job;
use App\Models\Jobs\JobWorkflow;
use App\Models\Company\Company;
use App\Models\Users\User;
#Notification
use App\Notifications\ManagerNotification;
use App\Notifications\StaffNotification;
use App\Notifications\ClientDocumentReturn;
use App\Notifications\AnnualReturnNotification;
use App\Notifications\AuditorAppointmentNotification;
use App\Notifications\AuditedFinancialStatement;
use App\Notifications\TaxAgentNotification;
use App\Notifications\ManagementAccountReview;
use App\Models\Jobs\Recurrence;
#event
use App\Events\NewJobCreatedEvent;
use App\Events\SeceterialJobMailEvent;
use App\Events\WorkflowNotUpdated;
use App\Events\WorkflowUpdates;
#default
use Carbon\Carbon;
use Log;
use DateTime;
class AccountJobMailReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account-workflow:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Account Workflow Reminder Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Account Workflow Reminder Daemon Started \n');
        $now  = Carbon::now()->format('Y-m-d');
        $jobs = Job::with('department','company','jobWorkflow','job_type.workflows.workflow_type.workflowoption','job_type.workflows.workflowNotify')->where('department_id',2)->get();
        foreach ($jobs as $key => $job) {
            $lastUpdatedWorkflow = JobWorkflow::where('job_id',$job->id)->latest('id')->first();
            $company = Company::with('company_documents')->find($job->company_id);
            if($lastUpdatedWorkflow != null){
                $lastUpdateWorkflowDate = $lastUpdatedWorkflow->created_at;
            }else{
                $lastUpdateWorkflowDate = null;   
            }
            $workflowId = JobWorkflow::where('job_id',$job->id)->pluck('job_type_workflow_id')->toArray();
            foreach ($job->jobWorkflow as $key => $workflow) {}
            // check job current workflow and notify
            // Status & Recurrence Notify
            if(!$job->jobWorkflow->isEmpty()){
                if ($workflow->option->options == 'Extended') {
                    $totalDays = 2;
                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $totalDays . 'days',strtotime($lastUpdateWorkflowDate)));
                    if ($reminderDate <= $now) {
                        $company->notify(new ManagementAccountReview($company));
                    }
                }
            }
            // Received Status
            if(!$job->jobWorkflow->isEmpty()){
                if ($workflow->option->options == 'Incomplete') {
                    $dt  = Carbon::now();
                    //$weekStart = $dt->startOfWeek()->format('Y-m-d');
                    $totalDays = 14;
                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $totalDays . 'days',strtotime($lastUpdateWorkflowDate)));
                    if ($reminderDate <= $now) {
                        if ($dt->dayOfWeek == Carbon::MONDAY) {
                            $company->notify(new ClientDocumentReturn($company));
                        }
                    }
                }
            }
            //Job Status & Manager Review if workflow not complete x days define as job type send warning notification
             if(!$job->job_type->workflows->isEmpty()){
                 foreach ($job->job_type->workflows as $key => $workflow) {
                    if($workflowId == null){
                         $arrayKey = 0;
                    }
                    if (in_array($workflow['id'], $workflowId)) {
                         $arrayKey = $key+1;
                    }else{
                        if ($key == $arrayKey) {
                            $lastDate = date('Y-m-d',strtotime($lastUpdateWorkflowDate));
                            $lastWorkflowHour = Carbon::parse($lastUpdateWorkflowDate)->format('H:i');
                            $today      = Carbon::now()->format('Y-m-d');
                            $start_date = Carbon::createFromFormat('Y-m-d', $lastDate);
                            $end_date   = Carbon::createFromFormat('Y-m-d', $today);
                            $days = $start_date->diffInDays($end_date);
                            \Log::info('Next Job Workflow - '.$workflow->workflow_type->name);
                            //Status & Notification Notify
                            if ($workflow->workflow_type->id == 18) {
                                if ($workflow->workflowNotify->notification_value !== null) {
                                    $getHours = $workflow->workflowNotify->notification_value;
                                    $deCodeHours = json_decode($getHours);
                                    $updateDate = new DateTime($lastUpdateWorkflowDate); // first date
                                    $today = new DateTime(); // second date
                                    $interval = $updateDate->diff($today); // get difference between two dates
                                    $calLastupdateHour = $interval->d; // convert days to hours and add hours from difference 
                                    $totalHour = $deCodeHours->days;
                                    if ($totalHour <= $calLastupdateHour) {
                                        event(New SeceterialJobMailEvent($job,$workflow));  // notification & mail
                                    }
                                }
                            }
                            // Assing User & Status
                            if ($workflow->workflow_type->id == 17) {
                                $updateDate = new DateTime($lastUpdateWorkflowDate); // first date
                                $today = new DateTime(); // second date
                                $interval = $updateDate->diff($today); 
                                $calLastupdateDay = $interval->d;
                                if ($calLastupdateDay >= 14) {
                                    event(New SeceterialJobMailEvent($job,$workflow));  // notification & mail
                                }
                            }
                            // Manager Review
                            if ($workflow->workflow_type->id == 21) {
                                //super_active
                                if ($job->company->acitivity_status == 1) {
                                    $total = 7;
                                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                    if ($reminderDate <= $now) {
                                        // notification & mail
                                        $description = 'This company status was super active with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                        event(New WorkflowNotUpdated($job,$workflow,$description));
                                    }
                                }
                                //active
                                if ($job->company->acitivity_status == 2) {
                                    $total = 5;
                                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                    if ($reminderDate <= $now) {
                                        // notification & mail
                                        $description = 'This company status was active with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                        event(New WorkflowNotUpdated($job,$workflow,$description));
                                    }
                                }
                                //semi_active
                                if ($job->company->acitivity_status == 3) {
                                    $total = 3;
                                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                    if ($reminderDate <= $now) {
                                        // notification & mail
                                        $description = 'This company status was semi active with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                        event(New WorkflowNotUpdated($job,$workflow,$description));
                                    }
                                }
                                //dormant
                                if ($job->company->acitivity_status == 4) {
                                    $total = 2;
                                    $reminderDate = date('Y-m-d', strtotime( ' + ' . $total . 'days',strtotime($lastUpdateWorkflowDate)));
                                    if ($reminderDate <= $now) {
                                        // notification & mail
                                        $description = 'This company status was dormant with a work limit of '. $total .' days, so you should complete the workflow quickly : '. $workflow->title .'( '.$workflow->workflow_type->name.' )';
                                        event(New WorkflowNotUpdated($job,$workflow,$description));
                                    }
                                }
                            }
                            //return documents and notification
                            // if ($workflow->workflow_type->id == 7) {
                            //     if ($workflow->workflowNotify->notification_value !== null) {
                            //         $getHours = $workflow->workflowNotify->notification_value;
                            //         $deCodeHours = json_decode($getHours);
                            //         $totalDays = $deCodeHours->days;
                            //         $reminderDate = date('Y-m-d', strtotime( ' + ' . $totalDays . 'days',strtotime($lastUpdateWorkflowDate)));
                            //         if ($reminderDate <= $now) {
                            //             $company = Company::with('company_documents')->find($job->company_id);
                            //             // notification & mail
                            //             event(New SeceterialJobMailEvent($job,$workflow));  // notification & mail
                            //         }
                            //     }
                            // }
                        }
                    }
                }        
            }
         }
        $this->info('Account Workflow Reminder Daemon End');
    }
}
