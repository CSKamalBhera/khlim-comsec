<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Log;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\RecurrenceScheduler::class,
        Commands\SeceterialJobMailReminder::class,
        Commands\AccountJobMailReminder::class,
        Commands\CompanyReminder::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $eventEndLog = new Logger('schedule_job');
        $eventEndLog->pushHandler(new StreamHandler(storage_path().'/logs/cronJob.log', Logger::DEBUG, true, 0777));
        $eventEndLog->info('============Cron Job log start============');
        // recurrence jobs scheduler
        $schedule->command('recurrence:scheduler')
            ->daily()
            ->timezone('Asia/Kuala_Lumpur');
        // seceterial job reminder
         $schedule->command('seceterial-job:reminder')
             ->daily()
             ->timezone('Asia/Kuala_Lumpur');
        // account workflow reminder
        $schedule->command('account-workflow:reminder')
            ->daily()
            ->timezone('Asia/Kuala_Lumpur');
        //company reminder
        $schedule->command('company:reminder')
            ->daily()
            ->timezone('Asia/Kuala_Lumpur');
        //tax agent reminder
        // $schedule->command('taxagent:reminder')
        //     ->dailyAt('12:00')
        //     ->timezone('Asia/Kuala_Lumpurq');
        // log when cron job are end
        $eventEndLog->info('============Cron Job log end============');
        //$schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
