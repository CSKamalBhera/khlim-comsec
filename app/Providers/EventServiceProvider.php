<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\WorkflowUpdates' => [
            'App\Listeners\WorkflowUpdateNotificationToManager',
            'App\Listeners\WorkflowUpdateNotificationToStaff',
            'App\Listeners\WorkflowUpdateNotificationToAssignedUser',
        ],
        'App\Events\WorkflowNotUpdated' => [
            'App\Listeners\WorkflowNotUpdateNotificationToManager',
            'App\Listeners\WorkflowNotUpdateNotificationToStaff',
            'App\Listeners\WorkflowNotUpdateNotificationToAssignedUser',
        ],
        'App\Events\NewUserRegistered' => [
            'App\Listeners\NewUserNotificationToAdmin',
            'App\Listeners\NewUserNotificationToUser',
        ],
        'App\Events\NewJobCreatedEvent' => [
            'App\Listeners\NewJobNotificationToManager',
            'App\Listeners\NewJobNotificationToStaff',
        ],
        'App\Events\SeceterialJobMailEvent' => [
            'App\Listeners\JobMailToManager',
        ],
        'Illuminate\Auth\Events\PasswordReset' => [
            'App\Listeners\LogPasswordReset',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
