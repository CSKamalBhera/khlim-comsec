@extends('layouts.app', [
'activePage' => 'Dashboard',
'menuParent' => 'Dashboard',
'title' => 'Dashboard',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">Dashboard</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">khlim</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<div id="page-container" class="main-content-boxed">
    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div class="content">
            <!-- Stats -->
            <div class="row">
                @hasrole(7)
                <div class="col-6 col-md-3 col-lg-6 col-xl-3">
                    <a class="block block-rounded block-link-pop" href="{{ route('users.index') }}">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700">{{ $staff }}</dt>
                                <dd class="text-muted mb-0">Total Staff</dd>
                            </dl>
                            <div class="p-2 item-rounded bg-body">
                                <i class="fa fa-users font-size-h3 text-primary"></i>
                            </div>
                        </div>
                    </a>
                </div>
                @endhasrole
                <div class="col-6 col-md-3 col-lg-6 col-xl-3">
                    <!-- New Customers -->
                    <a class="block block-rounded block-link-pop" href="{{ route('companies.index') }}">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700">{{ $company }}</dt>
                                <dd class="text-muted mb-0">Total Customers</dd>
                            </dl>
                            <div class="p-2 item-rounded bg-body">
                                <i class="fa fa-industry font-size-h3 text-primary"></i>
                            </div>
                        </div>
                    </a>
                    <!-- END New Customers -->
                </div>

                <div class="col-6 col-md-3 col-lg-6 col-xl-3">
                    <a class="block block-rounded block-link-pop" href="{{ route('AssignedJob') }}">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700">{{ $assignedJobs }}</dt>
                                <dd class="text-muted mb-0">Assigned Jobs</dd>
                            </dl>
                            <div class="p-2 item-rounded bg-body">
                                <i class="fa fa-tasks font-size-h3 text-primary"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-3 col-lg-6 col-xl-3">
                    <a class="block block-rounded block-link-pop" href="{{ route('CompleteJob') }}">
                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                            <dl class="mb-0">
                                <dt class="font-size-h2 font-w700">{{ $completeJobs }}</dt>
                                <dd class="text-muted mb-0">Completed Jobs</dd>
                            </dl>
                            <div class="p-2 item-rounded bg-body">
                                <i class="fa fa-check-circle font-size-h3 text-primary"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <!-- END Stats -->
        </div>
    </main>
    <!-- END Main Container -->
</div>  

@endsection
