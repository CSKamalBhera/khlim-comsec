@extends('layouts.app', [
'activePage' => 'permission-list',
'menuParent' => 'permission-list',
'title' => 'Permissions',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Permissions
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Permissions</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx">List</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Dynamic Table with Export Buttons -->
<div class="content">
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title"></h3>
            @can('permission_create')
            <a href="{{ route('permissions.create') }}" class="btn btn-primary">Create</a>
            @endcan
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">#</th>
                        <th>Name</th>
                        <th>Guard Name</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($permissions as $key => $permission)
                    <tr>
                        <td class="text-center">{{ $key +1 }}</td>
                        <td class="font-w600">
                            <a href="{{ route('permissions.show',$permission->id) }}">{{ ucwords($permission->name) }}</a>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <em class="text-muted">{{ $permission->guard_name }}</em>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            @if($permission->status == 1)
                            <span class="badge badge-info">Active</span>
                            @else
                            <span class="badge badge-danger">Inactive</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                @can('permission_edit')
                                <a href="{{ route('permissions.edit',$permission->id) }}" class="btn btn-sm btn-light"><i class="nav-main-link-icon si si-pencil"></i></a>
                                @endcan
                                @can('permission_status')
                                @if($permission->status == 1)
                                <a href="{{url('permissions/status/'.$permission->id)}}" class="btn btn-sm btn-light" title="active"><i class="fa fa-unlock"></i></a>
                                @else
                                <a href="{{url('permissions/status/'.$permission->id)}}" class="btn btn-sm btn-light" title="inactive"><i class="fa fa-lock"></i></a>
                                @endif
                                @endcan
                                @can('permission_delete')
                                <form action="{{ route('permissions.destroy',$permission->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-light delete" data-name="{{ $permission->name }}" title="{{__('Delete')}}">
                                        <i class="nav-main-link-icon si si-trash"></i>
                                    </button>
                                </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->
@endsection
