@extends('layouts.app', [
'activePage' => 'edit-permission',
'menuParent' => 'edit-permission',
'title' => 'Edit Permission',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                {{__('Permission')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item"><a href="{{ route('permissions.index') }}">Permission</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">Edit</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <form action="{{route('permissions.update', $permission->id)}}" method="POST">
                @method('PATCH')
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="name">Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name', $permission->name) }}" placeholder="Name">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="name">Status<span class="text-danger">*</span></label>
                            <select class="form-control  @error('status') is-invalid @enderror" name="status" style="width: 100%;">
                                <option value="">Select</option>
                                <option value="1" {{ old('status', $permission->status) == 1 ? 'selected' : '' }}>Active</option>
                                <option value="0" {{ old('status', $permission->status) == 0 ? 'selected' : '' }}>Inactive</option>
                            </select>
                            @error('status')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->
@endsection
