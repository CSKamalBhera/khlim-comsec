@extends('layouts.app', [
'activePage' => 'Permissions-details',
'menuParent' => 'Permissions-details',
'title' => 'Permissions Details',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                {{__('Permissions')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{route('permissions.index')}}">{{__('Permissions')}}</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a>{{__('Details')}}</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <div class="block block-rounded">
        <div class="block-content block-content-full ml-12 ">
            <div class="row">
                <div class="col-md-6">
                    <dt>{{ __('Permission Name') }}</dt>
                    <dd>{{$permission->name}}</dd>

                </div>
                <div class="col-md-6">
                    <dt>{{ __('Permissions Assined To Roles') }}</dt>
                    @foreach($rolePermissions->roles as $permission)
                    <dd>{{$permission->name}}</dd>
                    @endforeach
                </div>
            </div><!-- /.row -->
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection
