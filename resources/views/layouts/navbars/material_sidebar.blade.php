<!-- Sidebar -->
<nav id="sidebar" aria-label="Main Navigation">
    <!-- Side Header -->
    @php
    $appSetting = \App\Models\SettingSystem::first();
    @endphp
    <div class="content-header bg-white-5">
        <!-- Logo -->
        <a class="font-w600 text-dual" href="{{ url('/') }}">
            <span class="smini-visible">
                <i class="fa fa-circle-notch text-primary"></i>
            </span>
            <span class="smini-hide font-size-h5 tracking-wider">
                {{ $appSetting ? $appSetting->name : 'khlim' }}
                {{-- <span class="font-w400"><img src="{{$appSetting ? asset($appSetting->logo) : '' }}" height="50px" width="70px" class="rounded-lg"/></span> --}}
            </span>
        </a>
        <!-- END Logo -->

        <!-- Extra -->
        <div>
            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
            <a class="d-lg-none btn btn-sm btn-dual ml-1" data-toggle="layout" data-action="sidebar_close"
                href="javascript:void(0)">
                <i class="fa fa-fw fa-times"></i>
            </a>
            <!-- END Close Sidebar -->
        </div>
        <!-- END Extra -->
    </div>
    <!-- END Side Header -->
    <!-- Sidebar Scrolling -->
    <div class="js-sidebar-scroll">
        <!-- Side Navigation -->
        <div class="content-side">
            <ul class="nav-main">
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('dashboard') ? ' active' : '' }}" href="{{ route('dashboard') }}">
                        <i class="nav-main-link-icon si si-speedometer"></i>
                        <span class="nav-main-link-name">Dashboard</span>
                    </a>
                </li>
                @can('company_access')
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('companies','companies/*') ? ' active' : '' }}" href="{{ route('companies.index') }}">
                        <i class="nav-main-link-icon si si-user"></i>
                        <span class="nav-main-link-name">Customers</span>
                    </a>
                </li>
                @endcan
                @can('jobtype_access')
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('jobtype','jobtype/*') ? ' active' : '' }}" href="{{ route('jobtype.index') }}">
                        <i class="nav-main-link-icon si si-cursor"></i>
                        <span class="nav-main-link-name">Job Type</span>
                    </a>
                </li>
                @endcan
                @can('jobs_access')
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('jobs','jobs/*') ? ' active' : '' }}" href="{{ route('jobs.index') }}">
                        <i class="nav-main-link-icon fa fa-podcast"></i>
                        <span class="nav-main-link-name">Jobs</span>
                    </a>
                </li>
                @endcan
                @can('jobs_output_access')
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('job-output','job-output/*') ? ' active' : '' }}" href="{{ route('job-output.index') }}">
                        <i class="nav-main-link-icon fa fa-outdent"></i>
                        <span class="nav-main-link-name">Jobs Output</span>
                    </a>
                </li>
                @endcan
                @can('invoice_access')
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('invoices','invoices/*') ? ' active' : '' }}" href="{{ route('invoices.index') }}">
                        <i class="nav-main-link-icon fa fa-file-invoice"></i>
                        <span class="nav-main-link-name">Billing</span>
                    </a>
                </li>
                @endcan
                @can('recurrence_access')
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('recurrence','recurrence/*') ? ' active' : '' }}" href="{{ route('recurrence.index') }}">
                        <i class="nav-main-link-icon fa fa-recycle"></i>
                        <span class="nav-main-link-name">Recurrence</span>
                    </a>
                </li>
                @endcan
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('notifications','notifications/*') ? ' active' : '' }}" href="{{ route('notifications.index')}}">
                        <i class="nav-main-link-icon fa fa-bell"></i>
                        <span class="nav-main-link-name">Notification
                            @php
                                $unreadNotificationsCount = count(Auth::user()->unreadNotifications)
                            @endphp
                            @if ($unreadNotificationsCount == 0)
                              {{-- <i class="material-icons">0</i> --}}
                            @else
                            <span class="badge badge-info">  <span class="notification">{{ $unreadNotificationsCount }}</span></span>
                            @endif
                        </span>
                    </a>
                </li>
                @can('approval_access')
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('approvals','approvals/*') ? ' active' : '' }}" href="{{ route('approvals.index') }}">
                        <i class="nav-main-link-icon fa fa-universal-access"></i>
                        <span class="nav-main-link-name">Approval</span>
                    </a>
                </li>
                @endcan
                @can('department_access')
                <li class="nav-main-item"  style="margin-top:auto">
                    <a class="nav-main-link {{ request()->is('departments','departments/*') ? ' active' : '' }}" href="{{ route('departments.index') }}">
                        <i class="nav-main-link-icon fa fa-university"></i>
                        <span class="nav-main-link-name">Departments</span>
                    </a>
                </li>
                @endcan
                @can('user_access')
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('users','users/*') ? 'active' : '' }}" href="{{ route('users.index') }}">
                        <i class="nav-main-link-icon si si-user"></i>
                        <span class="nav-main-link-name">Staff</span>
                    </a>
                </li>
                @endcan
                @hasrole(7)
                <li class="nav-main-item {{ request()->is('roles','permissions','roles/*','permissions/*') ? ' open' : '' }}">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                        <i class="nav-main-link-icon fa fa-lock"></i>
                        <span class="nav-main-link-name">Roles</span>
                    </a>
                    <ul class="nav-main-submenu">
                        @can('role_access')
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ request()->is('roles','roles/*') ? 'active' : '' }}" href="{{ route('roles.index') }}">
                                <span class="nav-main-link-name">Roles</span>
                            </a>
                        </li>
                        @endcan
                        @can('permission_access')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('permissions','permissions/*') ? ' active' : '' }}" href="{{route('permissions.index')}}">
                                <span class="nav-main-link-name">Permission</span>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>
               
                @endhasrole
                @can('settings_access')
                <li class="nav-main-item">
                    <a class="nav-main-link {{ request()->is('settings/*') ? ' active' : '' }}" href="{{ route('settings.create') }}">
                        <i class="nav-main-link-icon fa fa-cog"></i>
                        <span class="nav-main-link-name">Settings</span>
                    </a>
                </li>
                @endcan
              </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- END Sidebar Scrolling -->
</nav>
<!-- END Sidebar -->
