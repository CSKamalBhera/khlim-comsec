 <div id="app" class="flex flex-col min-h-screen w-screen bg-cover">
    @include('layouts.navbars.guest')
    @include('layouts.partials.alert')
    @yield('content')
    @include('layouts.footers.guest')
</div>
