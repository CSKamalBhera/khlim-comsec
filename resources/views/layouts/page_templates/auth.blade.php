<div id="page-container" class="sidebar-o enable-page-overlay sidebar-dark side-scroll page-header-fixed main-content-narrow">
    @include('layouts.navbars.auth')
    @include('layouts.navbars.material_sidebar')
    @include('layouts.partials.alert')
    <!-- Main Container -->
    <main id="main-container">
        @yield('content')
    </main>
    <!-- END Main Container -->
    @include('layouts.footers.auth')
</div> 
