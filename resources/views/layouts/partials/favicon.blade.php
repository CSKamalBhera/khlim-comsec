<link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png') }}">
<link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">
<link rel="mask-icon" href="{{ asset('media/favicons/favicon.png') }}" color="#00bcd4">
<meta name="msapplication-TileColor" content="#00bcd4">
<meta name="theme-color" content="#ffffff">