@extends('layouts.app', [
'activePage' => 'settings',
'menuParent' => 'settings',
'title' => 'Settings',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Settings<small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Settings</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx text-black">List</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Basic -->
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">Settings</h3>
        </div>
        <div class="block-content block-content-full">
            <form action="{{ route('settings.store') }}" method="POST" enctype="multipart/form-data" id="sett">
                @csrf
                <div class="row">
                    <div class="col-lg-4">
                        <p class="font-size-sm text-muted">
                            Website Settings
                        </p>
                    </div>
                    <div class="col-lg-8 col-xl-8">
                        <div class="form-group">
                            <label for="example-text-input">Company Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Company Name" value="{{ $settingSystem ? $settingSystem->name : old('name') }}">
                            @error('name') 
                            <span class="invalid-feedback" role="alert"> 
                                <strong>{{ $message }}</strong> 
                            </span> 
                            @enderror 
                        </div>
                        <div class="form-group">
                            <label for="example-file-input">Company Logo</label>
                            <img  src="{{ $settingSystem ? asset($settingSystem->logo) : asset('assets/img/default.png') }}" width="70px" height="50px" class="rounded-lg"/>
                            <input type="file" class="hidden" name="logo" title="company logo">
                        </div>
                        <div class="form-group">
                            <label for="example-password-input">Trems & Conditions<span class="text-danger">*</span></label>
                            <textarea  class="form-control @error('termsCondition') is-invalid @enderror" id="example-password-input" name="termsCondition">{{ $settingSystem ? $settingSystem->description : old('termsCondition') }}</textarea>
                            @error('termsCondition') 
                            <span class="invalid-feedback" role="alert"> 
                                <strong>{{ $message }}</strong> 
                            </span> 
                            @enderror 
                        </div>
                    </div>
                </div>
                @if (!$settings->isEmpty())
                @php $set = array(); @endphp
                @foreach($settings as $key => $setting)
                @php $i = $key; $set[]= $setting->department_id;@endphp
                <div class="row push">
                    <input type="hidden" name="settings[id][]" value="{{ $setting->id }}"/>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="example-text-input">Department<span class="text-danger">*</span></label>
                            <select type="text" id="dep{{$i}}" class="dep form-control @error('settings.department_id.'.$i) is-invalid @enderror" name="settings[department_id][]" placeholder="select department" data-value="{{$setting->department_id}}">
                                <option value="" >Select</option>
                                @foreach($departments as $department)
                                @php
                                $disabled = '';
                                if(empty(old('settings')['department_id'])){
                                if(isset($department->settings)){
                                if($department->id == $department->settings->department_id){
                                if($department->id != $setting->department_id){
                                $disabled = 'disabled';
                                }
                                }
                                }
                                }
                                $dept_id = (!empty(old('settings')['department_id'][$i])) ? old('settings')['department_id'][$i] :$setting->department_id;
                                if(!empty(old('settings')['department_id']) && in_array($department->id,old('settings')['department_id'])){
                                if($dept_id != $department->id){
                                $disabled = 'disabled';
                                }
                                }

                                @endphp
                                <option value="{{ $department->id}}" @if ($department->id == $dept_id) selected @endif {{$disabled}}>{{  $department->name }}</option>
                                @endforeach
                            </select>
                            @error('settings.department_id.'.$i) 
                            <span class="invalid-feedback" role="alert"> 
                                <strong>{{ $message }}</strong> 
                            </span> 
                            @enderror
                            <div id='d' class='text-danger'></div>

                        </div>
                        <button type="button" value="{{ $setting->id }}" class="removeRow1 btn" {{($i == 0) ? 'disabled' : ''}}><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </div>
                    <div class="col-lg-8 col-xl-8">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Company Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('settings.companyName.'.$i) is-invalid @enderror" name="settings[companyName][]" placeholder="Company Name" value="{{ old('settings')['companyName'][$i] ?? $setting->name }}">
                                    @error('settings.companyName.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Tax No.<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('settings.tax_no.'.$i) is-invalid @enderror" name="settings[tax_no][]" placeholder="Tax No." value="{{ old('settings')['tax_no'][$i] ?? $setting->tax_no}}">
                                    @error('settings.tax_no.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Company Phone<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('settings.companyPhone.'.$i) is-invalid @enderror" name="settings[companyPhone][]" placeholder="Company Phone" value="{{ old('settings')['companyPhone'][$i] ?? $setting->phone_no }}">
                                    @error('settings.companyPhone.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Company Email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control  @error('settings.companyEmail.'.$i) is-invalid @enderror" name="settings[companyEmail][]" placeholder="Company Email" value="{{ old('settings')['companyEmail'][$i] ?? $setting->email }}">
                                    @error('settings.companyEmail.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Company Address<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyAddress.'.$i) is-invalid @enderror" name="settings[companyAddress][]">{{ old('settings')['companyAddress'][$i] ?? $setting->address }}</textarea>
                                    @error('settings.companyAddress.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Comments<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyComment.'.$i) is-invalid @enderror" name="settings[companyComment][]" >{{ old('settings')['companyComment'][$i] ?? $setting->comment }}</textarea>
                                    @error('settings.companyComment.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Invoice Header<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyHeader.'.$i) is-invalid @enderror" name="settings[companyHeader][]">{{ old('settings')['companyHeader'][$i] ?? $setting->invoice_header }}</textarea>
                                    @error('settings.companyHeader.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Invoice Footer<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyFooter.'.$i) is-invalid @enderror" name="settings[companyFooter][]">{{ old('settings')['companyFooter'][$i] ?? $setting->invoice_footer }}</textarea>
                                    @error('settings.companyFooter.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach


                @if(!empty(old('settings')['department_id']))
                @if($key+1 != count(old('settings')['department_id']))
                @php
                $count = count(old('settings')['department_id']);
                @endphp
                @for($i = $key+1; $i < $count; $i++) 
                {{--   @if(!in_array(old('settings')['department_id'][$i],$set))  --}}
                <div class="row  d{{$i}}">

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="example-text-input">Department<span class="text-danger">*</span></label>
                            <select type="text" class="dep form-control @error('settings.department_id.'.$i) is-invalid @enderror" name="settings[department_id][{{$i}}]">
                                <option value="" >Select</option>
                                @foreach($departments as $department)

                                @php
                                $selected = '';
                                $disabled = '';
                                $dept_id = (!empty(old('settings')['department_id'][$i])) ? old('settings')['department_id'][$i] :'';
                                if($dept_id == $department->id){
                                $selected = 'selected';
                                }
                                if(!empty(old('settings')['department_id']) && in_array($department->id,old('settings')['department_id'])){
                                if($dept_id != $department->id){
                                $disabled = 'disabled';
                                }
                                }
                                @endphp
                                <option value="{{ $department->id }}" {{$selected}} {{$disabled}}>{{  $department->name }}</option>
                                @endforeach
                            </select>
                            @error('settings.department_id.'.$i) 
                            <span class="invalid-feedback" role="alert"> 
                                <strong>{{ $message }}</strong> 
                            </span> 
                            @enderror 
                        </div>
                        <button type="button"  class="removeRow btn" {{($i == 0) ? 'disabled' : ''}}><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </div>

                    <div class="col-lg-8 col-xl-8">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Company Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control  @error('settings.companyName.'.$i) is-invalid @enderror" name="settings[companyName][{{$i}}]" value="{{old('settings')['companyName'][$i]??''}}" placeholder="Company Name"/>
                                    @error('settings.companyName.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Tax No.<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('settings.tax_no.'.$i) is-invalid @enderror" name="settings[tax_no][{{$i}}]" value="{{old('settings')['tax_no'][$i]??''}}" placeholder="Tax No." />
                                    @error('settings.tax_no.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Company Phone<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('settings.companyPhone.'.$i) is-invalid @enderror" name="settings[companyPhone][{{$i}}]" value="{{old('settings')['companyPhone'][$i]??''}}" placeholder="Company Phone" />
                                    @error('settings.companyPhone.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Company Email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('settings.companyEmail.'.$i) is-invalid @enderror" name="settings[companyEmail][{{$i}}]" value="{{old('settings')['companyEmail'][$i]??''}}" placeholder="Company Email" />
                                    @error('settings.companyEmail.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Company Address<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyAddress.'.$i) is-invalid @enderror" name="settings[companyAddress][{{$i}}]">{{old('settings')['companyAddress'][$i]??''}}</textarea>
                                    @error('settings.companyAddress.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Comments<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyComment.'.$i) is-invalid @enderror" name="settings[companyComment][{{$i}}]" >{{old('settings')['companyComment'][$i]??''}}</textarea>
                                    @error('settings.companyComment.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Invoice Header<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyHeader.'.$i) is-invalid @enderror" name="settings[companyHeader][{{$i}}]">{{old('settings')['companyHeader'][$i]??''}}</textarea>
                                    @error('settings.companyHeader.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Invoice Footer<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyFooter.'.$i) is-invalid @enderror" name="settings[companyFooter][{{$i}}]">{{old('settings')['companyFooter'][$i]??''}}</textarea>
                                    @error('settings.companyFooter.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- @endif --}}
                @endfor
                @endif
                @endif
                {{-- new create --}}
                <div class="col-lg-12 container1">
                </div>
                @if(!empty(old('settings')['department_id']))
                <input type="hidden" id="counter1" value="{{count(old('settings')['department_id'])-1}}">
                @else
                <input type="hidden" id="counter1" value="{{ $key }}">
                @endif


                <div class="col-md-12 m-3" id="addnew_feature">
                    <a href="#" class="edit btn btn-icon btn-secondary btn-sm mr-2 report">Add More</a>

                </div>
                @else

                @php
                if(!empty(old('settings')['department_id'])){
                $count = count(old('settings')['department_id']);
                }else{
                $count = 1;
                }
                @endphp
                @for($i = 0; $i < $count; $i++) 
                <div class="row push">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="example-text-input">Department<span class="text-danger">*</span></label>
                            <select type="text" class="work form-control @error('settings.department_id.'.$i) is-invalid @enderror" name="settings[department_id][]">
                                <option value="" >Select</option>
                                @foreach($departments as $department)
                                @php
                                $selected = '';
                                $disabled = '';
                                $dept_id = (!empty(old('settings')['department_id'][$i])) ? old('settings')['department_id'][$i] :'';
                                if($dept_id == $department->id){
                                $selected = 'selected';
                                }
                                if(!empty(old('settings')['department_id']) && in_array($department->id,old('settings')['department_id'])){
                                if($dept_id != $department->id){
                                $disabled = 'disabled';
                                }
                                }
                                @endphp
                                <option value="{{ $department->id }}" {{$selected}} {{$disabled}}>{{  $department->name }}</option>
                                @endforeach
                            </select>
                            @error('settings.department_id.'.$i) 
                            <span class="invalid-feedback d" role="alert"> 
                                <strong>{{ $message }}</strong> 
                            </span> 
                            @enderror 
                        </div>
                        <button type="button"  class="removeRow btn" {{($i == 0) ? 'disabled' : ''}}><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </div>

                    <div class="col-lg-8 col-xl-8">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Company Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control  @error('settings.companyName.'.$i) is-invalid @enderror" name="settings[companyName][]" value="{{old('settings')['companyName'][$i]??''}}" placeholder="Company Name"/>
                                    @error('settings.companyName.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Tax No.<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('settings.tax_no.'.$i) is-invalid @enderror" name="settings[tax_no][]" value="{{old('settings')['tax_no'][$i]??''}}" placeholder="Tax No." />
                                    @error('settings.tax_no.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Company Phone<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('settings.companyPhone.'.$i) is-invalid @enderror" name="settings[companyPhone][]" value="{{old('settings')['companyPhone'][$i]??''}}" placeholder="Company Phone" />
                                    @error('settings.companyPhone.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Company Email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('settings.companyEmail.'.$i) is-invalid @enderror" name="settings[companyEmail][]" value="{{old('settings')['companyEmail'][$i]??''}}" placeholder="Company Email" />
                                    @error('settings.companyEmail.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Company Address<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyAddress.'.$i) is-invalid @enderror" name="settings[companyAddress][]">{{old('settings')['companyAddress'][$i]??''}}</textarea>
                                    @error('settings.companyAddress.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Comments<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyComment.'.$i) is-invalid @enderror" name="settings[companyComment][]" >{{old('settings')['companyComment'][$i]??''}}</textarea>
                                    @error('settings.companyComment.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-text-input">Invoice Header<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyHeader.'.$i) is-invalid @enderror" name="settings[companyHeader][]">{{old('settings')['companyHeader'][$i]??''}}</textarea>
                                    @error('settings.companyHeader.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="example-file-input">Invoice Footer<span class="text-danger">*</span></label>
                                    <textarea class="form-control @error('settings.companyFooter.'.$i) is-invalid @enderror" name="settings[companyFooter][]">{{old('settings')['companyFooter'][$i]??''}}</textarea>
                                    @error('settings.companyFooter.'.$i) 
                                    <span class="invalid-feedback" role="alert"> 
                                        <strong>{{ $message }}</strong> 
                                    </span> 
                                    @enderror 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor  

                <div class="col-lg-12 container">
                </div>
                <input type="hidden" id="counter2" value="0">
                <div class="col-md-12 m-3" id="addnew_feature">
                    <a href="#" class="insert btn btn-icon btn-secondary btn-sm mr-2 report">Add More</a>

                </div>
                @endif

                <div class="col-md-12 m-3">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <!-- END Basic -->

</div>
<!-- END Page Content -->
@endsection
@push('scripts')
<script type="text/javascript">

    $(document).ready(function () {

        $('body').on('change', '.work', function () {
            $('select option').removeAttr('disabled', 'disabled');
            var t = parseInt($("#counter2").val());
            var u = t - 1;
            var w = u - 1;
            var du = $('select', '.p-' + u + '').val();
            var dw = $('select', '.p-' + w + '').val();

            var dl = $('select', '.p-' + t + '').val();
            $('select option[value="' + dl + '"]', '.push').attr('disabled', 'disabled');
            $('select option[value="' + dl + '"]', '.p-' + u + '').attr('disabled', 'disabled');
            $('select option[value="' + dl + '"]', '.p-' + w + '').attr('disabled', 'disabled');
            $('select option[value="' + du + '"]', '.p-' + w + '').attr('disabled', 'disabled');
            $('select option[value="' + du + '"]', '.push').attr('disabled', 'disabled');
            $('select option[value="' + dw + '"]', '.push').attr('disabled', 'disabled');
            var dc = $('select', '.push').val();
            $('select option[value="' + dc + '"]', '.p-' + u + '').attr('disabled', 'disabled');
            $('select option[value="' + dc + '"]', '.p-' + w + '').attr('disabled', 'disabled');
            $('select option[value="' + dc + '"]', '.p-' + t + '').attr('disabled', 'disabled');
            $('select option[value="' + dw + '"]', '.p-' + u + '').attr('disabled', 'disabled');
            $('select option[value="' + dw + '"]', '.p-' + t + '').attr('disabled', 'disabled');
            $('select option[value="' + du + '"]', '.p-' + t + '').attr('disabled', 'disabled');
        });

        $(".insert").click(function () {
            var t = parseInt($("#counter2").val()) + 1;
            var tds = '<div class="row p-' + t + '">' + $(".push").html() + '</div>';
            if ($('.push').length > 0) {
                $('.container').append(tds);
                $('select', '.p-' + t + '').val("");
                var u = t - 1;
                var w = u - 1;
                var x = w - 1;
                var dp = $('select', '.push').val();
                var du = $('select', '.p-' + u + '').val();
                var dw = $('select', '.p-' + w + '').val();
                $('select option[value="' + dp + '"]', '.p-' + t + '').attr('disabled', 'disabled');
                $('select option[value="' + du + '"]', '.p-' + t + '').attr('disabled', 'disabled');
                $('select option[value="' + dw + '"]', '.p-' + t + '').attr('disabled', 'disabled');
                $('select option[value="' + du + '"]', '.p-' + w + '').attr('disabled', 'disabled');
                $('select option[value="' + du + '"]', '.push').attr('disabled', 'disabled');
                $('select option[value="' + dw + '"]', '.push').attr('disabled', 'disabled');
                $('.p-' + t).find("span.invalid-feedback").remove();
                $('.is-invalid', '.p-' + t).removeClass();
                $('button', '.p-' + t).removeAttr('disabled');
                $("#counter2").val(t);
            } else {
                $(this).append(tds);
            }
        });

        // remove row
        $(document).on('click', '.removeRow', function () {
            $(this).closest('.row').remove();
            var count3 = parseInt($("#counter2").val()) - 1;
            $("#counter2").val(count3);
        });
    });

    $(document).ready(function () {
        $('body').on('change', '.dep', function () {
            var i = parseInt($("#counter1").val());
            var j = i - 1;
            var g = j - 1;
            var h = g - 1;
            var D2 = $('select', '.d' + g + '').val();
            var d3 = $('select', '.d' + i + '').val();
            var d4 = $('select', '.d' + j + '').val();

            $('select option[value="' + D2 + '"]', '.push').attr('disabled', 'disabled');
            $('select option[value="' + d3 + '"]', '.push').attr('disabled', 'disabled');
            $('select option[value="' + d4 + '"]', '.push').attr('disabled', 'disabled');
            $('select option[value="' + d3 + '"]', '.d' + j + '').attr('disabled', 'disabled');
            $('select option[value="' + D2 + '"]', '.d' + j + '').attr('disabled', 'disabled');
            $('select option[value="' + d4 + '"]', '.d' + i + '').attr('disabled', 'disabled');
            $('select option[value="' + D2 + '"]', '.d' + i + '').attr('disabled', 'disabled');
            $('select option[value="' + d3 + '"]', '.d' + g + '').attr('disabled', 'disabled');
            $('select option[value="' + d4 + '"]', '.d' + g + '').attr('disabled', 'disabled');
            var dp = $(this).val();
            var d = $(this).data('value');
            $('select', '.d' + i + '').data('myvalue', dp);
            var current = $(this);
            $('select', 'body').each(function () {
                var ap = $('#' + $(this).attr('id')).val();
                $('select option[value="' + ap + '"]', '.d' + i + '').attr('disabled', 'disabled');
                $('select option[value="' + ap + '"]', '.d' + j + '').attr('disabled', 'disabled');
                $('select option[value="' + ap + '"]', '.d' + g + '').attr('disabled', 'disabled');
                $('select option[value="' + ap + '"]', '.d' + h + '').attr('disabled', 'disabled');
                if ($(this).val() != current.val())
                {
                    $('#' + $(this).attr('id') + ' option[value="' + dp + '"]').attr('disabled', 'disabled');
                    $('#' + $(this).attr('id') + ' option[value="' + d + '"]').removeAttr('disabled', 'disabled');
                    $('select option[value="' + d + '"]', '.d' + i + '').removeAttr('disabled', 'disabled');
                    $('select option[value="' + d + '"]', '.d' + j + '').removeAttr('disabled', 'disabled');
                    $('select option[value="' + d + '"]', '.d' + g + '').removeAttr('disabled', 'disabled');
                }
            });
        });
        $(".edit").click(function () {
            var i = parseInt($("#counter1").val()) + 1;
            var tds = '<div class="row d' + i + '">' + $(".push").html() + '</div>';
            $('.container1').append(tds);

            var j = i - 1;
            var g = j - 1;
            var h = g - 1;
            $('select', '.d' + i + '').removeAttr("id");
            $('select', '.d' + i + '').removeAttr("data-value");
            var dj = $('select', '.d' + j + '').val();
            var d2 = $('select', '.d' + g + '').val();
            $('select', '.d' + i + '').attr('data-myvalue', 38)

//            $('select option[value="' + dj + '"]', '.d' + i + '').attr('disabled', 'disabled');
//            $('select option[value="' + d2 + '"]', '.d' + i + '').attr('disabled', 'disabled');
//            $('select option[value="' + dj + '"]', '.push').attr('disabled', 'disabled');
//            $('select option[value="' + d2 + '"]', '.push').attr('disabled', 'disabled');
            $('select option:selected', '.d' + i + '').attr('disabled', 'disabled');
            for (var l = 0; l < i; l++) {
                var dp = $('#dep' + l + '', 'body').val();
                var d = $('#dep' + l + '', 'body').data('value');
                if (d != dp) {
                    $('select option[value="' + dp + '"]', '.d' + i + '').attr('disabled', 'disabled');
                }
                if (d != dp) {
                    $('select option[value="' + d + '"]', '.d' + i + '').removeAttr('disabled', 'disabled');
                }
            }
            $('select', '.d' + i + '').val("");
            $('input', '.d' + i + '').val("");
            $('textarea', '.d' + i + '').val("");
            $("#counter1").val(i);
            $('.d' + i + '').find("span.invalid-feedback").remove();
            $('.is-invalid', '.d' + i + '').removeClass('is-invalid');
            $('button', '.d' + i).removeAttr('disabled');
            $('button', '.d' + i).removeAttr('value');
            $('button', '.d' + i).removeClass('removeRow1');
            $('button', '.d' + i).addClass('removeSetting');
        });
        // remove row and check data available on billings
        $(document).on('click', '.removeRow1', function () {
            swal({
                title: `Are you sure you want to delete ?`,
                text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            var idSetting = $(this).val();
                            if (idSetting) {
                                $.ajax({
                                    type: 'get',
                                    url: "{{route('deleteSetting')}}",
                                    data: 'id=' + idSetting,
                                    success: function (data) {
                                        console.log(data);
                                        if (data.success == false) {
                                            location.reload();
                                        }
                                        if (data.success == true) {
                                            $(this).closest('.row').remove();
                                            var count1 = parseInt($("#counter1").val()) - 1;
                                            $("#counter1").val(count1);
                                            location.reload();
                                        }
                                    },
                                });
                            }
                        }
                    });
        });
        // remove row for new append in edit
        $(document).on('click', '.removeSetting', function () {
            $(this).closest('.row').remove();
            var count2 = parseInt($("#counter1").val()) - 1;
            $("#counter1").val(count2);
        });
    });
</script>
@endpush