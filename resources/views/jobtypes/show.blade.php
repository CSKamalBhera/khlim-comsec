@extends('layouts.app', [
'activePage' => 'JobType-show',
'menuParent' => 'JobType-show',
'title' => 'JobType Show',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-12 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    {{__('Job Type')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('jobtype.index') }}">Job Type</a></li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">{{__('Details')}}</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content w-100">
    <div class="block block-rounded">
        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#btabs-home">General</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#btabs-workflow">
                    Work Flow
                </a>
            </li>
        </ul>
        <div class="block-content tab-content">
            <div class="tab-pane active" id="btabs-home" role="tabpanel">
                <div class="block-content block-content-full ml-12 ">
                    <div class="row">
                        <div class="col-md-6">
                            <dt>{{ __('JobType Name') }}</dt>
                            <dd>{{ucwords($jobtype->name)}}</dd>
                        </div>
                        <div class="col-md-6">
                            <dt>{{ __('Department') }}</dt>
                            <dd>{{ucwords($jobtype->department->name) }}</dd>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="btabs-workflow" role="tabpanel">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="inputFormRow">
                            @foreach($jobtype->workflows as $key => $workflow)
                            <div class="row">
                                <div class="col-md-4">
                                    <dd><label>{{$key+1}}. Title: </label> {{ucwords($workflow->title)}}</dd>
                                </div>
                                <div class="col-md-4">
                                    <dd><label>Type: </label> {{ucwords($workflow->workflow_type->name)}}</dd>
                                </div>
                                <div class="col-md-4">
                                    <dd> <label>Option: </label>
                                        @foreach($workflow->workflow_type->workflowoption as $key => $option)
                                        {{ ucwords($option->options) }}@if($loop->last) @else {{__(',') }} @endif
                                        @endforeach
                                    </dd>
                                </div>  
                            </div><!-- /.row -->
                            @if($workflow->workflowNotify)

                            @if($workflow->workflow_type_id == 3 || $workflow->workflow_type_id == 7 || $workflow->workflow_type_id == 18)
                            @php
                            $span = $workflow->workflow_type_id == 3 ? 'Hours':'Days';
                            @endphp
                            <div class="row add py-1">
                                <div class="col-md-4 offset-4">
                                    @php
                                    $notify = json_decode($workflow->workflowNotify->notification_value);
                                    $value = $workflow->workflow_type_id == 3 ? $notify->hours : $notify->days;
                                    @endphp
                                    <label> Notify,If more than</label></div><div class="col-md-4">{{($value !== NULL)?$value:'N/A '}} {{$span}}
                                </div>
                            </div>
                            @endif
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection
