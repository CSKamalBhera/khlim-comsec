@extends('layouts.app', [
'activePage' => 'jobtype-list',
'menuParent' => 'jobtype-list',
'title' => 'JobType',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-8 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    {{__('Job Type')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Job Type</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">{{__('List')}}</a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="col-sm-4">
                @can('jobtype_create')
                <div class="text-right">
                    <a href="{{ route('jobtype.create') }}" class="btn btn-primary">Create</a>
                </div>
                @endcan
            </div>
        </div>
    </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full   font-size-sm">
                <thead>
                    <tr>
                        <th class="text-left" style="width: 80px;">#</th>
                        <th>Title</th>
                        <th>Department</th>
                        <th>Created</th>
                        <th>Modified</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($jobtypes as $key => $jobtype)
                    <tr>
                        <td class="text-left">{{ $key +1 }}</td>
                        <td class="font-w600">
                            <a href="{{ route('jobtype.show',$jobtype->id) }}">{{ ucwords($jobtype->name) }}</a>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <span class="text-muted">{{ ucwords($jobtype->department->name) }}</span>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <span class="text-muted">{{ Carbon\Carbon::parse($jobtype->created_at)->format('d-m-Y') }}</span>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <span class="text-muted">{{ Carbon\Carbon::parse($jobtype->updated_at)->format('d-m-Y')}}</span>
                        </td>

                        <td class="text-center">
                            <div class="btn-group">
                                @can('jobtype_edit')
                                <a href="{{ route('jobtype.edit',$jobtype->id) }}" class="btn btn-sm btn-light" ><i class="fa fa-edit"></i></a>
                                @endcan
                                @can('jobtype_delete')
                                <form action="{{ route('jobtype.destroy',$jobtype->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-light delete" data-name="{{ $jobtype->name }}"
                                            title="{{__('Delete')}}">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->
@endsection
