@extends('layouts.app', [
'activePage' => 'create-jobtype',
'menuParent' => 'create-jobtype',
'title' => 'Create Job Type',
])
@section('content')

<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-12 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    {{__('Job Type')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('jobtype.index') }}">Job Type</a></li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">Create</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#btabs-home">General</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#btabs-workflow">WorkFlow</a>
            </li>
        </ul>
        <form action="{{ route('jobtype.store') }}" method="POST">
            @csrf
            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-home" role="tabpanel">
                    <div class="block-content block-content-full">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="name">Title<span class="text-danger">*</span></label>
                                    <input type="text"  class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name')}}" placeholder="Name" >
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="department">Departments<span class="text-danger">*</span></label>
                                    <select  class="department select2 form-control @error('department_id') is-invalid @enderror" id="department_id" name="department_id" style="width: 100%;" data-placeholder="Choose" >
                                        <option value="">{{__('Please Select')}}</option>
                                        @foreach($departments as $department)
                                        <option value="{{ $department->id }}" @if ($department->id == old('department_id')) selected @endif>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('department_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <a class="btn btn-secondary btnNext">Next <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-workflow" role="tabpanel">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="inputFormRow">
                                @php
                                if(!empty(old('flow'))){
                                $count = count(old('flow'));
                                }else{
                                $count = 1;
                                }
                                @endphp
                                @for($i = 1; $i <= $count; $i++) 
                                <div class="form-group jobtype_workflow_row" id="job-{{$i}}">
                                    <div class="row r-{{$i}}">
                                        <div class="col-md-4 col-xl-4">
                                            <label for="title">Title<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control @error('flow.'.$i.'.title') is-invalid @enderror"  name="flow[{{$i}}][title]"  value="{{old('flow')[$i]['title'] ?? ''}}" placeholder="Workflow Title" >
                                            @error('flow.'.$i.'.title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-3 col-xl-3">
                                            <label for="type">Type<span class="text-danger">*</span></label>
                                            <select data-option_id="{{$i}}" class="work form-control  @error('flow.'.$i.'.type') is-invalid @enderror" id="workflow_type-{{$i}}"  name="flow[{{$i}}][type]" style="width: 100%;" >
                                                <option value="">{{__('Please Select')}}</option>
                                                @if(!empty(old('department_id')))
                                                @php
                                                $types = \App\Models\Jobs\WorkflowType::where('department_id',old('department_id'))->get();
                                                @endphp
                                                @foreach($types as $type)
                                                @php
                                                $selected = '';
                                                $type_name = (!empty(old('flow'))) ? old('flow')[$i]['type'] :'';
                                                if($type->id == $type_name){
                                                $selected = 'selected';
                                                }
                                                @endphp
                                                <option value="{{ $type->id }}" {{ $selected }}>{{ $type->name}}</option>
                                                @endforeach
                                                @endif      
                                            </select>
                                            @error('flow.'.$i.'.type')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4 col-xl-3">
                                            <label for="option">Option<span class="text-danger">*</span></label>
                                            <select id="option-{{$i}}" class="select2 form-control  @error('flow.'.$i.'.option') is-invalid @enderror"   name="flow[{{$i}}][option][]" style="width: 100%;" data-placeholder="Choose many.."  multiple="multiple" >
                                                @if(isset(old('flow')[$i]['type']))
                                                @php
                                                $optionsWorkflow = \App\Models\Jobs\WorkflowOption::where('workflow_types_id',old('flow')[$i]['type'])->get();
                                                @endphp
                                                @foreach ($optionsWorkflow as $option)
                                                <option 
                                                    @if (old('flow')[$i]['type'] == $option->workflow_types_id) selected @endif >{{ $option->options }}</option>                                            
                                                @endforeach
                                                @endif
                                            </select>
                                            @error('flow.'.$i.'.option')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-1 col-xl-2">
                                            <label for="Action">Action</label>
                                            <div class="row" role="group" aria-label="Basic example" id="newRow">
                                                <button type="button" class="btn mx-2 btn-sm btn-primary add-more" title="{{__('Add More')}}"><i class="fa fa-plus"></i></button>
                                                <button type="button" class="btn btn-sm btn-danger removeRow" title="{{__('Remove')}}" {{($i == 1) ? 'disabled' : ''}}><i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- old value --}}

                                    @if(isset(old('flow')[$i]['type']))
                                    @php $j=0; @endphp
                                    @if(old('flow')[$i]['type'] == 3 || old('flow')[$i]['type'] == 7 || old('flow')[$i]['type'] == 18)
                                    @php
                                    $span = old('flow')[$i]['type'] == 3 ? 'Hours':'Days';
                                    @endphp
                                    <div class="row add py-1">
                                        <div class="col-md-3 col-xl-3 offset-md-4 offset-xl-4">
                                            @php $j++; @endphp
                                            <label> Notify,If more than</label></div><div class="col-md-4 col-xl-3 input-group"><input class="form-control notify @error('flow.'.$i.'.notify_'.$span) is-invalid @enderror" id="notify{{$i.$j.$span}}" type="text" name="flow[{{$i}}][notify_{{$span}}]" value="{{ old('flow')[$i]['notify_'.$span]??''}}" placeholder="{{$span}}">
                                            <div class="input-group-append"><span class="input-group-text">{{$span}}</span></div>
                                            @error('flow.'.$i.'.notify_'.$span)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span> 
                                            @enderror
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </div>
                                @endfor
                            </div>
                            @if(!empty(old('flow')))
                            <input type="hidden" id="counter" value="{{count(old('flow'))}}">
                            @else
                            <input type="hidden" id="counter" value="1">
                            @endif
                        </div>
                    </div>
                    <div class="text-right mb-3">
                        <a class="btn btn-light btnPrevious"><i class="fa fa-angle-left"></i> Previous</a>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>                
</div>
<!-- END Page Content -->
@endsection
@push('scripts')
<script type="text/javascript">

    $(document).ready(function() {
    // add row
    $(document).on('click', ".add-more", function () {
    var count = parseInt($("#counter").val()) + 1;
    var html = '';
    html += '<div class="form-group jobtype_workflow_row" id="job-' + count + '">';
    html += '<div class="row r-' + count + '">';
    html += '<div class="col-md-4 col-xl-4">';
    html += '<input type="text" class="form-control @error('flow.'.' + count + '.'.title') is-invalid @enderror" name="flow[' + count + '][title]" value="{{old('flow')[' + count + ']['title'] ?? ''}}" placeholder="Workflow Title" >';
    html += '@error('flow.'.' + count + '.'.title')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror';
    html += '</div>';
    html += '<div class="col-md-4 col-xl-3">';
    html += '<select data-option_id="' + count + '" class="work form-control  @error('flow.'.' + count + '.'.type') is-invalid @enderror"  id="workflow_type-' + count + '" name="flow[' + count + '][type]" style="width: 100%;"> <option value="">Please Select</option>  </select>';
    html += '@error('flow.'.' + count + '.'.type')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror';
    html += '</div>';
    html += '<div class="col-md-4 col-xl-3">';
    html += '<select class="opt select2 form-control  @error('flow.'.' + count + '.'.option') is-invalid @enderror"  id="option-' + count + '" name="flow[' + count + '][option][]" style="width: 100%;"  data-placeholder="Choose many.." multiple> </select>';
    html += '@error('flow.'.' + count + '.'.option')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror';
    html += '</div>';
    html += '<div class="col-md-1 col-xl-2">';
    html += '<div class="row">';
    html += '<button type="button" class="btn btn-sm mx-2 btn-primary add-more" title="{{__('Add More')}}"><i class="fa fa-plus"></i></button>';
    html += '<button type="button" class="btn btn-sm btn-danger removeRow" title="{{__('Remove')}}" {{($i == 0) ? 'disabled' : ''}}><i class="fa fa-minus"></i></button>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    $('#inputFormRow').append(html);
    $("#counter").val(count);
    $('.select2').select2();
    });
    // remove row
    $(document).on('click', '.removeRow', function () {
    var count = parseInt($("#counter").val()) - 1;
    $("#counter").val(count);
    $(this).closest('.jobtype_workflow_row').remove();
    });
    $('.select2').select2();
    });</script>

<script type="text/javascript">
    // CSRF Token
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function(){
    $(document).on('change', '.work', function(){
    var workflowType = $(this).val();
    var workflowOption = $(this).data('option_id');
    $('#option-' + workflowOption).empty();
    if (workflowType){
    $.ajax({
    type:'get',
            url:"{{route('getOptions')}}",
            data:'type=' + workflowType,
            success:function(data){
            var html = '<option disabled>Select options</option>';
            html += data;
            $('#option-' + workflowOption).html(html);
            }
    });
    $.ajax({
    type:'get',
            url:"{{route('getInputs')}}",
            data:'type=' + workflowType + '&id=' + workflowOption,
            success:function(data){
            $('.add', '#job-' + workflowOption).remove();
            var html = data;
            $(html).insertAfter('.r-' + workflowOption);
            }
    });
    } else{
    $('#option').html('<option value="">Select Type first</option>');
    }
    });
    $('.checkbox input[type=checkbox]').on('click', function(){
    $('.notify').attr("disabled", !this.checked);
    });
    // workflow type dependency on Deparment
    $(document).on('change', '.department', function(){
    var department = $(this).val();
    $('.work').empty();
    if (department){
    $.ajax({
    type:'get',
            url:"{{route('getTypes')}}",
            data:'department=' + department,
            success:function(data){
            var html = '<option value="">Select Type</option>';
            html += data;
            $('.work').html(html);
            }
    });
    };
    });
    // workflow type dependency on Deparment when new append
    $(document).on('focus', '.work', function(){
    var typeid = $(this).attr('id'); console.log($('#' + typeid).val());
    var department = $('.department').val();
//      $('#' + $(this).attr('id')).empty();
    if (!$('#' + typeid).val()){
    if (department){
    $.ajax({
    type:'get',
            url:"{{route('getTypes')}}",
            data:'department=' + department,
            success:function(data){
            var html = '<option value="">Select Type</option>';
            html += data;
            $('#' + typeid).html(html);
            }
    });
    };
    };
    });
    $(document).on('focus', '.jobtype', function(){
    var department = $('.department').val();
    if (department){
    $.ajax({
    type:'get',
            url:"{{route('getJobTypes')}}",
            data:'department=' + department,
            success:function(data){
            var html = '<option value="">Select Type</option>';
            html += data;
            $('#annual_jobtype_id').html(html);
            }
    });
    };
    });
    $(document).on('focus', '#afs_jobtype_id', function(){
    var department = $('.department').val();
    if (department){
    $.ajax({
    type:'get',
            url:"{{route('getJobTypes')}}",
            data:'department=' + department,
            success:function(data){
            var html = '<option value="">Select Type</option>';
            html += data;
            $('#afs_jobtype_id').html(html);
            }
    });
    };
    });
    $(document).on('focus', '#annual_return_jobtype_id', function(){
    var department = $('.department').val();
    if (department){
    $.ajax({
    type:'get',
            url:"{{route('getJobTypes')}}",
            data:'department=' + department,
            success:function(data){
            var html = '<option value="">Select Type</option>';
            html += data;
            $('#annual_return_jobtype_id').html(html);
            }
    });
    };
    });
    $(document).on('focus', '#jobtype_id', function(){
    var department = $('.department').val();
    if (department){
    $.ajax({
    type:'get',
            url:"{{route('getJobTypes')}}",
            data:'department=' + department,
            success:function(data){
            var html = '<option value="">Select Type</option>';
            html += data;
            $('#jobtype_id').html(html);
            }
    });
    };
    });
    });
    $(document).ready(function() {
    $('.btnNext').click(function () {
    $('.nav-tabs > .nav-item > .active').parent().next('li').find('a').trigger('click');
    });
    $('.btnPrevious').click(function () {
    $('.nav-tabs > .nav-item > .active').parent().prev('li').find('a').trigger('click');
    });
    });
    function enable(checkbox, note){
    var notify = document.getElementById('notify' + note);
    notify.disabled = checkbox.checked? false: true;
    if (!notify.disabled){
    notify.focus();
    notify.value = 0;
    }
    if (notify.disabled){
    notify.value = "";
    }
    };
    function paym(check){
    var remender = document.getElementById('example-radios-inline1');
    var remender2 = document.getElementById('example-radios-inline2');
    var remender3 = document.getElementById('example-radios-inline3');
    var remender4 = document.getElementById('example-radios-inline4');
    remender.disabled = check.checked? false: true;
    remender2.disabled = check.checked? false: true;
    remender3.disabled = check.checked? false: true;
    remender4.disabled = check.checked? false: true;
    if (!remender.disabled){
    remender.focus();
    }
//    if (remender.disabled){
//    remender.value = "";
//    }
    }

    function annual(check){
    var remender = document.getElementById('annual_jobtype_id');
    remender.disabled = check.checked? false: true;
    if (!remender.disabled){
    remender.focus();
    }
    }
    function afsAnnual(check){
    var remender = document.getElementById('afs_jobtype_id');
    remender.disabled = check.checked? false: true;
    if (!remender.disabled){
    remender.focus();
    }
    }
    function annualreturn(check){
    var remender = document.getElementById('annual_return_jobtype_id');
    remender.disabled = check.checked? false: true;
    if (!remender.disabled){
    remender.focus();
    }
    }

    function job_type(check){
    var remender = document.getElementById('jobtype_id');
    remender.disabled = check.checked? false: true;
    if (!remender.disabled){
    remender.focus();
    }
    }


</script>
@endpush
