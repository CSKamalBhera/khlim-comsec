@extends('errors.layout', [
'activePage' => '403',
'menuParent' => '403',
'title' => '403',
])
@section('content')

<div class="py-4">
    <!-- Error Header -->
    <h1 class="display-1 font-w600 text-flat">403</h1>
    <h2 class="h4 font-w400 text-muted">Your account does not have access to this routes..</h2>
    <h2 class="h4 font-w400 text-muted mb-5">We are sorry but you do not have permission to access this page..</h2>
    <!-- END Error Header -->
</div>

@endsection