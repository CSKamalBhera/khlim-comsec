@extends('errors.layout', [
'activePage' => '404',
'menuParent' => '404',
'title' => '404',
])
@section('content')
<div class="py-4">
    <!-- Error Header -->
    <h1 class="display-1 font-w600 text-city">404</h1>
    <h2 class="h4 font-w400 text-muted">Oooops! We cannot find this page..</h2>
    <h2 class="h4 font-w400 text-muted mb-5">We are sorry but the page you are looking for was not found..</h2>
    <!-- END Error Header -->

</div>

@endsection