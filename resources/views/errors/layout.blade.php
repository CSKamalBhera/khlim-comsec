<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{ isset($title) ? $title." |" : "" }} {{ config('app.name', 'khlim') }}</title>
    @include('layouts.partials.favicon')
    @include('layouts.partials.tags')
    
    <link rel="shortcut icon" href="assets/media/favicons/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/media/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/media/favicons/apple-touch-icon-180x180.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">
    <link rel="stylesheet" id="css-main" href="{{ asset('css/oneui.css') }}">
</head>

<body>
    <div id="page-container">
        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="hero">
                <div class="hero-inner text-center">
                    <div class="bg-grey">
                        <div class="content content-full overflow-hidden">
                            @include('layouts.page_templates.guest')
                        </div>
                    </div>
                    <div class="content content-full text-muted">
                        <!-- Error Footer -->
                        <p class="mb-1">
                            Would you like to let us know about it?
                        </p>
                        <a class="link-fx" href="javascript:void(0)">Report it</a> or <a class="link-fx"
                            href="{{ route('home') }}">Go Back to Dashboard</a>
                        <!-- END Error Footer -->
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
    <script src="{{ asset('js/oneui.app.js') }}"></script>
</body>

</html>