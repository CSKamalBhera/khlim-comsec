@extends('errors.layout', [
'activePage' => '401',
'menuParent' => '401',
'title' => '401',
])
@section('content')

<div class="py-4">
    <!-- Error Header -->
    <h1 class="display-1 font-w600 text-amethyst">401</h1>
    <h2 class="h4 font-w400 text-muted ">Your account has been deactivated..</h2>
    <h2 class="h4 font-w400 text-muted mb-5">We are sorry but you are not authorized to access this
        page..</h2>
    <!-- END Error Header -->

</div>

@endsection