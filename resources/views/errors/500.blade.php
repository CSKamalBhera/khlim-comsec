@extends('errors.layout', [
'activePage' => '500',
'menuParent' => '500',
'title' => '500',
])
@section('content')
<div class="py-4">
    <!-- Error Header -->
    <h1 class="display-1 font-w600 text-modern">500</h1>
    <h2 class="h4 font-w400 text-muted">We are sorry but our server encountered an internal error..</h2>
    <h2 class="h4 font-w400 text-muted mb-5">The server is having difficulty continuing, please contact us to identify the source of the problem..</h2>
    <!-- END Error Header -->
</div>
@endsection