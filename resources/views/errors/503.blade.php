@extends('errors.layout', [
'activePage' => '503',
'menuParent' => '503',
'title' => '503',
])
@section('content')
<div class="py-4">
    <!-- Error Header -->
    <h1 class="display-1 font-w600 text-smooth">503</h1>
    <h2 class="h4 font-w400 text-muted">We are sorry but our service is currently not available..</h2>
    <h2 class="h4 font-w400 text-muted mb-5">We broke the server..</h2>
    <!-- END Error Header -->
</div>
@endsection