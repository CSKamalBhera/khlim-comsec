@extends('errors.layout', [
'activePage' => '400',
'menuParent' => '400',
'title' => '400',
])
@section('content')
<div class="py-4">
    <!-- Error Header -->
    <h1 class="display-1 font-w600 text-default">400</h1>
    <h2 class="h4 font-w400 text-muted mb-5">We are sorry but your request contains bad syntax and cannot be fulfilled..</h2>
    <!-- END Error Header -->

</div>
<!-- END Page Content -->
@endsection