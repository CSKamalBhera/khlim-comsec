@extends('layouts.app', [
'activePage' => 'joboutput-list',
'menuParent' => 'joboutput-list',
'title' => 'JobOutput',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
        <div class="content content-full">
          <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-8 d-flex justify-content-start align-items-center">
              <h1 class="flex-sm-00-auto h3 my-2">
                  Jobs Output <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
              </h1>
              <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">Jobs Output</li>
                      <li class="breadcrumb-item" aria-current="page">
                          <a class="text-black text-muted">{{__('List')}}</a>
                      </li>
                  </ol>
              </nav>
            </div>
          </div>
       </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full font-size-sm">
                    <thead>
                        <tr>
                            <th class="text-left" style="width: 10px;">#</th>
                            <th>Job Type</th>
                            <th>Customer</th>
                            <th>Created</th>
                            <th style="width:145px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($joboutputs as $key => $jobOutput)
                        <tr>
                            <td class="text-left">{{ $key +1 }}</td>
                            <td class="font-w600">
                                {{ $jobOutput->job->job_type->name }}
                            </td>
                            <td class="d-none d-sm-table-cell">
                               <span class="text-muted">{{ $jobOutput->job->company->name }}</span>
                            </td>
                            <td>
                                <span class="text-muted">{{  \Carbon\Carbon::parse($jobOutput->created_at)->format('d-m-Y') }}</span>
                            </td>
                            <td class="font-w600 text-center">
                                <a class="btn btn-sm btn-light" href="{{ url('download/'.$jobOutput->id) }}"><i class="nav-main-link-icon fa fa-download"></i></a>
                                <a class="btn btn-sm btn-light" href="{{ route('job-output.show',$jobOutput->id) }}"><i class="nav-main-link-icon fa fa-eye"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->
@endsection
