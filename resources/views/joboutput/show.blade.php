@extends('layouts.app', [
'activePage' => 'joboutput',
'menuParent' => 'joboutput',
'title' => 'Job Output',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                Jobs Output <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('job-output.index') }}">Jobs Output</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">Details</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">{{ $joboutput->job->job_type->name }} - {{ $joboutput->job->company->name }}</h3>
        </div>

        <div class="block-content block-content-full">
            <div class="row push">
                <div class="col-lg-6 col-xl-6">
                    <label>Job </label>
                    <p><a href="{{ route('jobs.show',$joboutput->job_id) }}"> {{ $joboutput->job->job_type->name }} <i class="fa fa-external-link-alt"></i></a></p>
                    <label>Customer </label>
                    <p> {{ $joboutput->job->company->name }}</p>
                    <label>Staff Assigned </label>
                    <p> {{ $joboutput->job->staff->name }}</p>
                    <label>File :</label>
                    <p><a href="{{ route('output-file',$joboutput->id) }}"  target="_blank"> jobOutput.pdf <i class="fa fa-external-link-alt"></i></a></p>

                </div>
                <div class="col-lg-6">
                    <label>Job Type</label>
                    <p> {{ $joboutput->job->job_type->name }}</p>
                    <label>Department</label>
                    <p> {{ $joboutput->job->department->name }}</p>
                    <label>Manager Assigned</label>
                    <p> {{ $joboutput->job->manager->name }}</p>

                </div>
            </div>
        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->
@endsection
