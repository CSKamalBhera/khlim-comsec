@extends('layouts.app', [
  'activePage' => 'approval-list',
  'menuParent' => 'approval-list',
  'title' => 'Approvals',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                Approval <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Approval</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx  text-black">List</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <!-- Approval -->
    <div class="block block-rounded">
        <div class="block-header block-header-default">
            <h3 class="block-title"></h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                    <i class="si si-refresh"></i>
                </button>
            </div>
        </div>
        <div class="block-content">
            <!-- Intro Approval -->
            <form action="" method="GET">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                        <select class="form-control" required name="company">
                            <option disabled selected>Select Customer</option>
                            @foreach($companies as $key => $company)
                                <option value="{{ $company->id }}" @if($company->id == Request::get('company')) selected @endif>{{ $company->name }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                        <select class="form-control" required name="status">
                            <option disabled selected>Select Status</option>
                            <option value="2" @if(2 == Request::get('status')) selected @endif>Rejected</option>
                            <option value="1" @if(1 == Request::get('status')) selected @endif>Approved</option>
                        </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Filters</button>
                            <a href="{{ route('approvals.index') }}" class="btn btn-danger">Clear All</a>
                        </div>
                    </div>
                </div>
            </form>
            <table class="table table-borderless table-striped table-vcenter js-dataTable-full push" style="border-collapse: collapse!important">
                <thead class="border-bottom">
                    <tr style="display: none;">
                        <th></th>
                        <th class="d-none d-md-table-cell" style="width: 180px;"></th>
                    </tr>
                </thead>
                <tbody>
                    @if ($approvals->isEmpty())
                        <tr>No data Found</tr>
                    @endif
                    @foreach($approvals as $key => $approval)
                    <tr>
                        <td colspan="6">
                            <form action="{{ route('approvals.update',$approval->id) }}" method="POST">
                                @csrf
                                @method('put')
                                <a class="font-w600" href="{{ url('jobs',$approval->job_id) }}">{{ $approval->job->job_type->name }} ( {{ $approval->job->company->name }} )</a>
                                <div class="font-size-sm text-muted mt-1">{{ $approval ? $approval->description : 'Introduce yourself to our community' }}</div>
                                @if ($approval->status == null)
                                    <button type="submit" name="status" value="1" class="btn btn-secondary mt-2">Approve</button>
                                    <button type="submit" name="status" value="2" class="btn btn-alt-secondary mt-2">Reject</button>
                                @elseif($approval->status == 1)
                                <button type="submit" class="btn btn-secondary mt-2" disabled>Approve</button>
                                @else
                                <button type="submit" class="btn btn-secondary mt-2" disabled>Reject</button>
                                @endif
                            </form>
                        </td>

                        <td class="d-none d-md-table-cell text-center">
                            <span class="font-size-sm"><em>{{  \Carbon\Carbon::parse($approval->created_at)->format('d-m-Y g:i A') }}</em></span>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            <!-- END Intro Approval -->
        </div>
    </div>
    <!-- END Approval -->
</div>
<!-- END Page Content -->
@endsection
