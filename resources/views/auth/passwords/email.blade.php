@extends('layouts.app', [
  'activePage' => 'reset',
  'title' => 'Reset Password',
])

@section('content')
<div class="container">
    <div class="row justify-content-center pt-xl-4 pt-lg-4">
        <div class="col-md-10 col-lg-8 col-xl-6 justify-content-center">
            <div class="block block-rounded block-themed mt-5">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title text-md-center">{{ __('Forgot Password') }}</h3>
                </div>
                <div class="block-content">
                    <div class="p-sm-3 px-lg-4 py-lg-5">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <label for="email" class="col col-form-label text-md-center">{{ __('E-Mail Address') }}</label>
                            <div class="form-group row">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group text-md-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
