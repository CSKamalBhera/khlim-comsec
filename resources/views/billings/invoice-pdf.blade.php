
<!DOCTYPE html>
<html>
    <head>
        <title>Invoice</title>
        <style>
            table{width: 100%;}
            table.border-down{border:1px solid #353535}
            
            table tr td{ padding:0 0 0.2rem 0.3rem;}
            *{margin: 0;padding: 0;color: #353535;font-family: sans-serif;}
            p{font-size: 13px;}
            body{padding: 10px;}
            table, th, td {
              border-collapse: collapse;
              font-size: 13px;
            }
            .desciption td{ height:50px; border-bottom: 1px solid #ddd;}
            .signature-class p{margin-top: 3rem !important;padding-top: 0.5rem;width:calc(100% - 10px) !important;text-align: center;margin: auto;}
            .fixed-bottom {width:100%;position: fixed;left:0;right:0;bottom:0;text-align:center;height:40px;border-top:1px solid #ddd;}
            h2{text-transform: uppercase !important;}
            hr{border: 1px solid #000;height:0 !important;}
            .page-number:before {
            content: ": " counter(page);
            } 
        </style>
    </head>
    <body>
        <table width="100%">
            <thead>
                <tr>
                    <td colspan="12" style="text-align: center;">
                        <h2>{{ $invoice->setting->name }}.</h2>
                        
                    </td>
                </tr>
                <tr>
                   <td colspan="12" style="text-align: center !important;">
                        <div style="width:40%;margin:auto;text-transform: uppercase !important;">
                            <p> {{ $invoice->setting->address }}.<br></p>
                        </div>
                        <p >
                          (Company tax Reg No. : {{ $invoice->setting->tax_no }})<br>
                          (Service tax Reg No. : {{ $invoice->setting->tax_no }})<br>
                        </p>
                       
                   </td>   
                </tr>    
            
                <tr>
                   
                    <td colspan="8">
                        <table >
                            <tr>
                                <td>
                                    <p style="text-transform: uppercase !important;"><b>{{ $invoice->company->name }}.</b></p>
                                    <div style="width:50%">
                                        <p>{{ $companyAdderss ? $companyAdderss->address : '' }}</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                       
                                        <tr>
                                            <td style="width:20%; ">ATTN.</td>
                                            <td style="width:80%; text-transform: uppercase !important;">:{{$companyIncharge?$companyIncharge->name:''}}</td>
                                        </tr>
                                        <tr>
                                            <td>TEL.</td>
                                            <td>:{{isset($invoice->company->phone_no) ? $invoice->company->phone_no : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td>FAX</td>
                                            <td>:{{isset($invoice->company->fax_no) ? $invoice->company->fax_no : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td>A/C No.</td>
                                            <td>:{{isset($invoice->company->account_no) ? $invoice->company->account_no : '' }}</td>
                                        </tr>                                       
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td colspan="4">
                        <table style="width:100%">
                            <tr>
                                <td colspan="2"><h2><b>INVOICE</b></h2></td>
                            </tr>
                            <tr>
                                <td>NO.</td>
                                <td>: {{ $invoice->invoice_no }}</td>
                            </tr>
                            <tr>
                                <td>DATE</td>
                                <td>: {{ \Carbon\Carbon::parse($invoice->invoice_date)->format('d-m-Y') }}</td>
                            </tr>
                            <tr>
                                <td>TERM</td>
                                <td style="text-transform: uppercase !important;">: </td>
                            </tr>
                            <tr>
                                <td>AGENT</td>
                                <td style="text-transform: uppercase !important;">: Khlim</td>
                            </tr>
                            <tr>
                                <td>PAGE</td>
                                <td class="page-number"></td>
                            </tr>
                            <tr>
                                <td>PRINTED ON</td>
                                <td>:{{ \Carbon\Carbon::now()->format('d-m-Y') }}</td>
                            </tr>
                            <tr>
                                <td>PRINTED BY</td>
                                <td style="text-transform: uppercase !important;">:{{ auth()->user()->name }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="12" style="height: 450px;vertical-align: top;">
                        <table width="100%" class="desciption">
                            <thead style="border-top: 1px solid; border-bottom: 1px solid;">
                                <tr>
                                    <th width="3%"></th>
                                    <th style="text-align: left;padding: 0.5rem 0;" width="12%">ITEM NO.</th>
                                    <th style="text-align: left;" width="39%">DESCRIPTION</th>
                                    <th style="text-align: right;" width="13%">PRICE</th>
                                    <th style="text-align: center;" width="8%">TAX RATE</th>
                                    <th style="text-align: right;" width="5%">SST</th>
                                    <th style="text-align: center;" width="12%">TOTAL INCL. SST</th>
                                    <th style="text-align: right;" width="10%">TAX CODE</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($invoice->invoiceItems as $key => $item)
                            <tr style="border-bottom: 1px solid #ddd;">
                                <td style="text-align:center ;">{{ $key+1 }}</td>
                                <td style="text-align:left ;">035426</td>
                                <td style="text-align:left ;text-transform: uppercase !important;">
                                    <p class="font-w600 mb-1">{{ $item->service_name }}</p>
                                    
                                </td>
                                <td style="text-align:right;">
                                    {{config('app.currency_code')}} {{ number_format(round($item->amount,2), 2, '.', '') }}
                                </td>
                                <td style="text-align:center ;">
                                    @if($item->is_taxed ==1) 6 % @else 0 % @endif
                                </td>
                                <td style="text-align:right ;">
                                    @php $taxAmount = 0; @endphp
                                    @if($item->is_taxed == 1)
                                    @php $taxAmount = round(6 * ($item->amount / 100),2); @endphp
                                    {{config('app.currency_code')}} {{ number_format($taxAmount, 2, '.', '')}}
                                    @endif
                                </td>
                                <td style="text-align:right ;">{{config('app.currency_code')}} {{ number_format(round($item->total_amount,2), 2, '.', '')}}</td>
                                <td style="text-align:right ;">SV06</td>

                            </tr>
                            @endforeach
                            @php
                            $subTotal = round($invoice->invoiceItems->sum('amount'),2);
                            $total = round($invoice->invoiceItems->sum('total_amount'),2);
                            $taxAmount = $invoice->invoiceItems->where('is_taxed',1)->sum('amount');
                            $taxSSTAmount = round(6 * ($taxAmount / 100),2);
                            $totalDue = round($taxSSTAmount+$subTotal,2);
                            @endphp
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                             @php
                             $numberFormater = new \NumberFormatter("en",  \NumberFormatter::SPELLOUT);
                             $wordAmount = $numberFormater->format($totalDue);
                             @endphp
                    <td colspan="12" style="height: 50px;border-bottom:1px solid">
                        <p style="text-transform: uppercase !important;"><b>RINGGIT MALAYSIA :</b> {{ $wordAmount }} ONLY</p>
                    </td>
                </tr>
            </thead>
        </table>
        <table>
            <tr>
                    <td width="50%">
                        <table width="100%">
                            <tr>
                                <td>
                                <p>Payment by cheque should be crossed and make payable to<br> 
                        <p style="text-transform: uppercase !important;">{!! $invoice->setting->name !!}<br>
                        </p>
                            Direct deposit cheques or cash into our bank account:<br>
                            {!! $invoice->setting->invoice_footer !!}<br>
                            
                        </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%">
                        <table class="border-down">
                                <tr>
                                    <td colspan="2" style="text-align: right; padding: 5px;border-bottom: 1px solid #000;">MYR</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">SUB TOTAL</td>
                                    <td style="text-align: right; padding: 5px;">{{config('app.currency_code')}} {{ number_format($subTotal, 2, '.', '')}}</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;border-bottom: 1px solid #000;">TOTAL DISCOUNT</td>
                                    <td style="text-align: right;border-bottom: 1px solid #000; padding: 5px;">{{config('app.currency_code')}} 0.00</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">TOTAL EXCL. SST</td>
                                    <td style="text-align: right; padding: 5px;">{{config('app.currency_code')}} {{ number_format($subTotal, 2, '.', '')}}</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;border-bottom: 1px solid #000;">ADD SST</td>
                                    <td style="text-align: right;border-bottom: 1px solid #000; padding: 5px;">{{config('app.currency_code')}} {{ number_format($taxSSTAmount, 2, '.', '')}}</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">TOTAL PAYABLE INCL. SST</td>
                                    <td style="text-align: right; padding: 5px;">{{config('app.currency_code')}} {{ number_format($total, 2, '.', '')}}</td>
                                </tr>
                        </table>
                    </td>
                </tr>
        </table>
        <table class="signature-class" style="width:50% !important">
                <tr>
                    <td width="50%" height="100px">
                        <hr>
                        <p><b>AUTHORISED SIGNATURE(S)</b></p>
                    </td>
                    <td width="50%">
                    <hr>
                        <p><b>RECEIVED BY</b></p>
                    </td>
                </tr> 
        </table>
        <div class="fixed-bottom">
        <p class="font-size-sm text-muted text-center py-3 my-3 border-top" style="color:#757575;">Thank you very much for doing business with us. We look forward to working with you again!</p>
        </div>
    </body>
</html>