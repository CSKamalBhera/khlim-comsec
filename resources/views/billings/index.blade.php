@extends('layouts.app', [
'activePage' => 'billings',
'menuParent' => 'billings',
'title' => 'Billings',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-8 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    Billings <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Billings</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">List</a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="col-sm-4">
                @can('invoice_create')
                <div class="text-right">
                    <a href="{{ route('invoices.create') }}" class="btn btn-primary">Create</a>
                </div>
                @endcan
            </div>
        </div>
    </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <form action="" method="GET">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <select class="form-control" name="department">
                                <option disabled selected>Select Department</option>
                                @foreach($departments as $key => $department)
                                <option value="{{ $department->id }}" @if($department->id == Request::get('department')) selected @endif>{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <select class="form-control" name="status">
                                <option selected value="">Select</option>
                                <option value="0" @if (Request::get('status') == 0 ) selected @endif>Unpaid </option>
                                <option value="1" @if (Request::get('status') == 1) selected @endif>Paid </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Filters</button>
                            <a href="{{ route('invoices.index') }}" class="btn btn-danger">Clear All</a>
                        </div>
                    </div>
                </div>
            </form>
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full font-size-sm">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px;">#</th>
                        <th>CUSTOMER</th>
                        <th>BILL NO.</th>
                        <th>DUE</th>
                        <th class="d-none d-sm-table-cell text-center" style="width: 12%;">DATE</th>
                        <th style="width: 15%;">AMOUNT DUE</th>
                        <th class="text-center">STATUS</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($invoices as $key => $invoice)
                    <tr>
                        <td class="text-center">{{$key+1}}</td>
                        <td class="font-w600">
                            <a href="{{ route('invoices.show',$invoice->id) }}">{{ ucwords($invoice->company->name) }}</a>
                        </td>
                        <td class="font-w500">
                            <span class="text-muted">{{ $invoice->invoice_no }}</span>
                        </td>
                        <td class="font-w500">
                            <span class="text-muted">{{\Carbon\Carbon::parse($invoice->due_date)->format('Y-m-d')}}</span>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <span class="text-muted">{{\Carbon\Carbon::parse($invoice->invoice_date)->format('Y-m-d')}}</span>
                        </td>
                        <td class="text-center">
                            <span class="font-weight-bold">{{ config('app.currency_code') }} {{ number_format(round($invoice->total_amount,2), 2, '.', '') }}</span>
                        </td>
                        <td class="d-none d-sm-table-cell text-center font-size-lg">
                            @if ($invoice->status == 1)
                            <span class="badge badge-pill badge-status badge-success">Paid</span>
                            @elseif (\Carbon\Carbon::parse($invoice->due_date)->format('Y-m-d') < \Carbon\Carbon::now()->format('Y-m-d'))
                            <span class="badge badge-pill badge-status badge-danger">Overdue</span>
                            @elseif ($invoice->status == 0)
                            <span class="badge badge-pill badge-status badge-warning">Pending</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                @can('invoice_edit')
                                <a class="btn btn-sm btn-light" href="{{ route('invoices.show', $invoice->id) }}"><i class="nav-main-link-icon fa fa-edit"></i></a>
                                @endcan
                                @can('invoice_delete')
                                <form action="{{ route('invoices.destroy',$invoice->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-light delete" data-name="{{ $invoice->company->name }}"
                                            title="{{__('Delete')}}">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
<!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->
@endsection
