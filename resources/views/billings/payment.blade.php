@extends('layouts.app', [
'activePage' => 'payment',
'menuParent' => 'payment',
'title' => 'Payment',
])
@section('content')

<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Payment<small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item"><a href="{{ route('invoices.index') }}">Payment</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx text-black">Create</a>
                    </li>
                </ol>
            </nav>

        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">

        <form action="{{route('payments.store')}}" method="POST">
            @csrf

            <div class="block-content block-content-full">
                <div class="row">
                    <input type="hidden" name="invoice_id" value="{{$id}}">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="department">Payment Note</label>
                            <input type="text"  class="form-control form-control-alt @error('debtorCode') is-invalid @enderror" id="payment_note" name="payment_note" value="{{old('payment_note')}}" placeholder="Payment Note" >
                            @error('payment_note')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="name">Amount<span class="text-danger">*</span></label>
                            <input type="text"  class="form-control form-control-alt @error('amount') is-invalid @enderror" id="amount" name="amount" value="{{old('amount')}}" placeholder="amount" >
                            @error('amount')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="debtorCode">Description</label>
                            <textarea type="text"  class="form-control form-control-alt @error('description') is-invalid @enderror" id="description" name="description" value="{{old('description')}}" placeholder="description" >{{old('description')}}</textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection