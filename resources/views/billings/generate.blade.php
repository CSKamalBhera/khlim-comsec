@extends('layouts.app', [
'activePage' => 'create-invoice',
'menuParent' => 'create-invoice',
'title' => 'Create Invoice',
])
@section('content')

<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Billing<small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item"><a href="{{ route('invoices.index') }}">Billing</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx text-black">Create</a>
                    </li>
                </ol>
            </nav>

        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">

    <!-- Alternative Style -->
    <div class="block block-rounded">
        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#btabs-home">General</a>
            </li>
        </ul>
        <form action="{{ route('invoices.store') }}" method="POST">
            @csrf
            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-home" role="tabpanel">
                    <div class="block-content block-content-full">
                        <input type="hidden" name="jobtype_workflow_id" value="{{ $jobTypeWorkflow }}" id="jobtype_workflow_id">
                        <input type="hidden" name="workflow_type_id" value="{{ $workflowType }}" id="workflow_type_id">
                        <input type="hidden" name="workflow_option_id" value="{{ $workflowOption }}" id="workflow_option_id">
                        <input type="hidden" name="updatedBy" value="{{ $updatedBy}}" id="updatedBy">
                        <input type="hidden" name="jobWorkflow" value="{{ $jobWorkflow}}" id="jobWorkflow">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="company">Customer<span class="text-danger">*</span></label>
                                    <select class="company form-control @error('company_id') is-invalid @enderror" id="company_id" name="company_id" style="width: 100%;" data-placeholder="Choose" readonly>
                                      <option value="{{ $job->company_id }}"  selected >{{ $job->company->name }}</option>
                                    </select>
                                    @error('company_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="job">Jobs<span class="text-danger">*</span></label>
                                    <select id="jobs" class="js-select2 form-control @error('job_id') is-invalid @enderror" id="job_id" name="job_id" style="width: 100%;" data-placeholder="Choose" readonly>
                                       <option value="{{ $job->id }}" selected >{{$job->job_type->name }}</option>
                                    </select>
                                    @error('job_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="department">Departments<span class="text-danger">*</span></label>
                                    <select class="js-select2 form-control @error('department_id') is-invalid @enderror" id="department_id" name="department_id" style="width: 100%;" data-placeholder="Choose" readonly>
                                      <option value="{{ $job->department_id }}"  selected>{{ $job->department->name }}</option>
                                    </select>
                                    @error('department_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="name">Bill From:<span class="text-danger">*</span></label>
                                    <select class="js-select2 form-control @error('setting_id') is-invalid @enderror" id="setting_id" name="setting_id" style="width: 100%;" data-placeholder="Choose" readonly>
                                        @php
                                            $settings = \App\Models\Setting::where('department_id', $job->department_id)->get();
                                        @endphp
                                        @foreach($settings as $setting)
                                            <option value="{{ $setting->id }}"  selected >{{ $setting->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('setting_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="amount">Amount Due:<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount" name="amount" value="{{old('amount')}}" placeholder="Amount">
                                    @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="invoiceDate">Invoice Date:<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control @error('invoiceDate') is-invalid @enderror" id="invoiceDate" name="invoiceDate" value="{{old('invoiceDate')}}" placeholder="invoiceDate">
                                    @error('invoiceDate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="debtorCode">debtor A/C code:<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('debtorCode') is-invalid @enderror" id="debtorCode" name="debtorCode" value="{{old('debtorCode')}}" placeholder="debtor A/C Code">
                                    @error('debtorCode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="dueDate">Due Date:<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control @error('dueDate') is-invalid @enderror" id="dueDate" name="dueDate" value="{{old('dueDate')}}" placeholder="dueDate">
                                    @error('dueDate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="description">Description<span class="text-danger">*</span></label>
                                    <textarea type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" value="{{old('description')}}" placeholder="description">{{old('description')}}</textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>

    </div>

</div>
<!-- END Alternative Style -->
@endsection