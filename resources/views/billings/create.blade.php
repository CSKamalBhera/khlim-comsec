@extends('layouts.app', [
'activePage' => 'create-invoice',
'menuParent' => 'create-invoice',
'title' => 'Create Invoice',
])
@section('content')

<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Billing<small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item"><a href="{{ route('invoices.index') }}">Billing</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx text-black">Create</a>
                    </li>
                </ol>
            </nav>

        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">

    <!-- Alternative Style -->
    <div class="block block-rounded">
        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#btabs-home">General</a>
            </li>
        </ul>
        <form action="{{ route('invoices.store') }}" method="POST">
            @csrf
            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-home" role="tabpanel">

                    <div class="block-content block-content-full">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="company">Customer<span class="text-danger">*</span></label>
                                    <select class="company form-control @error('company_id') is-invalid @enderror" id="company_id" name="company_id" style="width: 100%;" data-placeholder="Choose" readonly="readonly">
                                        <option value="">{{__('Please Select')}}</option>
                                        @foreach($companies as $company)
                                        <option value="{{ $company->id }}" @if ($company->id == old('company_id')) selected @endif>{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('company_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="job">Jobs</label>
                                    <select id="jobs" class="js-select2 form-control @error('job_id') is-invalid @enderror" id="job_id" name="job_id" style="width: 100%;" data-placeholder="Choose">
                                        <option disabled selected>Select</option> 
                                        @if(!empty(old('company_id')))
                                        @php
                                        $jobs = \App\Models\Jobs\Job::with('job_type')->where('company_id', old('company_id'))->where('status',1)->get();
                                        @endphp
                                        @foreach($jobs as $job)
                                        <option value="{{ $job->id }}" @if($job->id == old('job_id')) selected @endif>{{$job->job_type->name }}</option>
                                        @endforeach 
                                        @endif
                                    </select>
                                    @error('job_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="department">Departments<span class="text-danger">*</span></label>
                                    <select class="js-select2 form-control @error('department_id') is-invalid @enderror" id="department_id" name="department_id" style="width: 100%;" data-placeholder="Choose">
                                        <option value="">{{__('Please Select')}}</option>
                                        @foreach($departments as $department)
                                        <option value="{{ $department->id }}" @if ($department->id == old('department_id')) selected @endif>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('department_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="name">Bill From:<span class="text-danger">*</span></label>
                                    <select class="js-select2 form-control @error('setting_id') is-invalid @enderror" id="setting_id" name="setting_id" style="width: 100%;" data-placeholder="Choose">
                                        <option value="">{{__('Please Select')}}</option>
                                        @if(!empty(old('department_id')))
                                        @php
                                        $settings = \App\Models\Setting::where('department_id', old('department_id'))->get();
                                        @endphp
                                        @foreach($settings as $setting)
                                        <option value="{{ $setting->id }}" @if ($setting->id == old('setting_id')) selected @endif>{{ $setting->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    @error('setting_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="amount">Amount Due:<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount" name="amount" value="{{old('amount')}}" placeholder="Amount">
                                    @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="invoiceDate">Invoice Date:<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control @error('invoiceDate') is-invalid @enderror" id="invoiceDate" name="invoiceDate" value="{{old('invoiceDate')}}" placeholder="invoiceDate">
                                    @error('invoiceDate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="dueDate">Due Date:<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control @error('dueDate') is-invalid @enderror" id="dueDate" name="dueDate" value="{{old('dueDate')}}" placeholder="dueDate">
                                    @error('dueDate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="debtorCode">Debtor A/C code:</label>
                                    <input type="text" class="form-control @error('debtorCode') is-invalid @enderror" id="debtorCode" name="debtorCode" value="{{old('debtorCode')}}" placeholder="Debtor A/C Code">
                                    @error('debtorCode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" value="{{old('description')}}" placeholder="description">{{old('description')}}</textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>

    </div>

</div>
<!-- END Alternative Style -->

<!-- END Page Content -->
@endsection
@push('scripts')
<script type="text/javascript">
    // CSRF Token
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function () {
        var companyId = '';
        $(document).on('change', '.company', function () {
            var companyId = $(this).val();
            $('#jobs').empty();
            $.ajax({
                type: 'get'
                , url: "{{route('getJobs')}}"
                , data: 'company=' + companyId
                , success: function (data) {
                    var html = '<option disabled selected value="">Select</option>';
                    html += data;
                    $('#jobs').html(html);
                }
            });
        });
        $(document).on('change', '#department_id', function () {
            var departmentId = $(this).val();

            $('#setting_id').empty();
            $.ajax({
                type: 'get'
                , url: "{{route('getSettings')}}"
                , data: 'department=' + departmentId
                , success: function (data) {
                    var html = '<option disabled selected value="">Select</option>';
                    html += data;
                    $('#setting_id').html(html);
                }
            });
        });
    });

</script>
@endpush
