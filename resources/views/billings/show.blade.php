@extends('layouts.app', [
'activePage' => 'invoice-details',
'menuParent' => 'invoice-details',
'title' => 'Invoice Details',
])
@section('content')

<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-8 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    Billing <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('invoices.index') }}">Billing</a></li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx  text-black">Details</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist" id="myTab">
            <li class="nav-item">
                <a class="nav-link active" id='home' href="#btabs-home">General</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id='invoice' href="#btabs-invoice"> Invoice</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id='payment' href="#btabs-payment"> Payment</a>
            </li>
        </ul>
        <div class="block-content tab-content">
            <div class="tab-pane active" id="btabs-home" role="tabpanel">
                <div class="block-content block-content-full p-0">
                    <table class="table table-borderless">
                        <thead>
                        <th class="font-size-sm py-0" style="width: 10px;">status</th>
                        <th class="py-0" style="width: 30px;">Customer</th>
                        <th class="py-0" style="width: 30px;">Amount</th>
                        <th class="py-0" style="width: 10px;">Date</th>
                        <th class="py-0" style="width: 10px;">Due Date</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="d-none d-sm-table-cell font-size-lg ">
                                    @if ($invoice->status == 1)
                                    <span class="badge badge-status badge-pill badge-success">Paid</span>
                                    @elseif (\Carbon\Carbon::parse($invoice->due_date)->format('Y-m-d') < \Carbon\Carbon::now()->format('Y-m-d'))
                                    <span class="badge badge-status badge-pill badge-danger">Overdue</span>
                                    @elseif ($invoice->status == 0)
                                    <span class="badge badge-status badge-pill badge-warning">Pending</span>
                                    @endif
                                </td>
                                <td>{{$invoice->company->name}}</td>

                                <td class="font-size-lg">{{config('app.currency_code')}} {{ number_format(round($invoice->total_amount,2), 2, '.', '')}}</td>
                                <td class="font-size-lg">{{\Carbon\Carbon::parse($invoice->invoice_date)->format('d-m-Y')}}</td>
                                <td class="font-size-lg">{{\Carbon\Carbon::parse($invoice->due_date)->format('d-m-Y')}}</td>
                            <tr>
                        </tbody>
                    </table>
                </div>
                <div class="block-content block-content-full job-timeline">
                    <div class="row border rounded-lg align-items-center">
                        <div class="col-md-8">
                            <ul class="fa-ul mt-0">
                                <li>
                                    @if($activities->log_name == 'Creation')
                                    <i class="fa fa-li fa-plus-circle"></i>
                                    @else
                                    <i class="fa fa-li fa-edit"></i>
                                    @endif
                                    {{ $activities->log_name }} on {{  \Carbon\Carbon::parse($activities->created_at)->format('d-m-Y g:i A') }} <br />by {{ $activities->properties['causer_name'] }}</li>
                            </ul>
                        </div>
                        <div class="col-md-4 text-right py-4">
                            @can('invoice_edit')
                            <a class="btn btn-sm btn-dark" href="{{ route('invoices.edit', $invoice->id) }}">Edit</a>
                            @endcan
                        </div>
                    </div>
                    <div class="row border rounded-lg my-3 align-items-center">
                        <div class="col-md-8">
                            <ul class="fa-ul mt-0">
                                <li>
                                    <i class="fa fa-li fa-download"></i> Last {{ $lastDawnload ? $lastDawnload->log_name : 'not downloaded' }} on {{ $lastDawnload ? \Carbon\Carbon::parse($lastDawnload->created_at)->format('d-m-Y g:i A') : '' }} <br />by {{ $lastDawnload ? $lastDawnload->properties['causer_name'] : 'User' }}</li>
                            </ul>
                        </div>
                        <div class="col-md-4 text-right py-4">
                            <a class="btn btn-sm btn-dark" href="{{url('/download-invoice/'.$invoice->id)}}" target="_blank">Download</a>
                        </div>
                    </div>
                    <div class="row border rounded-lg my-3  align-items-center">
                        <div class="col-md-8">
                            <ul class="fa-ul mt-0">
                                <li>
                                    <i class="fa fa-li fa-money-bill"></i>
                                    <span>Payment<br>
                                        <label>Amount Due: </label>
                                        {{ config('app.currency_code') }} {{ number_format(round($invoice->due_amount,2), 2, '.', '') }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 text-right py-4">
                            @can('payment_create')
                            <button type='button' class="btn btn-sm btn-dark hover" data-toggle="modal" data-target="#join_user" id="add_user">Record a payment</button>
                            @endcan
                        </div>
                    </div>
                    <div class="modal fade" id="join_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Record Payment</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body ">
                                    <div class="row ">
                                        <div class="col-md-12">
                                            <div class="loginPageBox" id="signup">
                                                <div class="" id="myTabContentSignup">
                                                    <div class="alert alert-success alert-block user-msg" style="display: none;">
                                                        <button type="button" class="close" data-dismiss="error span">×</button>
                                                    </div>
                                                    <form action="{{route('payments.store')}}" method="POST" id="paymentId">
                                                        @csrf
                                                        <div class="popupForm">
                                                            <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
                                                            <input type="hidden" name="paymentid" id="payment_id" value="{{1??old('paymentid')}}">
                                                            <div class="form-group">
                                                                <label for="department">Payment Note</label>
                                                                <input type="text" class="form-control @error('payment_note') is-invalid @enderror" id="payment_note" name="payment_note" value="{{old('payment_note')}}" placeholder="Payment Note">
                                                                @error('payment_note')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="name">Amount<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount" name="amount" value="{{ $invoice ? $invoice->due_amount : old('amount')}}" placeholder="amount">
                                                                @error('amount')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="paymentDate">Payment Date:<span class="text-danger">*</span></label>
                                                                <input type="date" class="form-control @error('paymentDate') is-invalid @enderror" id="paymentDate" name="paymentDate" value="{{old('paymentDate')}}" placeholder="invoiceDate">
                                                                @error('paymentDate')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>


                                                            <div class="form-group">
                                                                <label for="debtorCode">Description</label>
                                                                <textarea type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" value="{{old('description')}}" placeholder="description">{{old('description')}}</textarea>
                                                                @error('description')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                            <div class="formBtn">
                                                                <button type="submit" id="btn_payment" class="btn btn-primary">Submit</button>
                                                            </div>
                                                        </div>
                                                        <!--//popupForm-->
                                                    </form>

                                                </div><!--//tab-content-->

                                            </div><!--//loginPageBox-->
                                        </div><!--//col-md-8-->
                                    </div><!--//row-->
                                </div><!--//modal-body-->
                            </div>
                        </div>
                    </div>
                    <!--//modal-->
                </div>
                @if(!empty(old('paymentid')))
                <script type="text/javascript">
                    $(document).ready(function() {
                    $('#btn_payment').on('submit', function(e){
                    e.preventDefault();
                    });
                    $('#join_user').modal('show');
                    $('.close').on('click', function(){
                    $('#join_user').modal('hide');
                    $('.modal-backdrop').remove();
                    });
                    });
                </script>
                @endif
                @if(!empty(old('payment_id')))
                <script type="text/javascript">
                    $(document).ready(function() {
                    $('.modal-backdrop').remove();
                    $('#myTab a[href="#btabs-payment"]').tab('show');
                    $('#edit_user').modal('show');
                    });
                </script>
                @endif
                @if(!empty(old('invoiceItem')))
                <script type="text/javascript">
                    $(document).ready(function() {
                    $('#myTab a[href="#btabs-invoice"]').tab('show');
                    $('#btabs-invoice').show();
                    });
                </script>
                @endif
            </div>
            <div class="tab-pane" id="btabs-invoice" role="tabpanel">
                <div class="content generate-pdf-invoice">
                    <div class="block block-rounded">
                        <div class="block-header back-button">
                            <div class="col-md-6">
                                <h3 class="block-title">{{__('Invoice Items')}}</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="{{url('download-invoice/'.$invoice->id)}}" target="_blank" class="btn btn-secondary">{{__('Download')}}</a>
                                    {{-- <a href="{{url('invoice-preview/'.$invoice->id)}}" class="btn ml-2 btn-secondary">{{__('Preview')}}</a> --}}
                                </div>
                            </div>
                        </div>

                        <!-- tax invoice section -->
                        <div class="section text-invoice mt-3">
                            <div class="container">
                                <form action="{{ url('/invoice/items/edit/'.$invoice->id) }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
                                    @php $i=0; $sub_total= 0; $total= 0;@endphp
                                    <div class="row">
                                        <table class="table table-borderless table-vcenter push no-footer" id="mytable">
                                            <thead>
                                            <th>Item Name</th>
                                            <th>Amount</th>
                                            <th>Tax</th>
                                            </thead>
                                            <tbody>
                                                @foreach($invoice->invoiceItems as $key => $item)

                                                @php $i = $key; $sub_total += $item->amount; $total+=$item->total_amount; @endphp
                                                <tr>
                                            <input type="hidden" name="invoice_items_id[]" value="{{$item->id}}">
                                            <td style="width: 50%; padding: 10px;"><input type="text" class="form-control @error('invoiceItem.'.$i.'.service_name') is-invalid @enderror" name="invoiceItem[{{$i}}][service_name]" value="{{ old('invoiceItem')[$i]['service_name'] ?? ucwords($item->service_name) }}" placeholder="Service Name" >
                                                @error('invoiceItem.'.$i.'.service_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </td>
                                            <td style="width: 25%; padding: 10px;"><input type="text" class="form-control @error('invoiceItem.'.$i.'.amount') is-invalid @enderror" name="invoiceItem[{{$i}}][amount]" id='amount{{$i}}' value="{{old('invoiceItem')[$i]['amount']?? $item->amount}}" placeholder="Amount">
                                                @error('invoiceItem.'.$i.'.amount')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </td>
                                            <td style="width: 5%; padding: 15px;"><input type="checkbox" class="check" id="check{{$i}}" name="invoiceItem[{{$i}}][tax]" onclick="tax(this, {{$i}})" value='1' @if($item->is_taxed ==1) checked @endif></td>
                                            <td style="width: 5%; padding: 10px;"><button type="button" class="btn btn-sm  btn-danger btn-rounded removeRow" title="{{__('Remove')}}" @if($i==0) disabled @endif><i class="fa fa-minus"></i></button></td>
                                            </tr>
                                            @endforeach
                                            @if(!empty(old('invoiceItem')))
                                            @if($key+1 != count(old('invoiceItem')))
                                            @php
                                            $count = count(old('invoiceItem'));
                                            @endphp
                                            @for($i = $key+1; $i < $count; $i++) <tr>
                                                <td style="width: 50%; padding: 10px;"><input type="text" class="form-control  @error('invoiceItem.'.$i.'.service_name') is-invalid @enderror" name="invoiceItem[{{$i}}][service_name]" value="{{ old('invoiceItem')[$i]['service_name'] ?? '' }}" placeholder="Service Name">
                                                    @error('invoiceItem.'.$i.'.service_name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </td>
                                                <td style="width: 25%; padding: 10px;"><input type="text" class="form-control  @error('invoiceItem.'.$i.'.amount') is-invalid @enderror" name="invoiceItem[{{$i}}][amount]" id='amount{{$i}}' value="{{old('invoiceItem')[$i]['amount']??''}}" placeholder="Amount">
                                                    @error('invoiceItem.'.$i.'.amount')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </td>
                                                <td style="width: 5%; padding: 15px;"><input type="checkbox" class="check" id="check{{$i}}" name="invoiceItem[{{$i}}][tax]" onclick="tax(this, {{$i}})" value='1' @if(isset(old('invoiceItem')[$i]['tax']) && old('invoiceItem')[$i]['tax']==1) checked @endif></td>
                                                <td style="width: 5%; padding: 10px;"><button type="button" class="btn btn-sm  btn-danger btn-rounded removeRow" title="{{__('Remove')}}" @if($i==0) disabled @endif><i class="fa fa-minus"></i></button></td>
                                            </tr>
                                            @endfor
                                            @endif
                                            @endif
                                            </tbody>
                                        </table>
                                        <table class="table table-borderless table-vcenter push no-footer">
                                            <tbody>
                                                <tr>
                                            <input type="hidden" id="counter" value="{{$key}}">
                                            <div class="col-md-12" id="addnew_feature">
                                                <a href="#" class="insert btn btn-icon btn-secondary btn-sm mr-2 report">Add More</a>
                                            </div>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th class='text-right'>Sub Total:</th>
                                                    <td class='sub_total text-center'>{{ config('app.currency_code')}} {{number_format($sub_total, 2, '.', '')}}</td>
                                                </tr>
                                                <tr>
                                                    <th class='text-right'>6.00% SST:</th>
                                                    <td class='sst text-center'>{{ config('app.currency_code') }} {{ number_format(round($total - $sub_total,2), 2, '.', '') }}</td>
                                                </tr>
                                                <tr>
                                                    <th class='text-right'>Credit:</th>
                                                    <td class='credit text-center'>{{  config('app.currency_code') }} {{ number_format($invoice->payments->sum('amount'), 2, '.', '') }}</td>
                                                </tr>
                                                <tr>
                                                    <th class='text-right'>Total Due:</th>
                                                    <td class='total_due text-center'>{{ config('app.currency_code') }} {{ number_format($invoice->due_amount, 2, '.', '')}}</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="col-md-12 text-center block-content">
                                            @can('invoice_edit')
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                            <a href="{{  Request::url() }}" class="btn btn-alt-light ml-2">Cancel changes</a>
                                            @endcan
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="btabs-payment" role="tabpanel">
                <div class="block block-rounded">
                    <div class="block-content block-content-full">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                            <th class="text-center" style="width: 10px;">#</th>
                            <th class="text-center" style="width:100px;">DATE</th>
                            <th>DISCRIPTION</th>
                            <th>PAYMENT NOTE</th>
                            <th class="text-center">AMOUNT</th>
                            <th class="text-center">ACTION</th>
                            </thead>
                            <tbody>
                                @php $total = 0; @endphp
                                @foreach($invoice->payments as $key=> $payment)
                                <tr>
                                    <td class="text-left">{{ $key +1 }}</td>
                                    <td class="text-left">{{\Carbon\Carbon::parse($payment->date)->format('d-m-Y')}}</td>
                                    <td class="d-none d-sm-table-cell">{{$payment->description}}</td>
                                    <td class="d-none d-sm-table-cell">{{$payment->payment_note}}</td>
                                    <td class="text-center">{{ config('app.currency_code') }} {{number_format($payment->amount, 2, '.', '')}}</td>
                                    <td class="text-center">@can('payment_edit')<a href="javascript:void(0);" class="btn btn-sm btn-dark" data-toggle="modal" data-target="#edit_user" id="edituser" data-value="{{\Carbon\Carbon::parse($payment->date)->format('Y-m-d')}}" onclick="editUser({{$payment}});"><i class="fa fa-edit"></i></a>@endcan</td>
                                </tr>
                                @php $total += $payment->amount; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-right">
                            <label>Total:</label>
                            {{ config('app.currency_code') }} {{ number_format($total, 2, '.', '') ?? 'N/A' }}<br>
                            <label>Balance:</label>
                            {{ config('app.currency_code') }} {{ number_format($invoice->total_amount-$total, 2, '.', '') ?? 'N/A' }}<br>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="edit_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Edit Record Payment</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="loginPageBox" id="signup">
                                            <div class="" id="myTabContentSignup">
                                                <div class="alert alert-success alert-block user-msg" style="display: none;">
                                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                                </div>
                                                <form action="{{route('payments.update',$invoice->id)}}" method="POST">
                                                    @csrf
                                                    @method('put')
                                                    <div class="popupForm">
                                                        <input type="hidden" name="invoice_id" id="invoice_id" value="{{$invoice->id}}">
                                                        <input type="hidden" name="payment_id" id="payment_id" class='pay' value="{{old('payment_id')}}">
                                                        <div class="form-group">
                                                            <label for="department">Payment Note</label>
                                                            <input type="text" class="form-control @error('payment_note') is-invalid @enderror" id="payment_note1" name="payment_note" value="{{old('payment_note')}}" placeholder="Payment Note">
                                                            @error('payment_note')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="name">Amount<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount_edit" name="amount" value="{{old('amount')}}" placeholder="amount">
                                                            @error('amount')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="paymentDate">Payment Date:<span class="text-danger">*</span></label>
                                                            <input type="date" class="form-control @error('paymentDate') is-invalid @enderror" id="paymentDate1" name="paymentDate" value="{{old('paymentDate')}}" placeholder="invoiceDate">
                                                            @error('paymentDate')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="debtorCode">Description</label>
                                                            <textarea type="text" class="form-control @error('description') is-invalid @enderror" id="description1" name="description" value="{{old('description')}}" placeholder="description">{{old('description')}}</textarea>
                                                            @error('description')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                        <div class="formBtn">
                                                            <button type="submit" id="btn_user" class="btn btn-primary">Submit</button>
                                                        </div>
                                                    </div>
                                                    <!--//popupForm-->
                                                </form>

                                            </div><!--//tab-content-->

                                        </div><!--//loginPageBox-->
                                    </div><!--//col-md-8-->
                                </div><!--//row-->
                            </div><!--//modal-body-->
                        </div>
                    </div>
                </div>
                <!--//modal-->
                @if(!empty(old('payment_id')))
                @if (count($errors) > 0)
                <script type="text/javascript">
                    $(document).ready(function() {
                    $('#edit_user').modal('show');
                    $('.close').on('click', function() {
                    $('#edituser').modal('hide');
                    $('.modal-backdrop').remove();
                    });
                    });
                </script>
                @endif
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">
    function editUser(data){


    var d = $('#edituser').data('value');
    $('.pay').val(data['id']);
    $('#payment_note1').val(data['payment_note']);
    $('#amount_edit').val(data.amount);
    $('#paymentDate1').val(d);
    $('#description1').val(data['description']);
    $(document).find("span.invalid-feedback").remove();
    $(document).find("input.form-control").removeClass('is-invalid');
    $(document).find("textarea.form-control").removeClass('is-invalid');
    };
    $(document).ready(function() {
    $('#add_user').on('click', function(e){
    e.preventDefault();
    $(document).find("span.invalid-feedback").remove();
    $(document).find("input.form-control").removeClass('is-invalid');
    $(document).find("textarea.form-control").removeClass('is-invalid');
    });
    });
    $(document).ready(function(){
    $('#myTab a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
    });
    // store the currently selected tab in the hash value
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {

    e.preventDefault();
    var id = $(e.target).attr("href").substr(1);
    var scrollmem = $('html,body').scrollTop();
    window.location.hash = id;
    $('html,body').scrollTop(scrollmem);
    });
// on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $('#myTab a[href="' + hash + '"]').tab('show');
    });
    // for invoice Item edit append   
    $(document).ready(function(){

    $(".insert").click(function () {
    var count = parseInt($("#counter").val()) + 1;
    $("#mytable").each(function () {
    var tds = '<tr>';
    tds += '<td style="width: 25%; padding: 10px;"><input type="text" class="form-control  @error('invoiceItem.'.' + count + '.'.service_name') is-invalid @enderror" name="invoiceItem[' + count + '][service_name]" value="" placeholder="Service Name">@error('invoiceItem.'.' + count + '.'.service_name')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror</td><td style="width: 25%; padding: 10px;" ><input type="text"  class="form-control  @error('invoiceItem.'.' + count + '.'.amount') is-invalid @enderror" id="amount' + count + '" name="invoiceItem[' + count + '][amount]" value="" placeholder="Amount">@error('invoiceItem.'.' + count + '.'.amount')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror</td><td style="width: 5%; padding: 15px;"><input type="checkbox" class="check" id="check' + count + '" name="invoiceItem[' + count + '][tax]" onclick="tax(this,' + count + ')" value="1"></td><td style="width: 5%; padding: 10px;"><button type="button" class="btn btn-sm  btn-danger btn-rounded removeRow" title="{{__('Remove')}}"><i class="fa fa-minus"></i></button></td>';
    tds += '</tr>';
    $('tbody', this).append(tds);
    $("#counter").val(count);
    });
    });
    $(document).on('click', '.removeRow', function () {
    $(this).closest('tr').remove();
    var count = parseInt($("#counter").val()) - 1;
    $("#counter").val(count);
    });
    });


</script>
@endpush
