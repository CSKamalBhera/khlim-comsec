@extends('layouts.app', [
'activePage' => 'Invoice-preview',
'menuParent' => 'Invoice-preview',
'title' => 'Invoice Preview',
])
@section('content')

<!-- Page Content -->
<div class="content content-boxed">
    <!-- Invoice -->
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">#{{ $invoice->invoice_no }}</h3>
            <div class="block-options">
                <!-- Print Page functionality is initialized in Helpers.print() -->
                <button type="button" class="btn-block-option" onclick="One.helpers('print');">
                    <i class="si si-printer mr-1"></i> Print Invoice
                </button>
            </div>
        </div>
        <div class="text-center">
            <h2 class="text-center text-uppercase">{{ $invoice->setting->name }}</h2>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <label class="text-uppercase"><span>{{ $invoice->setting->address }}.</span></label>
                    <span class="text-center">(Company tax Reg No. : {{ $invoice->setting->tax_no }})</span><br>
                    <span class="text-center">(Service tax Reg No. : {{ $invoice->setting->tax_no }})</span>
                </div>
                <div class="col-md-3"></div>
            </div>


        </div>
        <div class="block-content">
            <div class="p-sm-3 px-xl-4">
                <!-- Invoice Info -->
                <div class="row mb-4">
                    <!-- Company Info -->
                    <div class="col-6 font-size-sm">
                        <span class="h3 text-uppercase">{{ $invoice->company->name }}</span><br>
                        <div class="row">
                        <div class="col-md-8">
                        <span>{{ $companyAdderss ? $companyAdderss->address : '' }}</span>
                        </div>
                        </div>
                        <div class="row">
                            <div class="text-left font-size-sm col-md-3"><span class="text-small">ATTN.: </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span class="text-uppercase">{{$companyIncharge?$companyIncharge->name:''}}</span></div>
                        </div>
                        <div class="row">
                            <div class="text-left font-size-sm col-md-3"><span class="text-small">TEL.: </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span>{{isset($invoice->company->phone_no) ? $invoice->company->phone_no : '' }}</span></div>
                        </div>
                        <div class="row">
                            <div class="text-left font-size-sm col-md-3"><span class="text-small">FAX.: </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span>{{isset($invoice->company->fax_no) ? $invoice->company->fax_no : '' }}</span></div>
                        </div>
                        <div class="row">
                            <div class="text-left font-size-sm col-md-3"><span class="text-small">A/C NO.: </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span>{{isset($invoice->company->account_no) ? $invoice->company->account_no : '' }}</span></div>
                        </div>
                    </div>
                    <!-- END Company Info -->

                    <!-- Client Info -->
                    <div class="col-6 text-right font-size-sm">
                        <p class="h3">INVOICE</p>

                        <div class="row">
                            <div class="text-right font-size-sm col-md-9"><span class="text-small">NO. : </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span>{{ $invoice->invoice_no }}</span></div>
                        </div>
                        <div class="row">
                            <div class="text-right font-size-sm col-md-9"><span class="text-small">DATE : </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span>{{ \Carbon\Carbon::parse($invoice->invoice_date)->format('d-m-Y') }}</span></div>
                        </div>
                        <div class="row">
                            <div class="text-right font-size-sm col-md-9"><span class="text-small">TEARM : </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span></span></div>
                        </div>
                        <div class="row">
                            <div class="text-right font-size-sm col-md-9"><span class="text-small">AGENT : </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span></span></div>
                        </div>
                        <div class="row">
                            <style>
                                .page-number:before {
                                 content: counter(page);
                                 } 
                            </style>
                            <div class="text-right font-size-sm col-md-9" ><span class="text-small" >PAGE : </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span class="page-number"></span></div>
                        </div>
                        <div class="row">
                            <div class="text-right font-size-sm col-md-9"><span class="text-small">PRINTED-ON : </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span>{{ \Carbon\Carbon::now()->format('d-m-Y') }}</span></div>
                        </div>
                        <div class="row">
                            <div class="text-right font-size-sm col-md-9"><span class="text-small">PRINTED-BY : </span> </div>
                            <div class="text-left font-size-sm col-md-3"><span>{{ strtoupper(auth()->user()->name) }}</span></div>
                        </div>
                    </div>
                    <!-- END Client Info -->
                </div>
                <!-- END Invoice Info -->

                <!-- Table -->
                <div class="table push">
                    <table class="table table-borderedless">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 60px;">ITEM NO.</th>
                                <th>DESCRIPTION</th>
                                <th class="text-center" style="width: 90px;">PRICE</th>
                                <th class="text-right" style="width: 120px;">TAX RATE</th>
                                <th class="text-right" style="width: 120px;">SST</th>
                                <th class="text-right" style="width: 120px;">TOTAL INCL. SST</th>
                                <th class="text-right" style="width: 120px;">TAX CODE</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($invoice->invoiceItems as $key => $item)
                            <tr class="border-bottom">
                                <td class="text-center">{{ $key+1 }}</td>
                                <td>
                                    <p class="font-w600 mb-1">{{ $item->service_name }}</p>
                                    <div class="text-muted"></div>
                                </td>
                                <td class="text-center">
                                {{config('app.currency_code')}}{{ number_format($item->amount, 2, '.', '') }}
                                </td>
                                <td class="text-right">
                                    @if($item->is_taxed ==1) 6 % @else 0 % @endif
                                </td>
                                <td class="text-right">
                                    @php $taxAmount = 0; @endphp
                                    @if($item->is_taxed == 1)
                                    @php $taxAmount = round(6 * ($item->amount / 100),2); @endphp
                                    {{config('app.currency_code')}}{{ number_format($taxAmount, 2, '.', '')}}
                                    @endif
                                </td>
                                <td class="text-right">{{config('app.currency_code')}}{{ number_format($item->total_amount, 2, '.', '')}}</td>
                                <td class="text-right">SV06</td>

                            </tr>
                            @endforeach

                            @php
                            $subTotal = $invoice->invoiceItems->sum('amount');
                            $total = $invoice->invoiceItems->sum('total_amount');
                            $taxAmount = $invoice->invoiceItems->where('is_taxed',1)->sum('amount');
                            $taxSSTAmount = round(6 * ($taxAmount / 100),2);
                            $totalDue = round($taxSSTAmount+$subTotal,2);
                            @endphp

                        </tbody>

                    </table>
                    @php
                    $numberFormater = new \NumberFormatter("en",  \NumberFormatter::SPELLOUT);
                    $wordAmount = $numberFormater->format($totalDue);
                    @endphp
                    <div class="row mt-8">
                        <span class="text-uppercase">RINGGIT MALAYSIA : {{config('app.currency_code')}}{{ $wordAmount }} ONLY</span>
                    </div>
                    <div class="row border-top">
                        <div class="col-md-8">
                            <span class="font-size-sm text-center py-3 my-3">
                                Payment by cheque should be crossed and make payable to
                            </span><br>
                            <span class="font-size-sm text-center py-3 my-3 text-uppercase">
                                {!! $invoice->setting->name !!}
                            </span><br>
                           
                            <span class="font-size-sm text-center py-3 my-3">
                                Direct deposit cheques or cash into our bank account:<br>
                                {!! $invoice->setting->invoice_footer !!}
                            </span>
                           
                        </div>
                        <div class="col-md-4">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td colspan="2" class="text-right" >MYR</td>
                                </tr>
                                    <tr>
                                        <td class="font-w600 text-right">SUB TOTAL</td>
                                        <td class="text-right">{{config('app.currency_code')}}{{ number_format($subTotal, 2, '.', '')}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-w600 text-right">TOTAL EXCL. SST</td>
                                        <td class="text-right">{{config('app.currency_code')}}{{ number_format($subTotal, 2, '.', '')}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-w600 text-right">ADD SST</td>
                                        <td class="text-right">{{config('app.currency_code')}}{{ number_format($taxSSTAmount, 2, '.', '')}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-w700 text-uppercase text-right bg-body-light">TOTAL PAYABLE INCL. SST</td>
                                        <td class="font-w700 text-right bg-body-light">{{config('app.currency_code')}}{{ number_format($total, 2, '.', '')}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="border-bottom border-dark"></p>
                                    <span>AUTHORISED SIGNATURE(S)</span>
                                </div>
                                <div class="col-md-6">
                                    <p class="border-bottom border-dark"></p>
                                    <span>RECEIVED BY</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- END Table -->

                <!-- Footer -->
                <p class="font-size-sm text-muted text-center py-3 my-3 border-top">
                    Thank you very much for doing business with us. We look forward to working with you again!
                </p>
                <!-- END Footer -->
            </div>
        </div>
    </div>
    <!-- END Invoice -->
</div>
<!-- END Page Content -->
@endsection
