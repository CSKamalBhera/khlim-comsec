@extends('layouts.app', [
  'activePage' => 'notification-list',
  'menuParent' => 'notification-list',
  'title' => 'Notifications',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                Notification <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('notifications.index') }}">Notification</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx  text-black">List</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <!-- Notification -->
    <div class="block block-rounded">
        <div class="block-header block-header-default">
            <h3 class="block-title">{{ $title }}</h3>
            <div class="block-options">
                <div class="dropdown">
                    <button class="dropdown-toggle btn btn-primary btn-round" type="button" id="multiDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons"></i>Filters
                    </button>
                    <div class="dropdown-menu" aria-labelledby="multiDropdownMenu">
                      <a class="dropdown-item" href="notifications-unread">@lang('Unread')</a>
                      <a class="dropdown-item" href="notifications-read">@lang('Read')</a>
                      <a class="dropdown-item" href="notifications-all">@lang('All')</a>
                    </div>
                </div>
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                    <i class="si si-refresh"></i>
                </button>
            </div>
        </div>
        <div class="block-content block-content-full">
            <table class="table table-borderless table-striped table-vcenter js-dataTable-full push" style="border-collapse: collapse!important">
                <thead>
                    <tr style="display: none;">
                        <th></th>
                        <th></th>
                        <th class="d-none d-md-table-cell" style="width: 180px;"></th>
                    </tr>
                </thead>
                <tbody>
                    @switch($filterread)
                            @case('unread')
                                @php($unreadNotificationsCount = count(Auth::user()->unreadNotifications))
                                @if($unreadNotificationsCount > 0)
                                    @foreach(Auth::user()->unreadNotifications as $notification)
                                        <tr class="border-bottom notification {{ $notification->read_at ? 'bg-white' : 'bg-light'}}">
                                            <td colspan="6">
                                                <form action="" method="POST">
                                                    @if ($notification->read_at == null) <span style="border-radius: 100%;"><i class="fa fa-circle text-danger fa-sm"></i></span> @endif
                                                    @if(isset($notification->data['action_link']))
                                                        <a class="font-w600" href="{{$notification->data['action_link']}}">{{$notification->data['title']}}</a>
                                                    @else
                                                        <a class="font-w600" href="#">    {{$notification->data['title']}}</a>
                                                    @endif
                                                    <span class="font-size-sm ml-2">
                                                        <a class="notificationDelete" href="notification-delete-{{ $notification->id }}" ><i class="fa fa-trash mr-1"></i>Delete</a></span>
                                                    <span class="font-size-sm ml-2">
                                                    </span>
                                                    <div class="font-size-sm text-muted mt-1">{{$notification->data['detail']}}</div>
                                                    @if(isset($notification->data['user_name']))
                                                        <span class="font-size-sm text-muted mt-1"> by {{$notification->data['user_name']}}</span>
                                                    @else
                                                    @endif
                                                    <div class="">
                                                        <a class="btn btn-secondary mt-2" href="notification-markasread-{{ $notification->id }}" name="markasread"><i class="fa fa-eye mr-1"> </i>Mark as read</a>
                                                    </div>
                                                </form>
                                            </td>
                                            <td><span class="label label-success-border">Unread</span></td>
                                            <td class="d-none d-md-table-cell text-center">
                                                <span class="font-size-sm"><em>{{ date('M-d-Y H:i',strtotime($notification->created_at)) }}</em></span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @break
                            @case('read')
                                @php($readNotificationsCount = count(Auth::user()->readNotifications))
                                @if($readNotificationsCount > 0)
                                    @foreach(Auth::user()->readNotifications as $notification)
                                        <tr class="border-bottom {{ $notification->read_at ? 'bg-white' : 'bg-light'}}">
                                            <td colspan="6">
                                                <form action="" method="POST">
                                                    @if ($notification->read_at == null) <span style="border-radius: 100%;"><i class="fa fa-circle text-danger fa-sm"></i></span> @endif
                                                    @if(isset($notification->data['action_link']))
                                                        <a class="font-w600" href="{{$notification->data['action_link']}}">{{$notification->data['title']}}</a>
                                                    @else
                                                        <a class="font-w600" href="#">{{$notification->data['title']}}</a>
                                                    @endif
                                                    <span class="font-size-sm ml-2">
                                                        <a class="notificationDelete" href="notification-delete-{{ $notification->id }}" ><i class="fa fa-trash mr-1"></i>Delete</a></span>

                                                    <div class="font-size-sm text-muted mt-1">{{$notification->data['detail']}}</div>
                                                    @if(isset($notification->data['user_name']))
                                                        <span class="font-size-sm text-muted mt-1"> by {{$notification->data['user_name']}}</span>
                                                    @else
                                                    @endif
                                                    <div class="">
                                                        <a href="notification-markasunread-{{ $notification->id }}" name="markasunread" class="btn btn-secondary mt-2"><i class="fa fa-eye mr-1"> </i>Mark as unread</a>
                                                    </div>
                                                </form>
                                            </td>
                                            <td><span class="label label-success-border">Unread</span></td>
                                            <td class="d-none d-md-table-cell text-center">
                                                <span class="font-size-sm"><em>{{ date('M-d-Y H:i',strtotime($notification->created_at)) }}</em></span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @break
                            @case('all')
                                @php($NotificationsCount = count(Auth::user()->Notifications))
                                @if($NotificationsCount > 0)
                                    @foreach(Auth::user()->Notifications as $key => $notification)
                                        <tr class="border-bottom {{ $notification->read_at ? 'bg-white' : 'bg-light'}}">
                                            @if(is_null($notification->read_at))
                                                <td colspan="6">
                                                    <form action="" method="POST">
                                                        @if ($notification->read_at == null) <span style="border-radius: 100%;"><i class="fa fa-circle text-danger fa-sm"></i></span> @endif
                                                        @if(isset($notification->data['action_link']))
                                                            <a class="font-w600" href="{{$notification->data['action_link']}}">{{$notification->data['title']}}</a>
                                                        @else
                                                               <a class="font-w600" href="#"> {{$notification->data['title']}}</a>
                                                        @endif
                                                        <span class="font-size-sm ml-2">
                                                            <a class="notificationDelete" href="notification-delete-{{ $notification->id }}" ><i class="fa fa-trash mr-1"></i>Delete</a></span>
                                                        <div class="font-size-sm text-muted mt-1">
                                                            {{$notification->data['detail']}}
                                                        </div>
                                                        @if(isset($notification->data['user_name']))
                                                        <span class="font-size-sm text-muted mt-1"> by {{$notification->data['user_name']}}</span>
                                                        @else
                                                        @endif
                                                        <div class="">
                                                            <a class="btn btn-secondary mt-2"href="notification-markasread-{{ $notification->id }}" name="markasread"><i class="fa fa-eye mr-1"> </i>Mark as read</a>
                                                        </div>
                                                    </form>
                                                </td>
                                                <td><span class="label label-success-border">Unread</span></td>
                                                <td class="d-none d-md-table-cell text-center">
                                                    <span class="font-size-sm"><em>{{ date('M-d-Y H:i',strtotime($notification->created_at)) }}</em></span>
                                                </td>
                                            @else
                                            <td colspan="6">
                                                <form action="" method="POST">
                                                    @if(isset($notification->data['action_link']))
                                                        <a class="font-w600" href="{{$notification->data['action_link']}}">{{$notification->data['title']}}</a>
                                                    @else
                                                           <a class="font-w600" href="#"> {{$notification->data['title']}}</a>
                                                    @endif
                                                    <span class="font-size-sm ml-2">
                                                        <a class="notificationDelete" href="notification-delete-{{ $notification->id }}" ><i class="fa fa-trash mr-1"></i>Delete</a></span>
                                                    <div class="font-size-sm text-muted mt-1">
                                                        {{$notification->data['detail']}}
                                                    </div>
                                                    @if(isset($notification->data['user_name']))
                                                        <span class="font-size-sm text-muted mt-1"> by {{$notification->data['user_name']}}</span>
                                                    @else
                                                    @endif
                                                    <div class="">
                                                        <a href="notification-markasunread-{{ $notification->id }}" name="markasunread" class="btn btn-secondary mt-2"><i class="fa fa-eye mr-1"> </i>Mark as unread</a>
                                                    </div>
                                                </form>
                                            </td>
                                            <td><span class="label label-success-border">Read</span></td>
                                            <td class="d-none d-md-table-cell text-center">
                                                <span class="font-size-sm"><em>{{ date('M-d-Y H:i',strtotime($notification->created_at)) }}</em></span>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @endif
                            @break
                    @endswitch
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Notification -->
</div>
<!-- END Page Content -->
@endsection
