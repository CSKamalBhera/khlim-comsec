@extends('layouts.app', [
'activePage' => 'edit-role',
'menuParent' => 'edit-role',
'title' => 'Edit Role',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                {{__('Roles')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Role</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">Edit</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
    </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <form action="{{route('roles.update', $role->id)}}" method="POST">
                @method('PATCH')
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="name">Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                   id="name" name="name" value="{{old('name', $role->name) }}" placeholder="Name">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="permission">Permissions</label>
                            <select class="select2 form-control @error('permission') is-invalid @enderror" id="permission" name="permission[]" style="width: 100%;" data-placeholder="Choose many.." multiple>
                                @php
                                $agents = array();
                                foreach($rolePermissions as $pm){
                                $agents[] = $pm;
                                }
                                @endphp
                                @foreach($permissions as $permission)
                                @php
                                $selected='';
                                if(is_array(old('permission')) && in_array($permission->id, old('permission'))){
                                $selected = 'selected="selected"';
                                }elseif(in_array($permission->id, $agents)){
                                $selected = 'selected';
                                }
                                @endphp
                                <option value="{{$permission->id}}" {{$selected}}>{{ $permission->name }}</option>
                                @endforeach
                            </select>
                            @error('permission')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-right">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->
@endsection
