@extends('layouts.app', [
'activePage' => 'role-details',
'menuParent' => 'role-details',
'title' => 'Role Details',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                {{__('Roles')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Role</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">Details</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Click Triggered -->
    <div class="block block-rounded">
        <div class="block-header">
            <h5>{{strtoupper($role->name)}}</h5>
        </div>
        <div class="block-content">
            <div class="row items-push text-start">
                @foreach($rolePermissions as $key => $permission)
                <div class="col-sm-6 col-md-3 col-xl-3">
                    <span>{{ $key+1 }}.</span> <label class="">{{$permission->name}}</button>
                </div>
            @endforeach

            @if(count($rolePermissions)==0)
            <label style="text-align: center">Permission Not Assigned</label>
            @endif
        </div>
    </div>
    <!-- END Click Triggered -->
</div>
@endsection
