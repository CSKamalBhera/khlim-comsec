@extends('layouts.app', [
'activePage' => 'role-list',
'menuParent' => 'role-list',
'title' => 'Roles',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                {{__('Roles')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Roles</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">List</a>
                    </li>
                </ol>
            </nav>
          </div>
          <div class="col-sm-4">
              <div class="text-right">
                @can('role_create')
                    <a href="{{ route('roles.create') }}" class="btn btn-primary">Create</a>
                @endcan
              </div>
          </div>
        </div>
    </div>
</div>
<!-- END Hero -->
<!-- Dynamic Table with Export Buttons -->
<div class="content">
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">#</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $key => $role)
                    <tr>
                        <td class="text-center">{{ $key +1 }}</td>
                        <td class="font-w600">
                            <a href="{{ route('roles.show',$role->id) }}">{{ ucwords($role->name) }}</a>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <em class="text-muted">{{ $role->slug }}</em>
                        </td>

                        <td class="d-none d-sm-table-cell">
                            @if($role->status == 1)
                            <span class="badge badge-info">Active</span>
                            @else
                            <span class="badge badge-danger">Inactive</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                @if($role->name == 'Super Admin')

                                @else
                                @can('role_edit')
                                <a href="{{ route('roles.edit',$role->id) }}" class="btn btn-sm btn-light"><i class="nav-main-link-icon fa fa-edit"></i></a>
                                @endcan
                                @can('role_status')
                                @if($role->status == 1)
                                <a href="{{url('roles/status/'.$role->id)}}" class="btn btn-sm btn-light" title="active"><i class="fa fa-unlock"></i></a>
                                @else
                                <a href="{{url('roles/status/'.$role->id)}}" class="btn btn-sm btn-light" title="inactive"><i class="fa fa-lock"></i></a>
                                @endif
                                @endcan
                                @can('role_delete')
                                <form action="{{ route('roles.destroy',$role->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-light delete-roles delete" data-toggle="delete" data-name="{{ $role->name }}" title="{{__('Delete')}}">
                                        <i class="nav-main-link-icon fa fa-trash"></i>
                                    </button>
                                    @endif
                                </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->
@endsection
