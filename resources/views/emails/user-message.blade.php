@component('mail::message')
Hi <b>{{ $message['name'] }}</b>,

<br>
Your Account is Successfully created by,
<br>
Email: <b>{{ $message['email'] }}</b>,
<br>
Password: <b>{{ $message['password'] }}</b>
<br>

@component('mail::button', ['url' => $url, 'color' => 'success'])
You can login by click
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
