@extends('layouts.app', [
'activePage' => 'recurrence-List',
'menuParent' => 'recurrence-List',
'title' => 'Recurrences',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                Recurrence <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Recurrence</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">List</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
    </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <form action="" method="GET">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Department</label>
                            <select class="department form-control" required name="department">
                                <option disabled selected>Select Department</option>
                                @foreach($departments as $key => $department)
                                <option value="{{ $department->id }}" @if($department->id == Request::get('department')) selected @endif>{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Run Date From</label>
                            <input type="date" class="form-control" name="start_date" value="{{ Request::get('start_date')}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>To</label>
                            <input type="date" class="form-control" name="end_date" value="{{ Request::get('end_date')}}">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <option value="" selected>Select</option>
                                <option value="0" @if(0==Request::get('status')) selected @endif>Pending</option>
                                <option value="1" @if(1==Request::get('status')) selected @endif>Complete</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <!-- <label>&nbsp;</label> -->
                            <div class="form">
                                <button type="submit" class="btn btn-primary">Filters</button>
                                <a href="{{  Request::url() }}" class="btn btn-danger">Clear All</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px;">#</th>
                        <th>Job Type</th>
                        <th>Customer</th>
                        <th>Created</th>
                        <th>Run Date</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($recurrences as $key => $recurrence)
                    <tr>
                        <td class="text-center">{{ $key +1 }}</td>
                        <td class="font-w600">
                            {{ ucwords($recurrence->job_type->name) }}
                        </td>
                        <td class="d-none d-sm-table-cell"><em class="text-muted">{{ ucwords($recurrence->company->name) }}</em></td>
                        <td><em class="text-muted">{{ $recurrence ?  \Carbon\Carbon::parse($recurrence->created_at)->format('d-m-Y') : '' }}</em></td>
                        <td><em class="text-muted">{{ $recurrence ?  \Carbon\Carbon::parse($recurrence->run_date)->format('d-m-Y') : ''  }}</em></td>
                        <td class="d-none d-sm-table-cell text-center">
                            @if($recurrence->status == 1)
                                <span class="badge badge-info">Complete</span>
                            @else
                                <span class="badge badge-danger">Pending</span>
                            @endif
                        </td>
                        <td class="font-w600 text-center">
                            <a class="btn btn-sm btn-light" href="{{ route('recurrence.show',$recurrence->id) }}"><i class="nav-main-link-icon si si-eye"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->
@endsection
