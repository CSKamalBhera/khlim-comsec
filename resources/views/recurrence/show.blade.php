@extends('layouts.app', [
'activePage' => 'recurrence Details',
'menuParent' => 'recurrence Details',
'title' => 'Recurrence Details',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                Recurrence <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('recurrence.index') }}">Recurrence</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx  text-black">Details</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">{{ $recurrence->job_type->name }} - {{ $recurrence->company->name }}</h3>
            @can('recurrence_edit')
            <a href="{{ route('recurrence.edit',$recurrence->id) }}" class="btn btn-secondary mx-2">Edit</a>
            @endcan
            @can('recurrence_delete')
            <form action="{{ route('recurrence.destroy', $recurrence->id) }}" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" data-name="{{ $recurrence->job_type->name }} - {{ $recurrence->company->name }}" class="btn btn btn-danger delete">Delete</button>
            </form>
            @endcan
        </div>

        <div class="block-content block-content-full">
            <div class="row push">
                <div class="col-lg-4 col-xl-4">
                    <label>Created</label>
                    <p>{{ $recurrence ?  \Carbon\Carbon::parse($recurrence->created_at)->format('d-m-Y') : ''  }}</p>
                    <label>Run Date</label>
                    <p>{{ $recurrence ?  \Carbon\Carbon::parse($recurrence->run_date)->format('d-m-Y') : ''  }}</p>
                    <label>Status</label>
                    <p>
                        @if($recurrence->status == 1)
                        Complete
                        @else
                        Pending
                        @endif
                    </p>
                    <label>Created By</label>
                    <p>{{ $recurrence ? $recurrence->created_user->name : ''  }}</p>
                </div>
                <div class="col-lg-4 col-xl-4">
                    <label>Company </label>
                    <p>{{ $recurrence->company->name }}</p>
                    <label>Staff Assigned </label>
                    <p>{{ $recurrence->staff->name }}</p>
                    <label>Department</label>
                    <p>{{ $recurrence->department->name }}</p>
                </div>
                <div class="col-lg-4">
                    <label>Job Type</label>
                    <p>{{ $recurrence->job_type->name }}</p>
                    <label>Manager Assigned</label>
                    <p>{{ $recurrence->manager->name }}</p>
                    <label>Recurrence From</label>
                    <p><a href="{{ route('jobs.show',$recurrence->job_id) }}">{{ $recurrence->job_type->name  }} - {{ $recurrence->company->name }} <i class="fa fa-external-link-alt"></i></a></p>

                </div>
            </div>

        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->
<div class="content">
    <div class="block block-rounded">
        <div class="block-content text-start">
            @foreach($recurrence->job_type->workflows as $key => $workflow)
            <p><span>{{ $key + 1 }} . </span> {{ $workflow->title }}</p>
            @endforeach
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection
