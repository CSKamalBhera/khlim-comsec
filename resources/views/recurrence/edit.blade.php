@extends('layouts.app', [
'activePage' => 'edit-recurrence',
'menuParent' => 'edit-recurrence',
'title' => 'Edit Recurrence',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                Recurrence <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('recurrence.index') }}">Recurrence</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx  text-black">Edit</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">{{ $recurrence->job_type->name }} - {{ $recurrence->company->name }}</h3>
        </div>

        <div class="block-content block-content-full">
            <form action="{{ route('recurrence.update',$recurrence->id) }}" method="POST">
                @csrf
                @method('put')
                <div class="row push">
                    <div class="col-lg-6">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">Customer</label>
                                <input type="text" disabled class="form-control form-control-alt" value="{{ $recurrence->company->name }}">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="job_type_id">Job Types</label>
                                <input type="text" disabled class="form-control form-control-alt" placeholder="Description" value="{{ $recurrence->job_type->name }}">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="description">Department</label>
                                <input type="text" disabled class="form-control form-control-alt" value="{{ $recurrence->department->name }}">
                            </div>
                        </div>
                    </div>
                    @php
                        $departUsers = \App\Models\Users\User::with('departments')
                            ->whereHas('departments', function($query) use ($recurrence){
                                $query->where('id',$recurrence->department_id);
                            })->get();
                    @endphp
                    <div class="col-lg-6">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="description">Manager<span class="text-danger">*</span></label>
                                <select class="form-control select2 @error('managerId') is-invalid @enderror" name="managerId">
                                    <option disabled>Select</option>
                                    @foreach($departUsers as $user)
                                    <option value="{{ $user->id }}" {{ old('managerId', $recurrence->manager_id) == $user->id ? 'selected' : '' }}>{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                @error('managerId')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="description">Staff<span class="text-danger">*</span></label>
                                <select class="form-control select2 @error('staffId') is-invalid @enderror" name="staffId">
                                    <option disabled>Select</option>
                                    @foreach($departUsers as $user)
                                    <option value="{{ $user->id }}" {{ old('staffId', $recurrence->staff_id) == $user->id ? 'selected' : '' }}>{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                @error('staffId')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">Run Date<span class="text-danger">*</span></label>
                                <input type="date" class="form-control @error('staffId') is-invalid @enderror" name="runDate" value="{{ $recurrence ?  \Carbon\Carbon::parse($recurrence->run_date)->format('Y-m-d') : '' }}">
                                @error('runDate')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->
@endsection
