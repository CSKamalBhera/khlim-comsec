@extends('layouts.app', [
'activePage' => 'edit-job',
'menuParent' => 'edit-job',
'title' => 'Edit job',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-12 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    Jobs <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('jobs.index') }}">Job</a></li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">Edit</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Page Content -->
@php
$departUsers = \App\Models\Users\User::with('departments')
->whereHas('departments', function($query) use ($job){
$query->where('id',$job->department_id);
})->get();
if ($job->jobWorkflow->isEmpty()) {
$disabled = '';
}else {
$disabled = 'disabled';
}
@endphp
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <div class="row push">
                <div class="col-lg-8 col-xl-5">
                    <form action="{{ route('jobs.update',$job->id) }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">Customer<span class="text-danger">*</span></label>
                                <select class="form-control select2 @error('companyId') is-invalid @enderror" name="companyId" style="width: 100%;" data-placeholder="Choose..">
                                    <option disabled>Select</option>
                                    @foreach($companies as $company)
                                    <option value="{{ $company->id }}" {{ old('companyId', $job->company_id) == $company->id ? 'selected' : $disabled }}>{{ $company->name }}</option>
                                    @endforeach
                                </select>
                                @error('companyId')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="description">Department<span class="text-danger">*</span></label>
                                <select class="work form-control select2 @error('departmentId') is-invalid @enderror" name="departmentId" style="width: 100%;" data-placeholder="Choose..">
                                    <option disabled>Select</option>
                                    @foreach($departments as $department)
                                    <option value="{{ $department->id }}" {{ old('departmentId', $job->department_id) == $department->id ? 'selected' : $disabled }} >{{ $department->name }}</option>
                                    @endforeach
                                </select>
                                @error('departmentId')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="description">Staff<span class="text-danger">*</span></label>
                                <select id="staff" class="form-control select2 @error('staffId') is-invalid @enderror" name="staffId" style="width: 100%;" data-placeholder="Choose..">
                                    <option disabled>Select</option>
                                    @foreach($departUsers as $user)
                                    <option value="{{ $user->id }}" {{ old('staffId', $job->staff_id) == $user->id ? 'selected' : '' }}>{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                @error('staffId')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="job_type_id">Job Types<span class="text-danger">*</span></label>
                                <select id="option" class="form-control select2 @error('job_type_id') is-invalid @enderror" name="job_type_id" style="width: 100%;" data-placeholder="Choose..">
                                    <option disabled >Select</option>
                                    @foreach($jobTypes as $jobType)
                                    <option value="{{ $jobType->id }}" {{ old('job_type_id', $job->job_type_id) == $jobType->id ? 'selected' : 'disabled' }}>{{ $jobType->name }}</option>
                                    @endforeach
                                </select>
                                @error('job_type_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="description">Manager<span class="text-danger">*</span></label>
                                <select id='manager' class="form-control select2 @error('managerId') is-invalid @enderror" name="managerId" style="width: 100%;" data-placeholder="Choose..">
                                    <option  disabled>Select</option>
                                    @foreach($departUsers as $user)
                                    <option value="{{ $user->id }}" {{ old('managerId', $job->manager_id) == $user->id ? 'selected' : '' }} >{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                @error('managerId')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
                <!-- Start Timeline -->
                <div class="block block-rounded job-timeline">

                    <div class="block-content block-content-full">
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">
                                <!-- END Timeline -->
                                <label>Timeline</label>
                                <ul class="fa-ul">
                                    @foreach($activities as $key => $activitie)

                                    <li>
                                        @if($activitie->log_name == 'creation')
                                        <i class="fa fa-li fa-plus-circle"></i>
                                        @else
                                        <i class="fa fa-li fa-edit"></i>
                                        @endif
                                        {{ $activitie->log_name }} on {{  \Carbon\Carbon::parse($activitie->created_at)->format('d-m-Y g:i A') }} by {{ $activitie->properties['causer_name'] }} </p>
                                        @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->
@endsection
@push('scripts')
<script type="text/javascript">
    // CSRF Token
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function () {
        var departmentId = '';
        $(document).on('change', '.work', function () {
            var departmentId = $(this).val();
            $('#option').empty();
            $.ajax({
                type: 'get',
                url: "{{route('getJobType')}}",
                data: 'department=' + departmentId,
                success: function (data) {
                    var html = '<option disabled selected value="">Select</option>';
                    html += data;
                    $('#option').html(html);
                }
            });

            $('#staff').empty();
            $.ajax({
                type: 'get',
                url: "{{route('getStaff')}}",
                data: 'department=' + departmentId,
                success: function (data) {
                    var html = '<option disabled selected value="">Select</option>';
                    html += data;
                    $('#staff').html(html);
                }
            });
            $('#manager').empty();
            $.ajax({
                type: 'get',
                url: "{{route('getManager')}}",
                data: 'department=' + departmentId,
                success: function (data) {
                    var html = '<option disabled selected value="">Select</option>';
                    html += data;
                    $('#manager').html(html);
                }
            });
        });
    });
</script>
@endpush
