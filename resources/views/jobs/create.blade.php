@extends('layouts.app', [
'activePage' => 'create-job',
'menuParent' => 'create-job',
'title' => 'Create Job',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-12 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    Jobs <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('jobs.index') }}">Job</a></li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">Create</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <form action="{{ route('jobs.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="description">Department<span class="text-danger">*</span></label>
                            <select class="work select2 form-control  @error('departmentId') is-invalid @enderror" name="departmentId" style="width: 100%;" data-placeholder="Choose..">
                                <option disabled selected>Select</option>
                                @foreach($departments as $department)
                                <option value="{{ $department->id }}" @if($department->id == old('departmentId')) selected @endif>{{ $department->name }}</option>
                                @endforeach
                            </select>
                            @error('departmentId')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="job_type_id">Job Types<span class="text-danger">*</span></label>
                            <select id="option" class="form-control select2 @error('job_type_id') is-invalid @enderror" name="job_type_id" style="width: 100%;" data-placeholder="Choose..">
                                <option disabled selected>Select</option>
                                @if(!empty(old('departmentId')))
                                @php
                                $jobTypes = \App\Models\Jobs\JobType::where('department_id',old('departmentId'))->get();
                                @endphp
                                @foreach($jobTypes as $jobType)
                                <option value="{{ $jobType->id }}" @if($jobType->id == old('job_type_id')) selected @endif>{{ $jobType->name }}</option>
                                @endforeach
                                @endif
                            </select>
                            @error('job_type_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="name">Customer<span class="text-danger">*</span></label>
                            <select class="form-control select2 @error('companyId') is-invalid @enderror" name="companyId" style="width: 100%;" data-placeholder="Choose..">
                                <option disabled selected>Select</option>
                                @foreach($companies as $company)
                                <option value="{{ $company->id }}" @if($company->id == old('companyId')) selected @endif>{{ $company->name }}</option>
                                @endforeach
                            </select>
                            @error('companyId')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="description">Staff<span class="text-danger">*</span></label>
                            <select id="staff" class="form-control select2 @error('staffId') is-invalid @enderror" name="staffId" style="width: 100%;" data-placeholder="Choose..">
                                <option disabled selected>Select</option>
                                @if(!empty(old('departmentId')))
                                @php
                                $id = old('departmentId');
                                $users = \App\Models\Users\User::with('departments','roles')
                                ->whereHas('departments', function($query) use ($id){
                                $query->where('id',$id);
                                })->get();
                                @endphp
                                @foreach($users as $user)
                                <option value="{{ $user->id }}" @if($user->id == old('staffId')) selected @endif>{{ $user->name }}</option>
                                @endforeach
                                @endif
                            </select>
                            @error('staffId')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="description">Manager<span class="text-danger">*</span></label>
                            <select id='manager' class="form-control select2 @error('managerId') is-invalid @enderror" name="managerId" style="width: 100%;" data-placeholder="Choose..">
                                <option disabled selected>Select</option>
                                @if(!empty(old('departmentId')))
                                @php
                                $id = old('departmentId');
                                $users = \App\Models\Users\User::with('departments','roles')
                                ->whereHas('departments', function($query) use ($id){
                                $query->where('id',$id);
                                })->get();
                                @endphp
                                @foreach($users as $user)
                                <option value="{{ $user->id }}" @if($user->id == old('managerId')) selected @endif>{{ $user->name }}</option>
                                @endforeach
                                @endif
                            </select>
                            @error('managerId')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->

@endsection
@push('scripts')
<script type="text/javascript">
// CSRF Token
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function () {
        var departmentId = '';
        $(document).on('change', '.work', function () {
            var departmentId = $(this).val();
            $('#option').empty();
            $.ajax({
                type: 'get',
                url: "{{route('getJobType')}}",
                data: 'department=' + departmentId,
                success: function (data) {
                    var html = '<option disabled selected value="">Select</option>';
                    html += data;
                    $('#option').html(html);
                }
            });
            $('#staff').empty();
            $.ajax({
                type: 'get',
                url: "{{route('getStaff')}}",
                data: 'department=' + departmentId,
                success: function (data) {
                    var html = '<option disabled selected value="">Select</option>';
                    html += data;
                    $('#staff').html(html);
                }
            });
            $('#manager').empty();
            $.ajax({
                type: 'get',
                url: "{{route('getManager')}}",
                data: 'department=' + departmentId,
                success: function (data) {
                    var html = '<option disabled selected value="">Select</option>';
                    html += data;
                    $('#manager').html(html);
                }
            });
        });

//    $(document).on('focus','#option', function(){
//       var departmentId = $('.work').val();
//       if (!$('.work').val()){
//        $('#option').empty();
//        $.ajax({
//            type:'get',
//            url:"{{route('getJobType')}}",
//            data:'department='+departmentId,                
//            success:function(data){
//                var html = '<option disabled selected value="">Select</option>';
//                html += data;
//                $('#option').html(html);
//            }
//        });
//       }; 
//    });
//    
//    $(document).on('focus','#staff', function(){
//       var departmentId = $('.work').val();
//       if (!$('.work').val()){
//        $('#staff').empty();
//        $.ajax({
//            type:'get',
//            url:"{{route('getStaff')}}",
//            data:'department='+departmentId,                
//            success:function(data){
//                var html = '<option disabled selected value="">Select</option>';
//                html += data;
//                $('#staff').html(html);
//            }
//        }); 
//       }; 
//    });
//    
//    $(document).on('focus','#manager', function(){
//       var departmentId = $('.work').val();
//       if (!$('.work').val()){
//        $('#manager').empty();
//        $.ajax({
//            type:'get',
//            url:"{{route('getManager')}}",
//            data:'department='+departmentId,                
//            success:function(data){
//                var html = '<option disabled selected value="">Select</option>';
//                html += data;
//                $('#manager').html(html);
//            }
//        }); 
//       }; 
//    });

    });

</script>
@endpush
