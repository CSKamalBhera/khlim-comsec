@extends('layouts.app', [
'activePage' => 'jobs-list',
'menuParent' => 'jobs-list',
'title' => 'Jobs',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-8 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    Job <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Job</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">List</a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="col-sm-4">
                <div class="text-right">
                    @can('jobs_create')
                    <a href="{{ route('jobs.create') }}" class="btn btn-primary">Create</a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full font-size-sm">
                <thead>
                    <tr>
                        <th class="text-left" style="width: 10px;">#</th>
                        <th>Job Type</th>
                        <th>Customer</th>
                        <th>Created</th>
                        <th>Status</th>
                        <th>Updated</th>
                        <th style="width:10px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($jobs as $key => $job)
                    <tr>
                        <td>{{ $key +1 }}</td>
                        <td><a href="{{ route('jobs.show',$job->id) }}">{{ $job->job_type->name }}</a></td>
                        <td>{{ $job->company->name ?? 'N/A' }}</td>
                        <td>{{ $job->created_at ? \Carbon\Carbon::parse($job->created_at)->format('d-m-Y') : ''}}</td>
                        <td>
                            @foreach($job->jobWorkflow as $key => $value)
                            @endforeach
                            @if($job->jobWorkflow->last() && isset($value))
                            {{ $value->jobTypeWorkflow->title }} <span class="badge badge-success">{{ $value->option->options }}</span>
                            @else
                            <span class="badge badge-info ">New</span>
                            @endif
                        </td>
                        <td>{{ $job->updated_at ? \Carbon\Carbon::parse($job->updated_at)->format('d-m-Y') : ''}}</td>
                        <td class="font-w600 text-center" style="width: 10px;">
                            @can('jobs_edit')
                            <a class="btn btn-sm btn-light" href="{{ route('jobs.edit',$job->id) }}"><i class="fa fa-edit"></i></a>
                            @endcan
                            <!--@if($job->status == 1)
                                <a href="{{url('job/status/'.$job->id)}}" class="btn btn-sm btn-light" title="active"><i class="fa fa-unlock"></i></a>
                            @else
                                <a href="{{url('job/status/'.$job->id)}}" class="btn btn-sm btn-light" title="active"><i class="fa fa-lock"></i></a>
                            @endif-->
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->
@endsection
