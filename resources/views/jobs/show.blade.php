@extends('layouts.app', [
'activePage' => 'jobs-workflow',
'menuParent' => 'jobs-workflow',
'title' => 'Jobs Workflow',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-12 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    Jobs <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('jobs.index') }}">Job</a></li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">Workflow</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content content-boxed">
    <!-- User Profile -->
    <div class="block block-rounded">
        <div class="block-header">
            <h3 class="block-title">{{ $job->job_type->name  }} - {{ $job->company->name  }}</h3>
        </div>
        <div class="block-content">

            <div class="row push">
                <div class="col-lg-4" style="border-right: 2px solid grey;">
                    <div class="form-group">
                        <label for="one-profile-edit-username">Customer</label>
                        <h5 class="font-size-md text-muted">{{ $job->company->name }}</h5>
                    </div>
                    <div class="form-group">
                        <label for="one-profile-edit-username">Staff Assigned</label>
                        <h5 class="font-size-md text-muted">{{ $job->staff->name }}</h5>
                    </div>
                    <div class="form-group">
                        <label for="one-profile-edit-username">Job Type</label>
                        <h5 class="font-size-md text-muted">{{ $job->job_type->name }}</h5>
                    </div>
                    <div class="form-group">
                        <label for="text-muted">Department</label>
                        <h5 class="font-size-md text-muted">{{ $job->department->name }}</h5>
                    </div>
                    <div class="form-group">
                        <label for="text-muted">Manager Assigned</label>
                        <h5 class="font-size-md text-muted">{{ $job->manager->name }}</h5>
                    </div>
                </div>
                <div class="col-lg-8 col-xl-8">
                    @php
                    $assignedUser = \App\Models\Jobs\JobWorkflow::where('job_id',$job->id)->where('assigned_id','!=',null)->pluck('assigned_id')->toArray();
                    @endphp
                    {{-- check job assing manager and staff disabled functionlty --}}
                    @if($job->manager_id == auth()->user()->id || $job->staff_id == auth()->user()->id || in_array(auth()->user()->id, $assignedUser))
                    @php
                    $disabled = 0;
                    @endphp
                    @else
                    @php
                    $disabled = 'disabled';
                    @endphp
                    @endif
                    {{-- job workflow loop start --}}
                    @foreach ($job->job_type->workflows as $index => $workflow)
                    <span>{{ $index + 1 }} . </span> <label for="one-profile-edit-username">{{ $workflow->title }}</label>
                    {{-- check job workflow done mark status loop start --}}
                    @foreach($job->jobWorkflow as $key => $job_workflow)
                    @if($job->manager_id == auth()->user()->id || $job->staff_id == auth()->user()->id || in_array(auth()->user()->id, $assignedUser))
                    @php $disabled = $key+1; @endphp
                    @if(isset($approval))
                    @if ($approval->status == 1)
                    @php $disabled = $key+1; @endphp
                    @elseif($approval->status == 2)
                    @php $disabled = $key+1; @endphp
                    @else
                    @php $disabled = 'disabled'; @endphp
                    @endif
                    @endif
                    @if($job_workflow->option->options == 'Not processed' || $job_workflow->option->options == 'Queried' || $job_workflow->option->options == 'Pending' || $job_workflow->option->options == 'On Hold' || $job_workflow->option->options == 'Decline' || $job_workflow->option->id == 14 || $job_workflow->option->id == 15 || $job_workflow->option->id == 16 || $job_workflow->option->options == 'Close' 
                    || $job_workflow->option->options == 'Extended' || $job_workflow->option->options == 'Incomplete')
                    @php $disabled = 'disabled'; @endphp
                    @endif
                    @else
                    @php $disabled = 'disabled'; @endphp
                    @endif
                    {{-- check job workflow which workflow are submited --}}
                    @if ($job_workflow->job_type_workflow_id == $workflow->id)

                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group ml-3">
                                @if ($job_workflow->option->options == 'Assigned')
                                <select class="form col-lg-3 ml-2" required disabled style="padding: 5px; border-radius: 4px;" name="assignId">
                                    <option selected value="">Select</option>
                                    @foreach($users as $user)
                                    <option value="{{ $user->id }}" @if($user->id == $job_workflow->assigned_id) selected @endif>{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                @endif

                                {{-- ssm result extend document without payment--}}
                                @if ($job_workflow->option->options == 'Queried' || $job_workflow->option->id == 14 || $job_workflow->option->id == 15 || $job_workflow->option->id == 16)
                                <form action="{{ route('job-workflow.update',$job_workflow->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <input type="hidden" name="job_id" value="{{ $job->id }}">
                                    <input type="hidden" name="workflow_type_id" value="{{ $workflow->workflow_type->id }}">
                                    <select class="form col-lg-3 ml-2" style="padding: 5px; border-radius: 4px;" name="workflow_option_id">
                                        @foreach($workflow->workflow_type->workflowoption as $key => $workflowOption)
                                        <option value="{{ $workflowOption->id  }}" @if($job_workflow->workflow_option_id == $workflowOption->id)
                                            selected
                                            @endif>{{ $workflowOption->options  }}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" class="btn btn-alt-primary mx-2">Submit</button>
                                </form>
                                @elseif ($job_workflow->option->options == 'Pending')
                                <form action="{{ route('job-workflow.update',$job_workflow->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <input type="hidden" name="workflow_type_id" value="{{ $workflow->workflow_type->id }}">
                                    <input type="hidden" name="job_id" value="{{ $job->id }}">
                                    <select class="form col-lg-3 ml-2" style="padding: 5px; border-radius: 4px;" name="workflow_option_id">
                                        @foreach($workflow->workflow_type->workflowoption as $key => $workflowOption)
                                        <option value="{{ $workflowOption->id  }}" @if($job_workflow->workflow_option_id == $workflowOption->id)
                                            selected
                                            @endif>{{ $workflowOption->options  }}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" class="btn btn-alt-primary mx-2">Submit</button>
                                </form>
                                @elseif ($job_workflow->option->options == 'On Hold' || $job_workflow->option->options == 'Incomplete')
                                <form action="{{ route('job-workflow.update',$job_workflow->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <input type="hidden" name="workflow_type_id" value="{{ $workflow->workflow_type->id }}">
                                    <input type="hidden" name="job_id" value="{{ $job->id }}">
                                    <select class="form col-lg-3 ml-2" style="padding: 5px; border-radius: 4px;" name="workflow_option_id">
                                        @foreach($workflow->workflow_type->workflowoption as $key => $workflowOption)
                                        <option value="{{ $workflowOption->id  }}" @if($job_workflow->workflow_option_id == $workflowOption->id)
                                            selected
                                            @endif>{{ $workflowOption->options  }}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" class="btn btn-alt-primary mx-2">Submit</button>
                                </form>
                                @elseif ($job_workflow->option->options == 'Extended')
                                <form action="{{ route('job-workflow.update',$job_workflow->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <input type="hidden" name="job_workflow" value="{{ $job_workflow->id }}">
                                    <input type="hidden" name="job_type_workflow_id" value="{{ $workflow->id }}">
                                    <input type="hidden" name="workflow_type_id" value="{{ $workflow->workflow_type->id }}">
                                    <input type="hidden" name="job_id" value="{{ $job->id }}">
                                    <select class="form col-lg-3 ml-2" style="padding: 5px; border-radius: 4px;" name="workflow_option_id">
                                        @foreach($workflow->workflow_type->workflowoption as $key => $workflowOption)
                                        <option value="{{ $workflowOption->id  }}" @if($job_workflow->workflow_option_id == $workflowOption->id)
                                            selected
                                            @endif>{{ $workflowOption->options  }}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" class="btn btn-alt-primary mx-2">Submit</button>
                                </form>
                                @else
                                <button type="submit" class="btn btn-alt-primary mx-2" disabled>{{ $job_workflow->option->options  }}</button>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <p> {{ \Carbon\Carbon::parse($job_workflow->created_at)->format('d-m-Y g:i A') }} by {{ $job_workflow->user->name }}</p>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    <form action="{{ route('job-workflow.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    {{-- job workflow option loop start --}}
                                    @php
                                    $workflowId = \App\Models\Jobs\JobWorkflow::where('job_id',$job->id)->pluck('job_type_workflow_id')->toArray();
                                    $departUsers = \App\Models\Users\User::with('departments')
                                    ->whereHas('departments', function($query) use ($job){
                                    $query->where('id',$job->department_id);
                                    })->get();
                                    @endphp
                                    @if(in_array($workflow['id'], $workflowId))
                                    @else
                                    <input type="hidden" id="job_id" name="job_id" value="{{ $job->id }}">
                                    <input type="hidden" name="job_type_id" value="{{ $job->job_type->id }}">
                                    <input type="hidden" name="job_type_workflow_id" value="{{ $workflow->id }}">
                                    <input type="hidden" name="workflow_type_id" value="{{ $workflow->workflow_type->id }}">
                                    <input type="hidden" name="job_workflow" value="">
                                    @if ($workflow->workflow_type->name == 'Assign User & Status')
                                    <div class="row my-2 ml-1">
                                        <select class="form col-lg-4 ml-4 " @if ($index !=$disabled) disabled @else active @endif style="padding: 5px; border-radius: 4px;" name="assignId">
                                            <option selected value="">Select</option>
                                            @foreach($departUsers as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                    @if($workflow->workflow_type->name == 'Status & JobOutput')
                                    <div class="custom-file my-3 ml-2 dropzone" id="dropbox">
                                        <!-- Populating custom file input label with the selected filename (data-toggle="custom-file-input" is initialized in Helpers.coreBootstrapCustomFileInput()) -->
                                        <input type="file" class="visually-hidden custom-file-input @error('file') is-invalid @enderror" data-toggle="custom-file-input" id="file" name="file" style="padding: 5px; border-radius: 4px;" @if ($index !=$disabled) disabled @endif>
                                        <div class="dropzone-conetnt">
                                            <i class="fa fa-file-upload"></i>
                                            <div id="fileList">
                                                <label class="custom-file" for="file">Choose a new pdf</label>
                                            </div>
                                        </div>
                                        @error('file')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    @endif
                                    <div class="row my-2 ml-1">
                                        <select class="col-lg-4 ml-4 form" @if ($index !=$disabled) disabled @else active @endif style="padding: 5px; border-radius: 4px;" name="workflow_option_id">
                                            @foreach($workflow->workflow_type->workflowoption as $key => $workflowOption)
                                            <option value="{{ $workflowOption->id  }}">{{ $workflowOption->options  }}</option>
                                            @endforeach
                                        </select>
                                        <button type="submit" @if ($index !=$disabled) disabled @else active @endif class="btn btn-alt-primary mx-2">Submit</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                    @endforeach
                    <!-- Start Timeline -->
                    <div class="block block-rounded">

                        <div class="block-content block-content-full">
                            <div class="row">
                                <div class="activity col-lg-12 col-xl-12">
                                    <!-- END Timeline -->
                                    <label>Timeline</label>
                                    <ul class="booking-list">
                                        <li>
                                            <div class="comment more">
                                                @foreach($activities as $key => $activitie)
                                                @if($key < 5)
                                                @php $activity = json_decode($activitie->properties)@endphp
                                                <p>
                                                    @if($activitie->log_name == 'creation')
                                                    <i class="fa fa-plus-circle"></i>
                                                    @else
                                                    <i class="fa fa-edit"></i>
                                                    @endif
                                                    {{ $activitie->log_name }},{{$activity->JobWorkflow_name?? ""}} : {{$activity->new_value ??""}} on {{ \Carbon\Carbon::parse($activitie->created_at)->format('d-m-Y g:i A') }} by {{ $activitie->properties['causer_name'] }} 
                                                </p>
                                                @endif
                                                @endforeach
                                                <span class="read-more-show d-none cursor-pointer">Read More <i class="fa fa-angle-down"></i> ....</span>
                                                <span class="read-more-content"> 
                                                    @foreach($activities as $key => $activitie)
                                                    @if($key > 4)
                                                    @php $activity = json_decode($activitie->properties)@endphp
                                                    <p>
                                                        @if($activitie->log_name == 'creation')
                                                        <i class="fa fa-plus-circle"></i>
                                                        @else
                                                        <i class="fa fa-edit"></i>
                                                        @endif
                                                        {{ $activitie->log_name }},{{$activity->JobWorkflow_name?? ""}} : {{$activity->new_value ??""}} on {{ \Carbon\Carbon::parse($activitie->created_at)->format('d-m-Y g:i A') }} by {{ $activitie->properties['causer_name'] }} </p> @endif
                                                    @endforeach
                                                    <span class="read-more-hide d-none cursor-pointer">Read Less <i class="fa fa-angle-up"></i></span> </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END User Profile -->
</div>
@endsection
@push('scripts')
<script>
//    let dropbox;
//    dropbox = document.getElementById("file");
//    dropbox.addEventListener("click", handleFiles, false);
//    dropbox.addEventListener("change", handleFiles, false);
//    fileList = document.getElementById("fileList");
//
//    function handleFiles() {
//        if (!this.files.length) {
//            fileList.innerHTML = " <label class='custom-file' for='file'>Choose a new pdf</label>";
//        } else {
//            fileList.innerHTML = "";
//            const list = document.createElement("ul");
//            fileList.appendChild(list);
//            for (let i = 0; i < this.files.length; i++) {
//                const li = document.createElement("li");
//                list.appendChild(li);
//
//                const img = document.createElement("img");
//                img.src = URL.createObjectURL(this.files[i]);
//                img.height = 60;
//                img.onload = function() {
//                    URL.revokeObjectURL(this.src);
//                }
//                //   li.appendChild(img);
//                const info = document.createElement("span");
//                info.innerHTML = this.files[i].name + ": " + (this.files[i].size / (1024 * 1024)).toFixed(2) + "MB";
//                li.appendChild(info);
//            }
//        }
//    }


    // Hide the extra content initially, using JS so that if JS is disabled, no problemo:
    $('.read-more-content').addClass('d-none')
    $('.read-more-show, .read-more-hide').removeClass('d-none')

    // Set up the toggle effect:
    $('.read-more-show').on('click', function (e) {
        $(this).next('.read-more-content').removeClass('d-none');
        $(this).addClass('d-none');
        e.preventDefault();
    });

    // Changes contributed by @diego-rzg
    $('.read-more-hide').on('click', function (e) {
        var p = $(this).parent('.read-more-content');
        p.addClass('d-none');
        p.prev('.read-more-show').removeClass('d-none'); // Hide only the preceding "Read More"
        e.preventDefault();
    });

</script>
@endpush
