@extends('layouts.app', [
  'activePage' => 'department-list',
  'menuParent' => 'department-list',
  'title' => 'Departments',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                Department <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Department</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">List</a>
                    </li>
                </ol>
            </nav>
          </div>
          <div class="col-sm-4">
              <div class="text-right">
                  @can('department_create')
                    <a href="{{ route('departments.create') }}" class="btn btn-primary">Create</a>
                  @endcan
              </div>
          </div>
        </div>
   </div>
</div>
<!-- END Hero -->
        <!-- Dynamic Table with Export Buttons -->
    <div class="content">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full font-size-sm">
                    <thead>
                        <tr>
                            <th class="text-left" style="width: 80px;">#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($departments as $key => $department)
                            <tr>
                                <td class="text-left">{{ $key +1 }}</td>
                                <td >
                                    {{ ucwords($department->name) }}
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    <span class="text-muted">{{ $department->description }}</span>
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    @if($department->status == 1)
                                    <span class="badge badge-info">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                </td>
                                <td class="font-w600">
                                    @can('department_edit')
                                    <a class="btn btn-sm btn-light" href="{{ route('departments.edit',$department->id) }}"><i class="nav-main-link-icon fa fa-edit"></i></a>
                                    @endcan
                                    @can('department_delete')
                                    <form action="{{ route('departments.destroy', $department->id) }}" method="POST" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button data-name="{{ $department->name }}" type="submit" class="btn btn-sm btn-light delete"><i class="nav-main-link-icon fa fa-trash"></i></button>
                                    </form>
                                    @endcan
                                    @can('department_status')
                                    @if($department->status == 1)
                                        <a href="{{url('departments/status/'.$department->id)}}" class="btn btn-sm btn-light" title="active"><i class="fa fa-unlock"></i></a>
                                    @else
                                        <a href="{{url('departments/status/'.$department->id)}}" class="btn btn-sm btn-light" title="active"><i class="fa fa-lock"></i></a>
                                    @endif
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->
@endsection
