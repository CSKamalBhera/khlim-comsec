@extends('layouts.app', [
'activePage' => 'edit-department',
'menuParent' => 'edit-department',
'title' => 'Edit Department',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-8 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                {{__('Department')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('departments.index') }}">Department</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">Edit</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
   </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <form action="{{ route('departments.update',$department->id) }}" method="POST">
                @csrf
                @method('put')
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="name">Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                name="name" placeholder="Name" value="{{ $department->name ? $department->name : old('name') }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        <label for="description">Description<span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('description') is-invalid @enderror" id="description"
                                name="description" placeholder="Description" value="{{ $department->description ? $department->description : old('description') }}">
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->
@endsection
