@extends('layouts.app', [
'activePage' => 'customer-details',
'menuParent' => 'customer-details',
'title' => 'Customer Details',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-12 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    Customer <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Customer</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">Details</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- END Hero -->
<div class="content">
    <!-- Block Tabs -->

    <div class="row">
        <div class="col-lg-12">
            <!-- Block Tabs Default Style -->
            <div class="block block-rounded">
                <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home" href="#btabs-static-home">General</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-static-billing">Biliing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#btabs-static-payment">Payment</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile" href="#btabs-static-document">
                            Document
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pro" href="#btabs-static-portfolio">
                            Portfolio Holder
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="reminder" href="#btabs-static-reminder">
                            Reminders
                        </a>
                    </li>
                </ul>
                <div class="block-content tab-content">
                    <div class="tab-pane pb-4 active" id="btabs-static-home" role="tabpanel">
                        <div class="row">
                            <div class="col-md-3">
                                <ul class="nav flex-column nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#info">Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#contact">Contact</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#g-address">Address</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#bank-details">Bank Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#Person-in-Charge">Person-in-Charge</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#Responsible-Accountants">Responsible Professional
                                            Firm</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#Shares">Directors & Shareholders</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#Group-of-Companies">Group of Companies</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#Financial-Information">Financial Information</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="block-content tab-content">
                                    <div class="tab-pane active" id="info" role="tabpanel">
                                        <div class="block-header">
                                            <h5 >INFO</h5>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="name">Company Name :</label>
                                                        {{  $company->name ?? 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="email">Email :</label>
                                                        {{ $company->email ?? 'N/A' }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="company_roc"> ROC:</label>
                                                        {{  $company->roc ?? 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="incorporation_date">Incorporation Date:</label>
                                                        {{  $company->incorporation_date ?? 'N/A' }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="e_no">E no.:</label>
                                                        {{  $company->e_no ?? 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="c_no">C no.:</label>
                                                        {{ $company->c_no ?? 'N/A' }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="company_fak">Formerly Known As (FKA) :</label>
                                                        {{ $company->company_fak ?? 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="date_of_change">Date of Change</label>
                                                        {{  \Carbon\Carbon::parse($company->date_of_change)->format('Y-m-d') ?? 'N/A' }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="natureof_business">Nature of Business:</label>
                                                        {{  $company->natureof_business ?? 'N/A' }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="contact" role="tabpanel">
                                        <div class="block-header">
                                            <h5 >CONTACT</h5>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="phone">Phone: </label>
                                                        {{ $company->phone_no ?? 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="fax">Fax:</label>
                                                        {{  $company->fax_no ?? 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="contact">Contact No.</label>
                                                        {{ $company->contact ?? 'N/A'}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="bank-details" role="tabpanel">
                                        <div class="block-header">
                                            <h5 >BANK DETAILS</h5>
                                        </div>
                                        <div class="block-content block-content-full">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="bank_name">Bank Name: </label>
                                                        {{  $company->bank_name ?? 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="account_number">Account Number:</label>
                                                        {{ $company->account_no ?? 'N/A' }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="g-address" role="tabpanel">
                                        <div class="block-header">
                                            <h5 >ADDRESS</h5>
                                        </div>
                                        <div class="block-content block-content-full">

                                            @foreach($company->addresses as $address)
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="address_type">{{ $address->addresse_type }} @if($address->addresse_type == 'Delivery Address') (if different from the above) @endif </label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="address_name"> Name: </label>
                                                        {{ $address->name ? $address->name : 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="address_phone">Phone:</label>
                                                        {{ $address->phone ? $address->phone : 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="address">Address:</label>
                                                        {{ $address->address ? $address->address : 'N/A' }}
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="Person-in-Charge" role="tabpanel">
                                        <div class="block-header">
                                            <h5 >PERSON-IN-CHARGE</h5>
                                        </div>
                                        <div class="block-content block-content-full">
                                            @foreach($company->company_incharge as $key => $companyINCharge)
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="incharge_department">Department:</label><br>
                                                        {{ $companyINCharge->departments->name ?? 'N/A'}}
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="incharge_name">Name:</label><br>
                                                        {{ $companyINCharge->name ?? 'N/A'}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-4"><div class="form-group"></div></div>    
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="incharge_phone">Phone:</label><br>
                                                        {{ $companyINCharge->phone ?? 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="email">Email:</label><br>
                                                        {{  $companyINCharge->email ?? 'N/A'}}
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="Responsible-Accountants" role="tabpanel">
                                        <div class="block-header">
                                            <h5 >APPOINTED PROFESSIONAL FIRM</h5>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <div class="row">
                                                @foreach($departments as $key => $department)
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="responsible_name">{{ $department->name }}</label>
                                                        @foreach($company->account_responsible as $key1 => $accountResponsible)
                                                        @if( $accountResponsible->departments->id == $department->id)
                                                        @foreach (json_decode($accountResponsible->name) as $val => $value)
                                                        <div class="row mb-2">
                                                            <div class="col-sm-12">
                                                                {{ $value ? $value : 'N/A'}}
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="Shares" role="tabpanel">
                                        <div class="block-header">
                                            <h5 >DIRECTORS AND SHAREHOLDERS</h5>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="share_capital">Share Capital :</label>
                                                        @php
                                                        $fmt = new NumberFormatter($locale = 'en_MY', NumberFormatter::CURRENCY);
                                                        @endphp
                                                        {{ $company->shares ? $fmt->format($company->shares->share_capital) : '' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="shareholder_name">Shareholder :</label><br>
                                                        @if($company->shares->shareholder_name != null)
                                                        @foreach (json_decode($company->shares->shareholder_name) as $val => $value)
                                                        <div class="row py-2">
                                                            <div class="col-lg-8">
                                                                {{ isset($value) ? $val:'N/A' }}
                                                            </div>
                                                            <div class="col-lg-3">
                                                                {{ $value ?? 'N/A' }} @if(isset($value))%@endif
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        @endif
                                                        <label for="shareholder_updated_on">Updated on :</label><br>
                                                        {{ \Carbon\Carbon::parse($company->shares->shareholder_updated_on )->format('Y-m-d') ?? 'N/A'}}
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="director">Director :</label>
                                                        @if($company->shares->director != null)
                                                        @foreach (json_decode($company->shares->director) as $key => $value)
                                                        <div class="row mb-2">
                                                            <div class="col">
                                                                {{ $value ?? 'N/A' }}
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        @else
                                                        {{'N/A'}}
                                                        @endif
                                                        <label for="director_updated_on">Updated on:</label><br>
                                                        {{  \Carbon\Carbon::parse($company->shares->director_updated_on )->format('Y-m-d') ?? 'N/A' }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="Group-of-Companies" role="tabpanel">
                                        <div class="block-header">
                                            <h5 >GROUP OF COMPANIES</h5>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label for="groupof_companies">Group of Companies :</label>

                                                    @if($company->related_company != null)
                                                    @foreach (json_decode($company->related_company->name) as $key => $value)

                                                    @if($value != null)
                                                    <div class="form-group">{{ $value ? $value : 'N/A'  }} </div>
                                                    @else
                                                    {{'N/A'}}
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="Financial-Information" role="tabpanel">
                                        <div class="block-header">
                                            <h5 >FINANCIAL INFORMATION</h5>
                                        </div>
                                        <div class="block-content block-content-full">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="financial_year_end">Financial Year End: </label>
                                                        {{ $company->financial_year_end ? $company->financial_year_end : 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="annual_return_date">Annual Return Date: </label>
                                                        {{ $company->annual_return_date ? date("jS F",strtotime($company->annual_return_date)) : 'N/A'}}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="turnover">Turnover: </label>

                                                        {{$company->turnover? $fmt->format($company->turnover) : 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="profit_before_tax">Profit before Tax: </label>
                                                        {{  $fmt->format($company->profit_before_tax) ?? 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="strike_of">Status of the company: </label>
                                                        @if($company->acitivity_status  == 4)
                                                        Dormant
                                                        @elseif($company->acitivity_status  == 3)
                                                        Semi Active
                                                        @elseif($company->acitivity_status == 2)
                                                        Active
                                                        @elseif($company->acitivity_status == 1)
                                                        Super Active
                                                        @elseif($company->acitivity_status == 5)
                                                        Winding Up
                                                        @elseif($company->acitivity_status == 6)
                                                        Striking Off
                                                        @else
                                                        {{'N/A'}}
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="status_date">Date: </label>
                                                        {{ $company->status_date ? \Carbon\Carbon::parse($company->status_date)->format('Y-m-d') : 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="annual_meeting_date">Annual General Meeting Date (if applicable):
                                                        </label>
                                                        {{ $company->annual_meeting_date ?? 'N/A' }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-header">
                                            <h5 >MISC</h5>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="ep_certificate">Exempt Private Certificate (EPC): </label>
                                                        {{  $company->ep_certificate ?? 'N/A'}}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="applied_date">Applied Date:</label>
                                                        {{ $company->applied_date ? \Carbon\Carbon::parse($company->applied_date)->format('Y-m-d') : 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="new_due_date">New Due Date: </label>
                                                        {{ $company->new_due_date ? \Carbon\Carbon::parse($company->new_due_date)->format('Y-m-d') : 'N/A' }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="extension_time">Extension of Time (EOT): </label>
                                                        {{  $company->extension_time ?? 'N/A'}}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                </div>
                                                <!--                                                <div class="col-lg-6">
                                                                                                    <div class="form-group">
                                                                                                        <label for="company_etr">eTR (CTOS): </label>
                                                                                                        {{ $company->company_etr  ? $company->company_etr : 'N/A'}}
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-lg-4">
                                                                                                    <div class="form-group">
                                                                                                        <label for="etr_date">Date: </label>
                                                                                                        {{ $company->etr_date ? \Carbon\Carbon::parse($company->etr_date)->format('Y-m-d') : 'N/A' }}
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                            <div class="row">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="btabs-static-payment" role="tabpanel">
                        <div class="block block-rounded">
                            <div class="block-content block-content-full">
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    <thead>
                                    <th class="text-center" style="width: 80px;">#</th>
                                    <th>DATE</th>
                                    <th>DISCRIPTION</th>
                                    <th>PAYMENT NOTE</th>
                                    <th>AMOUNT</th>
                                    </thead>
                                    <tbody>
                                        @php $total = 0; @endphp
                                        @foreach($company->payments as $key=> $payment)
                                        <tr>
                                            <td class="text-center">{{ $key+1 }}</td>
                                            <td class="font-w600">
                                                {{ \Carbon\Carbon::parse($payment->created_at)->format('d-m-Y') }}
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                {{ $payment->payment_note }}
                                            </td>
                                            <td>
                                                {{ $payment->payment_note }}
                                            </td>
                                            <td>{{config('app.currency_code')}} {{ round($payment->amount,2) }}</td>
                                        </tr>
                                        @php $total += $payment->amount; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    <label>Total:</label>
                                    {{config('app.currency_code')}} {{round($total,2)??'N/A'}}<br>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="btabs-static-billing" role="tabpanel">
                        <div class="block block-rounded">
                            <div class="block-content block-content-full">
                                <table class="table table-borderedless table-striped table-vcenter display" id="example" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th style="">Invoice</th>
                                            <th class="">From</th>
                                            <th class="">Amount</th>
                                            <th class="">Paid</th>
                                            <th class="">Balance</th>
                                            <th class="">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @php  $amount_total =$invoice_amount=$paid=0;@endphp
                                        @foreach($company->company_invoices as $key=> $invoice)
                                        <tr colspan="" data-toggle="collapse"  class="details-control" data-child-value="{{$invoice}}">
                                            <td class="details-control d-none d-sm-table-cell">
                                                <em class="font-size-sm text-muted">{{\Carbon\Carbon::parse($invoice->invoice_date)->format('Y-m-d')}}</em>
                                            </td>
                                            <td class="details-control font-w600 font-size-sm">
                                                <div class="py-1">
                                                    {{$invoice->invoice_no}}
                                                </div>
                                            </td>
                                            <td class="details-control font-w600 font-size-sm">
                                                <div class="py-1">
                                                    {{$invoice->department->name}}
                                                </div>
                                            </td>
                                            <td class="details-control font-w600 font-size-sm">
                                                <div class="py-1">
                                                    {{config('app.currency_code')}} {{number_format(round($invoice->total_amount,2), 2, '.', '')}}
                                                </div>
                                            </td>

                                            @php $total = 0;
                                            foreach($invoice->payments as $payment){
                                            $total += $payment->amount;
                                            }
                                            @endphp
                                            <td class="details-control">
                                                <div class="py-1">
                                                    {{config('app.currency_code')}} {{number_format(round($total,2), 2, '.', '')}}
                                                </div>
                                            </td>
                                            <td class="details-control font-w600 font-size-sm">
                                                <div class="py-1">
                                                    {{config('app.currency_code')}} {{number_format(round($invoice->total_amount - $total,2), 2, '.', '')}}
                                                </div>
                                            </td>
                                            <td class="details-control text-center">
                                                <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#demo{{$key}}"><i class="fa fa-angle-down"></i> </button>
                                                @can('invoice_edit')
                                                <a href="{{ route('invoices.edit', $invoice->id) }}" class="btn btn-sm btn-light"><i class="nav-main-link-icon si si-pencil"></i></a>
                                                @endcan
                                            </td>
                                        </tr>
                                        @php
                                        $amount_total += $total;
                                        $invoice_amount += $invoice->total_amount;
                                        $paid += abs($invoice->total_amount - $total);
                                        @endphp
                                        @endforeach
                                    </tbody>
                                    <tr>
                                        <td><label> Total</label></td>
                                        <td></td>
                                        <td></td>
                                        <td><label>{{config('app.currency_code')}} {{number_format(round($invoice_amount,2), 2, '.', '')}}</label></td>
                                        <td><label>{{config('app.currency_code')}} {{number_format(round($amount_total,2), 2, '.', '')}}</label></td>
                                        <td><label>{{config('app.currency_code')}} {{number_format(round($paid,2), 2, '.', '')}}</label></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- END Table Sections -->
                        <script>
                            /* Formatting function for row details - modify as you need */
                            function format(d) {
                                // `d` is the original data object for the row

                                return '<div class="row">' +
                                        '<div class="col-md-6">' +
                                        '<p>Invoice No : ' + d.invoice_no + '</p>' +
                                        '</div>' +
                                        '<div class="col-md-6 text-center">' +
                                        '<p>Due Date : ' + moment(d.due_date).format('DD-MM-YYYY') + '</p>' +
                                        '</div>' +
                                        '<div class="col-md-12">' +
                                        '<p>Debtor A/C code : ' + d.debtor_code + '</p' +
                                        '</div>' +
                                        '<div class="">' +
                                        '<p>Type of service : ' + d.job.job_type.name + '</p>' +
                                        '</div>' +
                                        '<div class="">' +
                                        '<p>Description : ' + d.description + '</p>' +
                                        '</div>' +
                                        '</div>';
                            }
                            ;

                            $(document).ready(function () {
                                var table = $('#example').DataTable({});
                                // Add event listener for opening and closing details
                                $('.btn').on('click', function () {

                                    var tr = $(this).closest('tr');
                                    var row = table.row(tr);

                                    if (row.child.isShown()) {
                                        // This row is already open - close it
                                        row.child.hide();
                                        tr.removeClass('shown');
                                        $('i', this).addClass("fa-angle-down");
                                        $('i', this).removeClass("fa-angle-up");

                                    } else {
                                        // Open this row
                                        row.child(format(tr.data('child-value'))).show();
                                        tr.addClass('shown');
                                        $('i', this).removeClass("fa-angle-down");
                                        $('i', this).addClass("fa-angle-up");
                                    }
                                });
                            });
                        </script>
                    </div>

                    <div class="tab-pane" id="btabs-static-document" role="tabpanel">
                        @foreach($company->company_documents as $key => $document)
                        <div class="block-content block-content-full">
                            <div class="row">
                                <table id="mytable-{{$key}}" style="width: 100%;">
                                    <thead>
                                    <th style="width: 30%;">{{ $document->report_type }}</th>
                                    @if($document->report_type != "Annual Return")
                                    <th class="text-center">Received Date</th>
                                    @endif
                                    <th >Submitted</th>
                                    </thead>
                                    <tbody>
                                        @php   $d = array(); @endphp
                                        @foreach(json_decode($document->report_year) as $key1 => $year)
                                        @php $i = $key;  $d[] = $key1; @endphp
                                        <tr>
                                            <td style="width: 25%; padding: 10px;">
                                                {{ $year ?? 'N/A' }}
                                            </td>
                                            @if($document->report_type != "Annual Return")
                                            <td class="text-center" style="width: 25%; padding: 10px;" >
                                                {{ json_decode( $document->receive_date)[$key1] ?? 'N/A' }}
                                            </td>
                                            @endif
                                            <td style="width: 25%; padding: 10px;" >
                                                {{ json_decode($document->submited)[$key1]?? 'N/A' }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="tab-pane" id="btabs-static-portfolio" role="tabpanel">
                        <div class="block-header">
                            <h5 class="font-w600">PORTFOLIO HOLDER</h5>
                        </div>
                        @foreach($departments as $key => $department)
                        <div class="block-content block-content-full">
                            <label for="one-ecom-product-meta-keywords">{{ $department->name }}</label>
                            @php
                            $k = array();
                            $users = \App\Models\Users\User::with('departments')->whereHas('departments', function($query) use ($department){
                            $query->where('id',$department->id);
                            })->get();
                            @endphp
                            <div class="row">
                                <table id='port-{{$key}}' class="append" style="width: 100%;">
                                    <tbody >
                                        @if(count($company->company_portfolios)!=0)
                                        @foreach($company->company_portfolios as $key1 => $portfolio)
                                        @php      $k[] = $portfolio->department_id; @endphp
                                        @if($portfolio->department_id == $department->id)
                                        <tr id='portfolio'>
                                            <td style="width: 25%; padding: 10px;">
                                                @foreach( $users as $user)
                                                @if ($user->id == $portfolio->user_id) {{$user->name}} @endif
                                                @endforeach
                                            </td>
                                            <td style="width: 25%; padding: 10px;">
                                                {{\Carbon\Carbon::parse($portfolio->date)->format('Y-m-d')?? 'N/A'}}
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                        @endif
                                        @if(count($company->company_portfolios) <= count($departments))
                                        @if(!in_array($department->id,$k))
                                        <tr>
                                            <td style="width: 25%; padding: 10px;">{{'N/A'}}</td>
                                            <td style="width: 25%; padding: 10px;">{{'N/A'}}</td>
                                        </tr>  
                                        @endif
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="tab-pane" id="btabs-static-reminder" role="tabpanel">
                        <div class="block-header">
                            <h5 class="font-w600">REMINDER</h5>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="row add pt-3">
                                @php
                                $disabled ='';
                                $secretarial_fees = optional($company->company_reminders)->secretarialFees ; 
                                @endphp
                                <div class="col-md-3 col-xl-3 ">
                                    <input type="checkbox" class="check" id="check" @if(isset($secretarial_fees) && $secretarial_fees !== "0") checked @endif disabled>
                                    <label class="ml-2"> Secretarial fees</label>
                                </div>
                                <div class="col-md-6 col-xl-6 offset-md-2 offset-xl-2 form-group"
                                     style="margin-bottom: 0.5rem;">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="example-radios-inline1" name="company_reminder[secretarialfees]" value="monthly" @if(isset($secretarial_fees) && $secretarial_fees == "monthly")checked @endif disabled>
                                        <label class="form-check-label" for="example-radios-inline1">Monthly</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="example-radios-inline2" name="company_reminder[secretarialfees]" value="quarterly" @if(isset($secretarial_fees) && $secretarial_fees == "quarterly")checked @endif disabled>
                                        <label class="form-check-label" for="example-radios-inline2">Quarterly</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="example-radios-inline3" name="company_reminder[secretarialfees]" value="halfYearly" @if(isset($secretarial_fees) && $secretarial_fees == "halfYearly")checked @endif disabled>
                                        <label class="form-check-label" for="example-radios-inline3"> Half-Yearly</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="example-radios-inline4" name="company_reminder[secretarialfees]" value="yearly" @if(isset($secretarial_fees) && $secretarial_fees == "yearly")checked @endif disabled>
                                        <label class="form-check-label" for="example-radios-inline4">Annually</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="row add pt-3">
                                @php
                                $annual_return = isset(old('company_reminder')['annualReturn']) ? old('company_reminder')['annualReturn'] : optional($company->company_reminders)->is_annualReturn_notify;
                                @endphp
                                <div class="col-md-9 col-xl-9 text-start">
                                    <input type="checkbox" name="company_reminder[annualReturn]" id="annualReturn"  value="1" @if(isset($annual_return) && $annual_return == 1)checked @endif disabled>
                                    <label class="ml-2"> Create Recurrence job for Annual Return. </label>
                                </div>
                            </div>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="row add pt-3">
                                @php
                                $taxAgent = isset(old('company_reminder')['taxAgent']) ? old('company_reminder')['taxAgent'] : optional($company->company_reminders)->is_taxAgent_notify;
                                @endphp
                                <div class="col-md-9 col-xl-9  text-start">
                                    <input type="checkbox" name="company_reminder[taxAgent]" value="1" @if(isset($taxAgent) && $taxAgent == 1)checked @endif disabled>
                                    <label class="ml-2"> Reminder on the next 15th January for Tax Form E. </label>
                                </div>
                            </div>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="row add pt-3">
                                @php
                                $auditorAppoinment = isset(old('company_reminder')['auditorAppoinment']) ? old('company_reminder')['auditorAppoinment'] : optional($company->company_reminders)->is_auditorAppoinment_notify;
                                @endphp
                                <div class="col-md-9 col-xl-9  text-start">
                                    <input type="checkbox" name="company_reminder[auditorAppoinment]" value="1" @if(isset($auditorAppoinment) && $auditorAppoinment == 1)checked @endif disabled>
                                    <label class="ml-2"> Reminder After 12 months for Appointment of Auditors.</label>
                                </div>
                            </div>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="row add pt-3">
                                <div class="col-md-9 col-xl-9 text-start">
                                    @php
                                    $audited_statement = isset(old('company_reminder')['audited_statement']) ? old('company_reminder')['audited_statement'] : optional($company->company_reminders)->is_auditStatement_notify;
                                    @endphp
                                    <input type="checkbox" name="company_reminder[audited_statement]" id="audited_statement" value="1" @if(isset($audited_statement) && $audited_statement == 1) checked @endif disabled>
                                    <label class="ml-2"> Reminder After 3 months Company's financial year end for AFS submission.</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Block Tabs Default Style -->
        </div>
    </div>
    <!-- END Block Tabs -->
</div>
<!-- END Page Content -->
@endsection
