@extends('layouts.app', [
'activePage' => 'edit-customers',
'menuParent' => 'edit-customers',
'title' => 'Edit Customers',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-12 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
               Customer <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Customer</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">Edit</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    {{-- <h2 class="content-heading">{{ $company->name }}</h2> --}}
<!-- Block Tabs Default Style -->
<div class="block block-rounded bs-example">
    @php
    $i = $j = $k = $l = $m = $n = $o = $p = $q = $g = $d = 0;
    $stars = [];
    $stars = ['name', 'email', 'company_roc', 'incorporation_date', 'e_no', 'c_no', 'company_fka', 'contact', 'phone', 'fax', 'bank_name', 'account_number', 'address_name.*', 'address_phone.*', 'address.*', 'incharge_name.*', 'incharge_phone.*', 'incharge_email.*', 'responsible_name.*.*', 'share_capital', 'shareholder_name.*', 'values.*', 'director.*', 'groupof_companies.*', 'financial_year_end', 'annual_return_date', 'turnover', 'profit_before_tax', 'activity_status', 'status_date', 'ep_certificate', 'applied_date', 'new_due_date', 'extension_time', 'company_etr', 'etr_date', 'document_type.*.report_year.*'];
    @endphp
    @foreach ($stars as $key => $star)
    @if ($errors->has($stars[$key]))
    @if (in_array($key, range(0, 6)))
    @php $i++; @endphp
    @endif
    @if (in_array($key, range(7, 9)))
    @php $j++; @endphp
    @endif
    @if (in_array($key, range(10, 11)))
    @php $k++; @endphp
    @endif
    @if (in_array($key, range(12, 14)))
    @php $l++; @endphp
    @endif
    @if (in_array($key, range(15, 17)))
    @php $m++; @endphp
    @endif
    @if (in_array($key, range(18, 18)))
    @php $n++; @endphp
    @endif
    @if (in_array($key, range(19, 22)))
    @php $o++; @endphp
    @endif
    @if (in_array($key, range(23, 23)))
    @php $p++; @endphp
    @endif
    @if (in_array($key, range(24, 35)))
    @php $q++; @endphp
    @endif
    @if (in_array($key, range(0, 35)))
    @php $g++; @endphp
    @endif
    @if ($key == 36)
    @php $d++; @endphp
    @endif
    @endif
    @endforeach
    <ul class="nav nav-tabs nav-tabs-block" role="tablist" id="myTab">
        <li class="nav-item">
            <a class="nav-link active" id="home" data-toggle="tab" href="#btabs-static-home">General @if($g!=0)<span class="text-danger">*</span>@endif</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="docum" data-toggle="tab" href="#btabs-static-document">
                Document @if($d!=0)<span class="text-danger">*</span>@endif
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile" data-toggle="tab" href="#btabs-static-portfolio">
                Portfolio Holder
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="reminder" href="#btabs-static-reminder">
                Reminders
            </a>
        </li>
    </ul>
    <form action="{{ route('companies.update',$company->id) }}" method="POST">
        @csrf
        @method('put')
        <div class="block-content tab-content">
            <div class="tab-pane fade show active" id="btabs-static-home" role="tabpanel">
                <!-- Alternative Style -->
                <div class="row">
                    <div class="col-md-3">

                        <ul class="nav flex-column nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#info">Info @if($i!=0)<span class="text-danger">*</span>@endif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#contact">Contact @if($j!=0)<span class="text-danger">*</span>@endif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#g-address">Address @if($l!=0)<span class="text-danger">*</span>@endif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#bank-details">Bank Details @if($k!=0)<span class="text-danger">*</span>@endif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#Person-in-Charge">Person-in-Charge @if($m!=0)<span class="text-danger">*</span>@endif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#Responsible-Accountants">Responsible Professional
                                    Firm
                                    @if ($n != 0)<span class="text-danger">*</span>@endif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#Shares">Directors & Shareholders
                                    @if ($o != 0)<span class="text-danger">*</span>@endif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#Group-of-Companies">Group of Companies @if($p!=0)<span class="text-danger">*</span>@endif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#Financial-Information">Financial Information @if($q!=0)<span class="text-danger">*</span>@endif</a>
                            </li>

                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="block-content tab-content">
                            <div class="tab-pane active" id="info" role="tabpanel">
                                <div class="block-header">
                                    <h5 >EDIT CUSTOMERS</h3>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="name">Company Name<span
                                                        class="text-danger">*</span></label>
                                                <input type="text"
                                                       class="form-control  @error('name') is-invalid @enderror"
                                                       id="name" name="name" placeholder="Company Name"
                                                       value="{{ old('name', $company->name) }}">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="email">Email<span
                                                        class="text-danger">*</span></label>
                                                <input type="email"
                                                       class="form-control  @error('email') is-invalid @enderror"
                                                       name="email" placeholder="Email"
                                                       value="{{ old('email', $company->email) }}">
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="company_roc">Registration No.</label>
                                                <input type="text"
                                                       class="form-control  @error('company_roc') is-invalid @enderror"
                                                       id="company_roc" name="company_roc" placeholder="Company ROC"
                                                       value="{{ old('company_roc', $company->roc) }}">
                                                @error('company_roc')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="incorporation_date">Incorporation Date<span
                                                        class="text-danger">*</span></label>
                                                <input type="date" class="form-control " name="incorporation_date"
                                                       value="{{ old('incorporation_date', $company->incorporation_date) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="e_no">E no.</label>
                                                <input type="text" class="form-control " id="e_no" name="e_no"
                                                       placeholder="E no." value="{{ old('e_no', $company->e_no) }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="c_no">C no.</label>
                                                <input type="text"
                                                       class="form-control   @error('c_no') is-invalid @enderror"
                                                       id="c_no" name="c_no" placeholder="C no."
                                                       value="{{ old('c_no', $company->c_no) }}">
                                                @error('c_no')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="company_fka">Formerly Known As (FKA)</label>
                                                <input type="text"
                                                       class="form-control  @error('company_fka') is-invalid @enderror"
                                                       id="company_fka" name="company_fka"
                                                       placeholder="Formerly Known As (FKA)"
                                                       value="{{ old('company_fka', $company->company_fak) }}">
                                                @error('company_fka')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="date_of_change">Date of Change</label>
                                                <input type="date"
                                                       class="form-control  @error('date_of_change') is-invalid @enderror"
                                                       id="date_of_change" name="date_of_change"
                                                       value="{{ old('date_of_change',  \Carbon\Carbon::parse($company->date_of_change)->format('Y-m-d')) }}"
                                                       placeholder="Date of Change">
                                                @error('date_of_change')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="natureof_business">Nature of Business</label>
                                                <textarea
                                                    class="form-control   @error('natureof_business') is-invalid @enderror"
                                                    id="natureof_business"
                                                    name="natureof_business">{{ old('natureof_business', $company->natureof_business) }}</textarea>
                                                @error('natureof_business')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="contact" role="tabpanel">
                                <div class="row">
                                    <h5 for="one-ecom-product-meta-keywords">CONTACT</h5>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="phone">Phone <span class="text-danger">*</span></label>
                                            <input type="text"
                                                   class="form-control   @error('phone') is-invalid @enderror"
                                                   name="phone" placeholder="Phone"
                                                   value="{{ old('phone', $company->phone_no) }}">
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="fax">Fax</label>
                                            <input type="text"
                                                   class="form-control   @error('fax') is-invalid @enderror"
                                                   name="fax" placeholder="Fax"
                                                   value="{{ old('fax', $company->fax_no) }}">
                                            @error('fax')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="contact">Contact No.</label>
                                            <input type="text"
                                                   class="form-control   @error('contact') is-invalid @enderror"
                                                   id="contact" name="contact" placeholder="Contact No."
                                                   value="{{ old('contact', $company->contact) }}">
                                            @error('contact')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="bank-details" role="tabpanel">
                                <div class="row">
                                    <h5 for="one-ecom-product-meta-keywords">BANK DETAILS</h5>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="bank_name">Bank Name </label>
                                            <input type="text"
                                                   class="form-control   @error('bank_name') is-invalid @enderror"
                                                   id="bank_name" name="bank_name" placeholder="Bank Name"
                                                   value="{{ old('bank_name', $company->bank_name) }}">
                                            @error('bank_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="account_number">Account Number</label>
                                            <input type="text"
                                                   class="form-control   @error('account_number') is-invalid @enderror"
                                                   id="account_number" name="account_number"
                                                   placeholder="Account Number"
                                                   value="{{ old('account_number', $company->account_no) }}">
                                            @error('account_number')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="g-address" role="tabpanel">
                                <div class="row">
                                    <h5 for="one-ecom-product-meta-keywords">ADDRESS</h5>
                                </div>
                                @foreach ($company->addresses as $key => $address)
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="address_type">{{ $address->addresse_type }}
                                                @if ($address->addresse_type == 'Delivery Address') (if different from the above) @endif</label>
                                            <input type="hidden" id="address_type" name="address_type[]"
                                                   value="{{ $address->addresse_type }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="address_name"> Name </label>
                                            <input type="text"
                                                   class="form-control   @error('address_name.' . $key) is-invalid @enderror"
                                                   id="address_name" name="address_name[]"
                                                   value="{{ old('address_name.' . $key, $address->name) }}">
                                            @error('address_name.' . $key)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="address_phone">Phone</label>
                                            <input type="text"
                                                   class="form-control   @error('address_phone.' . $key) is-invalid @enderror"
                                                   id="address_phone" name="address_phone[]"
                                                   value="{{ old('address_phone.' . $key, $address->phone) }}">
                                            @error('address_phone.' . $key)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <textarea type="text"
                                                      class="form-control   @error('address.' . $key) is-invalid @enderror"
                                                      id="address"
                                                      name="address[]"> {{ old('address.' . $key, $address->address) }}</textarea>
                                            @error('address.' . $key)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="tab-pane" id="Person-in-Charge" role="tabpanel">
                                <div class="row">
                                    <h5 for="one-ecom-product-meta-keywords">PERSON-IN-CHARGE</h5>
                                </div>
                                <div class="row">
                                    @php $inchargeKey = 0;@endphp
                                    @foreach ($company->company_incharge as $key => $companyINCharge)
                                    @php $inchargeKey = $key; @endphp
                                    {{-- @if ($companyINCharge->name != null) --}}
                                    @if (empty(old('incharge_name')) || $companyINCharge->name != null)
                                    <div class="row">
                                        <input type="hidden" name="incharge_id[]"
                                               value="{{ $companyINCharge->id }}">
                                        <div class="col-lg-4">
                                            <label for="incharge_department">Department:</label>
                                            <select
                                                class="select2 form-control @error('incharge_department_id.' . $inchargeKey) is-invalid @enderror"
                                                id="department_id"
                                                name="incharge_department_id[{{ $inchargeKey }}]"
                                                style="width: 100%;" data-placeholder="Choose">
                                                <option value="">{{ __('Please Select') }}</option>
                                                @foreach ($departments as $department)
                                                @php
                                                $selected = '';
                                                $incharge_department = !empty(old('incharge_department_id')) ? old('incharge_department_id')[$inchargeKey] : $companyINCharge->department_id;
                                                if ($incharge_department == $department->id) {
                                                $selected = 'selected';
                                                }
                                                @endphp
                                                <option value="{{ $department->id }}"
                                                        {{ $selected }}>{{ $department->name }}
                                                </option>
                                                @endforeach
                                            </select>
                                            @error('incharge_department_id.' . $inchargeKey)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-8">
                                            <label for="incharge_name">Name:</label>
                                            <input type="text"
                                                   class="form-control  @error('incharge_name.' . $inchargeKey) is-invalid @enderror"
                                                   id="incharge_name"
                                                   name="incharge_name[{{ $inchargeKey }}]"
                                                   value="{{ $companyINCharge ? $companyINCharge->name : old('incharge_name')[$inchargeKey] }}">
                                            @error('incharge_name.' . $inchargeKey)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label for="incharge_phone">Phone:</label>

                                            <input type="text"
                                                   class="form-control  @error('incharge_phone.' . $inchargeKey) is-invalid @enderror"
                                                   id="incharge_phone"
                                                   name="incharge_phone[{{ $inchargeKey }}]"
                                                   value="{{ $companyINCharge ? $companyINCharge->phone : old('incharge_phone')[$inchargeKey] }}">
                                            @error('incharge_phone.' . $inchargeKey)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="email">Email:</label>

                                            <input type="email"
                                                   class="form-control  @error('incharge_email.' . $inchargeKey) is-invalid @enderror"
                                                   id="incharge_email"
                                                   name="incharge_email[{{ $inchargeKey }}]"
                                                   value="{{ $companyINCharge ? $companyINCharge->email : old('incharge_email')[$inchargeKey] }}">
                                            @error('incharge_email.' . $inchargeKey)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-sm-12 text-left">
                                            <button type="button"
                                                    data-name="{{ $companyINCharge->name }}"
                                                    class="delete btn btn-sm"
                                                    {{ $key == 0 ? 'disabled' : '' }}><i
                                                    class="fa fa-trash" aria-hidden="true"></i> Remove
                                                this person-in-charge</button>
                                        </div>

                                    </div>
                                    @endif
                                    {{-- @endif --}}
                                    @endforeach

                                    @if (!empty(old('incharge_name')) || !empty(old('incharge_phone')) || !empty(old('incharge_email')) || !empty(old('incharge_department_id')))
                                    @php
                                    if ($companyINCharge->name != null) {
                                    if ($inchargeKey != count(old('incharge_name')) || $inchargeKey != count(old('incharge_phone')) || $inchargeKey != count(old('incharge_email')) || $inchargeKey != count(old('incharge_department_id')) || $inchargeKey != count(old('incharge_name'))) {
                                    $count = count(old('incharge_name'));
                                    $i = $inchargeKey + 1;
                                    }
                                    } else {
                                    $count = count(old('incharge_name'));
                                    $i = 0;
                                    }
                                    @endphp

                                    @for ($i = $i; $i < $count; $i++)
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for="incharge_department">Department:</label>
                                            <select
                                                class="select2 form-control @error('incharge_department_id.' . $i) is-invalid @enderror"
                                                id="department_id"
                                                name="incharge_department_id[{{ $i }}]"
                                                style="width: 100%;" data-placeholder="Choose">
                                                <option value="">{{ __('Please Select') }}</option>
                                                @foreach ($departments as $department)
                                                @php
                                                $selected = '';
                                                if (old('incharge_department_id')) {
                                                if (old('incharge_department_id')[$i] == $department->id) {
                                                $selected = 'selected';
                                                }
                                                }
                                                @endphp
                                                <option value="{{ $department->id }}"
                                                        {{ $selected }}>{{ $department->name }}
                                                </option>
                                                @endforeach
                                            </select>
                                            @error('incharge_department_id.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>
                                        <div class="col-lg-8">
                                            <label for="incharge_name">Name:</label>
                                            <input type="text"
                                                   class="form-control   @error('incharge_name.' . $i) is-invalid @enderror"
                                                   id="incharge_name"
                                                   name="incharge_name[{{ $i }}]"
                                                   value="{{ old('incharge_name')[$i] ?? '' }}"
                                                   placeholder="Name">
                                            @error('incharge_name.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label for="incharge_phone">Phone:</label>
                                            <input type="text"
                                                   class="form-control   @error('incharge_phone.' . $i) is-invalid @enderror"
                                                   id="incharge_phone"
                                                   name="incharge_phone[{{ $i }}]"
                                                   value="{{ old('incharge_phone')[$i] ?? '' }}"
                                                   placeholder="Phone">
                                            @error('incharge_phone.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="email">Email:</label>
                                            <input type="email"
                                                   class="form-control   @error('incharge_email.' . $i) is-invalid @enderror"
                                                   id="incharge_email"
                                                   name="incharge_email[{{ $i }}]"
                                                   value="{{ old('incharge_email')[$i] ?? '' }}"
                                                   placeholder="Email">
                                            @error('incharge_email.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <button type="button" class="delete_incharge btn "
                                                {{ $i == 0 ? 'disabled' : '' }}><i class="fa fa-trash"
                                                                           aria-hidden="true"></i></button>
                                    </div>
                                    @endfor
                                    @endif
                                </div>
                                <div class="container4 py-2" style="width: 100%;">
                                </div>
                                @if (!empty(old('incharge_name')) || !empty(old('incharge_phone')) || !empty(old('incharge_email')) || !empty(old('incharge_department_id')))
                                <input type="hidden" id="counterIncharge"
                                       value="{{ count(old('incharge_name')) - 1 && count(old('incharge_phone')) - 1 && count(old('incharge_email')) - 1 && count(old('incharge_department_id')) - 1 }}">
                                @else
                                <input type="hidden" id="counterIncharge" value="{{ $inchargeKey }}">
                                @endif
                                <div class="col-md-12" id="addnew_feature">
                                    <a href="#" id="companyGroup-more"
                                       class="btn btn-icon btn-secondary btn-sm mr-2 add_companyINCharge_form_field">Add
                                        More</a>
                                </div>
                            </div>
                            <div class="tab-pane" id="Responsible-Accountants" role="tabpanel">
                                <div class="row">
                                    <h5 for="one-ecom-product-meta-keywords">APPOINTED PROFESSIONAL FIRM</h5>
                                </div>
                                <div class="row">
                                    @foreach ($departments as $key => $department)
                                    <div class="col-lg-12 mt-2">
                                        <label for="responsible_name">{{ $department->name }}</label>
                                        @foreach ($company->account_responsible as $key1 => $accountResponsible)

                                        @if ($accountResponsible->departments->id == $department->id)
                                        <input type="hidden" id="responsible_name"
                                               name="department_id[]"
                                               value="{{ $accountResponsible->departments->id }}">
                                        @foreach (json_decode($accountResponsible->name) as $val => $value)
                                        @php $i = $val; @endphp
                                        {{-- @if ($value != null) --}}
                                        <div class="row ml-0">
                                            <input type="hidden"
                                                   name="accountResponsible_id[{{ $department->id }}]"
                                                   value="{{ $accountResponsible->id }}">
                                            <input type="text"
                                                   class="form-control  my-2 col-md-10 @error('responsible_name.' . $department->id . '.' . $i) is-invalid @enderror"
                                                   id="responsible_name"
                                                   name="responsible_name[{{ $department->id }}][{{ $i }}]"
                                                   value="{{ $value ? $value : old('responsible_name')[$department->id][$i] ?? '' }}">
                                            @error('responsible_name.' . $department->id . '.' . $i)
                                            <span class="invalid-feedback mt-0 mb-2" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <button type="button"
                                                    class="delete_Responsible btn mx-2 my-2  px-2 py-2"
                                                    {{ $i == 0 ? 'disabled' : '' }}><i
                                                    class="fa fa-trash"
                                                    aria-hidden="true"></i></button>
                                        </div>
                                        {{-- @endif --}}
                                        @endforeach
                                        @endif
                                        @endforeach

                                        @if (!empty(old('responsible_name')[$department->id]))
                                        @if ($val != count(old('responsible_name')[$department->id]))
                                        @php
                                        $count = count(old('responsible_name')[$department->id]);
                                        @endphp
                                        @for ($i = $val + 1; $i < $count; $i++)
                                        <div class="row ml-2">
                                            <input type="text"
                                                   class="form-control  my-2 col-md-10 @error('responsible_name.' . $department->id . '.' . $i) is-invalid @enderror"
                                                   id="responsible_name"
                                                   name="responsible_name[{{ $department->id }}][]"
                                                   value="{{ old('responsible_name')[$department->id][$i] ?? '' }}"
                                                   placeholder="Name">

                                            @error('responsible_name.' . $department->id . '.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <button type="button"
                                                    class="delete_Responsible btn mx-2 my-2  px-2 py-2"
                                                    {{ $i == 0 ? 'disabled' : '' }}><i
                                                    class="fa fa-trash"
                                                    aria-hidden="true"></i></button>
                                        </div>
                                        @endfor
                                        @endif
                                        @endif
                                    </div>
                                    <div class="container_{{ $key }} px-1 col-lg-12">
                                    </div>
                                    <input type="hidden" id="counter" value="0">
                                    <div class="col-md-12" id="addnew_feature">
                                        <a href="#" id="add_responsibleAccount_form_{{ $key }}"
                                           class="btn btn-icon btn-secondary btn-sm mr-2 add_responsibleAccount_form_{{ $key }}">Add
                                            More</a>
                                    </div>
                                    <script>
                                        $(document).ready(function() {
                                        var max_fields = 15;
                                        var wrapper = $(".container_{{ $key }}");
                                        var add_button = $(".add_responsibleAccount_form_{{ $key }}");
                                        var x = 1;
                                        $(add_button).click(function(e) {
                                        e.preventDefault();
                                        if (x < max_fields) {
                                        x++;
                                        $(wrapper).append(
                                                '<div class="col-md-12" id="new_plan"><div class="row ml-2"><input type="text" required class="form-control  my-2 col-md-10" name="responsible_name[{{ $department->id }}][]" placeholder="Name"><a href="#" class="delete_Responsible btn mx-2 my-2  px-2 py-2"><i class="fa fa-trash" aria-hidden="true"></i></a></div></div>'
                                                ); //add input box
                                        } else {
                                        alert('You Reached the limits')
                                        }
                                        });
                                        $(document).on("click", ".delete_Responsible", function(e) {
                                        e.preventDefault();
                                        $(this).parent('div').remove();
                                        x--;
                                        })
                                        });
                                    </script>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tab-pane" id="Shares" role="tabpanel">
                                <div class="row">
                                    <h5 for="one-ecom-product-meta-keywords">DIRECTORS AND SHAREHOLDERS</h5>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="share_capital">Share Capital <span
                                                    class="text-danger">*</span></label>
                                            <input type="text"
                                                   class="form-control   @error('share_capital') is-invalid @enderror"
                                                   id="share_capital" name="share_capital"
                                                   value="{{ old('share_capital', $company->shares->share_capital) }}">
                                            @error('share_capital')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                    </div>

                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="shareholder_name">Shareholder </label><br>
                                            @php
                                            $w = [];
                                            foreach (json_decode($company->shares->shareholder_name) as $val => $value) {
                                            $w[] = $value;
                                            }
                                            @endphp
                                            @php
                                            $index = 0;
                                            $i = 0;
                                            @endphp
                                            @if (!in_array(null, $w))
                                            @foreach (json_decode($company->shares->shareholder_name) as $val => $value)
                                            @php
                                            $i = $index;
                                            @endphp
                                            @if ($value != null)
                                            <div class="row py-2">
                                                <div class="col-lg-7">
                                                    <input type="text"
                                                           class="form-control   @error('shareholder_name.' . $i) is-invalid @enderror"
                                                           name="shareholder_name[{{ $i }}]"
                                                           value="{{ $val ?? old('shareholder_name')[$i] }}">
                                                    @error('shareholder_name.' . $i)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="col-lg-3">
                                                    <input
                                                        class="form-control  @error('values.' . $i) is-invalid @enderror"
                                                        type="text" id="fee-2" class="fee"
                                                        name="values[{{ $i }}]"
                                                        value="{{ $value ?? old('values')[$i] }}">
                                                    @error('values.' . $i)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <button type="button"
                                                        class="delete_shareholder btn text-center"
                                                        {{ $i == 0 ? 'disabled' : '' }}><i
                                                        class="fa fa-trash"
                                                        aria-hidden="true"></i></button>
                                            </div>
                                            @php
                                            $index++;
                                            @endphp
                                            @endif
                                            @endforeach
                                            @endif

                                            @if (!empty(old('shareholder_name')))
                                            @php
                                            if ($i != count(old('shareholder_name'))) {
                                            $count = count(old('shareholder_name'));
                                            } elseif ($i == 0) {
                                            $count = count(old('shareholder_name'));
                                            }
                                            @endphp
                                            @for ($i = $index; $i < $count; $i++)
                                            <div class="row py-2">
                                                <div class="col-lg-7">
                                                    <input type="text"
                                                           class="form-control   @error('shareholder_name.' . $i) is-invalid @enderror"
                                                           name="shareholder_name[{{ $i }}]"
                                                           placeholder="Share Holder"
                                                           value="{{ old('shareholder_name')[$i] ?? '' }}">

                                                    @error('shareholder_name.' . $i)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="col-lg-3">
                                                    <input
                                                        class="form-control  @error('values.' . $i) is-invalid @enderror"
                                                        type="text" id="fee-2" class="fee"
                                                        placeholder="%" name="values[{{ $i }}]"
                                                        value="{{ old('values')[$i] ?? '' }}">
                                                    @error('values.' . $i)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>

                                                <button type="button"
                                                        class="delete_shareholder btn text-center"
                                                        {{ $i == 0 ? 'disabled' : '' }}><i
                                                        class="fa fa-trash"
                                                        aria-hidden="true"></i></button>

                                            </div>
                                            @endfor
                                            @endif
                                            @if (in_array(null, $w) && empty(old('shareholder_name')))
                                            <div class="row py-2">
                                                <div class="col-lg-7">
                                                    <input type="text"
                                                           class="form-control   @error('shareholder_name.' . $i) is-invalid @enderror"
                                                           name="shareholder_name[{{ $i }}]"
                                                           placeholder="Share Holder"
                                                           value="{{ old('shareholder_name')[$i] ?? '' }}">
                                                    @error('shareholder_name.' . $i)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="col-lg-3">
                                                    <input
                                                        class="form-control  @error('values.' . $i) is-invalid @enderror"
                                                        type="text" id="fee-2" class="fee"
                                                        placeholder="%" name="values[{{ $i }}]"
                                                        value="{{ old('values')[$i] ?? '' }}">
                                                    @error('values.' . $i)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <button type="button"
                                                        class="delete_shareholder btn text-center"
                                                        {{ $i == 0 ? 'disabled' : '' }}><i
                                                        class="fa fa-trash" aria-hidden="true"></i></button>
                                            </div>
                                            @endif
                                            <div class="container2 py-2">
                                            </div>
                                        </div>
                                        @if (!empty(old('shareholder_name')))
                                        <input type="hidden" id="counterShareholder"
                                               value="{{ count(old('shareholder_name')) - 1 }}">
                                        @else
                                        <input type="hidden" id="counterShareholder"
                                               value="{{ $i }}">
                                        @endif
                                        <div class="col-md-12" id="addnew_feature">
                                            <a href="#" id="companyGroup-more"
                                               class="btn btn-icon btn-secondary btn-sm mr-2 add_shareholder_form_field">Add
                                                More</a>
                                        </div>
                                        <br>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="shareholder_updated_on">Updated on</label>
                                                <input type="date"
                                                       class="form-control   @error('shareholder_updated_on') is-invalid @enderror"
                                                       value="{{ $company->shares ? \Carbon\Carbon::parse($company->shares->shareholder_updated_on)->format('Y-m-d') : old('shareholder_updated_on') }}"
                                                       name="shareholder_updated_on">
                                                @error('shareholder_updated_on')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="">
                                            <label for="director">Director </label>
                                            @php
                                            $d = [];
                                            foreach (json_decode($company->shares->director) as $value) {
                                            $d[] = $value;
                                            }
                                            @endphp
                                            @php $i = 0; @endphp
                                            @if (!in_array(null, $d))
                                            @foreach (json_decode($company->shares->director) as $key => $value)
                                            @php $i = $key; @endphp
                                            @if ($value != null)
                                            <div class="row py-2">
                                                <input type="text"
                                                       class="form-control  col-md-8 ml-3 @error('director.' . $i) is-invalid @enderror"
                                                       id="director" name="director[{{ $i }}]"
                                                       value="{{ $value ? $value : old('director')[$i] ?? '' }}"
                                                       placeholder="Director Name">
                                                @error('director.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <button type="button"
                                                        class="delete_director btn py-2 px-2 mx-2"
                                                        {{ $i == 0 ? 'disabled' : '' }}><i
                                                        class="fa fa-trash"
                                                        aria-hidden="true"></i></button>
                                            </div>
                                            @endif
                                            @endforeach
                                            @endif
                                            @if (!empty(old('director')))
                                            @php
                                            if (!in_array(null, $d)) {
                                            if ($i != count(old('director'))) {
                                            $count = count(old('director'));
                                            $i = $i + 1;
                                            }
                                            } else {
                                            $count = count(old('director'));
                                            $i = 0;
                                            }
                                            @endphp
                                            @for ($i = $i; $i < $count; $i++)
                                            <div class="row py-2">
                                                <input type="text"
                                                       class="form-control  col-md-8 ml-3 @error('director.' . $i) is-invalid @enderror"
                                                       id="director" name="director[{{ $i }}]"
                                                       placeholder="Director Name"
                                                       value="{{ old('director')[$i] ?? '' }}">
                                                @error('director.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <button type="button"
                                                        class="delete_director btn py-2 px-2 mx-2"
                                                        {{ $i == 0 ? 'disabled' : '' }}><i
                                                        class="fa fa-trash"
                                                        aria-hidden="true"></i></button>
                                            </div>
                                            @endfor
                                            @endif
                                            @if (in_array(null, $d) && empty(old('director')))
                                            <div class="row py-2">
                                                <input type="text"
                                                       class="form-control  col-md-8 ml-3 @error('director.' . $i) is-invalid @enderror"
                                                       id="director" name="director[{{ $i }}]"
                                                       placeholder="Director Name"
                                                       value="{{ old('director')[$i] ?? '' }}">
                                                @error('director.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <button type="button"
                                                        class="delete_director btn py-2 px-2 mx-2"
                                                        {{ $i == 0 ? 'disabled' : '' }}><i
                                                        class="fa fa-trash" aria-hidden="true"></i></button>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="container3 py-2">
                                        </div>
                                        @if (!empty(old('director')))
                                        <input type="hidden" id="counterDirector"
                                               value="{{ count(old('director')) - 1 }}">
                                        @else
                                        <input type="hidden" id="counterDirector" value="{{ $i }}">
                                        @endif
                                        <div class="col-md-12" id="addnew_feature">
                                            <a href="#" id="companyGroup-more"
                                               class="btn btn-icon btn-secondary btn-sm mr-2 add_director_form_field">Add
                                                More</a>
                                        </div>
                                        <br>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="director_updated_on">Updated on</label>
                                                <input type="date"
                                                       class="form-control   @error('director_updated_on') is-invalid @enderror"
                                                       value="{{ \Carbon\Carbon::parse($company->shares->director_updated_on)->format('Y-m-d') ?? old('director_updated_on') }}"
                                                       name="director_updated_on">
                                                @error('director_updated_on')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="Group-of-Companies" role="tabpanel">
                                <div class="row">
                                    <h5 for="one-ecom-product-meta-keywords">GROUP OF COMPANIES</h5>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label for="groupof_companies">Group of Companies </label>
                                        <input type="hidden" name="related_companyId"
                                               value="{{ $company->related_company->id }}">
                                        @php $i = 0; @endphp
                                        @php
                                        $group = [];
                                        foreach (json_decode($company->related_company->name) as $value) {
                                        $group[] = $value;
                                        }
                                        @endphp@if (!in_array(null, $group))
                                        @foreach (json_decode($company->related_company->name) as $key => $value)
                                        @php $i = $key; @endphp
                                        @if ($value != null)
                                        <div class="row ml-2 py-2">
                                            <input type="text"
                                                   class="form-control  col-md-10 @error('groupof_companies.' . $i) is-invalid @enderror"
                                                   name="groupof_companies[{{ $i }}]"
                                                   value="{{ $value ? $value : old('groupof_companies')[$i] }}">
                                            @error('groupof_companies.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <button type="button" class="delete_group btn"
                                                    {{ $i == 0 ? 'disabled' : '' }}><i
                                                    class="fa fa-trash"
                                                    aria-hidden="true"></i></button>
                                        </div>
                                        @endif
                                        @endforeach
                                        @endif
                                        @if (!empty(old('groupof_companies')))
                                        @php
                                        if (!in_array(null, $group)) {
                                        if ($i != count(old('groupof_companies'))) {
                                        $count = count(old('groupof_companies'));
                                        $i = $i + 1;
                                        }
                                        } else {
                                        $count = count(old('groupof_companies'));
                                        $i = 0;
                                        }
                                        @endphp
                                        @for ($i = $i; $i < $count; $i++)
                                        <div class="row ml-2 py-2">
                                            <input type="text"
                                                   class="form-control  col-md-10 @error('groupof_companies.' . $i) is-invalid @enderror"
                                                   name="groupof_companies[{{ $i }}]"
                                                   placeholder="Group Of Companies"
                                                   value="{{ old('groupof_companies')[$i] ?? '' }}">
                                            @error('groupof_companies.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <button type="button" class="delete_group btn "
                                                    {{ $i == 0 ? 'disabled' : '' }}><i
                                                    class="fa fa-trash" aria-hidden="true"></i></button>
                                        </div>
                                        @endfor
                                        @endif
                                        @if (in_array(null, $group) && empty(old('groupof_companies')))
                                        <div class="row ml-2 py-2">
                                            <input type="text"
                                                   class="form-control  col-md-10 @error('groupof_companies.' . $i) is-invalid @enderror"
                                                   name="groupof_companies[{{ $i }}]"
                                                   placeholder="Group Of Companies"
                                                   value="{{ old('groupof_companies')[$i] ?? '' }}">
                                            @error('groupof_companies.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <button type="button" class="delete_group btn "
                                                    {{ $i == 0 ? 'disabled' : '' }}><i class="fa fa-trash"
                                                                               aria-hidden="true"></i></button>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-lg-12 container1">
                                    </div>
                                    @if (!empty(old('groupof_companies')))
                                    <input type="hidden" id="counterRelatedCompany"
                                           value="{{ count(old('groupof_companies')) - 1 }}">
                                    @else
                                    <input type="hidden" id="counterRelatedCompany"
                                           value="{{ $i }}">
                                    @endif
                                    <div class="col-md-12" id="addnew_feature">
                                        <a href="#" id="companyGroup-more"
                                           class="btn btn-icon btn-secondary btn-sm mr-2 add_form_field">Add
                                            More</a>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="Financial-Information" role="tabpanel">
                                <div class="block-header">
                                    <h5 for="one-ecom-product-meta-keywords">FINANCIAL INFORMATION</h5>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="financial_year_end">Financial Year End <span
                                                        class="text-danger">*</span></label>
                                                <input type="date"
                                                       class="form-control   @error('financial_year_end') is-invalid @enderror"
                                                       name="financial_year_end"
                                                       value="{{ old('financial_year_end', \Carbon\Carbon::parse($company->financial_year_end)->format('Y-m-d')) }}">
                                                @error('financial_year_end')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="annual_return_date">Annual Return Date </label>
                                                <input type="text"
                                                       class="date form-control   @error('annual_return_date') is-invalid @enderror"
                                                       name="annual_return_date"
                                                       value="{{ old('annual_return_date', $company->annual_return_date) }}">
                                                @error('annual_return_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="turnover">Turnover </label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span
                                                            class="input-group-text">RM</span></div>
                                                    <input type="text"
                                                           class="form-control   @error('turnover') is-invalid @enderror"
                                                           name="turnover"
                                                           value="{{ old('turnover', $company->turnover) }}">
                                                    @error('turnover')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="profit_before_tax">Profit before Tax </label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span
                                                            class="input-group-text">RM</span></div>
                                                    <input type="text"
                                                           class="form-control   @error('profit_before_tax') is-invalid @enderror"
                                                           name="profit_before_tax" placeholder="Profit before Tax"
                                                           value="{{ old('profit_before_tax', $company->profit_before_tax) }}">
                                                    @error('profit_before_tax')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="annual_meeting_date">Annual General Meeting Date (if
                                                    applicable)
                                                </label>
                                                <input type="date"
                                                       class="form-control   @error('annual_meeting_date') is-invalid @enderror"
                                                       id="annual_meeting_date" name="annual_meeting_date"
                                                       value="{{ old('annual_meeting_date', $company->annual_meeting_date) }}">
                                                @error('annual_meeting_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="activity_status">Status of the company </label>
                                                <select
                                                    class="form-control   @error('activity_status') is-invalid @enderror"
                                                    name="activity_status">
                                                    <option selected value="">Select</option>
                                                    <option value="1"
                                                            {{ old('activity_status', $company->acitivity_status) == '1' ? 'selected' : '' }}>
                                                        Super Active</option>
                                                    <option value="2"
                                                            {{ old('activity_status', $company->acitivity_status) == '2' ? 'selected' : '' }}>
                                                        Active</option>
                                                    <option value="3"
                                                            {{ old('activity_status', $company->acitivity_status) == '3' ? 'selected' : '' }}>
                                                        Semi Active</option>
                                                    <option value="4"
                                                            {{ old('activity_status', $company->acitivity_status) == '4' ? 'selected' : '' }}>
                                                        Dormant</option>
                                                    <option value="5"
                                                            {{ old('activity_status', $company->acitivity_status) == '5' ? 'selected' : '' }}>
                                                        Winding Up</option>
                                                    <option value="6"
                                                            {{ old('activity_status', $company->acitivity_status) == '6' ? 'selected' : '' }}>
                                                        Striking Off</option>
                                                </select>
                                                @error('activity_status')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="status_date">Date </label>
                                                <input type="date"
                                                       class="form-control   @error('status_date') is-invalid @enderror"
                                                       value="{{ old('status_date', \Carbon\Carbon::parse($company->status_date)->format('Y-m-d')) }}"
                                                       name="status_date">
                                                @error('status_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-header">
                                    <h5 for="one-ecom-product-meta-keywords">MISC</h5>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="ep_certificate">Exempt Private Company
                                                    Certificate(EPC)<span class="text-danger">*</span></label>
                                                <select
                                                    class="form-control   @error('ep_certificate') is-invalid @enderror"
                                                    name="ep_certificate">
                                                    <option selected value="">Select</option>
                                                    <option value="No"
                                                            {{ old('ep_certificate', $company->ep_certificate) == 'No' ? 'selected' : '' }}>
                                                        No </option>
                                                    <option value="Yes"
                                                            {{ old('ep_certificate', $company->ep_certificate) == 'Yes' ? 'selected' : '' }}>
                                                        Yes</option>
                                                </select>
                                                @error('ep_certificate')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="applied_date">Applied Date</label>
                                                <input type="date"
                                                       class="form-control   @error('applied_date') is-invalid @enderror"
                                                       name="applied_date"
                                                       value="{{ old('applied_date', \Carbon\Carbon::parse($company->applied_date)->format('Y-m-d')) }}">
                                                @error('applied_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group NDD">
                                                <label for="new_due_date">New Due Date</label>
                                                <input type="date"
                                                       class="form-control   @error('new_due_date') is-invalid @enderror"
                                                       name="new_due_date" value="{{ old('new_due_date') }}">
                                                @error('new_due_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="extension_time">Extension of Time (EOT)</label>
                                                <select
                                                    class="form-control EOT  @error('extension_time') is-invalid @enderror"
                                                    name="extension_time">
                                                    <option selected value="">Select</option>
                                                    <option value="No"
                                                            {{ old('extension_time') == 'No' ? 'selected' : '' }}>No
                                                    </option>
                                                    <option value="Yes"
                                                            {{ old('extension_time') == 'Yes' ? 'selected' : '' }}>
                                                        Yes</option>
                                                </select>
                                                @error('extension_time')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                        </div>
                                        <!--                                        <div class="col-lg-6">
                                                                                                                                                                    <div class="form-group">
                                                                                                                                                                        <label for="company_etr">eTR (CTOS) <span class="text-danger">*</span></label>
                                                                                                                                                                        <input type="text" class="form-control   @error('company_etr') is-invalid @enderror" name="company_etr" placeholder="eTR (CTOS)" value="{{ old('company_etr', $company->company_etr) }}">
                                                                                                                                                                        @error('company_etr')
                                                                                                                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                                                                                                                        <strong>{{ $message }}</strong>
                                                                                                                                                                                                        </span>
                                                                                                                                                                        @enderror
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                                <div class="col-lg-4">
                                                                                                                                                                    <div class="form-group">
                                                                                                                                                                        <label for="etr_date">Date <span class="text-danger">*</span></label>
                                                                                                                                                                        <input type="date" class="form-control   @error('etr_date') is-invalid @enderror" name="etr_date" value="{{ old('etr_date', \Carbon\Carbon::parse($company->etr_date)->format('Y-m-d')) }}">
                                                                                                                                                                        @error('etr_date')
                                                                                                                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                                                                                                                        <strong>{{ $message }}</strong>
                                                                                                                                                                                                        </span>
                                                                                                                                                                        @enderror
                                                                                                                                                                    </div>
                                                                                                                                                                </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Alternative Style -->
                    </div>
                </div>
                <div class="text-right">
                    <a class="btn btn-secondary btnNext">Next <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="tab-pane fade" id="btabs-static-document" role="tabpanel">
                @foreach ($company->company_documents as $key => $document)
                <div class="block-content block-content-full">
                    <div class="row">
                        <input type="hidden" name="document_type[{{ $key }}][typeId]"
                               value="{{ $document->id }}">
                        <table id="mytable-{{ $key }}" style="width: 100%;">
                            <input type="hidden" name="document_type[{{ $key }}][type]"
                                   value="{{ $document->report_type }}" id="report_type">
                            <thead>
                            <th style="width: 30%;">{{ $document->report_type }}</th>
                            @if ($document->report_type != 'Annual Return')
                            <th class="text-center">Received Date</th>
                            @else
                            <th></th>
                            @endif
                            <th>Submitted</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                                @php $d = array(); @endphp
                                @foreach (json_decode($document->report_year) as $key1 => $year)
                                @if ($year != null)
                                @php
                                $i = $key1;
                                $d[] = $key1;
                                @endphp
                                <tr>
                                    <td style="width: 25%; padding: 10px;"><input type="text"
                                                                                  class="form-control   @error('document_type.' . $key . '.report_year.' . $i) is-invalid @enderror"
                                                                                  value="{{ $year ? $year ?? old('document_type')[$key]['report_year'][$i] : '' }}"
                                                                                  name="document_type[{{ $key }}][report_year][]"
                                                                                  placeholder="Year">
                                        @error('document_type.' . $key . '.report_year.' . $i)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                    @if ($document->report_type != 'Annual Return')
                                    <td style="width: 25%; padding: 10px;"><input type="date"
                                                                                  class="form-control  @error('document_type.' . $key . '.recevie_date.' . $i) is-invalid @enderror"
                                                                                  id='datepicker'
                                                                                  name="document_type[{{ $key }}][recevie_date][]"
                                                                                  value="{{ json_decode($document->receive_date)[$key1] ? json_decode($document->receive_date)[$key1] ?? old('document_type')[$key]['recevie_date'][$i] : '' }}">
                                        @error('document_type.' . $key . '.recevie_date.' . $i)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                    @else
                                    <td style="width: 25%; padding: 10px;">
                                        <input type="hidden"
                                               name="document_type[{{ $key }}][recevie_date][]"
                                               value="">
                                    </td>
                                    @endif
                                    <td style="width: 25%; padding: 10px;"><input type="date"
                                                                                  class="form-control  @error('document_type.' . $key . '.submitted.' . $i) is-invalid @enderror"
                                                                                  id='datepicker'
                                                                                  name="document_type[{{ $key }}][submitted][]"
                                                                                  value="{{ json_decode($document->submited)[$key1] ? json_decode($document->submited)[$key1] ?? old('document_type')[$key]['submitted'][$i] : '' }}">
                                        @error('document_type.' . $key . '.submitted.' . $i)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                    <td><button type="button" class="delete1 btn"
                                                {{ $key1 == 0 ? 'disabled' : '' }}><i
                                                class="fa fa-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                                @if (!empty(old('document_type')[$key]['report_year']))
                                @php
                                if (!empty($d)) {
                                $count = count(old('document_type')[$key]['report_year']);
                                $i = $key1 + 1;
                                } else {
                                $count = count(old('document_type')[$key]['report_year']);
                                $i = 0;
                                }
                                @endphp
                                @for ($i = $i; $i < $count; $i++)
                                <tr class='document'>
                                    <td style="width: 25%; padding: 10px;"><input type="text"
                                                                                  class="form-control   @error('document_type.' . $key . '.report_year.' . $i) is-invalid @enderror"
                                                                                  name="document_type[{{ $key }}][report_year][]"
                                                                                  value="{{ old('document_type')[$key]['report_year'][$i] ?? '' }}"
                                                                                  placeholder="Year">
                                        @error('document_type.' . $key . '.report_year.' . $i)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                    @if ($document->report_type != 'Annual Return')
                                    <td style="width: 25%; padding: 10px;"><input type="date"
                                                                                  class="form-control  @error('document_type.' . $key . '.recevie_date.' . $i) is-invalid @enderror"
                                                                                  name="document_type[{{ $key }}][recevie_date][]"
                                                                                  value="{{ old('document_type')[$key]['recevie_date'][$i] ?? '' }}">
                                        @error('document_type.' . $key . '.recevie_date.' . $i)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                    @else
                                    <td style="width: 25%; padding: 10px;">
                                        <input type="hidden"
                                               name="document_type[{{ $key }}][recevie_date][]"
                                               value="">
                                    </td>
                                    @endif
                                    <td style="width: 25%; padding: 10px;"><input type="date"
                                                                                  class="form-control  @error('document_type.' . $key . '.submitted.' . $i) is-invalid @enderror"
                                                                                  name="document_type[{{ $key }}][submitted][]"
                                                                                  value="{{ old('document_type')[$key]['submitted'][$i] ?? '' }}">
                                        @error('document_type.' . $key . '.submitted.' . $i)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                    <td><button type="button" class="delete1 btn"
                                                {{ $i == 0 ? 'disabled' : '' }}><i
                                                class="fa fa-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                                @endfor
                                @elseif(empty($d))
                                @php $count = 1; @endphp
                                @for ($i = 0; $i < $count; $i++)
                                <tr class='document'>
                                    <td style="width: 25%; padding: 10px;"><input type="text"
                                                                                  class="form-control   @error('document_type.' . $key . '.report_year.' . $i) is-invalid @enderror"
                                                                                  name="document_type[{{ $key }}][report_year][]"
                                                                                  value="{{ old('document_type')[$key]['report_year'][$i] ?? '' }}"
                                                                                  placeholder="Year">
                                        @error('document_type.' . $key . '.report_year.' . $i)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                    @if ($document->report_type != 'Annual Return')
                                    <td style="width: 25%; padding: 10px;"><input type="date"
                                                                                  class="form-control  @error('document_type.' . $key . '.recevie_date.' . $i) is-invalid @enderror"
                                                                                  name="document_type[{{ $key }}][recevie_date][]"
                                                                                  value="{{ old('document_type')[$key]['recevie_date'][$i] ?? '' }}">
                                        @error('document_type.' . $key . '.recevie_date.' . $i)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                    @else
                                    <td style="width: 25%; padding: 10px;">
                                        <input type="hidden"
                                               name="document_type[{{ $key }}][recevie_date][]"
                                               value="">
                                    </td>
                                    @endif
                                    <td style="width: 25%; padding: 10px;"><input type="date"
                                                                                  class="form-control  @error('document_type.' . $key . '.submitted.' . $i) is-invalid @enderror"
                                                                                  name="document_type[{{ $key }}][submitted][]"
                                                                                  value="{{ old('document_type')[$key]['submitted'][$i] ?? '' }}">
                                        @error('document_type.' . $key . '.submitted.' . $i)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                    <td><button type="button" class="remove btn "
                                                {{ $i == 0 ? 'disabled' : '' }}><i
                                                class="fa fa-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                                @endfor
                                @endif
                            </tbody>
                        </table>
                        <div class="col-md-12" id="addnew_feature">
                            <a href="#" id="{{ $key }}"
                               class="insert btn btn-icon btn-secondary btn-sm mr-2 report">Add More</a>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-lg-12 m-3">
                    <a class="btn btn-light btnPrevious"><i class="fa fa-angle-left"></i> Previous</a>
                    <a class="btn btn-secondary btnNext">Next <i class="fa fa-angle-right"></i></a>

                </div>
            </div>

            <div class="tab-pane fade" id="btabs-static-portfolio" role="tabpanel">
                <div class="block-header">
                    <h5 class="font-w600">PORTFOLIO HOLDER</h5>
                </div>
                @foreach ($departments as $key => $department)
                <div class="block-content block-content-full">
                    <label for="one-ecom-product-meta-keywords">{{ $department->name }}</label>
                    @php
                    $j = 0;
                    $k = [];
                    $users = \App\Models\Users\User::with('departments')
                    ->where('status', 1)
                    ->whereHas('departments', function ($query) use ($department) {
                    $query->where('id', $department->id);
                    })
                    ->get();
                    @endphp
                    <div class="row mb-2">
                        <input type="hidden" name="company_portfolio[{{ $key }}][department_id]"
                               value="{{ $department->id }}">
                        <table id='port-{{ $key }}' class="append" style="width: 100%;">
                            <tbody>
                                @isset($company->company_portfolios)
                                @foreach ($company->company_portfolios as $key1 => $portfolio)
                                @php $k[] = $portfolio->department_id;  @endphp
                                @if ($portfolio->department_id == $department->id)
                                @php $i = $key; @endphp
                                <tr id='portfolio'>
                            <input type="hidden"
                                   name="company_portfolio[{{ $key }}][portfolio_id][]"
                                   value="{{ $portfolio->id }}">
                            <td style="width: 25%; padding: 10px;">
                                <select
                                    class="form-control  @error('company_portfolio.' . $key . '.user_id.' . $i) is-invalid @enderror"
                                    name="company_portfolio[{{ $key }}][user_id][]">
                                    <option disabled selected>Select</option>
                                    @foreach ($users as $user)
                                    <option value="{{ $user->id }}"
                                            @if ($user->id == $portfolio->user_id) selected  @endif>{{ $user->name }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('company_portfolio.' . $key . '.user_id.' . $i)

                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                            <td style="width: 25%; padding: 10px;">
                                <input type="date"
                                       class="form-control  @error('company_portfolio.' . $key . '.date.' . $i) is-invalid @enderror"
                                       value="{{ \Carbon\Carbon::parse($portfolio->date)->format('Y-m-d') ?? old('company_portfolio')[$key]['date'][$i] }}"
                                       name="company_portfolio[{{ $key }}][date][]">
                                @error('company_portfolio.' . $key . '.date.' . $i)
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                            <td style="width: 20%; padding: 10px;">

                                <button type="button" value="{{ $portfolio->id }}"
                                        class="removeRow btn btn-sm btn-alt-danger"
                                        title="{{ __('Delete') }}"><i
                                        class="fa fa-fw fa-times text-danger"></i></button>

                            </td>
                            </tr>
                            @php $j++; @endphp
                            @endif
                            @endforeach
                            @endisset

                            @if (!empty(old('company_portfolio')[$key]['user_id']))
                            @if ($j != 0 && count(old('company_portfolio')[$key]['user_id']) > $j)
                            @php
                            $count = count(old('company_portfolio')[$key]['user_id']);
                            @endphp
                            @for ($i = $j; $i < $count; $i++)
                            <tr class="">
                                <td style="width: 25%; padding: 10px;">
                                    <select
                                        class="form-control  @error('company_portfolio.' . $key . '.user_id.' . $i) is-invalid @enderror"
                                        name="company_portfolio[{{ $key }}][user_id][]">
                                        <option disabled selected>Select</option>
                                        @foreach ($users as $user)
                                        @php
                                        $selected = '';
                                        $user_id = !empty(old('company_portfolio')[$key]['user_id']) ? old('company_portfolio')[$key]['user_id'][$i] : '';
                                        if ($user_id == $user->id) {
                                        $selected = 'selected';
                                        }
                                        @endphp
                                        <option value="{{ $user->id }}"
                                                {{ $selected }}>{{ $user->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                    @error('company_portfolio.' . $key . '.user_id.' . $i)
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                                <td style="width: 25%; padding: 10px;">
                                    <input type="date"
                                           class="form-control  @error('company_portfolio.' . $key . '.date.' . $i) is-invalid @enderror"
                                           name="company_portfolio[{{ $key }}][date][]"
                                           value="{{ old('company_portfolio')[$key]['date'][$i] ?? '' }}">
                                    @error('company_portfolio.' . $key . '.date.' . $i)
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                                <td style="width: 20%; padding: 10px;">
                                    <button type="button"
                                            class="removeRowPortfolio btn btn-sm btn-alt-danger"
                                            href="javascript:void(0)" data-toggle="tooltip"
                                            title="Delete">
                                        <i class="fa fa-fw fa-times text-danger"></i>
                                    </button>
                                </td>
                            </tr>
                            @endfor
                            @endif
                            @endif

                            @if (!in_array($department->id, $k))
                            @php
                            if (!empty(old('company_portfolio')[$key]['user_id'])) {
                            $count = count(old('company_portfolio')[$key]['user_id']);
                            } else {
                            $count = 1;
                            }
                            @endphp
                            @for ($i = 0; $i < $count; $i++)
                            <tr class="">
                                <td style="width: 25%; padding: 10px;">
                                    <select
                                        class="form-control  @error('company_portfolio.' . $key . '.user_id.' . $i) is-invalid @enderror"
                                        name="company_portfolio[{{ $key }}][user_id][]">
                                        <option disabled selected>Select</option>
                                        @foreach ($users as $user)
                                        @php
                                        $selected = '';
                                        $user_id = !empty(old('company_portfolio')[$key]['user_id']) ? old('company_portfolio')[$key]['user_id'][$i] : '';
                                        if ($user_id == $user->id) {
                                        $selected = 'selected';
                                        }
                                        @endphp
                                        <option value="{{ $user->id }}"
                                                {{ $selected }}>{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('company_portfolio.' . $key . '.user_id.' . $i)
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                                <td style="width: 25%; padding: 10px;">
                                    <input type="date"
                                           class="form-control  @error('company_portfolio.' . $key . '.date.' . $i) is-invalid @enderror"
                                           name="company_portfolio[{{ $key }}][date][]"
                                           value="{{ old('company_portfolio')[$key]['date'][$i] ?? '' }}">
                                    @error('company_portfolio.' . $key . '.date.' . $i)
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                                <td style="width: 20%; padding: 10px;">
                                    <button type="button"
                                            class="removeRowPortfolio btn btn-sm btn-alt-danger"
                                            href="javascript:void(0)" data-toggle="tooltip"
                                            title="Delete" {{ $i == 0 ? 'disabled' : '' }}>
                                        <i class="fa fa-fw fa-times text-danger"></i>
                                    </button>
                                </td>
                            </tr>
                            @endfor
                            @endif
                            </tbody>
                        </table>
                        <div class="col-md-12" id="addnew_feature">
                            <a href="#" id="{{ $key }}"
                               class="portfolio btn btn-icon btn-secondary btn-sm mr-2">Add More</a>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-lg-12 m-3">
                    <a class="btn btn-secondary btnPrevious">Previous</a>
                    <a class="btn btn-secondary btnNext">Next</a>
                </div>
            </div>
            <div class="tab-pane fade" id="btabs-static-reminder" role="tabpanel">
                <div class="block-header">
                    <h5 class="font-w600">REMINDER</h5>
                </div>
                <div class="block-content block-content-full">
                    <input type="hidden" name="company_reminder_id"
                           value="{{ optional($company->company_reminders)->id }}">
                    <div class="row add pt-3">
                        @php
                        $disabled = '';

                        $secretarial_fees = isset(old('company_reminder')['secretarialfees']) ? old('company_reminder')['secretarialfees'] : optional($company->company_reminders)->secretarialFees;
                        if ($secretarial_fees === '0') {
                        $disabled = 'disabled';
                        }
                        @endphp
                        <div class="col-md-3 col-xl-3 ">
                            <input type="checkbox" class="check" id="check" onclick="paym(this)"
                                   @if (isset($secretarial_fees) && $secretarial_fees !== '0') checked @endif>
                            <label class="ml-2"> Secretarial fees</label>
                        </div>
                        <div class="col-md-6 col-xl-6 offset-md-2 offset-xl-2 form-group"
                             style="margin-bottom: 0.5rem;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="example-radios-inline1"
                                       name="company_reminder[secretarialfees]" value="monthly"
                                       @if (isset($secretarial_fees) && $secretarial_fees == 'monthly')checked @endif {{ $disabled }}>
                                <label class="form-check-label" for="example-radios-inline1">Monthly</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="example-radios-inline2"
                                       name="company_reminder[secretarialfees]" value="quarterly"
                                       @if (isset($secretarial_fees) && $secretarial_fees == 'quarterly')checked @endif {{ $disabled }}>
                                <label class="form-check-label" for="example-radios-inline2">Quarterly</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="example-radios-inline3"
                                       name="company_reminder[secretarialfees]" value="halfYearly"
                                       @if (isset($secretarial_fees) && $secretarial_fees == 'halfYearly')checked @endif {{ $disabled }}>
                                <label class="form-check-label" for="example-radios-inline3"> Half-Yearly</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="example-radios-inline4"
                                       name="company_reminder[secretarialfees]" value="yearly"
                                       @if (isset($secretarial_fees) && $secretarial_fees == 'yearly')checked @endif {{ $disabled }}>
                                <label class="form-check-label" for="example-radios-inline4">Annually </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row add pt-3">
                        @php
                        $annual_return = isset(old('company_reminder')['annualReturn']) ? old('company_reminder')['annualReturn'] : optional($company->company_reminders)->is_annualReturn_notify;
                        @endphp
                        <div class="col-md-9 col-xl-9 text-start">
                            <input type="checkbox" name="company_reminder[annualReturn]" id="annualReturn" value="1"
                                   @if (isset($annual_return) && $annual_return == 1)checked @endif>
                            <label class="ml-2"> Create Recurrence job for Annual Return. </label>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row add pt-3">
                        @php
                        $taxAgent = isset(old('company_reminder')['taxAgent']) ? old('company_reminder')['taxAgent'] : optional($company->company_reminders)->is_taxAgent_notify;
                        @endphp
                        <div class="col-md-9 col-xl-9  text-start">
                            <input type="checkbox" name="company_reminder[taxAgent]" value="1"
                                   @if (isset($taxAgent) && $taxAgent == 1)checked @endif>
                            <label class="ml-2"> Reminder on the next 15th January for Tax Form E.
                            </label>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row add pt-3">
                        @php
                        $auditorAppoinment = isset(old('company_reminder')['auditorAppoinment']) ? old('company_reminder')['auditorAppoinment'] : optional($company->company_reminders)->is_auditorAppoinment_notify;
                        @endphp
                        <div class="col-md-9 col-xl-9  text-start">
                            <input type="checkbox" name="company_reminder[auditorAppoinment]" value="1"
                                   @if (isset($auditorAppoinment) && $auditorAppoinment == 1)checked @endif>
                            <label class="ml-2"> Reminder After 12 months for Appointment of
                                Auditors.</label>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row add pt-3">
                        <div class="col-md-9 col-xl-9 text-start">
                            @php
                            $audited_statement = isset(old('company_reminder')['audited_statement']) ? old('company_reminder')['audited_statement'] : optional($company->company_reminders)->is_auditStatement_notify;
                            @endphp
                            <input type="checkbox" name="company_reminder[audited_statement]" id="audited_statement"
                                   value="1" @if (isset($audited_statement) && $audited_statement == 1) checked @endif>
                            <label class="ml-2"> Reminder After 3 months Company's financial year end for
                                AFS submission.</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 m-3">
                    <a class="btn btn-secondary btnPrevious">Previous</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>
</div>
<!-- END Page Content -->
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
    var max_fields = 15;
    var wrapper = $(".container1");
    var add_button = $(".add_form_field");
    var x = 1;
    $(add_button).click(function(e) {

    e.preventDefault();
    var count = parseInt($("#counterRelatedCompany").val()) + 1;
    if (x < max_fields) {
    x++;
    $(wrapper).append(
            '<div class="" id="new_plan"><div class="row ml-2 py-2"><input type="text" required class="form-control  col-md-10" id="groupof_companies"  name="groupof_companies[' + count + ']" value="{{ old('groupof_companies.')[' + count + '] ?? ''}}" placeholder="Group Of Companies"/><button type="button" class="delete_group btn " {{($i == 0) ? 'disabled' : ''}}><i class="fa fa-trash" aria-hidden="true"></i></button></div></div>'
            ); //add input box
    $("#counterRelatedCompany").val(count);
    } else {
    alert('You Reached the limits')
    }

    });
    $(document).on("click", ".delete_group", function(e) {
    e.preventDefault();
    $(this).parent('div').remove();
    x--;
    var count = parseInt($("#counterRelatedCompany").val()) - 1;
    $("#counterRelatedCompany").val(count);
    })
    });
    // Shareholder
    $(document).ready(function() {

    var max_fields = 15;
    var wrapper = $(".container2");
    var add_button = $(".add_shareholder_form_field");
    var i = 0;
    var x = 1;
    $(add_button).click(function(e) {
    e.preventDefault();
    var count = parseInt($("#counterShareholder").val()) + 1;
    if (x < max_fields) {
    x++;
    $(wrapper).append(
            '<div id="new_plan"><div class="row"><div class="col-lg-7 py-2"><input type="text" class="form-control "  name="shareholder_name[' + count + ']" placeholder="Share Holder" required value="{{ old('shareholder_name.')[' + count + '] ?? ''}}"></div><div class="col-lg-3 py-2"><input class="form-control "  type="text" id="fee-2" placeholder="%" class="fee" required name="values[' + count + ']" value="{{ old('values.')[' + count + '] ?? ''}}"></div><a href="#" class="delete_shareholder btn text-center my-3 px-2 py-2"><i class="fa fa-trash" aria-hidden="true"></i></a></div></div>'

            ); //add input box

    i++;
    $("#counterShareholder").val(count);
    } else {
    alert('You Reached the limits')
    }
    });
    $(document).on("click", ".delete_shareholder", function(e) {
    e.preventDefault();
    $(this).parent('div').remove();
    x--;
    var count = parseInt($("#counterShareholder").val()) - 1;
    $("#counterShareholder").val(count);
    })

    });
    // director
    $(document).ready(function() {
    var max_fields = 15;
    var wrapper = $(".container3");
    var add_button = $(".add_director_form_field");
    var x = 1;
    $(add_button).click(function(e) {
    e.preventDefault();
    var count = parseInt($("#counterDirector").val()) + 1;
    if (x < max_fields) {
    x++;
    $(wrapper).append(
            '<div class="row py-2"><input type="text" class="form-control  col-md-8 ml-3" id="director" required name="director[' + count + ']"  placeholder="Director Name" value="{{ old('director.')[' + count + '] ?? ''}}"/><a href="#" class="delete_director btn py-2 px-2 mx-2"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'
            ); //add input box


    $("#counterDirector").val(count);
    } else {
    alert('You Reached the limits')
    }
    });
    $(document).on("click", ".delete_director", function(e) {
    e.preventDefault();
    $(this).parent('div').remove();
    x--;
    var count = parseInt($("#counterDirector").val()) - 1;
    $("#counterDirector").val(count);
    });
    });
    // person incharge

    $(document).ready(function() {

    var max_fields = 15;
    var wrapper = $(".container4");
    var add_button = $(".add_companyINCharge_form_field");
    var x = 1;
    $(add_button).click(function(e) {


    e.preventDefault();
    var count = parseInt($("#counterIncharge").val()) + 1;
    if (x < max_fields) {


    x++;
    $(wrapper).append(
            '<div id="new_plan"><div class="row"><div class="col-lg-4"><div class="form-group"><label for="department">Departments</label><select  class="select2 form-control @error('department_id') is-invalid @enderror" id="department_id" name="incharge_department_id[' + count + ']" style="width: 100%;" data-placeholder="Choose" ><option value="">{{__('Please Select')}}</option>@foreach($departments as $department)<option value="{{ $department->id }}" @if ($department->id == old('department_id')) selected @endif>{{ $department->name }}</option>@endforeach</select></div></div><div class="col-lg-8 py-1"><label for="incharge_name">Name:</label><input type="text"  class="form-control "  name="incharge_name[' + count + ']" placeholder="Name" value="{{ old('incharge_name.')[' + count + '] ?? ''}}"></div></div><div class="row"><div class="col-lg-4 py-1"></div><div class="col-lg-4 py-1"><label for="incharge_phone">Phone:</label><input type="text"  class="form-control "  name="incharge_phone[' + count + ']" placeholder="Phone" value="{{ old('incharge_phone.')[' + count + ']?? ''}}"></div><div class="col-lg-4 py-1"><label for="incharge_email">Email:</label><input type="email"  class="form-control " name="incharge_email[' + count + ']" placeholder="Email" value="{{ old('incharge_email.')[' + count + '] ?? ''}}"></div></div><a href="#" class="delete_incharge btn"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'
            ); //add input box

    $('.select2').select2();
    $("#counterIncharge").val(count);
    //    
    } else {
    alert('You Reached the limits')
    }

    });
    $(document).on('click', '.delete_incharge', function (e) {
    e.preventDefault();
    $(this).closest('div').remove();
    x--;
    var count = parseInt($("#counterIncharge").val()) - 1;
    $("#counterIncharge").val(count);
    });
    });
    // next and previous
    $(document).ready(function() {

    $('.btnNext').click(function () {
    $('.nav-tabs > .nav-item > .active').parent().next('li').find('a').trigger('click');
    });
    $('.btnPrevious').click(function () {
    $('.nav-tabs > .nav-item > .active').parent().prev('li').find('a').trigger('click');
    });
    });
    $(document).ready(function(){
    $('#myTab a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
    });
    // store the currently selected tab in the hash value
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
    var id = $(e.target).attr("href").substr(1);
    window.location.hash = id;
    });
    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $('#myTab a[href="' + hash + '"]').tab('show');
    });
    // portfolio holder

    $(document).ready(function() {

    var t = 0;
    $(".portfolio").click(function(e) {
    e.preventDefault();
    t++;
    $("#port-" + this.id).each(function() {

    var tds = '<tr class="p-' + t + '" id="portfolio">';
    jQuery.each($('tr:last td', this), function() {
    tds += '<td style="width: 25%; padding: 10px;">' + $(this).html() + '</td>';
    });
    tds += '</tr>';
    if ($('tbody', this).length > 0) {
    $('tbody', this).append(tds);
    $('select', '.p-' + t).val("");
    $('input', '.p-' + t).val("");
    $('button', '.p-' + t).val("");
    $('button', '.p-' + t).removeAttr('disabled');
    $('button', '.p-' + t).removeClass('removeRow');
    $('button', '.p-' + t).addClass('removeRowPortfolio');
    } else {
    $(this).append(tds);
    }
    });
    });
    // remove row
    $(document).on('click', '.danger1', function () {

    $(this).closest('tr').remove();
    });
    });
    $(document).ready(function() {
    // remove row
    $(document).on('click', '.removeRow', function (e) {
    e.preventDefault();
    swal({
    title: `Are you sure you want to delete ${name}?`,
            text: "If you delete this, it will be gone forever.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
    })
            .then((willDelete) => {
            if (willDelete) {
            var idPortfolio = $(this).val();
            if (idPortfolio){
            $.ajax({
            type:'get',
                    url:"{{route('deletePortfolio')}}",
                    data:'id=' + idPortfolio,
                    success:function(data){
                    console.log(data);
                    }
            });
            }

            $(this).closest('tr').remove();
            location.reload();
            }
            });
    });
    $(document).on('click', '.removeRowPortfolio', function (e) {
    e.preventDefault();
    $(this).closest('tr').remove();
    });
    });
    // documents 
    $(document).ready(function () {

    $(".insert").click(function () {
    var pid = parseInt(this.id);
    $("#mytable-" + this.id).each(function () {
    var type = $('#report_type', this).val();
    var tds = '<tr>';
    tds += '<td style="width: 25%; padding: 10px;"><input type="text" class="form-control   @error('document_type.'.' + pid + '.'.report_year') is-invalid @enderror" name="document_type[' + pid + '][report_year][]" value="{{old('document_type')[' + pid + ']['report_year'] ?? ''}}" placeholder="Year">@error('document_type.'.' + pid + '.'.report_year')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span> @enderror</td>' + ((type != "Annual Return")?'<td style="width: 25%; padding: 10px;" ><input type="date"  class="form-control  @error('document_type.'.' + pid + '.'.recevie_date') is-invalid @enderror" name="document_type[' + pid + '][recevie_date][]" value="{{old('document_type')[' + pid + ']['recevie_date'] ?? ''}}" >@error('document_type.'.' + pid + '.'.recevie_date')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span> @enderror</td>':'<td style="width: 25%; padding: 10px;" ></td>') + '<td style="width: 25%; padding: 10px;" ><input type="date"  class="form-control  @error('document_type.'.' + pid + '.'.submitted') is-invalid @enderror" name="document_type[' + pid + '][submitted][]" value="{{old('document_type')[' + pid + ']['submitted'] ?? ''}}">@error('document_type.'.' + pid + '.'.submitted')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span> @enderror</td><td><a href="#" class="delete1 btn"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
    tds += '</tr>';
    if ($('tbody', this).length > 0) {
    $('tbody', this).append(tds);
    } else {
    $(this).append(tds);
    }
    });
    });
    // remove row
    $(document).on('click', '.delete1', function (e) {
    e.preventDefault();
    $(this).closest('tr').remove();
    });
    });
    $(".date").datepicker({
    fontAwesome: "font-awesome",
            format: "dd-MM",
            minView: "month",
            startDate: "01-01",
            endDate: "31-12",
            autoclose: true,
            changeYear: false
    });
    function paym(check) {
    var remender = document.getElementById('example-radios-inline1');
    var remender2 = document.getElementById('example-radios-inline2');
    var remender3 = document.getElementById('example-radios-inline3');
    var remender4 = document.getElementById('example-radios-inline4');
    remender.disabled = check.checked ? false : true;
    remender2.disabled = check.checked ? false : true;
    remender3.disabled = check.checked ? false : true;
    remender4.disabled = check.checked ? false : true;
    if (!remender.disabled && !remender2.disabled && !remender3.disabled && !remender4.disabled) {
    remender.focus();
    remender2.focus();
    remender3.focus();
    remender4.focus();
    }
    //    if (remender.disabled && remender2.disabled && remender3.disabled && remender4.disabled){
    //    remender.value = "";
    //    remender2.value = "";
    //    remender3.value = "";
    //    remender4.value = "";
    //    }
    }

    $(document).ready(function() {
    $('.NDD').addClass('d-none');
    $('.EOT').on('click', function (e) {
    e.preventDefault();
        if($(this).val()== 'Yes'){
    $('.NDD').removeClass('d-none');
        }else{
    $('.NDD').addClass('d-none');
    }
    });
    });
</script>
@endpush
