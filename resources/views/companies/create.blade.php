@extends('layouts.app', [
'activePage' => 'create-customers',
'menuParent' => 'create-customers',
'title' => 'Create Customers',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-12 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    Customer <small
                        class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Customer</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">Create</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        @php
        $i = $j = $k = $l = $m = $n = $o = $p = $q = $g = $d = 0;
        $stars = [];
        $stars = ['name', 'email', 'company_roc', 'incorporation_date', 'e_no', 'c_no', 'company_fka', 'contact', 'phone', 'fax', 'bank_name', 'account_number', 'address_name.*', 'address_phone.*', 'address.*', 'incharge_name.*', 'incharge_phone.*', 'incharge_email.*', 'responsible_name.*.*', 'share_capital', 'shareholder_name.*', 'values.*', 'director.*', 'groupof_companies.*', 'financial_year_end', 'annual_return_date', 'turnover', 'profit_before_tax', 'activity_status', 'status_date', 'ep_certificate', 'applied_date', 'new_due_date', 'extension_time', 'company_etr', 'etr_date', 'document_type.*.report_year.*'];
        @endphp
        @foreach ($stars as $key => $star)
        @if ($errors->has($stars[$key]))
        @if (in_array($key, range(0, 6)))
        @php $i++; @endphp
        @endif
        @if (in_array($key, range(7, 9)))
        @php $j++; @endphp
        @endif
        @if (in_array($key, range(10, 11)))
        @php $k++; @endphp
        @endif
        @if (in_array($key, range(12, 14)))
        @php $l++; @endphp
        @endif
        @if (in_array($key, range(15, 17)))
        @php $m++; @endphp
        @endif
        @if (in_array($key, range(18, 18)))
        @php $n++; @endphp
        @endif
        @if (in_array($key, range(19, 22)))
        @php $o++; @endphp
        @endif
        @if (in_array($key, range(23, 23)))
        @php $p++; @endphp
        @endif
        @if (in_array($key, range(24, 35)))
        @php $q++; @endphp
        @endif
        @if (in_array($key, range(0, 35)))
        @php $g++; @endphp
        @endif
        @if ($key == 36)
        @php $d++; @endphp
        @endif
        @endif
        @endforeach
        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist" id="myTab">
            <li class="nav-item">
                <a class="nav-link active" id="home" href="#btabs-static-home">General @if ($g != 0)<span class="text-danger">*</span>@endif</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile" href="#btabs-static-document">
                    Document @if ($d != 0)<span class="text-danger">*</span>@endif
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pro" href="#btabs-static-portfolio">
                    Portfolio Holder
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="reminder" href="#btabs-static-reminder">
                    Reminders
                </a>
            </li>
        </ul>
        <form action="{{ route('companies.store') }}" method="POST">
            @csrf

            <div class="block-content tab-content">
                <div class="tab-pane active" id="btabs-static-home" role="tabpanel">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="nav flex-column nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#info">Info @if ($i != 0)<span class="text-danger">*</span>@endif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#contact">Contact @if ($j != 0)<span class="text-danger">*</span>@endif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#g-address">Address @if ($l != 0)<span class="text-danger">*</span>@endif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#bank-details">Bank Details
                                        @if ($k != 0)<span class="text-danger">*</span>@endif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#Person-in-Charge">Person-in-Charge
                                        @if ($m != 0)<span class="text-danger">*</span>@endif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#Responsible-Accountants">Responsible Professional
                                        Firm
                                        @if ($n != 0)<span class="text-danger">*</span>@endif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#Shares">Directors & Shareholders
                                        @if ($o != 0)<span class="text-danger">*</span>@endif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#Group-of-Companies">Group of Companies
                                        @if ($p != 0)<span class="text-danger">*</span>@endif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#Financial-Information">Financial Information
                                        @if ($q != 0)<span class="text-danger">*</span>@endif</a>
                                </li>

                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="block-content tab-content">
                                <div class="tab-pane active" id="info" role="tabpanel">
                                    <div class="block-header">
                                        <h5>CREATE CUSTOMERS</h3>
                                    </div>
                                    <div class="block-content block-content-full">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Company Name<span
                                                            class="text-danger">*</span></label>
                                                    <input type="text"
                                                           class="form-control  @error('name') is-invalid @enderror"
                                                           value="{{ old('name') }}" name="name"
                                                           placeholder="Company Name">
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email<span
                                                            class="text-danger">*</span></label>
                                                    <input type="text"
                                                           class="form-control  @error('email') is-invalid @enderror"
                                                           id="email" name="email" value="{{ old('email') }}"
                                                           placeholder="Email">
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="company_roc">Registration No.</label>
                                                    <input type="text"
                                                           class="form-control  @error('company_roc') is-invalid @enderror"
                                                           id="company_roc" name="company_roc"
                                                           value="{{ old('company_roc') }}" placeholder="Company ROC">
                                                    @error('company_roc')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="incorporation_date">Incorporation Date<span
                                                            class="text-danger">*</span></label>
                                                    <input type="date"
                                                           class="form-control  @error('incorporation_date') is-invalid @enderror"
                                                           id="incorporation_date" name="incorporation_date"
                                                           value="{{ old('incorporation_date') }}">
                                                    @error('incorporation_date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="e_no">E no.</label>
                                                    <input type="text"
                                                           class="form-control  @error('e_no') is-invalid @enderror"
                                                           id="e_no" name="e_no" placeholder="E no."
                                                           value="{{ old('e_no') }}">
                                                    @error('e_no')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="c_no">C no.</label>
                                                    <input type="text"
                                                           class="form-control   @error('c_no') is-invalid @enderror"
                                                           id="c_no" name="c_no" placeholder="C no."
                                                           value="{{ old('c_no') }}">
                                                    @error('c_no')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="company_fka">Formerly Known As (FKA)</label>
                                                    <input type="text"
                                                           class="form-control  @error('company_fka') is-invalid @enderror"
                                                           id="company_fka" name="company_fka"
                                                           value="{{ old('company_fka') }}"
                                                           placeholder="Formerly Known As (FKA)">
                                                    @error('company_fka')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="date_of_change">Date of Change</label>
                                                    <input type="date"
                                                           class="form-control  @error('date_of_change') is-invalid @enderror"
                                                           id="date_of_change" name="date_of_change"
                                                           value="{{ old('date_of_change') }}"
                                                           placeholder="Date of Change">
                                                    @error('date_of_change')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="natureof_business">Nature of Business</label>
                                                    <textarea
                                                        class="form-control   @error('natureof_business') is-invalid @enderror"
                                                        name="natureof_business"
                                                        placeholder="Nature of Business">{{ old('natureof_business') }}</textarea>
                                                    @error('natureof_business')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="contact" role="tabpanel">
                                    <div class="row">
                                        <h5 for="one-ecom-product-meta-keywords">CONTACT</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="phone">Phone <span class="text-danger">*</span></label>
                                                <input type="text"
                                                       class="form-control   @error('phone') is-invalid @enderror"
                                                       id="phone" name="phone" placeholder="Phone"
                                                       value="{{ old('phone') }}">
                                                @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="fax">Fax</label>
                                                <input type="text"
                                                       class="form-control   @error('fax') is-invalid @enderror" id="fax"
                                                       name="fax" placeholder="Fax" value="{{ old('fax') }}">
                                                @error('fax')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="contact">Contact No.</label>
                                                <input type="text"
                                                       class="form-control   @error('contact') is-invalid @enderror"
                                                       id="contact" name="contact" placeholder="Contact No."
                                                       value="{{ old('contact') }}">
                                                @error('contact')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="bank-details" role="tabpanel">
                                    <div class="row">
                                        <h5 for="one-ecom-product-meta-keywords">BANK DETAILS</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="bank_name">Bank Name
                                                </label>
                                                <input type="text"
                                                       class="form-control   @error('bank_name') is-invalid @enderror"
                                                       id="bank_name" name="bank_name" placeholder="Bank Name"
                                                       value="{{ old('bank_name') }}">
                                                @error('bank_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="account_number">Account Number</label>
                                                <input type="text"
                                                       class="form-control   @error('account_number') is-invalid @enderror"
                                                       id="account_number" name="account_number"
                                                       placeholder="Account Number" value="{{ old('account_number') }}">
                                                @error('account_number')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="g-address" role="tabpanel">
                                    <div class="row">
                                        <h5 for="one-ecom-product-meta-keywords">ADDRESS</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="address_type">Business Address </label>
                                                <input type="hidden" id="address_type" name="address_type[]"
                                                       value="Business Address">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="address_name"> Name </label>
                                                <input type="text"
                                                       class="form-control   @error('address_name.0') is-invalid @enderror"
                                                       id="address_name" name="address_name[]" placeholder="Name"
                                                       value="{{ old('address_name.0') }}">
                                                @error('address_name.0')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="address_phone">Phone</label>
                                                <input type="text"
                                                       class="form-control   @error('address_phone.0') is-invalid @enderror"
                                                       id="address_phone" name="address_phone[]" placeholder="Phone"
                                                       value="{{ old('address_phone.0') }}">
                                                @error('address_phone.0')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <textarea type="text"
                                                          class="form-control   @error('address.0') is-invalid @enderror"
                                                          id="address" name="address[]"
                                                          placeholder="Address">{{ old('address.0') }}</textarea>
                                                @error('address.0')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="address_type">Delivery Address(if different from the
                                                    above)</label>
                                                <input type="hidden" class="form-control " id="address_type"
                                                       name="address_type[]" value="Delivery Address">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="address_name"> Name </label>
                                                <input type="text"
                                                       class="form-control   @error('address_name.1') is-invalid @enderror"
                                                       id="address_name" name="address_name[]" placeholder="Name"
                                                       value="{{ old('address_name.1') }}">
                                                @error('address_name.1')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="address_phone">Phone</label>
                                                <input type="text"
                                                       class="form-control   @error('address_phone.1') is-invalid @enderror"
                                                       id="address_phone" name="address_phone[]" placeholder="Phone"
                                                       value="{{ old('address_phone.1') }}">
                                                @error('address_phone.1')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <textarea type="text"
                                                          class="form-control   @error('address.1') is-invalid @enderror"
                                                          id="address" name="address[]"
                                                          placeholder="Address">{{ old('address.1') }}</textarea>
                                                @error('address.1')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="address_type">Registered Address </label>
                                                <input type="hidden" class="form-control " id="address_type"
                                                       name="address_type[]" value="Registered Address">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="address_name"> Name </label>
                                                <input type="text"
                                                       class="form-control   @error('address_name.2') is-invalid @enderror"
                                                       id="address_name" name="address_name[]" placeholder="Name"
                                                       value="{{ old('address_name.2') }}">
                                                @error('address_name.2')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="address_phone">Phone</label>
                                                <input type="text"
                                                       class="form-control   @error('address_phone.2') is-invalid @enderror"
                                                       id="address_phone" name="address_phone[]" placeholder="Phone"
                                                       value="{{ old('address_phone.2') }}">
                                                @error('address_phone.2')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <textarea type="text"
                                                          class="form-control   @error('address.2') is-invalid @enderror"
                                                          id="address" name="address[]"
                                                          placeholder="Address">{{ old('address.2') }}</textarea>
                                                @error('address.2')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="Person-in-Charge" role="tabpanel">
                                    <div class="row">
                                        <h5 for="one-ecom-product-meta-keywords">PERSON-IN-CHARGE</h5>
                                    </div>
                                    <div class="row">
                                        @php
                                        if (!empty(old('incharge_name'))) {
                                        $count = count(old('incharge_name'));
                                        } else {
                                        $count = 1;
                                        }
                                        @endphp
                                        @for ($i = 0; $i < $count; $i++)
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label for="incharge_department">Department:</label>
                                                <select
                                                    class="select2 form-control @error('incharge_department_id.' . $i) is-invalid @enderror"
                                                    id="department_id"
                                                    name="incharge_department_id[{{ $i }}]"
                                                    style="width: 100%;" data-placeholder="Choose">
                                                    <option value="">{{ __('Please Select') }}</option>
                                                    @foreach ($departments as $department)
                                                    @php
                                                    $selected = '';
                                                    if (old('incharge_department_id')) {
                                                    if (old('incharge_department_id')[$i] == $department->id) {
                                                    $selected = 'selected';
                                                    }
                                                    }
                                                    @endphp
                                                    <option value="{{ $department->id }}"
                                                            {{ $selected }}>{{ $department->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('incharge_department_id.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-8">
                                                <label for="incharge_name">Name:</label>
                                                <input type="text"
                                                       class="form-control   @error('incharge_name.' . $i) is-invalid @enderror"
                                                       id="incharge_name" name="incharge_name[{{ $i }}]"
                                                       value="{{ old('incharge_name')[$i] ?? '' }}"
                                                       placeholder="Name">
                                                @error('incharge_name.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <label for="incharge_phone">Phone:</label>
                                                <input type="text"
                                                       class="form-control   @error('incharge_phone.' . $i) is-invalid @enderror"
                                                       id="incharge_phone" name="incharge_phone[{{ $i }}]"
                                                       value="{{ old('incharge_phone')[$i] ?? '' }}"
                                                       placeholder="Phone">
                                                @error('incharge_phone.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="email">Email:</label>
                                                <input type="email"
                                                       class="form-control   @error('incharge_email.' . $i) is-invalid @enderror"
                                                       id="incharge_email" name="incharge_email[{{ $i }}]"
                                                       value="{{ old('incharge_email')[$i] ?? '' }}"
                                                       placeholder="Email">
                                                @error('incharge_email.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <button type="button" class="delete_incharge btn "
                                                    {{ $i == 0 ? 'disabled' : '' }}><i class="fa fa-trash"
                                                                               aria-hidden="true"></i></button>
                                        </div>
                                        @endfor
                                    </div>
                                    <input type="hidden" id="counter4" value="{{ $i - 1 }}">
                                    <div class="container4 py-2">
                                    </div>
                                    <div class="col-md-12" id="addnew_feature">
                                        <a href="#" id="companyGroup-more"
                                           class="add_companyINCharge_form_field  btn btn-icon btn-secondary btn-sm mr-2 ">Add
                                            More</a>
                                    </div>
                                </div>
                                <div class="tab-pane" id="Responsible-Accountants" role="tabpanel">
                                    <div class="row">
                                        <h5 for="one-ecom-product-meta-keywords">APPOINTED PROFESSIONAL FIRM</h5>
                                    </div>
                                    <div class="row">
                                        @foreach ($departments as $key => $department)
                                        <div class="col-lg-12">
                                            <div class="row ml-2 mt-3">
                                                <label for="responsible_name">{{ $department->name }} </label>
                                                <input type="hidden" class="responsible_id" name="department_id[]"
                                                       value="{{ $department->id }}">
                                            </div>
                                            @php
                                            if (!empty(old('responsible_name')[$department->id])) {
                                            $count = count(old('responsible_name')[$department->id]);
                                            } else {
                                            $count = 1;
                                            }
                                            @endphp
                                            @for ($i = 0; $i < $count; $i++)
                                            <div class="row ml-2">
                                                <input type="text"
                                                       class="form-control  my-2 col-md-10 @error('responsible_name.' . $department->id . '.' . $i) is-invalid @enderror"
                                                       id="responsible_name"
                                                       name="responsible_name[{{ $department->id }}][]"
                                                       value="{{ old('responsible_name')[$department->id][$i] ?? '' }}"
                                                       placeholder="Name">
                                                @error('responsible_name.' . $department->id . '.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <button type="button"
                                                        class="delete_Responsible btn mx-2 my-2  px-2 py-2"
                                                        {{ $i == 0 ? 'disabled' : '' }}><i
                                                        class="fa fa-trash" aria-hidden="true"></i></button>
                                            </div>
                                            @endfor
                                            <div class="container_{{ $department->id }} py-2">
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="addnew_feature">
                                            <a href="#" id="add_responsibleAccount_form_{{ $department->id }}"
                                               class="btn btn-icon btn-secondary btn-sm mr-2 add_responsibleAccount_form_{{ $department->id }}">Add
                                                More</a>
                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function() {
                                            var max_fields = 15;
                                            var wrapper = $(".container_{{ $department->id }}");
                                            var add_button = $(".add_responsibleAccount_form_{{ $department->id }}");
                                            var x = 1;
                                            $(add_button).click(function(e) {
                                            e.preventDefault();
                                            var count3 = parseInt($("#counter3").val()) + 1;
                                            if (x < max_fields) {
                                            x++;
                                            $(wrapper).append(
                                                    '<div class="" id="new_plan"><div class="row ml-2"><input type="text" class="form-control  my-2 col-md-10 " name="responsible_name[{{ $department->id }}][]" placeholder="Name" value="{{ old('responsible_name.')[$department->id][' + count3 + '] ?? '' }}"><a href="#" class="delete_Responsible btn mx-2 my-2  px-2 py-2"><i class="fa fa-trash" aria-hidden="true"></i></a></div></div>'
                                                    ); //add input box

                                            $("#counter3").val(count3);
                                            } else {
                                            alert('You Reached the limits')
                                            }
                                            });
                                            $(document).on("click", ".delete_Responsible", function(e) {
                                            e.preventDefault();
                                            $(this).parent('div').remove();
                                            x--;
                                            var count3 = parseInt($("#counter3").val()) - 1;
                                            $("#counter3").val(count3);
                                            });
                                            });
                                        </script>
                                        @endforeach
                                        <input type="hidden" id="counter3" value="0">
                                    </div>
                                </div>
                                <div class="tab-pane" id="Shares" role="tabpanel">
                                    <div class="row">
                                        <h5 for="one-ecom-product-meta-keywords">DIRECTORS AND SHAREHOLDERS</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="share_capital">Share Capital<span
                                                        class="text-danger">*</span> </label>
                                                <input type="text"
                                                       class="form-control   @error('share_capital') is-invalid @enderror"
                                                       id="share_capital" name="share_capital"
                                                       value="{{ old('share_capital') }}" placeholder="Share Capital">
                                                @error('share_capital')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8">

                                            <label for="shareholder_name">Shareholder </label><br>
                                            @php
                                            if (!empty(old('shareholder_name'))) {
                                            $count = count(old('shareholder_name'));
                                            } else {
                                            $count = 1;
                                            }
                                            @endphp
                                            @for ($i = 0; $i < $count; $i++)
                                            <div class="row py-2">
                                                <div class="col-lg-7">
                                                    <input type="text"
                                                           class="form-control   @error('shareholder_name.' . $i) is-invalid @enderror"
                                                           name="shareholder_name[{{ $i }}]"
                                                           placeholder="Share Holder"
                                                           value="{{ old('shareholder_name')[$i] ?? '' }}">

                                                    @error('shareholder_name.' . $i)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="col-lg-3">
                                                    <input
                                                        class="form-control  @error('values.' . $i) is-invalid @enderror"
                                                        type="text" id="fee-2" class="fee"
                                                        placeholder="%" name="values[{{ $i }}]"
                                                        value="{{ old('values')[$i] ?? '' }}">
                                                    @error('values.' . $i)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <button type="button" class="delete_shareholder btn text-center"
                                                        {{ $i == 0 ? 'disabled' : '' }}><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i></button>
                                            </div>
                                            @endfor
                                            <div class="container2 py-2">
                                            </div>
                                            <input type="hidden" id="counter1" value="0">
                                            <div class="col-md-12" id="addnew_feature">
                                                <a href="#" id="companyGroup-more"
                                                   class="btn btn-icon btn-secondary btn-sm mr-2 add_shareholder_form_field">Add
                                                    More</a>
                                            </div>
                                            <br>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="shareholder_updated_on">Updated on</label>
                                                    <input type="date"
                                                           class="form-control   @error('shareholder_updated_on') is-invalid @enderror"
                                                           value="{{ old('shareholder_updated_on') }}"
                                                           name="shareholder_updated_on">
                                                    @error('shareholder_updated_on')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="director">Director </label>
                                            @php
                                            if (!empty(old('director'))) {
                                            $count = count(old('director'));
                                            } else {
                                            $count = 1;
                                            }
                                            @endphp
                                            @for ($i = 0; $i < $count; $i++)
                                            <div class="row py-2">
                                                <input type="text"
                                                       class="form-control  col-md-8 ml-3 @error('director.' . $i) is-invalid @enderror"
                                                       id="director" name="director[{{ $i }}]"
                                                       placeholder="Director Name"
                                                       value="{{ old('director')[$i] ?? '' }}">
                                                @error('director.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror

                                                <button type="button" class="delete_director btn py-2 px-2 mx-2"
                                                        {{ $i == 0 ? 'disabled' : '' }}><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i></button>

                                            </div>
                                            @endfor

                                            <div class="container3 py-2">
                                            </div>
                                            <input type="hidden" id="counter2" value="0">
                                            <div class="col-lg-12" id="addnew_feature">
                                                <a href="#" id="companyGroup-more"
                                                   class="btn btn-icon btn-secondary btn-sm mr-2 add_director_form_field">Add
                                                    More</a>
                                            </div>
                                            <br>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="director_updated_on">Updated on</label>
                                                    <input type="date"
                                                           class="form-control   @error('director_updated_on') is-invalid @enderror"
                                                           value="{{ old('director_updated_on') }}"
                                                           name="director_updated_on">
                                                    @error('director_updated_on')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="Group-of-Companies" role="tabpanel">
                                    <div class="row">
                                        <h5 for="one-ecom-product-meta-keywords">GROUP OF COMPANIES</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            @php
                                            if (!empty(old('groupof_companies'))) {
                                            $count = count(old('groupof_companies'));
                                            } else {
                                            $count = 1;
                                            }
                                            @endphp
                                            @for ($i = 0; $i < $count; $i++)
                                            <div class="row ml-2 py-2">
                                                <input type="text"
                                                       class="form-control  col-md-10 @error('groupof_companies.' . $i) is-invalid @enderror"
                                                       name="groupof_companies[{{ $i }}]"
                                                       placeholder="Group Of Companies"
                                                       value="{{ old('groupof_companies')[$i] ?? '' }}">
                                                @error('groupof_companies.' . $i)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <button type="button" class="delete_group btn "
                                                        {{ $i == 0 ? 'disabled' : '' }}><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i></button>
                                            </div>
                                            @endfor

                                        </div>
                                        <div class="col-lg-12 container1">
                                        </div>
                                        <input type="hidden" id="counter" value="0">
                                        <div class="col-md-12" id="addnew_feature">
                                            <a href="#" id="companyGroup-more"
                                               class="btn btn-icon btn-secondary btn-sm mr-2 add_form_field">Add
                                                More</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="Financial-Information" role="tabpanel">
                                    <div class="block-header">
                                        <h5 for="one-ecom-product-meta-keywords">FINANCIAL INFORMATION</h5>
                                    </div>
                                    <div class="block-content block-content-full">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="financial_year_end">Financial Year End <span
                                                            class="text-danger">*</span></label>
                                                    <input type="date"
                                                           class="form-control   @error('financial_year_end') is-invalid @enderror"
                                                           id="financial_year_end" name="financial_year_end"
                                                           value="{{ old('financial_year_end') }}"
                                                           placeholder="Financial Year End">
                                                    @error('financial_year_end')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="annual_return_date">Annual Return Date </label>
                                                    <input type="text"
                                                           class="date form-control   @error('annual_return_date') is-invalid @enderror"
                                                           id="annual_return_date" name="annual_return_date"
                                                           value="{{ old('annual_return_date') }}"
                                                           placeholder="31 March"
                                                           min="{{ Carbon\Carbon::now()->firstOfYear()->format('Y-m') }}"
                                                           max="{{ Carbon\Carbon::now()->lastOfYear()->format('Y-m') }}">
                                                    @error('annual_return_date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="turnover">Turnover </label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span
                                                                class="input-group-text">RM</span></div>
                                                        <input type="text"
                                                               class="form-control   @error('turnover') is-invalid @enderror"
                                                               id="turnover" name="turnover" placeholder="Turnover"
                                                               value="{{ old('turnover') }}">
                                                        @error('turnover')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="profit_before_tax">Profit before Tax </label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span
                                                                class="input-group-text">RM</span></div>
                                                        <input type="text"
                                                               class="form-control   @error('profit_before_tax') is-invalid @enderror"
                                                               id="profit_before_tax" name="profit_before_tax"
                                                               placeholder="Profit before Tax"
                                                               value="{{ old('profit_before_tax') }}">
                                                        @error('profit_before_tax')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="annual_meeting_date">Annual General Meeting Date (if
                                                        applicable) </label>
                                                    <input type="date"
                                                           class="form-control   @error('annual_meeting_date') is-invalid @enderror"
                                                           id="annual_meeting_date" name="annual_meeting_date"
                                                           value="{{ old('annual_meeting_date') }}">
                                                    @error('annual_meeting_date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="activity_status ">Status of the company </label>
                                                    <select
                                                        class="form-control   @error('activity_status') is-invalid @enderror"
                                                        name="activity_status">
                                                        <option selected value="">Select</option>
                                                        <option value="1" @if (old('activity_status') == '1') selected @endif>Super Active
                                                        </option>
                                                        <option value="2" @if (old('activity_status') == '2') selected @endif>Active</option>
                                                        <option value="3" @if (old('activity_status') == '3') selected @endif>Semi Active</option>
                                                        <option value="4" @if (old('activity_status') == '4') selected @endif>Dormant</option>
                                                        <option value="5" @if (old('activity_status') == '5') selected @endif>Winding Up</option>
                                                        <option value="6" @if (old('activity_status') == '6') selected @endif>Striking Off
                                                        </option>
                                                    </select>
                                                    @error('activity_status')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="status_date">Date </label>
                                                    <input type="date"
                                                           class="form-control   @error('status_date') is-invalid @enderror"
                                                           value="{{ old('status_date') }}" name="status_date">
                                                    @error('status_date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="block-header">
                                        <h5 for="one-ecom-product-meta-keywords">MISC</h5>
                                    </div>
                                    <div class="block-content block-content-full">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="ep_certificate">Exempt Private Company
                                                        Certificate(EPC)<span class="text-danger">*</span></label>
                                                    <select
                                                        class="form-control   @error('ep_certificate') is-invalid @enderror"
                                                        name="ep_certificate">
                                                        <option selected value="">Select</option>
                                                        <option value="No" @if (old('ep_certificate') == 'No') selected @endif>No </option>
                                                        <option value="Yes" @if (old('ep_certificate') == 'Yes') selected @endif>Yes</option>
                                                    </select>
                                                    @error('ep_certificate')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="applied_date">Applied Date</label>
                                                    <input type="date"
                                                           class="form-control   @error('applied_date') is-invalid @enderror"
                                                           value="{{ old('applied_date') }}" name="applied_date">
                                                    @error('applied_date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group NDD">
                                                    <label for="new_due_date">New Due Date </label>
                                                    <input type="date"
                                                           class="form-control   @error('new_due_date') is-invalid @enderror"
                                                           value="{{ old('new_due_date') }}" name="new_due_date">
                                                    @error('new_due_date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror

                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="extension_time">Extension of Time (EOT)</label>
                                                    <select
                                                        class=" EOT form-control   @error('extension_time') is-invalid @enderror"
                                                        name="extension_time" placeholder="Extension of Time (EOT)">
                                                        <option selected value="">Select</option>
                                                        <option value="No" @if (old('extension_time') == 'No') selected @endif>No </option>
                                                        <option value="Yes" @if (old('extension_time') == 'Yes') selected @endif>Yes</option>
                                                    </select>
                                                    @error('extension_time')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                            </div>
                                            <!--                                                <div class="col-lg-6">
                                                                                                                                                                                    <div class="form-group">
                                                                                                                                                                                        <label for="company_etr">eTR (CTOS) <span
                                                                                                                                                                                                class="text-danger">*</span></label>
                                                                                                                                                                                        <input type="text"
                                                                                                                                                                                            class="form-control   @error('company_etr') is-invalid @enderror"
                                                                                                                                                                                            value="{{ old('company_etr') }}" name="company_etr"
                                                                                                                                                                                            placeholder="eTR (CTOS) ">
                                                                                                                                                                                        @error('company_etr')
                                                                                                                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                                                                                                                        <strong>{{ $message }}</strong>
                                                                                                                                                                                                        </span>
                                                                                                                                                                                        @enderror
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                                                                                                                <div class="col-lg-6">
                                                                                                                                                                                    <div class="form-group">
                                                                                                                                                                                        <label for="etr_date">Date <span
                                                                                                                                                                                                class="text-danger">*</span></label>
                                                                                                                                                                                        <input type="date"
                                                                                                                                                                                            class="form-control   @error('etr_date') is-invalid @enderror"
                                                                                                                                                                                            value="{{ old('etr_date') }}" name="etr_date">
                                                                                                                                                                                        @error('etr_date')
                                                                                                                                                                                                        <span class="invalid-feedback" role="alert">
                                                                                                                                                                                                        <strong>{{ $message }}</strong>
                                                                                                                                                                                                        </span>
                                                                                                                                                                                        @enderror
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>-->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- comment -->
                    </div>
                    <div class="text-right">
                        <a class="btn btn-secondary btnNext">Next <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>

                <div class="tab-pane" id="btabs-static-document" role="tabpanel">
                    @php $j = 0; @endphp
                    @foreach ($documentType as $key => $item)
                    <div class="block-content block-content-full">
                        <div class="row">
                            <input type="hidden" name="document_type[{{ $key }}][type]"
                                   value="{{ $item }}">
                            <table id="mytable-{{ $key }}" style="width: 100%;">
                                <thead>
                                <th style="width: 30%;">{{ $item }}</th>
                                @if ($item != 'Annual Return')
                                <th class="text-center">Received Date</th>
                                @else
                                <th></th>
                                @endif
                                <th>Submitted</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                    @php
                                    if (!empty(old('document_type')[$key]['report_year'])) {
                                    $count = count(old('document_type')[$key]['report_year']);
                                    } else {
                                    $count = 1;
                                    }
                                    @endphp

                                    @for ($i = 0; $i < $count; $i++)
                                    <tr class='document'>
                                        <td style="width: 25%; padding: 10px;"><input type="text"
                                                                                      class="form-control   @error('document_type.' . $key . '.report_year.' . $i) is-invalid @enderror"
                                                                                      name="document_type[{{ $key }}][report_year][]"
                                                                                      value="{{ old('document_type')[$key]['report_year'][$i] ?? '' }}"
                                                                                      placeholder="Year">
                                            @error('document_type.' . $key . '.report_year.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </td>
                                        @if ($item != 'Annual Return')
                                        <td style="width: 25%; padding: 10px;"><input type="date"
                                                                                      class="form-control  @error('document_type.' . $key . '.recevie_date.' . $i) is-invalid @enderror"
                                                                                      name="document_type[{{ $key }}][recevie_date][]"
                                                                                      value="{{ old('document_type')[$key]['recevie_date'][$i] ?? '' }}">
                                            @error('document_type.' . $key . '.recevie_date.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </td>
                                        @else
                                        <td style="width: 25%; padding: 10px;">
                                            <input type="hidden"
                                                   name="document_type[{{ $key }}][recevie_date][]"
                                                   value="">
                                        </td>
                                        @endif
                                        <td style="width: 25%; padding: 10px;"><input type="date"
                                                                                      class="form-control  @error('document_type.' . $key . '.submitted.' . $i) is-invalid @enderror"
                                                                                      name="document_type[{{ $key }}][submitted][]"
                                                                                      value="{{ old('document_type')[$key]['submitted'][$i] ?? '' }}">
                                            @error('document_type.' . $key . '.submitted.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </td>
                                        <td><button type="button" class="remove btn "
                                                    {{ $i == 0 ? 'disabled' : '' }}><i class="fa fa-trash"
                                                                               aria-hidden="true"></i></button></td>
                                    </tr>
                                    @endfor
                                </tbody>
                            </table>

                            <div class="col-md-12" id="addnew_feature">
                                <a href="#" id="{{ $key }}"
                                   class="insert btn btn-icon btn-secondary btn-sm mr-2 report">Add More</a>
                            </div>
                        </div>
                        @php $j++; @endphp
                    </div>
                    @endforeach

                    <div class="col-lg-12 m-3">
                        <a class="btn btn-light btnPrevious"><i class="fa fa-angle-left"></i> Previous</a>
                        <a class="btn btn-secondary btnNext">Next<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                <!-- END Alternative Style -->
                <div class="tab-pane" id="btabs-static-portfolio" role="tabpanel">
                    <div class="block-header">
                        <h5 class="font-w600">PORTFOLIO HOLDER</h5>
                    </div>
                    @foreach ($departments as $key => $department)
                    <div class="block-content block-content-full">
                        <label for="one-ecom-product-meta-keywords">{{ $department->name }}</label>
                        @php
                        $users = \App\Models\Users\User::with('departments')
                        ->where('status', 1)
                        ->whereHas('departments', function ($query) use ($department) {
                        $query->where('id', $department->id);
                        })
                        ->get();
                        @endphp
                        <div class="row mb-2">
                            <input type="hidden" name="company_portfolio[{{ $key }}][department_id]"
                                   value="{{ $department->id }}">
                            <table id='port-{{ $key }}' style="width: 100%;">
                                <tbody>
                                    @php
                                    if (!empty(old('company_portfolio')[$key]['user_id'])) {
                                    $count = count(old('company_portfolio')[$key]['user_id']);
                                    } else {
                                    $count = 1;
                                    }
                                    @endphp
                                    @for ($i = 0; $i < $count; $i++)
                                    <tr class='portfolio-'>
                                        <td style="width: 25%; padding: 10px;">
                                            <select
                                                class="form-control  @error('company_portfolio.' . $key . '.user_id.' . $i) is-invalid @enderror"
                                                name="company_portfolio[{{ $key }}][user_id][]">
                                                <option selected value="">Select</option>
                                                @foreach ($users as $user)
                                                @php
                                                $selected = '';
                                                $user_id = !empty(old('company_portfolio')[$key]['user_id']) ? old('company_portfolio')[$key]['user_id'][$i] : '';
                                                if ($user_id == $user->id) {
                                                $selected = 'selected';
                                                }
                                                @endphp
                                                <option value="{{ $user->id }}" {{ $selected }}>
                                                    {{ $user->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('company_portfolio.' . $key . '.user_id.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </td>
                                        <td style="width: 25%; padding: 10px;">
                                            <input type="date"
                                                   class="form-control  @error('company_portfolio.' . $key . '.date.' . $i) is-invalid @enderror"
                                                   name="company_portfolio[{{ $key }}][date][]"
                                                   value="{{ old('company_portfolio')[$key]['date'][$i] ?? '' }}">
                                            @error('company_portfolio.' . $key . '.date.' . $i)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </td>
                                        <td style="width: 20%; padding: 10px;">
                                            <button type="button" class="danger btn btn-sm btn-alt-danger"
                                                    data-toggle="tooltip" title="Delete"
                                                    {{ $i == 0 ? 'disabled' : '' }}>
                                                <i class="fa fa-fw fa-times text-danger"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @endfor
                                </tbody>
                            </table>
                            <div class="col-md-12" id="addnew_feature">
                                <a href="#" id="{{ $key }}"
                                   class="portfolio btn btn-icon btn-secondary btn-sm mr-2">Add More</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-lg-12 m-3">
                        <a class="btn btn-light btnPrevious"><i class="fa fa-angle-left"></i> Previous</a>
                        <a class="btn btn-secondary btnNext">Next<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                <div class="tab-pane" id="btabs-static-reminder" role="tabpanel">
                    <div class="block-header">
                        <h5 class="font-w600">REMINDER</h5>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="row add pt-3">
                            <div class="col-md-3 col-xl-3 ">
                                <input type="checkbox" class="check" id="check" onclick="paym(this)"
                                       @if (isset(old('company_reminder')['secretarialfees']) && old('company_reminder')['secretarialfees'] != null) checked @endif>
                                <label class="ml-2"> Secretarial fees</label>
                            </div>
                            <div class="col-md-6 col-xl-6 offset-md-2 offset-xl-2 form-group"
                                 style="margin-bottom: 0.5rem;">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="example-radios-inline1"
                                           name="company_reminder[secretarialfees]" value="monthly"
                                           @if (isset(old('company_reminder')['secretarialfees']) && old('company_reminder')['secretarialfees'] == 'monthly')checked @endif @if (!isset(old('company_reminder')['secretarialfees'])) disabled @endif>
                                    <label class="form-check-label" for="example-radios-inline1">Monthly</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="example-radios-inline2"
                                           name="company_reminder[secretarialfees]" value="quarterly"
                                           @if (isset(old('company_reminder')['secretarialfees']) && old('company_reminder')['secretarialfees'] == 'quarterly')checked @endif @if (!isset(old('company_reminder')['secretarialfees'])) disabled @endif>
                                    <label class="form-check-label" for="example-radios-inline2">Quarterly</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="example-radios-inline3"
                                           name="company_reminder[secretarialfees]" value="halfYearly"
                                           @if (isset(old('company_reminder')['secretarialfees']) && old('company_reminder')['secretarialfees'] == 'halfYearly')checked @endif @if (!isset(old('company_reminder')['secretarialfees'])) disabled @endif>
                                    <label class="form-check-label" for="example-radios-inline3"> Half-Yearly</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="example-radios-inline4"
                                           name="company_reminder[secretarialfees]" value="yearly"
                                           @if (isset(old('company_reminder')['secretarialfees']) && old('company_reminder')['secretarialfees'] == 'yearly')checked @endif @if (!isset(old('company_reminder')['secretarialfees'])) disabled @endif>
                                    <label class="form-check-label" for="example-radios-inline4">Annually</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="row add pt-3">
                            <div class="col-md-9 col-xl-9 text-start">
                                <input type="checkbox" name="company_reminder[annualReturn]" id="annualReturn" value="1"
                                       @if (isset(old('company_reminder')['annualReturn']) && old('company_reminder')['annualReturn'] == 1)checked @endif>
                                <label class="ml-2"> Create Recurrence job for Annual Return. </label>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="row add pt-3">
                            <div class="col-md-9 col-xl-9  text-start">
                                <input type="checkbox" name="company_reminder[taxAgent]" value="1"
                                       @if (isset(old('company_reminder')['taxAgent']) && old('company_reminder')['taxAgent'] == 1)checked @endif>
                                <label class="ml-2"> Reminder on the next 15th January for Tax Form E.
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="row add pt-3">
                            <div class="col-md-9 col-xl-9  text-start">
                                <input type="checkbox" name="company_reminder[auditorAppoinment]" value="1"
                                       @if (isset(old('company_reminder')['auditorAppoinment']) && old('company_reminder')['auditorAppoinment'] == 1)checked @endif>
                                <label class="ml-2"> Reminder After 12 months for Appointment of
                                    Auditors</label>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="row add pt-3">
                            <div class="col-md-9 col-xl-9 text-start">
                                <input type="checkbox" name="company_reminder[audited_statement]" id="audited_statement"
                                       value="1" @if (isset(old('company_reminder')['audited_statement']) && old('company_reminder')['audited_statement'] == 1)checked @endif>
                                <label class="ml-2"> Reminder After 3 months Company's financial year end for
                                    AFS submission.</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 m-3">
                        <a class="btn btn-light btnPrevious"><i class="fa fa-angle-left"></i> Previous</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
    </div>
</form>
</div>
</div>

<!-- END Page Content -->
@endsection
@push('scripts')
<script>
    // group of company
    $(document).ready(function() {
    var max_fields = 15;
    var wrapper = $(".container1");
    var add_button = $(".add_form_field");
    var x = 1;
    $(add_button).click(function(e) {
    e.preventDefault();
    var count = parseInt($("#counter").val()) + 1;
    if (x < max_fields) {
    x++;
    $(wrapper).append(
            '<div class="" id="new_plan"><div class="row ml-2 py-2"><input type="text" class="form-control  col-md-10 " id="groupof_companies"  name="groupof_companies[' +
            count +
            ']" value="{{ old('groupof_companies.')[' + count + '] ?? '' }}" placeholder="Group Of Companies"/><a href="#" class="delete_group btn "><i class="fa fa-trash" aria-hidden="true"></i></a></div></div>'
            ); //add input box
    $("#counter").val(count);
    } else {
    alert('You Reached the limits')
    }
    });
    $(document).on("click", ".delete_group", function(e) {
    e.preventDefault();
    $(this).parent('div').remove();
    x--;
    var count = parseInt($("#counter").val()) - 1;
    $("#counter").val(count);
    })
    });
    // shareholder
    $(document).ready(function() {
    var max_fields = 15;
    var wrapper = $(".container2");
    var add_button = $(".add_shareholder_form_field");
    var x = 1;
    $(add_button).click(function(e) {
    e.preventDefault();
    var count1 = parseInt($("#counter1").val()) + 1;
    if (x < max_fields) {
    x++;
    $(wrapper).append(
            '<div class="row"><div class="col-lg-7 py-2"><input type="text" class="form-control "  name="shareholder_name[' +
            count1 +
            ']" placeholder="Share Holder" value="{{ old('shareholder_name.')[' + count1 + '] ?? '' }}"></div><div class="col-lg-3 py-2"><input class="form-control "  type="text" id="fee-2" placeholder="%" class="fee" name="values[' +
            count1 +
            ']" value="{{ old('values.')[' + count1 + '] ?? '' }}"></div><a href="#" class="delete_shareholder btn "><i class="fa fa-trash" aria-hidden="true"></i></a></div>'
            ); //add input box
    $("#counter1").val(count1);
    } else {
    alert('You Reached the limits')
    }
    });
    $(document).on("click", ".delete_shareholder", function(e) {
    e.preventDefault();
    $(this).parent('div').remove();
    x--;
    var count1 = parseInt($("#counter1").val()) - 1;
    $("#counter1").val(count1);
    });
    });
    // director
    $(document).ready(function() {
    var max_fields = 15;
    var wrapper = $(".container3");
    var add_button = $(".add_director_form_field");
    var x = 1;
    $(add_button).click(function(e) {
    e.preventDefault();
    var count2 = parseInt($("#counter2").val()) + 1;
    if (x < max_fields) {
    x++;
    $(wrapper).append(
            '<div class="row py-2"><input type="text" class="form-control  col-md-8 ml-3 " id="director" name="director[' +
            count2 +
            ']"  placeholder="Director Name" value="{{ old('director.')[' + count2 + '] ?? '' }}"/><a href="#" class="delete_director btn py-2 px-2 mx-2"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'
            ); //add input box

    $("#counter2").val(count2);
    } else {
    alert('You Reached the limits')
    }
    });
    $(document).on("click", ".delete_director", function(e) {
    e.preventDefault();
    $(this).parent('div').remove();
    x--;
    var count2 = parseInt($("#counter2").val()) - 1;
    $("#counter2").val(count2);
    })
    });
    // person incharge
    $(document).ready(function() {

    var max_fields = 15;
    var wrapper = $(".container4");
    var add_button = $(".add_companyINCharge_form_field");
    var i = 0;
    var x = 1;
    $(add_button).click(function(e) {

    e.preventDefault();
    var count4 = parseInt($("#counter4").val()) + 1;
    if (x < max_fields) {

    x++;
    $(wrapper).append(
            '<div id="new_plan"><div class="row"><div class="col-lg-4"><div class="form-group"><label for="department">Departments</label><select  class="select2 form-control @error('department_id') is-invalid @enderror" id="department_id" name="incharge_department_id[' +
            count4 +
            ']" style="width: 100%;" data-placeholder="Choose" ><option value="">{{ __('Please Select') }}</option>@foreach ($departments as $department)<option value="{{ $department->id }}" @if ($department->id == old('department_id')) selected @endif>{{ $department->name }}</option>@endforeach</select></div></div><div class="col-lg-8 py-2"><label for="incharge_name">Name:</label><input type="text" class="form-control "  name="incharge_name[' +
            count4 +
            ']" placeholder="Name" value="{{ old('incharge_name.')[' + count4 + '] ?? '' }}"></div></div><div class="row"><div class="col-lg-4 py-2"></div><div class="col-lg-4 py-2"><label for="incharge_phone">Phone:</label><input type="text" class="form-control "  name="incharge_phone[' +
            count4 +
            ']" placeholder="Phone" value="{{ old('incharge_phone.')[' + count4 + '] ?? '' }}"></div><div class="col-lg-4 py-2"><label for="incharge_email">Email:</label><input type="email"  class="form-control " name="incharge_email[' +
            count4 +
            ']" placeholder="Email" value="{{ old('incharge_email.')[' + count4 + '] ?? '' }}"></div></div><a href="#" class="delete_incharge btn "><i class="fa fa-trash" aria-hidden="true"></i></a></div>'
            ); //add input box
    $('.select2').select2({
    allowClear: true
    });
    $("#counter4").val(count4);
    //
    } else {
    alert('You Reached the limits')
    }
    });
    $(document).on("click", ".delete_incharge", function(e) {
    e.preventDefault();
    $(this).parent('div').remove();
    x--;
    var count4 = parseInt($("#counter4").val()) - 1;
    $("#counter4").val(count4);
    });
    });
    // next and previous
    $(document).ready(function() {

    $('.btnNext').click(function() {
    $('.nav-tabs > .nav-item > .active').parent().next('li').find('a').trigger('click');
    });
    $('.btnPrevious').click(function() {
    $('.nav-tabs > .nav-item > .active').parent().prev('li').find('a').trigger('click');
    });
    });
    $(document).ready(function() {
    $('#myTab a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
    });
    // store the currently selected tab in the hash value
    var scrollmem = $('html,body').scrollTop();
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
    e.preventDefault();
    var id = $(e.target).attr("href").substr(1);
    window.location.hash = id;
    $('html,body').scrollTop(scrollmem);
    // window.scrollTo(0, 0);
    });
    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $('#myTab a[href="' + hash + '"]').tab('show');
    });
    // portfolio holder
    $(document).ready(function() {

    $(".portfolio").click(function() {

    $("#port-" + this.id).each(function() {
    var tds = '<tr>';
    jQuery.each($('tr:last td', this), function() {
    tds += '<td style="width: 25%; padding: 10px;">' + $(this).html() +
            '</td>';
    });
    tds += '</tr>';
    if ($('tbody', this).length > 0) {
    $('tbody', this).append(tds);
    $('button', this).removeAttr('disabled');
    $('button', '.portfolio-').attr('disabled', 'disabled');
    } else {
    $(this).append(tds);
    }
    });
    });
    // remove row
    $(document).on('click', '.danger', function() {

    $(this).closest('tr').remove();
    });
    });
    // documents 
    $(document).ready(function() {

    $(".insert").click(function() {
    var d = this.id;
    $("#mytable-" + this.id).each(function() {
    var tds = '<tr>';
    jQuery.each($('tr:last td', this), function() {
    tds += '<td class="d-' + d +
            '" style="width: 25%; padding: 10px;">' + $(this).html() +
            '</td>';
    });
    tds += '</tr>';
    if ($('tbody', this).length > 0) {
    $('tbody', this).append(tds);
    $('input', '.d-' + d + '').val("");
    $('input', '.d-' + d + '').find("span.invalid-feedback").remove();
    $('.is-invalid', '.d-' + d + '').removeClass('is-invalid');
    $('button', this).removeAttr('disabled');
    $('button', '.document').attr('disabled', 'disabled');
    } else {
    $(this).append(tds);
    }
    });
    });
    // remove row
    $(document).on('click', '.remove', function() {

    $(this).closest('tr').remove();
    });
    });
    $(".date").datepicker({
    fontAwesome: "font-awesome",
            format: "dd-MM",
            minView: "month",
            startDate: "01-01",
            endDate: "31-12",
            autoclose: true,
            changeYear: false
    });

    function paym(check) {
    var remender = document.getElementById('example-radios-inline1');
    var remender2 = document.getElementById('example-radios-inline2');
    var remender3 = document.getElementById('example-radios-inline3');
    var remender4 = document.getElementById('example-radios-inline4');
    remender.disabled = check.checked ? false : true;
    remender2.disabled = check.checked ? false : true;
    remender3.disabled = check.checked ? false : true;
    remender4.disabled = check.checked ? false : true;
    if (!remender.disabled && !remender2.disabled && !remender3.disabled && !remender4.disabled) {
    remender.focus();
    remender2.focus();
    remender3.focus();
    remender4.focus();
    }
    //    if (remender.disabled && remender2.disabled && remender3.disabled && remender4.disabled){
    //    remender.value = "";
    //    remender2.value = "";
    //    remender3.value = "";
    //    remender4.value = "";
    //    }
    }
    $(document).ready(function() {
    $('.NDD').addClass('d-none');
    $('.EOT').on('click', function(e) {
    e.preventDefault();
    if ($(this).val() == 'Yes') {
    $('.NDD').removeClass('d-none');
    } else {
    $('.NDD').addClass('d-none');
    }
    });
    });
</script>
@endpush
