@extends('layouts.app', [
'activePage' => 'customers-list',
'menuParent' => 'customers-list',
'title' => 'Customers',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <div class="col-sm-8 d-flex justify-content-start align-items-center">
                <h1 class="flex-sm-00-auto h3 my-2">
                    Customers <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Customers</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="text-black text-muted">List</a>
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="col-sm-4">
                @can('company_create')
                <div class="text-right">
                    <a href="{{ route('companies.create') }}" class="btn btn-primary">Create</a>
                </div>
                @endcan
            </div>
        </div>
    </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full  font-size-sm" id="example">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px;">#</th>
                        <th>Customer Name</th>
                        <th class="d-none d-sm-table-cell">Email</th>
                        <th style="width: 20%;">Contact Number</th>
                        <th class="" style="width: 15%;">Activity Status</th>
                        <th class="text-center" style="width:145px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($companies as $key => $company)
                    <tr>
                        <td class="text-center">{{ $key +1 }}</td>
                        <td class="font-w600">
                            <a href="{{ route('companies.show',$company->id) }}">{{ ucwords($company->name) }}</a>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <span class="text-muted">{{ $company->email }}</span>
                        </td>
                        <td>
                            <span class="text-muted">{{ $company->phone_no }}</span>
                        </td>
                        <td class="">
                            @if($company->acitivity_status == 1)
                            <span class="badge  badge-success">Super Active</span>
                            @elseif($company->acitivity_status == 2)
                            <span class="badge  badge-primary">Active</span>
                            @elseif($company->acitivity_status == 3)
                            <span class="badge badge-info">Semi Active</span>
                            @elseif($company->acitivity_status == 4)
                            <span class="badge  badge-danger">Dormant</span>
                            @elseif($company->acitivity_status == 5)
                            <span class="badge  badge-dark">Winding Up</span>
                            @elseif($company->acitivity_status == 6)
                            <span class="badge  badge-warning">Striking Off</span> 
                            @endif
                        </td>
                        <td class="font-w600 text-center">
                            <div class="btn-group">
                                @can('company_edit')    
                                <a class="btn btn-sm btn-light" href="{{ route('companies.edit',$company->id) }}"><i class="fa fa-edit"></i></a>
                                @endcan
                                @can('company_delete')
                                <form action="{{ route('companies.destroy', $company->id) }}" method="POST" >
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button data-name="{{ $company->name }}" type="submit" class="btn btn-sm btn-light delete"><i class="fa fa-trash"></i></button>
                                </form>
                                @endcan
                                @can('company_status')
                                @if($company->status == 1)
                                <a href="{{url('company/status/'.$company->id)}}" class="btn btn-sm btn-light" title="active"><i class="fa fa-unlock"></i></a>
                                @else
                                <a href="{{url('company/status/'.$company->id)}}" class="btn btn-sm btn-light" title="Inactive"><i class="fa fa-lock"></i></a>
                                @endif
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->
@endsection
