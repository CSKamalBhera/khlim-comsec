@extends('layouts.app', [
'activePage' => 'edit-staff',
'menuParent' => 'edit-staff',
'title' => 'Edit Staff',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-12 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                {{__('Staff')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Staff</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">Edit</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
   </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <form action="{{ route('users.update',$user->id) }}" method="POST">
                @csrf
                @method('put')
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="name">Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                id="name" name="name" placeholder="Name"
                                value="{{ $user ? $user->name : old('name') }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="username">User Name<span class="text-danger">*</span></label>
                            <input type="text"
                                class="form-control @error('username') is-invalid @enderror"
                                id="username" name="username" placeholder="Username"
                                value="{{ $user ? $user->username : old('username') }}">
                            @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="phone_number">Phone<span class="text-danger">*</span></label>
                            <input type="text"
                                class="form-control @error('phone_number') is-invalid @enderror"
                                id="phone_number" name="phone_number" placeholder="Phone Number"
                                value="{{ $user ? $user->phone_no : old('phone_number') }}">
                            @error('phone_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="emercency_contact">Emergency Contact</label>
                            <input type="text" class="form-control" id="emercency_contact"
                                name="emercency_contact" placeholder="Emergency Contact"
                                value="{{ $user ? $user->emergency_no : old('emercency_contact') }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="ic_number">IC Number<span class="text-danger">*</span></label>
                            <input type="text"
                                class="form-control  @error('ic_number') is-invalid @enderror"
                                id="ic_number" name="ic_number" placeholder="840230-10-5969"
                                value="{{ $user ? $user->ic_number : old('ic_number') }}">
                            @error('ic_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="email">Email<span class="text-danger">*</span></label>
                            <input type="email"
                                class="form-control  @error('email') is-invalid @enderror" id="email"
                                name="email" placeholder="Email" value="{{ $user ? $user->email : old('email') }}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="password">Password<span class="text-danger">*</span></label>
                            <input type="password"
                                class="form-control  @error('password') is-invalid @enderror"
                                id="password" name="password" placeholder="Password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="password_confirmation">Confirm Password<span class="text-danger">*</span></label>
                            <input type="password"
                                class="form-control  @error('password_confirmation') is-invalid @enderror"
                                id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                            @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="one-ecom-product-meta-keywords">Department<span class="text-danger">*</span></label>
                            <select class="select2 form-control  @error('department') is-invalid @enderror"
                                id="one-ecom-product-meta-keywords" name="department[]" style="width: 100%;"
                                data-placeholder="Choose many.." multiple>

                                @foreach($departments as $department)

                                <option value="{{ $department->id }}"
                                    @foreach($user->departments as $department_id)
                                        @if ($department->id == $department_id->id) selected="selected" @endif
                                    @endforeach >{{ $department->name }}</option>
                                @endforeach
                            </select>
                            @error('department')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="one-ecom-product-meta-keywords">Role<span class="text-danger">*</span></label>
                            <select class="form-control  @error('role') is-invalid @enderror" name="role"
                                style="width: 100%;" data-placeholder="Choose many..">
                                <option disabled selected>Select</option>
                                @foreach ($user->roles as $roleId)
                                @if($roleId->status == 0)
                                 <option value="{{ $roleId->id }}" selected="selected" >{{ $roleId->name }}</option>
                                @endif
                                @endforeach
                                @foreach($roles as $role)
                                @php
                                    $disabled = '';
                                @endphp
                                @if($role->id == 7)
                                @php
                                    $disabled = 'disabled';
                                @endphp
                                @endif

                                <option value="{{ $role->id }}"{{$disabled}} @if ($role->id == $roleId->id) selected="selected"
                                    @endif >{{ $role->name }}</option>
                                @endforeach

                            </select>
                            @error('role')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="text-right">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->
@endsection
