@extends('layouts.app', [
    'activePage' => 'create-staff',
    'menuParent' => 'create-staff',
    'title' => 'Create Staff',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <div class="col-sm-12 d-flex justify-content-start align-items-center">
            <h1 class="flex-sm-00-auto h3 my-2">
                {{__('Staff')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Staff</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="text-black text-muted">Create</a>
                    </li>
                </ol>
            </nav>
          </div>
        </div>
   </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Alternative Style -->
    <div class="block block-rounded">

        <div class="block-content block-content-full">
            <form action="{{ route('users.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="name">Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                name="name" placeholder="Name" value="{{  old('name')}}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="username">User Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('username') is-invalid @enderror" id="username"
                                name="username" placeholder="Username" value="{{ old('username')}}">
                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="phone_number">Phone<span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('phone_number') is-invalid @enderror" id="phone_number"
                                name="phone_number" placeholder="Phone Number" value="{{ old('phone_number')}}">
                            @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="emercency_contact">Emergency Contact</label>
                            <input type="text" class="form-control  @error('emercency_contact') is-invalid @enderror" id="emercency_contact"
                                name="emercency_contact" placeholder="Emergency Contact" value="{{ old('emercency_contact')}}">
                            @error('emercency_contact')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="ic_number">IC Number<span class="text-danger">*</span></label>
                            <input type="text" class="form-control  @error('ic_number') is-invalid @enderror" id="ic_number"
                                name="ic_number" placeholder="840230-10-5969" value="{{ old('ic_number')}}">
                            @error('ic_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="email">Email<span class="text-danger">*</span></label>
                            <input type="email" class="form-control  @error('email') is-invalid @enderror" id="email"
                                name="email" placeholder="Email" value="{{ old('email')}}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="password">Password<span class="text-danger">*</span></label>
                            <input type="password" class="form-control  @error('password') is-invalid @enderror" id="password"
                                name="password" placeholder="Password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="password_confirmation">Confirm Password<span class="text-danger">*</span></label>
                            <input type="password" class="form-control  @error('password_confirmation') is-invalid @enderror" id="password_confirmation"
                                name="password_confirmation" placeholder="Confirm Password">
                            @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="one-ecom-product-meta-keywords">Department<span class="text-danger">*</span></label>
                            <select class="select2 form-control  @error('department') is-invalid @enderror" id="one-ecom-product-meta-keywords" name="department[]" style="width: 100%;" data-placeholder="Choose many.." multiple>
                                @foreach($departments as $department)
                                @php
                                $selected = '';

                                if(is_array(old('department')) && in_array($department->id, old('department'))){
                                $selected = 'selected="selected"';
                                }
                                @endphp
                                <option value="{{ $department->id }}" {{$selected}}>{{ $department->name }}</option>
                                @endforeach
                            </select>
                            @error('department')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="one-ecom-product-meta-keywords">Role<span class="text-danger">*</span></label>
                            <select class="form-control  @error('role') is-invalid @enderror" name="role" style="width: 100%;" data-placeholder="Choose many..">
                                <option disabled selected>Select</option>
                                @foreach($roles as $role)

                                @php
                                $disabled = '';
                                @endphp
                                @if($role->id == 7)
                                @php
                                $disabled = 'disabled';
                                @endphp
                                @endif
                                @php
                                $selected = '';
                                @endphp
                                @if($role->id == old('role'))
                                @php
                                $selected = 'selected';
                                @endphp
                                @endif
                                <option value="{{ $role->id }}" {{$disabled}} {{$selected}}>{{ $role->name }}</option>
                                @endforeach
                            </select>
                            @error('role')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="text-right">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->
</div>
<!-- END Page Content -->
<script text="javascript">
$('.select2').select2();
</script>
@endsection
