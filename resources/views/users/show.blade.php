@extends('layouts.app', [
'activePage' => 'staff-details',
'menuParent' => 'staff-details',
'title' => 'Staff Details',
])
@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                {{__('Staff')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="{{route('users.index')}}">{{__('Staff')}}</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a>{{__('Details')}}</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Block Tabs -->
    <div class="row">
        <div class="col-lg-12">
            <!-- Block Tabs Default Style -->
            <div class="block block-rounded">
                <ul class="nav nav-tabs nav-tabs-block" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link active" id="home" data-toggle="tab" href="#btabs-static-home">General</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile" data-toggle="tab" href="#btabs-static-profile">Jobs</a>
                    </li>
                </ul>
                <div class="block-content tab-content">
                    <div class="tab-pane active" id="btabs-static-home" role="tabpanel">
                        <div class="row push">
                            <div class="col-lg-6 col-xl-6">
                                <label>{{ __('Name') }} </label>
                                <p>{{ $user->name ? ucwords($user->name) : 'N/A'}}</p>
                                <label>{{ __('Phone') }} </label>
                                <p> {{ $user->phone_no ? $user->phone_no : 'N/A' }}</p>
                                <label>{{ __('Ic Number') }}</label>
                                <p> {{ $user->ic_number ? $user->ic_number : 'N/A'}}</p>
                                <label>{{ __('Department') }} :</label>
                                <p>
                                    @foreach ($user->departments->pluck('name') as $department)
                                    {{ ucfirst($department)}}@if($loop->last) @else {{__(',') }} @endif
                                    @endforeach
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <label>{{ __('User Name') }}</label>
                                <p> {{ $user->username ? ucwords($user->username) : 'N/A' }}</p>
                                <label>{{ __('Emergency Contact') }}</label>
                                <p>{{ $user->emergency_no ? $user->emergency_no : 'N/A' }}</p>
                                <label>{{ __('Email') }}</label>
                                <p> {{ $user->email ? $user->email : 'N/A'}}</p>
                                <label>{{ __('Role') }}</label>
                                <p>
                                    @foreach ($user->roles->pluck('name') as $role)
                                    <span class="badge badge-success">{{$role}}</span>
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="btabs-static-profile" role="tabpanel">
                        <!-- Dynamic Table Full Pagination -->
                        <div class="block block-rounded">
                            <div class="block-header">
                            </div>
                            <div class="block-content block-content-full">
                                <form action="" method="GET" id="evaluationForm">

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Filters by Department</label>
                                                <select class="department form-control" required name="department">
                                                    <option disabled selected>Select</option>
                                                    @foreach($departments as $key => $department)
                                                    <option value="{{ $department->id }}" @if($department->id == Request::get('department')) selected @endif>{{ $department->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Filters by Start Date</label>
                                                <input type="date" class="form-control form-control-alt" name="start_date" value="{{ Request::get('start_date')}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Filters by End Date</label>
                                                <input type="date" class="form-control form-control-alt" name="end_date" value="{{ Request::get('end_date')}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    <option value="" selected>Select</option>
                                                    <option value="0" @if(Request::get('status')===0) selected @endif>Not Done</option>
                                                    <option value="1" @if(1==Request::get('status')) selected @endif>Done</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">

                                                <div class="form fil">
                                                    <button type="submit" class="btn btn-primary">Filters</button>
                                                    <a href="{{  Request::url().'#btabs-static-profile' }}" class="clear btn btn-danger" id="reset-btn">Clear All</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _js/pages/be_tables_datatables.js -->
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 80px;">ID</th>
                                            <th class="d-none d-sm-table-cell">Role</th>
                                            <th class="d-none d-sm-table-cell">Department</th>
                                            <th style="width: 15%;">Staff</th>
                                            <th style="width: 15%;">Manager</th>
                                            <th style="width: 15%;">Job</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($jobs as $key => $job)
                                        <tr>
                                            <td class="text-center font-size-sm">{{ $key+ 1 }}</td>
                                            <td class="d-none d-sm-table-cell">
                                                @foreach ($user->roles->pluck('name') as $role)
                                                <span class="badge badge-success">{{$role}}</span>
                                                @endforeach
                                            </td>
                                            <td class="d-none d-sm-table-cell font-size-sm">
                                                <em class="text-muted">{{ $job->department->name }}</em>
                                            </td>
                                            <td>
                                                <em class="text-muted font-size-sm">{{ $job->staff->name }}</em>
                                            </td>
                                            <td>
                                                <em class="text-muted font-size-sm">{{ $job->manager->name }}</em>
                                            </td>
                                            <td>
                                                <a href="{{ route('jobs.show',$job->id) }}" class="font-size-sm">{{ $job->job_type->name }}</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END Dynamic Table Full Pagination -->
                    </div>

                </div>
            </div>
            <!-- END Block Tabs Default Style -->
        </div>
    </div>
    <!-- END Block Tabs -->
</div>
<script>
    //redirect to specific tab
    $(document).ready(function() {
        if ($('.department').val()) {
            $("#btabs-static-home").removeClass('active');
            $("#home").removeClass('active');
            $("#btabs-static-profile").addClass('active show');
            $("#profile").addClass('active');
        }
    });
    $(document).ready(function() {
        $('#myTab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        // store the currently selected tab in the hash value
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });
        // on load of the page: switch to the currently selected tab
        var hash = window.location.hash;
        $('#myTab a[href="' + hash + '"]').tab('show');
    });

</script>
@endsection
