@extends('layouts.app', [
  'activePage' => 'staff-list',
  'menuParent' => 'staff-list',
  'title' => 'Staff',
])
@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
      <div class="row d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
        <div class="col-sm-8 d-flex justify-content-start align-items-center">
          <h1 class="flex-sm-00-auto h3 my-2">
              {{__('Staff')}} <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted"></small>
          </h1>
          <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item">Staff</li>
                  <li class="breadcrumb-item" aria-current="page">
                      <a class="text-black text-muted">List</a>
                  </li>
              </ol>
          </nav>
        </div>
        <div class="col-sm-4">
            <div class="text-right">
                @can('user_create')
                <a href="{{ route('users.create') }}" class="btn btn-primary">Create</a>
                @endcan
            </div>
        </div>
      </div>
    </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block block-rounded">

        <div class="block-content block-content-full">
          <form action="" method="GET" class="w-100">
              <div class="row">
                  <div class="col-lg-3">
                      <div class="form-group">
                      <select class="form-control" required name="department">
                          <option disabled selected>Select Department</option>
                          @foreach($departments as $key => $department)
                              <option value="{{ $department->id }}" @if($department->id == Request::get('department')) selected @endif>{{ $department->name }}</option>
                          @endforeach
                      </select>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="form-group">
                      <select class="form-control" required name="role">
                          <option disabled selected>Select Role</option>
                          @foreach($roles as $key => $role)
                              <option value="{{ $role->id }}"  @if($role->id == Request::get('role')) selected @endif>{{ $role->name }}</option>
                          @endforeach
                      </select>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="form-group">
                          <button type="submit" class="btn btn-primary">Filter</button>
                          <a href="{{ route('users.index') }}" class="btn btn-outline-secondary">Clear All</a>
                      </div>
                  </div>
              </div>
          </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full font-size-sm">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Department</th>
                            <th>Email</th>
                            <th>Contact Number</th>
                            <th>Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key => $user)
                        <tr>
                            <td class="text-center">{{ $key +1 }}</td>
                            <td class="font-w600">
                                <a href="{{ route('users.show',$user->id) }}">{{ ucwords($user->name) }}</a>
                            </td>
                            <td class="font-w500">
                                @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $role)
                                        <span class="text-muted">{{ $role }}</span>
                                    @endforeach
                                @endif
                            </td>
                            <td class="font-w500">
                                @if(!empty($user->departments))
                                    @foreach($user->departments as $department)
                                        <span class="badge badge-info text-muted">{{ ucwords($department->name) }}</span>
                                    @endforeach
                                @endif
                            </td>
                            <td class="font-size-sm">{{ $user->email }}</td>
                            <td>
                                <em class="text-muted">{{ $user->phone_no }}</em>
                            </td>
                            <td class="">
                                @if($user->status == 1)
                                <span class="badge badge-success">Active</span>
                                @else
                                    <span class="badge badge-danger">Inactive</span>
                                @endif
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    @if($role =='Super Admin')
                                    @else
                                        @can('user_edit')
                                            <a class="btn btn-sm btn-light" href="{{ route('users.edit',$user->id) }}"><i class="fa fa-edit"></i></a>
                                        @endcan
                                        @can('user_delete')
                                        <form action="{{ route('users.destroy', $user->id) }}" method="POST" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" data-name="{{ $user->name }}" class="btn btn-sm btn-light delete"><i class="fa fa-trash"></i></button>
                                        </form>
                                        @endcan
                                        @can('user_status')
                                        @if($user->status == 1)
                                            <a href="{{url('user/status/'.$user->id)}}" class="btn btn-sm btn-light" title="active"><i class="fa fa-unlock"></i></a>
                                        @else
                                            <a href="{{url('user/status/'.$user->id)}}" class="btn btn-sm btn-light" title="active"><i class="fa fa-lock"></i></a>
                                        @endif
                                        @endcan
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
@endsection